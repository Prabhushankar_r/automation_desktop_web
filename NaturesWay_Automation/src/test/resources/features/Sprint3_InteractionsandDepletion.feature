@InteractionsandDepletionPage @Sprint3
Feature: InteractionsandDepletionPage: This feature deals with the InteractionsandDepletion_Page functionality of the application on Home page

  Background: 
    Given I am in home page of integrative pro
    When promo pop up is displayed
    When close ConfirmMessage
 
    
 @InteractionsandDepletion_Page_Feature-1 @Sprint3 @Regression
 Scenario: verify the display of Interaction & Depletion
    When click on search icon
    Then verify the search pane is displayed  
    And search for "Acetyl L-Carnitine" and select it
    And verify the below menus in left pane
    |Menus In PDP page|
    |Details|
    |Ingredients|
    |Directions|
    |Interactions|
    |Patient Support|
    |Faqs|
    And navigate to InteractionsandDepletion
    And Expand all interactios listed 
    
    
    
    
    
    