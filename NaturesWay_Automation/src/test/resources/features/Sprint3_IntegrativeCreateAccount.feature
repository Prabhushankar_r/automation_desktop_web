@IntegrativeCreateAccount @Sprint3
Feature: IntegrativeCreateAccount: This feature deals with the creating an account

  Background: 
    Given I am in home page of integrative pro
    When promo pop up is displayed
    When close ConfirmMessage
    
 @CreateAccount_01 @TC_01,@TC_02,@TC_03,@TC_04 @Sprint3 @Regression
  Scenario: To verify 2 types of account creation availability
  And click on create account button
  And verify the header in create account section
  And verify both radio buttons should not be selected 
  
  
  @CreateAccount_02 @TC_05,T@C_06,@TC_07,@TC_08,@TC_09,@TC_10,@TC_11-@TC_38 @Sprint3 @Regression
  Scenario: To verify error message for About You section
  And click on create account button
  And do not enter values in prefix field
  Then display error message for prefix "The Prefix is required."
  And do not enter values in firstname field
  Then display error message for firstname "The First Name is required."
  And do not enter values in lastname field
  Then display error message for lastname "The Last Name is required."  
  And do not enter values in post nominals field  
  Then do not display error message for post nominals "The Post Nominals is required."
  And do not enter values in practitioner field 
  Then display error message for practitioner "The Practitioner Type is required."
  And do not enter values in Phone No field   
  Then display error message for Phone No "The Phone Number is required."
  And do not enter values in email address field   
  Then display error message for email address "The Email Address is required."
  And do not enter values in license field   
  Then do not display error message for license "The License Number is required."
  And do not enter values in intend field   
  Then display error message for intend "The Purpose of Use is required."
  
  
  
  @CreateAccount_03 @Sprint3 @Regression
  Scenario Outline: Display error message when user enters invalid email address
   And click on create account button
   And verify watermark and enter invalid email "<email>" 
   Then display error for invalid email "Please enter a valid email address." 
   Examples:
 | email  |
 #| abc   |     
 #|abc@  |
 # |abc@topmail|
 |abc.com|

   
   
  @CreateAccount_04 @Sprint3 @Regression
  Scenario: Display error message when user enters invalid phone number
   And click on create account button
   And verify watermark and enter invalid PhoneNo "123"
   Then display error for invalid PhoneNo "Please enter valid phone number"
   
     
   @CreateAccount_05 @Sprint3 @Regression
   Scenario: To verify app displays error when security field is empty
   And click on create account button
   And do not enter values in password field   
   Then display error message for password "The password is required."
   And do not enter values in reenter password field   
   Then display error message for re-enter password "The Re-enter password is required."
      
   
   
  @CreateAccount_06 @Sprint3 @Regression
   Scenario Outline: To verify error message when passwords do not match
    And click on create account button
    And verify watermark and enter value in password section "<Password>" and "<ConfirmPassword>"
    #And verify watermark and enter value in ConfirmPassword 
    Then display error for not matching password "Password not matching."
      Examples:
    |Password| |ConfirmPassword|
    |asd  | |fhh|
#    |57757| |08|
    
    
  
  @CreateAccount_07 @Sprint3 @Regression
  Scenario: To verify error message for clinic or bussiness section
   And click on create account button
   And do not enter values in BussinessName field
   Then display error message for BussinessName "The Business Name is required."
   And do not enter values in Address1 field
   Then display error message for Address1 "The Address 1 is required."  
   And do not enter values in Address2 field
   Then do not display error message for Address2 "The Address 2 is required."
   And do not enter values in ZipCode field
   Then display error message for ZipCode "The Zip Code is required."
   And do not enter values in City field
   Then display error message for City "The City is required."
   And do not enter values in State field
   Then display error message for State "The State is required."
    And do not enter values in NoOfPractitioner field
    Then display error message for NoOfPractitioner "The Number of Practitioners is required." 
    And do not enter values in BussinessPhone field
    Then display error message for BussinessPhone "The Business Phone is required."
      
    
             
  
  @CreateAccount_08 @Sprint3 @Regression
  Scenario: To verify error message for About You section - Medical Student
  And click on create account button
  And click on Medical Student radio button
  And do not enter values in prefix field
  Then display error message for prefix "The Prefix is required."
  And do not enter values in firstname field
  Then display error message for firstname "The First Name is required."
  And do not enter values in lastname field
  Then display error message for lastname "The Last Name is required."  
  And do not enter values in student email address field   
  Then display error message for student email address "The Student Email Address is required."
  And do not enter values in Phone No field   
  Then display error message for Phone No "The Phone Number is required."
  And do not enter values in Address1 field for student
   Then display error message for Address1 in student "The Address 1 is required." 
   And do not enter values in Address2 field for student 
   Then do not display error message for Address2 in student "The Address 2 is required."
   And do not enter values in ZipCode field for student
   Then display error message for ZipCode in student "The Zip Code is required."
   And do not enter values in City field for student
   Then display error message for City in student "The City is required."
   And do not enter values in State field for student
   Then display error message for State in student "The State is required."
   
  
 
  @CreateAccount_09 @Sprint3 @Regression
   Scenario: To verify app displays error when security field is empty - Medical Student
   And click on create account button
   And click on Medical Student radio button  
   And do not enter values in password field   
   Then display error message for password "The password is required."
   And do not enter values in reenter password field   
   Then display error message for re-enter password "The Re-enter password is required."
      


@CreateAccount_10 @Sprint3 @Regression
   Scenario Outline: To verify error message when passwords do not match - Medical Student
    And click on create account button
    And click on Medical Student radio button    
    And verify watermark and enter value in password section "<Password>" and "<ConfirmPassword>"
    Then display error for not matching password "Password not matching."     
    Examples:
    |Password| |ConfirmPassword|
    |asd  | |fhh|
   # |57757| |08|


  @CreateAccount_11 @Sprint3 @Regression
   Scenario: To verify error message when university sections are left empty
   And click on create account button
   And click on Medical Student radio button 
   And do not enter values in College field
  Then display error message for College "The College or University Name is required." 
   And do not enter values in GraduationMonth field
  Then display error message for GraduationMonth "The Graduation Month is required."
  And do not enter values in GraduationYear field
  Then display error message for GraduationYear "The Graduation Year is required." 
  And do not enter values in Degree field
  Then display error message for Degree "The Degree Obtained is required."
  And do not enter values in Program field
  Then display error message for Program "The program or focus is required."
  
  
   
   @CreateAccount_12 @Sprint3 @Regression
   Scenario: To verify submit button is disabled when mandatory fields are empty
   And click on create account button
   And click on Medical Student radio button
   Then verify submit button is disabled 
   
   

@CreateAccount_13 @TC_39,@TC_40,@TC_41,@TC_43,@TC_44,@TC_46,@TC_48,@TC_49,@Sprint3 @Regression
Scenario: To create account in HealthCare Practioner section
	And click on create account button
	Then verify submit button is disabled 
	When click healthcare practioner radio button
	When select value in prefix field
	Then enter the First name "Navya"
	And enter the Last name "Sampath"
	And enter the Post Nominal "U"
	Then select Practitioner Type value
	And enter the Phone number "909012345678900"
	And enter the email address 
    And enter license number "100"
    Then select usage Type value
    And enter password "Photon@123"
    And enter re-enter password "Photon@123"
    Then enter business name "Clinic"
    And enter Address1 value "1650 Grand Concourse"
    And enter zipcode value "10457"
    Then prepopulate state and city field
    Then select Practitioner count value
    And enter the business Phone number "909012345678900"
    Then check the resale policy
    And check the subscribe button
    Then verify question text box
    And enter the maximum number of accepted value
    Then click on Submit Account Request    
    Then verify account is created successfully   
    Then verify username is displayed in header
        
    
	
	
	@CreateAccount_14 @TC_50 @Sprint3 @Regression
Scenario: To create account in HealthCare Practioner section
	And click on create account button
	When click healthcare practioner radio button
	When select value in prefix field
	Then enter the First name "Navya"
	And enter the Last name "Sampath"
	And enter the Post Nominal "U"
	Then select Practitioner Type value as "Other"
	And enter the "Dentist" in other Practitioner Type
	And enter the Phone number "909012345678900"
	And enter the email address
    And enter license number "100"
    Then select usage Type value
    And enter password "Photon@123"
    And enter re-enter password "Photon@123"
    Then enter business name "Clinic"
    And enter Address1 value "1650 Grand Concourse"
    And enter zipcode value "10457"
    Then prepopulate state and city field
    Then select Practitioner count value
    And enter the business Phone number "909012345678900"
    Then check the resale policy
    And check the subscribe button
    Then click on Submit Account Request  
    Then verify account is created successfully    
    Then verify username is displayed in header
	
	
@CreateAccount_15 @TC_51,@TC_52,@TC_53,@TC_54-@TC_82,@TC_84,@TC_86,@TC_87,@TC_88-@tc_93,@Sprint3 @Regression
Scenario: To create account for Medical Student
   And click on create account button
   And click on Medical Student radio button 
   When select value in prefix field
	Then enter the First name "Navya"
	And enter the Last name "Sampath"
	And enter the student email address
	And enter the Phone number "909012345678900"
	And enter Address1 value for medical student "1650 Grand Concourse"
    And enter zipcode value "10457" for medical student
    Then prepopulate city and state for medical student   
    And enter password "Photon@123"
    And enter re-enter password "Photon@123"
    And enter college name "College1"
    Then select the graduation month
    And select the graduation year
    Then enter the degree obtained "B.E"
    And enter the Program "CSE"
    And check the subscribe button
    Then click on Submit Account Request  
    Then verify account is created successfully    
    
  
