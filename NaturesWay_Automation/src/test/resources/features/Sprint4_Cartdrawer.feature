@Cartdrawer @Sprint4
Feature: Cartdrawer: This feature deals with the Cartdrawer_Page functionality of the application on Home page

  Background: 
    Given I am in home page of integrative pro
    When promo pop up is displayed
    When close ConfirmMessage

  @Cartdrawer_Feature-1 @Sprint4  @TC_01
  Scenario: Verify the display of Sign in Account
    And click on mega menu products
    And click view all products
    And click product image
    And click on sign-in product detail page
    Then I enter valid login credentials
    And click on password eye icon
    And I click on sign in button in modal window
    Then verify user is logged successfully
    


  @Cartdrawer_Page_Feature-2 @Sprint4  @TC_02,
  Scenario: Verify the proceed to checkout functionality in cart drawer
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    And verify the cart icon in header is empty
    And click on mega menu products
    And click view all products
    And click product image
    And select the quality drop down
    And select the quality
    And select the unit drop down
    And select the unit
    And click Add to Cart button
    Then display Cart Drawer screen
    Then display continue shopping and proceed to checkout CTA button
    And click proceed to checkout
    Then display cart page 
    

  @Cartdrawer_Feature-3 @Sprint4  
  Scenario: Verify the continue shopping functionality in cart drawer
    And I click on sign-in
    Then I enter valid login credentials
    And click on password eye icon
    And I click on submit
    Then verify user is logged successfully
    And verify the cart icon in header is empty
    And click on mega menu products
    And click view all products
    And click product image
    And select the quality drop down
    And select the quality
    And select the unit drop down
    And select the unit
    And click Add to Cart button
    Then display Cart Drawer screen
    And click continue shopping
    Then display product listing page
    

  @Cartdrawer_Feature-4 @Sprint4  
  Scenario: Verify the delete functionality in cart drawer screen
    And I click on sign-in
    Then I enter valid login credentials
    And click on password eye icon
    And I click on submit
    Then verify user is logged successfully
    And verify the cart icon in header is empty
    And click on mega menu products
    And click view all products
    And click product image
    And select the quality drop down
    And select the quality
    And select the unit drop down
    And select the unit
    And click Add to Cart button
    Then display Cart Drawer screen
    When delete link is selected in cart drawer screen
    
    #Yet to implement
#  @Cartdrawer_Page_Feature-5 @Sprint4  
#  Scenario: Verify the save for later functionality in cart drawer screen
#    And I click on sign-in
#    Then I enter valid "july10qa@yopmail.com" and valid "Photon@123"
#    And click on password eye icon
#    And I click on submit
#    And click on mega menu products
#    And click view all products
#    And click product image
#    And select the quality drop down
#    And select the quality
#    And select the unit drop down
#    And select the unit
#    And click Add to Cart button
#    And click Save for later in cart drawer screen
    
