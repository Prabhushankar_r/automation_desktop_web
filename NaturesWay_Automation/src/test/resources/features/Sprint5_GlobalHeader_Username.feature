@GlobalHeader_Username @Sprint5
Feature: HowToOrderPage: This feature deals with the HowToOrder_Page functionality of the application on Home page

  Background: 
    Given I am in home page of integrative pro
    When promo pop up is displayed
    When close ConfirmMessage


  @GlobalHeader_Username_Feature-1 @Sprint5  
  Scenario: verify the display of user name in the global header
    And I click on sign-in
    Then I enter valid login credentials
    And I click on submit
    Then verify user is logged successfully   
    Then verify username is displayed in header
    