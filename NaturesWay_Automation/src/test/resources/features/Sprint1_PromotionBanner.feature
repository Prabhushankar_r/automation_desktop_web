@PromotionBanner @Sprint1
Feature: PromotionBanner
  Description: This feature deals with the PromotionBanner functionality of the application

  Background: 
    Given I am in home page of integrative pro
     When promo pop up is displayed  
    And close ConfirmMessage

  @PromotionBanner @TC1 @Sprint1 @Regression
  Scenario: Verify the display of promotionBanner
    Then verify the display of promotionBanner
    #And I click on PromotionBanner
    
