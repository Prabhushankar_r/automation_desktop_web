@HomePage @Sprint1
Feature: HomePage : This feature deals with the home page functionality of the application

  Background: 
    Given I am in home page of integrative pro
    When promo pop up is displayed
    And close ConfirmMessage

  @HomeFeature-1 @Smoke @TC30 @TC31 @TC32 @TC47 @Sprint1 @Regression
  Scenario: Verify the b2b home page and its elements
    And I should see the Our story title
    And I should see the Our story short description
    And I should see Our Story section should show an image or a video component to user
    And I click on homepage "Read More" Button
    

  @Home_Page_Feature-2 @Smoke @TC40 @Tc42 @TC43 @TC44 @TC45 @TC46 @TC48 @Homepage_FeaturedProducts @Sprint1 @Regression
  Scenario: Verify products are displayed under Feature products section
    Then I should see the Featured Products icon
    Then verify the display of feature product title
    Then verify the display of feature product description
    And I should see the Product images
    And I should see the background images
    And I should see Featured products title
    And I should see Short description of product
    Then user should able to scroll the feature products
    And click on view product details button
    

  @Home_Page_Feature-3 @Smoke @Tc50 @Tc52 @Tc53 @Sprint1 @Regression
  Scenario: Verify Image carousel below global header
   Then I should see the image carousel
    And I should see the carousel title
    And I should see the carousel description
    And I Click on "Read More" button
    

#  @Home_Page_Feature-4 @Sprint1 @Regression
#  Scenario: Verify the scroll the feature products
#    Then user should able to scroll the feature products
#    

#  @Home_Page_Feature-5 @Sprint1 @Regression
#  Scenario: Verify the display of  health section
#    Then I should see the Health Condition icon
#    Then verify the display of health condition Title
#    Then verify the display of health condition description
#    And I should see checkiterations button of Health Condition
#    
