@ProductDetailPage2 @Sprint2
Feature: ProductDetailPage: This feature deals with the ProductDetail_Page functionality of the application on Home page

  Background: 
     Given I am in home page of integrative pro
     When promo pop up is displayed  
    And close ConfirmMessage

  @ProductDetailPage2_Feature-1 @Sprint2 @Regression
  Scenario: Verify the display of Product Image
    And click on mega menu products
    And click view all products
    And click product image
    And clicking on the small preview images
   
  
  @ProductDetailPage2_Feature2 @Sprint2 @Regression 
  Scenario: Verify the navigation  of PDP by selecting Product Image
    And click on mega menu products
    And click view all products
    And click product image
    And select the quality drop down
    And select the quality
    And select the unit drop down
    And select the unit
    

  @ProductDetailPage2_Feature-3 @Sprint2 @Regression 
  Scenario: Verify the display of Product favorite icon
    And click on mega menu products
    And click view all products
    And click product image
    Then verify the favorites icon in PDP
    
    
    
     @ProductDetailPage2_Feature-4 @Sprint2 @Regression 
    Scenario: Verify the favorite functionality for unauthenticated user
    When click on search icon
    Then verify the search pane is displayed
    And search for "Esberitox" and select it
    When favorite icon is clicked for unauthenticated user
    Then display signin modal
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then display the product detail page with favorite 
    


  @ProductDetailPage2_Feature-5 @Sprint2 @Regression
  Scenario: verify the display of Share Button
    And click on mega menu products
    And click view all products
    And click product image
   Then verify the share icon in PDP
   When click on share icon in PDP
    And I should see social media icons
    And click on copy right link
    And It should display white colour tick mark
    

  @ProductDetailPage2_Feature-6 @Sprint2 @Regression
  Scenario: verify the display of FAQS
    When I tap on Sign In
    Then I enter valid login credentials
    And I click on sign in button in modal window  
    Then verify user is logged successfully
    And click on mega menu products
    And click view all products
    And click product image
    And navigate to faqs
    And click on question
    And click on contact-us
    

  @ProductDetailPage2_Feature-7 @Sprint2 @Regression 
  Scenario: Verify the display of Bread Crumbs
    And click on mega menu products
    And click view all products
    And click product title
    And click ProductDetail Page Bread Crumbs
    

  @ProductDetailPage2_Feature-8 @Sprint2 @Regression 
  Scenario: Verify the display of Related Articles 
    When click on search icon
    Then verify the search pane is displayed
    And search for "Esberitox" and select it
    And verify the display of related article header
    


  @ProductDetailPage2_Feature-9 @Sprint2 @Regression 
  Scenario: Verify navigation on clicking images in related product section
    When click on search icon
    Then verify the search pane is displayed
    And search for "Esberitox" and select it
    And verify the display of related article header
    And click on related articles images
    

 @ProductDetailPage2_Feature-10 @Sprint2 @Regression
  Scenario: Verify navigation on clicking drugname in related product section
    When click on search icon
    Then verify the search pane is displayed
    And search for "Esberitox" and select it
    And verify the display of related article header
    And click on related articles module titles
    

  @ProductDetailPage2_Feature-11 @Sprint2 @Regression 
  Scenario: Verify navigation on clicking viewdetails in related product section
   When click on search icon
    Then verify the search pane is displayed
    And search for "Esberitox" and select it
    And verify the display of related article header
    And click on view details button
    

  @ProductDetailPage2_Feature-12 @Sprint2 @Regression 
  Scenario: Verify the display of Feature Articles in PDP 
    When click on search icon
    Then verify the search pane is displayed
    And search for "Esberitox" and select it
    And verify the display of featured Article title in product detail page
    And verify the display of featured Article description in product detail page
    

  @ProductDetailPage2_Feature-13 @Sprint2 @Regression 
  Scenario: Verify clicking of Featured articles images in PDP
    When click on search icon
    Then verify the search pane is displayed
    And search for "Esberitox" and select it
    And verify the display of featured Article title in product detail page
    And click on featured article images in PDP
    

  @ProductDetailPage2_Feature-14 @Sprint2 @Regression 
  Scenario: Verify clicking of Featured articles tiles in PDP
    When click on search icon
    Then verify the search pane is displayed
    And search for "Esberitox" and select it
    And verify the display of featured Article title in product detail page
    And click on tiles in featured articles of PDP
    

  @ProductDetailPage2_Feature-15 @Sprint2 @Regression 
  Scenario: Verify clicking of read more in Featured articles of PDP
    When click on search icon
    Then verify the search pane is displayed
    And search for "Esberitox" and select it
    And verify the display of featured Article title in product detail page
    And click on read more featured article of PDP
    
    
    
    