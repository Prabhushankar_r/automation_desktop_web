@articleListing @Sprint1
Feature: articleslisteing: This feature deals with the articleslisteing_Page functionality of the application on Home page

  Background: 
    Given I am in home page of integrative pro
    When promo pop up is displayed
    Then close ConfirmMessage

  @articleListing_Page_Feature-1 @TC193 @Tc201 @Tc198 @Sprint1 @Regression @Specific
  Scenario: verify the display OF articleListing page
    And click on resources
    And click on article tile
    Then user should able to view the article title
    And click on readmore button
   

  @articleListing_Page_Feature-2 @Smoke @Tc192 @TC193 @Tc204 @Tc195 @Sprint1 @Regression @Specific
  Scenario: verify the display of Back to home bread crumb
    And click on resources
    And click on article tile
    Then verify the display of back to home button
    When click on back to home button and display homepage
  
  
   

  @articleListing_Page_Feature-3 @Tc192 @TC193 @Tc198 @Tc200 @Tc194 @Tc199 @Sprint1 @Regression
  Scenario: verify the display of Article tiles in an article listing page
    And click on resources
    And click on article tile
    Then user should able to view the components
      | TileDetails      |
      | Hero Banner      |
      | Article Title    |
      | Date             |
      | Image            |
      | Article Category |
    And user should able to view the authour name
    When I click on article image
   

  @Article_Details_Page_Feature-4 @Tc200 @TC193 @Tc205 @Tc213 @Tc214 @Tc217 @Sprint1 @Regression
  Scenario: verify the display of emailaddress on the Article listing page guest user
    And click on resources
    And click on article tile
    When I click on article image
    And click on share icon
    And I should see textbox for providing Email ID
    And provide email id and click on send button
    And It should display Email sent successfully success message
   

  @Article_Details_Page_Feature-5 @TC193 @Tc205 @Sprint1 @Regression
  Scenario: verify the display of copy link on the Article listing page guest user
    And click on resources
    And click on article tile
    When I click on article image
    And click on share icon
    And I should see social media icons
    And click on copy right link
    And It should display white colour tick mark
   

  @Article_Details_Page_Feature-6 @Tc231 @Tc232 @Sprint1 @Regression
  Scenario: verify the display of featured products - view product details on the Article details page Logged in user
    And click on resources
    And click on article tile
    When I click on article image
    And I should see Featured products
    And I should see View Product Details
    And I Click on View Product Details button


  @Article_Listing_Page_Feature-7 @Tc196 @TC193 @Sprint1 @Regression
  Scenario: verify the display of scroll bar on the Article listing page Logged in user
    And click on resources
    And click on article tile
    Then user should able to view the scroll bar and should be able to drag down the scroll bar successfully
    Then user should see minimum 5 articles count on article listing page
   

  @Article_Details_Page_Feature-8 @Smoke @TC205 @Tc208 @Sprint1 @Regression
  Scenario: verify the display of copy link on the Article listing page guest user
    And click on resources
    And click on article tile
    When I click on article image
    And I click on article bread crumb
    And click on BACK TO ARTICLES Bread Crumb
   

  
