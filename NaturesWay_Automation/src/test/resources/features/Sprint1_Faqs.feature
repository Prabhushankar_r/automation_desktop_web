@FAQS_Page_Feature @Sprint1
Feature: Faqs: This feature deals with the FAQ'S_Page functionality of the application on Home page

  Background: 
    Given I am in home page of integrative pro
    When promo pop up is displayed
    Then close ConfirmMessage

  @FAQS_Page_Feature-1 @TC75 @76 @Tc77 @Tc78 @Tc79 @Tc80 @Tc81 @Tc82 @Tc83 @Tc84 @Tc85 @Tc86 @Tc87 @Tc186 @Tc187 @Tc188 @Sprint1 @Regression
  Scenario: verify the display OF FAQ'S page
    And click on FAQs footer link
    And Check FAQs page content
    And It should display Question Answer section
    Then verify contact us button in FAQ page
    

  @FAQS_Page_Feature-2 @Tc75 @Sprint1 @Regression
  Scenario: verify the display OF FAQ'S page
    And click on FAQs footer link
    And verify the display of back to home button
    
