@PlaceorderPage
Feature: PromoCodePage: This feature deals with the PromoCode_Page functionality of the application on Home page

  Background: 
    Given I am in home page of integrative pro
    And close ConfirmMessage

  @Placeorder_Page_Feature-1 @Cadence4 
  Scenario: Verify the display of PlaceOrder
  And Clicking on it should Place the Order and navigate the User to Order Confirmation Page
  And close browser