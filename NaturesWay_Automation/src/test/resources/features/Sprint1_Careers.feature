@career  @Sprint1
Feature: career: This feature deals with the career_Page functionality of the application on Home page

  Background: 
    Given I am in home page of integrative pro
    When promo pop up is displayed
    Then close ConfirmMessage

  @career_Page_Feature-1 @Tc189 @Tc190 @Sprint1 @Regression @specific
  Scenario: verify the display OF herobanner
    And click on carrer link in globel footer
    Then vefify the display of title
    And verify the display of discription
    And click on View Career Opportunities
    
  @career_Page_Feature-2 @Sprint1 @Regression @Specific
  Scenario: verify the display OF Our Employees section
    And click on carrer link in globel footer
    Then verify the image
    And verify the view of description
    And click on Learn Our Story

  @career_Page_Feature-3 @Tc191 @Sprint1 @Regression @Specific
  Scenario: verify the display OF Nature's Way� offers competitive wages and benefits
    And click on carrer link in globel footer
    Then verify the display of Benefits
      | Bs                                                             |
      | Benefits                                                       |
      | Medical Insurance                                              |
      | Dental Insurance                                               |
      | Voluntary Vision Insurance                                     |
      | Company paid short and long term disability and life insurance |
      | 401(k) with employer match and profit sharing                  |
    And verify the display of Perks
      | Ps                                                  |
      | Perks                                               |
      | Paid Time Off                                       |
      | Paid Holidays                                       |
      | Wellness incentives                                 |
      | Generous product discounts  for you and your family |
      | Educational Reimbursement                           |


  @career_Page_Feature-4 @Sprint1 @Regression
  Scenario: verify the display OF Join our team banner image
    And click on carrer link in globel footer
    And verify the display of Join our team title
    And verify the display of description
    When user should be able to click View Career Opportunities
