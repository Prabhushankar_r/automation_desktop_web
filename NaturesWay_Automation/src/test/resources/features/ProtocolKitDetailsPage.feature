@ProtocolKitDetailsPage
Feature: ProtocolKitDetailsPage: This feature deals with the ProtocolKitDetails_Page functionality of the application on Home page

  Background: 
    Given I am in home page of integrative pro
    And close ConfirmMessage

  @ProtocolKitDetails_Page_Feature-1 @Cadence3  @smoke3
  Scenario: verify the display of favorite
    And click on mega menu products
    And click Protocol Kits
    And click sub-items
    And click protokol kit favorites
    Then I enter valid login credentials
    And click on password eye icon
    And I click on submit
    And close browser

  @ProtocolKitDetails_Page_Feature-2 @Cadence3 
  Scenario: verify the display of share article
    And click on mega menu products
    And click Protocol Kits
    And click sub-items
    And click on share article
    And I should see textbox for providing Email ID
    And provide email id and click on send button
    And It should display Email sent successfully success message
    And close browser

  @ProtocolKitDetails_Page_Feature-3 @Cadence3 
  Scenario: verify the display of Price
    And click on mega menu products
    And click Protocol Kits
    And click sub-items
    And verify the display of Price
    And close browser

  @ProtocolKitDetails_Page_Feature-4 @Cadence3 
  Scenario: verify the display of Size
    And click on mega menu products
    And click Protocol Kits
    And click sub-items
    And click Size
    And close browser

  @ProtocolKitDetails_Page_Feature-5 @Cadence3 
  Scenario: verify the display of Quantity
    And click on mega menu products
    And click Protocol Kits
    And click sub-items
    And select the quality drop down
    And select the quality
    And close browser

  @ProtocolKitDetails_Page_Feature-6 @Cadence3 
  Scenario: verify the display of Add to cart (Authenticated User or Registered User Awaiting Verification)
    And click on mega menu products
    And click Protocol Kits
    And click sub-items
    And I click on sign-in
    Then I enter valid login credentials
    And click on password eye icon
    And I click on submit
    And select the quality drop down
    And select the quality
    And click Add to Cart button
    And close browser

  @ProtocolKitDetails_Page_Feature-7 @Cadence3 
  Scenario: verify the display of How to Order/ Sign In or Create a Professional Account
    And click on mega menu products
    And click Protocol Kits
    And click sub-items
    And select the quality drop down
    And select the quality
    And click how to order
    And close browser

  @ProtocolKitDetails_Page_Feature-8 @Cadence3  @smoke3
  Scenario: verify the display of How to Order/ Sign In or Create a Professional Account
    And click on mega menu products
    And click Protocol Kits
    And click sub-items
    And click on sign-in product detail page
    Then I enter valid login credentials
    And click on password eye icon
    And I click on submit
    And select the quality drop down
    And select the quality
    And click Add to Cart button
    And close browser

  @ProtocolKitDetails_Page_Feature-9 @Cadence3 
  Scenario: verify the display of How to Order/ Sign In or Create a Professional Account
    And click on mega menu products
    And click Protocol Kits
    And click sub-items
    And click on sign-in product detail page
    And I click on create an account
    #When user enters valid "abc@gmail.com" and pass "Abc@123" and comfirm "Abc@123"
    #And click on continue as create the new Account
    And close browser

  @ProtocolKitDetails_Page_Feature-10 @Cadence3 
  Scenario: verify the display of Images
    And click on mega menu products
    And click Protocol Kits
    And click sub-items
    And Clicking the image within the main image pane, should open a light box
    And clicking on the small preview images
    And close browser

  @ProtocolKitDetails_Page_Feature-11 @Cadence3 
  Scenario: verify the display of What’s Included
    And click on mega menu products
    And click Protocol Kits
    And click sub-items
    And verify header
    And verify title
    And verify image
    And verify image title
    And verify image description
    And close browser

  @ProtocolKitDetails_Page_Feature-12 @Cadence3 
  Scenario: verify the display of What’s Included
    And click on mega menu products
    And click Protocol Kits
    And click sub-items
    And verify header
    And verify title
    And verify image
    And verify image title
    And verify image description
    And close browser

  @ProtocolKitDetails_Page_Feature-13 @Cadence3 
  Scenario: verify the display of About
    And click on mega menu products
    And click Protocol Kits
    And click sub-items
    And Nutrition Information should be visible in this section
    And close browser

  @ProtocolKitDetails_Page_Feature-14 @Cadence3 
  Scenario: verify the display of About
    And click on mega menu products
    And click Protocol Kits
    And click sub-items
    And User should be able to see Label
    And close browser

  @ProtocolKitDetails_Page_Feature-15 @Cadence3 
  Scenario: verify the display of About
    And click on mega menu products
    And click Protocol Kits
    And click sub-items
    And User should be able to see the Product Title
    And close browser

  @ProtocolKitDetails_Page_Feature-16 @Cadence3 
  Scenario: verify the display of About
    And click on mega menu products
    And click Protocol Kits
    And click sub-items
    And User should be able to view Product information
    And close browser

  @ProtocolKitDetails_Page_Feature-17 @Cadence3 
  Scenario: verify the display of About
    And click on mega menu products
    And click Protocol Kits
    And click sub-items
    And View Product Details
    And close browser

  @ProtocolKitDetails_Page_Feature-18 @Cadence3  @smoke3
  Scenario: verify the display of Additional Resources- Articles
    And click on mega menu products
    And click Protocol Kits
    And click sub-items
    And verify feature Articles icon
    And verify the display of related Feature Products header
    And click on feature poducts images
    And close browser

  @ProtocolKitDetails_Page_Feature-19 @Cadence3 
  Scenario: verify the display of Additional Resources- Articles
    And click on mega menu products
    And click Protocol Kits
    And click sub-items
    And verify feature Articles icon
    And verify the display of related Feature Products header
    And click on feature products titles
    And close browser

  @ProtocolKitDetails_Page_Feature-20 @Cadence3 
  Scenario: verify the display of Additional Resources- Articles
    And click on mega menu products
    And click Protocol Kits
    And click sub-items
    And verify feature Articles icon
    And verify the display of related Feature Products header
    And click on read more feature products
    And close browser

  @ProtocolKitDetails_Page_Feature-21 @Cadence3  @smoke3
  Scenario: verify the display of Bread Crumbs
    And click on mega menu products
    And click Protocol Kits
    And click sub-items
    And click ProductDetail Page Bread Crumbs
    And close browser
