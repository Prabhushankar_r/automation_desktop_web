@AboutUsquality @Sprint2
Feature: AboutUsquality: This feature deals with the AboutUsquality_Page functionality of the application on Home page

  Background: 
   Given I am in home page of integrative pro
     When promo pop up is displayed 
     And close ConfirmMessage

  @AboutUsquality_Page_Feature-1  @Cadence2 @Regression 
  Scenario: verify the display of hero banner
  And click on about us
  And click on Quality
  And verify the display of about us header
  And verify the display of quality hero banner title
  And verify the display of quality hero banner description
  
  
  @AboutUsquality_Page_Feature-2  @Cadence2 @Regression 
  Scenario: verify the display of Vedio section
  And click on about us
  And click on Quality
  And click on video
  
  
  
  @AboutUsquality_Page_Feature-3 @Sprint2 @Regression @TC-19
  Scenario: verify the content with left images
  When click on about us
  And click on Quality
  Then verify the title for contents with left image in qualitypage
  And verify the description for contents with left image in qualitypage
  
  
  
  @AboutUs_Page_Feature-4 @Sprint2 @Regression @TC-20
  Scenario: verify the content with right images
  When click on about us
  And click on who we are
  Then verify the title for contents with right image in qualitypage
  And verify the description for contents with right image in qualitypage
  
  
   @AboutUsquality_Page_Feature-7  @Cadence2 @Regression 
  Scenario: verify the display of quality footer banner
  And click on about us
  And click on Quality
  And verify the display of quality footer banner title
  And verify the display of quality footer banner description
  And verfy the display of quality footer banner images
  
  
  
   @AboutUsquality_Page_Feature-8  @Cadence2 @Regression 
  Scenario: verify the display of quality footer Ajax component
  And click on about us
  And click on Quality
  And verify the display of quality quality footer Ajax component title
  And verify the display of quality quality footer Ajax component description
  And verfy the display of quality quality footer Ajax component images
  
  
  @AboutUsquality_Page_Feature-9  @Cadence2 @Regression 
  Scenario: verify the display of bread crumb
  And click on about us
  And click on who we are
  Then verify the display of back to home button
  When click on back to home button and display homepage
  
    
