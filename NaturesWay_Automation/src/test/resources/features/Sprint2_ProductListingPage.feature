@ProductListingPage @Sprint2
Feature: ProductListingPage: This feature deals with the ProductListeing_Page functionality of the application on Home page

  Background: 
     Given I am in home page of integrative pro
     When promo pop up is displayed  
    And close ConfirmMessage

  @ProductListing_Page_Feature-1 @Sprint2 @Regression
  Scenario: Verify the display of Products Image
    And click on mega menu products
    And click view all products
    And click product image
    And user navigate back to the page
    

  @ProductListing_Page_Feature-2 @Sprint2 @Regression
  Scenario: Verify the display of Products Title
    And click on mega menu products
    And click view all products
    And click product title
    And user navigate back to the page
    

  @ProductListing_Page_Feature-3 @Sprint2 @Regression
  Scenario: Verify the display of Products View Details
    And click on mega menu products
    And click view all products
    And click on product view details
    And user navigate back to the page
    


  @ProductListing_Page_Feature-4 @Sprint2 @Regression 
  Scenario: Verify the selection and deselection of all flters
    And click on mega menu products
    And click view all products
    And user clicks on a filter clicking on the checkbox applied filters are Now Filter By
    And click Clear All Filters
    

  @ProductListing_Page_Feature-5 @Sprint2 @Regression 
  Scenario: Verify the selection and clearing of FORM section
    And click on mega menu products
    And click view all products
    And user selects all items in form section
    And click Clear All Filters
    

  @ProductListing_Page_Feature-6 @Sprint2 @Regression 
  Scenario: Verify the selection and clearing of Dietary Needs section
    And click on mega menu products
    And click view all products
    And user selects all items in dietary needs section
    And click Clear All Filters
    

  @ProductListing_Page_Feature-7 @Sprint2 @Regression  
  Scenario: Verify the selection and clearing of Product Type section
    And click on mega menu products
    And click view all products
    And user selects all items in product type section
    And click Clear All Filters
    

  @ProductListing_Page_Feature-8 @Sprint2 @Regression  
  Scenario: Verify the selection and clearing of Health section
    And click on mega menu products
    And click view all products
    And user selects all items in health section
    And click Clear All Filters
    

  @ProductListing_Page_Feature-9 @Sprint2 @Regression  
  Scenario: Verify the selection and clearing of Formulation section
    And click on mega menu products
    And click view all products
    And user selects all items in formulation section
    And click Clear All Filters
    

  @ProductListing_Page_Feature-10 @Sprint2 @Regression
  Scenario: Verify the display of Sort on the right-hand rail
    And click on mega menu products
    And click view all products
    And sort the products from Product Name A-Z, Product Name Z-A, Popularity
    

#  @ProductListing_Page_Feature-11 @Sprint2 @Regression 
#  Scenario: Verify the display of Show More Results
#    And click on mega menu products
#    And click view all products
#    And click on show more results

  @ProductListing_Page_Feature-12 @Sprint2 @Regression
  Scenario: Verify the display of Bread Crumb
    And click on mega menu products
    And click view all products
    And click product title
    And click ProductDetail Page Bread Crumbs
    
