@contactUs  @Sprint1
Feature: contactUs: This feature deals with the ContactUs_Page functionality of the application on Home page

  Background: 
    Given I am in home page of integrative pro
    When promo pop up is displayed
    Then close ConfirmMessage
    
  @contactUs_Page_Feature-1 @Tc173 @Sprint1 @Regression
  Scenario: verify the hero banner
    And click on contact-us link in globel footer
    Then user should able to view the herobanner
    And verify the contactus title
    And verify thre contactus description

  @contactUs_Page_Feature-2 @Tc185 @Sprint1 @Regression
  Scenario: verify the display of call us model
    And click on contact-us link in globel footer
    Then user should able to view call us image and title
    Then user should able to view call us description
    And user should able to view the fax
    And click on phone number
   
    

  @contactUs_Page_Feature-3 @Sprint1 @Regression
  Scenario: verify vthe display of chat with us section model
    And click on contact-us link in globel footer
    Then user should able to view chat with us image and title
    And user should able to view chat with us days and time
    When user click on chat button
   

  @contactUs_Page_Feature-4 @Tc177 @Tc179 @Tc182 @Tc181 @Sprint1 @Regression
  Scenario: verify the display of email us section model
    And click on contact-us link in globel footer
    Then user should able to view email us image and title
    And user should able to view description
    When user click on email us
   

  @contactUs_Page_Feature-5 @Tc178 @Sprint1 @Regression
  Scenario: verify the display of location section
    And click on contact-us link in globel footer
    Then verify the display of Integrative Therapeutics
    And verify the display of Customer Care Hours
    And verify the display of Phone
    And verify the display Fax
    Then verify the display of back to home button
   

  @contactUs_Page_Feature-6 @Tc173 @174 @Tc181 @Tc184 @Sprint1 @Regression
  Scenario: verify user can sumbit the form successfully by giving valid details
    And click on contact-us link in globel footer
    Then verify the display of title
    When user should able to enter the text in name text box
    And user should able to enter the mail
    And user select the value Practitioner
    And user selects the value Inquiry
    And user enters text in email message box
    Then Verify submit button is enabled
    And click on submit button
    Then verify the display of success message
   

  @contactUs_Page_Feature-7 @Tc174 @Regression
  Scenario: verify the error message is displayed properly when provide invalid email address
    And click on contact-us link in globel footer
    Then verify the display of title
    When user enter the name as "Test"
    When user enters the invalid email id "test12.com"
    Then verify system provides the error message for invalid email address "Please enter a valid email address."
   

  @contactUs_Page_Feature-8 @Tc180 @Regression
  Scenario: Verify the Submit button without filling the Form
    And click on contact-us link in globel footer
    Then verify the display of title
    When user click on email us
    Then verify the submit button should be in disabled mode.
   
