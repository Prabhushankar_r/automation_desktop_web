@UserAccountPage @Sprint3
Feature: UseraccountMyaccount : This feature deals with my account features

Background: 
    Given I am in home page of integrative pro
    When promo pop up is displayed
    When close ConfirmMessage
    
 @UseraccountMyaccount_Feature-1 @Sprint3 @Regression @TC_01,@TC_03,@TC_04,@TC_07,@TC_10,@TC_13,@TC_16,@TC_19
 Scenario: verify app displays my account component for authenticated user
    And I click on sign-in
    Then I enter valid login credentials 
    And I click on submit
    Then verify user is logged successfully
    Then verify chevron is displayed
    Then display the below components in account dropdown
    |Drop Down Menu|
    |Account Details|
    |My Orders|
    |Payment Information|
    |Favorites|
    |Sign Out|
    |Shipping Information|
    |Practitioner Directory| 
      
   
@UseraccountMyaccount_Feature-2 @Sprint3 @Regression @TC_06
Scenario: Navigate to account information page on selection
    And I click on sign-in
    Then I enter valid login credentials
    And I click on submit
    Then verify user is logged successfully   
    Then verify user icon for account information
    When click on Account Information from dropdown
    Then display the "Account Details" Page 
    
    

@UseraccountMyaccount_Feature-3 @Sprint3 @Regression @TC_09
Scenario: Navigate to My Order page on selection
    And I click on sign-in
    Then I enter valid login credentials
    And I click on submit
    Then verify user is logged successfully
    Then verify user icon for my order 
    Then click on My Orders from dropdown
    Then "My Orders" Page should be displayed
    
    
    
@UseraccountMyaccount_Feature-4 @Sprint3 @Regression @TC_12
Scenario: Navigate to Payment option page on selection
    And I click on sign-in
    Then I enter valid login credentials
    And I click on submit
    Then verify user is logged successfully
    Then verify user icon for payment method 
    When click on Payment methods from dropdown
    Then display "Payment Methods" Page 
    
    
    
 @UseraccountMyaccount_Feature-5 @Sprint3 @Regression @TC_18
 Scenario: Navigate to Favorites page on selection
    And I click on sign-in
    Then I enter valid login credentials
    And I click on submit
    Then verify user is logged successfully
    Then verify user icon for Favorites 
    When click on Favorites from dropdown
    Then display "Favorites" page 
    
    

@UseraccountMyaccount_Feature-6 @Sprint3 @Regression @TC_18
 Scenario: Navigate to find a practitioner profile page on selection
    And I click on sign-in
    Then I enter valid login credentials
    And I click on submit
    Then verify user is logged successfully
    Then verify user icon for practitioner profile 
    When click on find a practitioner profile from dropdown
    Then "Find a Practitioner Profile" Page should display
  
      
   
   @UseraccountMyaccount_Feature-7 @Sprint3 @Regression @TC_18
 Scenario: Navigate to shipping addresses page on selection
    And I click on sign-in
    Then I enter valid login credentials
    And I click on submit
    Then verify user is logged successfully
    Then verify user icon for shipping address
    When click on shipping addresses from dropdown
    Then Display "Shipping Addresses" Page
    
   
     
 @UseraccountMyaccount_Feature-8 @Sprint3 @Regression @TC_21,@TC_22
 Scenario: Verify sign out functionality
    And I click on sign-in
    Then I enter valid login credentials
    And I click on submit
    Then verify user is logged successfully
    Then verify user icon for sign out
    Then I Should be logout succesfully  
    
