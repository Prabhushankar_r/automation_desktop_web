@MyAccount_Favorites @Sprint5
Feature: MyAccount Favorites: This feature deals with the favorite functionality of the My account page

  Background: 
    Given I am in home page of integrative pro
    When promo pop up is displayed
    When close ConfirmMessage
    
  @MyAccount_Favorites_Feature-1 @Sprint4  
  Scenario: Verify the welcome message, header and Account no in favorite page
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully 
    Then verify user icon for Favorites 
    When click on Favorites from dropdown
    Then display "Favorites" page 
    And display the welcome message in favorite page 
    
    
    
  @MyAccount_Favorites_Feature-2 @Sprint4  
  Scenario: Verify the 24 products is displayed favorite page
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully 
    Then verify user icon for Favorites 
    When click on Favorites from dropdown
    Then display "Favorites" page 
    And verify the product list in favorite page 
     
    
    @MyAccount_Favorites_Feature-3 @Sprint4  
  Scenario: Verify the remove from favorite link functionality
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully 
    Then verify user icon for Favorites 
    When click on Favorites from dropdown
    Then display "Favorites" page 
    And verify whether favorite page has a product
    When click on remove from favorite link
    
    
     @MyAccount_Favorites_Feature-4 @Sprint4  
  Scenario: Verify the page navigation on selecting View Details in Favorite Page
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully 
    Then verify user icon for Favorites 
    When click on Favorites from dropdown
    Then display "Favorites" page 
    And verify whether favorite page has a product
    When click on view details link in favorite page
    Then display product detail page on selecting view detail link
    
     
    