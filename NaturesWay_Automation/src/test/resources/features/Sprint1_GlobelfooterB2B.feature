@GlobalFooterFeature  @Sprint1
Feature: Global Footer: This feature deals with the global footer functionality of the application

  Background: 
    Given I am in home page of integrative pro
     When promo pop up is displayed 
     And close ConfirmMessage

  @GlobalFooterFeature-1 @Tc54 @TC55 @Tc56 @Sprint1 @Regression
  Scenario: verify the display of  menu items in globalfooter
    Then user should be able to view the following components
      | Footer Components |
      | Home Page Footer Link     |
      | News letter section       |
      | Social Media navigation   |
      | Key and copyright notices |
    

  @GlobalFooterFeature-2 @Tc54 @TC55 @Tc56 @Sprint1 @Regression
  Scenario: verify the display of Newsletter components in globalfooter
    Then user should able to view the Newsletter section
    Then user enters email
    And click on submit
    

  @GlobalFooterFeature-3 @Tc61 @Tc62 @Sprint1 @Regression
  Scenario: verify the display of Social media section in globalfooter
    Then user should able to view social media icons
      | icons     |
      | Instagram |
      | LinkledIn |
      | Facebook  |
    

  @GlobalFooterFeature-4 @Smoke @Tc63 @Tc64 @Tc65 @Tc66 @Tc67 @Tc68 @Tc164 @Tc166 @Tc171 @Sprint1 @Regression
  Scenario: verify the display of  Anchor Tags(Links) in globalfooter
    Then user should able to view the anchor Tags
      | Anchor Tags        |
      | Contact            |
      | Sitemap            |
      | LEGAL              |
      | CAREERS            |
      | INTEGRATIVE CANADA |
      | FAQs               |
    When user click on anchor Tags
      | Tags               |
      | Contact            |
      | Sitemap            |
      | LEGAL              |
      | CAREERS            |
      | INTEGRATIVE CANADA |
      | FAQs               |
    

  @GlobalFooterFeature-5 @Tc69 @Tc70 @Sprint1 @Regression
  Scenario: verify the display of  Key copyright notices in globalfooter
    Then user should able to view the Key statement
    And user should able to see the copyright notices
    
