@Resources_PatientResourcePage @Sprint2
Feature: PatientResource: This feature deals with the PatientResource_Page functionality of the application on Home page

  Background: 
   Given I am in home page of integrative pro
    When promo pop up is displayed  
    And close ConfirmMessage

  @PatientResource_Page_Feature-1 @Cadence2 @Regression
  Scenario: verify the display of hero banner
    And click on resources
    And click Patient Resource
    And verify the display of resource header
    And verify the display of resource hero banner title
    And verify the display of resource hero banner description
    
    
  @PatientResource_Page_Feature-2 @Cadence2 @Regression 
  Scenario: verify the display of Resource Type
    And click on resources
    And click Patient Resource
    And verify the display of resource header
    And verify the display of two filters
    And verify the display of Resources should be displayed alphabetically
    

  @PatientResource_Page_Feature-3 @Cadence2 @Regression
  Scenario: verify the display of Product Information
    And click on resources
    And click Patient Resource
    And click product information
    Then verify the count for product information
    #And verify the display of Product Information resources
    And verify the display of Patient Resource PDFs
    


  @PatientResource_Page_Feature-4 @Cadence2 @Regression 
  Scenario: verify the display of Health Conditions
    And click on resources
    And click Patient Resource
    And click on Health Conditions 
    Then verify the count for Health Conditions
    Then verify the display of Patient Resource PDFs related to Health Conditions
    


  @PatientResource_Page_Feature-5 @Cadence2 @Regression 
  Scenario: verify the display of title and image in product information tab : Patient Resource
    And click on resources
    And click Patient Resource
    And click product information
    And verify the display of title for product information tab
    Then verify the display of images for product information tab
    


  @PatientResource_Page_Feature-6 @Cadence2 @Regression 
  Scenario: verify the display of title and image in health condition tab : Patient Resource
    And click on resources
    And click Patient Resource
    And click on Health Conditions 
    And verify the display of title for health condition tab
    Then verify the display of images for health condition tab
    


  @PatientResource_Page_Feature-7 @Cadence2 @Regression
  Scenario: verify the display of bread crumb
    And click on resources
    And click Patient Resource
    And verify the display of back to home button
    
