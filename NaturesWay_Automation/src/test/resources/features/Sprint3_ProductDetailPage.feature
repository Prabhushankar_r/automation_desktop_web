@ProductDetailPagePage_3 @Sprint3
Feature: ProductDetailPage: This feature deals with the ProductDetail_Page functionality of the application on Home page

    Background: 
    Given I am in home page of integrative pro
    When promo pop up is displayed
    When close ConfirmMessage
    
    @ProductDetailPage_Feature-1 @Sprint3 @Regression @Sprint3
  	Scenario: Verify the display of Product Details-Product Variants
    And I click on sign-in
    Then I enter valid login credentials
    And I click on submit
    Then verify user is logged successfully
    When click on search icon
    Then verify the search pane is displayed
    And search for "Vitaline CoQ10 100mg Tropical" and select it
    And selecting a variant, the page should be updated accordingly
    And selecting a size the page should be updated accordingly
    And select the quality drop down
    And select the quality
    And select the unit drop down
    And select the unit
    And click Add to Cart button
    Then display Cart Drawer screen
    Then verify recently added product is displayed in cart drawer
    

    @ProductDetailPage_Feature-2 @Sprint3 @Regression
  Scenario: Verify the display of Product Details-One Time Purchase
    And I click on sign-in
    Then I enter valid login credentials
    And I click on submit
    Then verify user is logged successfully
    When click on search icon
    Then verify the search pane is displayed
    And search for "Esberitox" and select it 
    Then verify Qty and Units dropdown are displayed
    And verify the price change when units and quanity values are changed
    And click Add to Cart button
    Then display Cart Drawer screen
    Then verify recently added product is displayed in cart drawer
    
   
   @ProductDetailPage_Feature-3 @Sprint3 @Regression
  Scenario: Verify the display of Product Details-Add to Cart
     And I click on sign-in
    Then I enter valid login credentials
    And I click on submit
    Then verify user is logged successfully
    #And verify the cart icon in header is empty
    When Hover on the 'Products' Mega menu
    And Hover on the 'BY TYPE' category
    Then Verify the below sub category list of 'BY TYPE'
    When click anyone subcategory of 'BY TYPE'
    Then Verify user can navigate to appropriate subcategory page of 'BY TYPE'
    And click product title
    And click Add to Cart button and verify the count
    Then display Cart Drawer screen
    Then verify recently added product is displayed in cart drawer
    
    
    @ProductDetailPage_Feature-4 @Sprint3 @Regression
  Scenario: Verify the display of Product Details-Create a Professional Account
    When click on search icon
    Then verify the search pane is displayed
    When select the product from suggested search list
    Then verify the suggested retail price displayed in PDP
    When click how to order
    Then verify how to order page    
    
  
   @ProductDetailPage_Feature-5 @Sprint3 @Regression
  Scenario: Verify the display of Product Details-Our Story
     And click on mega menu products
    And click view all products
    And click product title
    Then verify the mandatory components in detail page
    And verify the mandatory button and links in PDP for Unauthenticated user
    
    
    
  @ProductDetailPage_Feature-6 @Sprint3 @Regression
   Scenario: To verify product details patient support material
    When click on search icon
    Then verify the search pane is displayed
    And search for "Esberitox" and select it 
    When navigate to patient support menu in pdp
    Then verify the title 'Patient Support' in pdp
    And verify the PDF available under patient support in pdp
    Then verify the redirection for PDF under patient support
    
   
   @ProductDetailPage_Feature-7 @Sprint3 @Regression
  Scenario: Verify the display of Product Details -FAQs
   And I click on sign-in
   Then I enter valid login credentials
   And I click on submit
   Then verify user is logged successfully
   And click on mega menu products
   And click view all products
   And click product title 
   When navigate to FAQs menu in pdp
   Then display FAQs section in PDP
   And verify Q&A section are displayed in FAQ
   When expand question section in FAQ
   Then display answer under FAQ
   
   
   @ProductDetailPage_Feature-8 @Sprint3 @Regression
  Scenario: Verify the display of Product Details-Overview
    And I click on sign-in
    Then I enter valid login credentials
    And I click on submit
    Then verify user is logged successfully
    And click on mega menu products
    And click view all products
    And click product title 
    And verify the below menus in left pane
    |Menus In PDP page|
    |Details|
    |Ingredients|
    |Directions|
    |Interactions|
    |Patient Support|
    |Faqs|
   
     
    @ProductDetailPage_Feature-9 @Sprint3 @Regression 
  Scenario: Verify the display of Product Details-Favorite
   And I click on sign-in
    Then I enter valid login credentials
    And I click on submit
    Then verify user is logged successfully
    And click on mega menu products
    And click view all products
    And click product image
    And click favorites
    
    
   @ProductDetailPage_Feature-10 @Sprint3 @Regression
  Scenario: verify the display of Product Details-Share
    And click on mega menu products
    And click view all products
    And click product image
    When click on share icon in PDP
    And I should see social media icons
    And click on copy right link
    And It should display white colour tick mark
    
   
    
 @ProductDetailPage_Feature-11 @Sprint3 @Regression
  Scenario: verify the display of Product Details-Recommended Products
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    #And verify the cart icon in header is empty
    When click on search icon
    Then verify the search pane is displayed
    And search for "Oxyperm" and select it
    Then verify discontinued label in PDP
   
   
    
    
    
  
  