@MiniCartPage
Feature: MiniCart: This feature deals with the MiniCart_Page functionality of the application on Home page

  Background: 
    Given I am in home page of integrative pro
    And close ConfirmMessage

  @MiniCart_Page_Feature-3 @Cadence2  @too
  Scenario: Verify the display of MiniCart
    And I click on sign-in
    Then I enter valid login credentials
    And click on password eye icon
    And I click on submit
    And click on mega menu products
    And click view all products
    And click product image
    And select the quality drop down
    And select the quality
    And select the unit drop down
    And select the unit
    And click Add to Cart button
    And click headercart
    And close browser

    