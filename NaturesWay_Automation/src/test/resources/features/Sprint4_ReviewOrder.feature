@ReviewOrderPage
Feature: ReviewOrderPage: This feature deals with the ReviewOrder_Page functionality of the application on Home page

  Background: 
    Given I am in home page of integrative pro
    And close ConfirmMessage

  @ReviewOrder_Page_Feature-1 @Cadence4 
  Scenario: Verify the display of Product Image and Product information
    And verify Product Name
    And verify Product Price
    And verify Sub-total
    And close browser

  @ReviewOrder_Page_Feature-2 @Cadence4 
  Scenario: Verify the display of Editable information
    And Select edit quantity
    And Selectverify unit vs case
    And close browser

  @ReviewOrder_Page_Feature-3 @Cadence4 
  Scenario: Verify the display Delete
    And Select Delete
    And close browser

  
    
  @ReviewOrder_Page_Feature-4 @Cadence4 
  Scenario: Verify the display Save for Later
    And Select Save for Later
    And close browser 
    
  @ReviewOrder_Page_Feature-5 @Cadence4 
  Scenario: Verify the display Continue Shopping
    
    And close browser   
