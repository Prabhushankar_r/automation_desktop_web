@DiscontinuedProduct @Sprint4
Feature: DiscontinuedProduct This feature deals with the discontinued product functionality

  Background: 
    Given I am in home page of integrative pro
    When promo pop up is displayed
    When close ConfirmMessage
    
 @DiscontinuedProduct_01 @Sprint4
  Scenario: To verify discontinued label in product detail page
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    #And verify the cart icon in header is empty
    When click on search icon
    Then verify the search pane is displayed
    And search for "Oxyperm" and select it
    Then verify discontinued label in PDP
    
    
    