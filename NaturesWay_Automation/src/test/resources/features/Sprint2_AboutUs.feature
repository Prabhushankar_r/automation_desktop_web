@AboutUs_Who_We_Are @Sprint2
Feature: AboutUs: This feature deals with the AboutUs : Who We Are page functionality

  Background: 
    Given I am in home page of integrative pro
    When promo pop up is displayed
    When close ConfirmMessage


@AboutUs_Page_Feature-1  @Sprint2 @Regression @TC-16
  Scenario: verify the display of bread crumb
  When click on about us
  And click on who we are
  Then verify the display of back to home button
  When click on back to home button and display homepage
  
  
  
  @AboutUs_Page_Feature-2  @Sprint2 @Regression @TC-17
  Scenario: verify the display of hero banner
  When click on about us
  And click on who we are
  Then verify the display of about us header
  And verify the display of hero banner title
  And verify the display of hero banner description
  
  
  @AboutUs_Page_Feature-3 @Sprint2 @Regression @TC-19
  Scenario: verify the content with left images
  When click on about us
  And click on who we are
  Then verify the title for contents with left image
  And verify the description for contents with left image
  And click on read more button on left side content
  
  
  
  @AboutUs_Page_Feature-4 @Sprint2 @Regression @TC-20
  Scenario: verify the content with right images
  When click on about us
  And click on who we are
  Then verify the title for contents with right image
  And verify the description for contents with right image
  And click on read more button on right side content
  
  
 
