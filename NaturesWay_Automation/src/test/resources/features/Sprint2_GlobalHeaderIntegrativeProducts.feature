@Resources_GlobalHeaderIntegrativeProducts  @Sprint2
Feature: GlobalHeaderIntegrativeProductsPage: This feature deals with the GlobalHeaderIntegrativeProducts_Page functionality of the application on Home page

  Background: 
    Given I am in home page of integrative pro
    When promo pop up is displayed
    And close ConfirmMessage

  
  @GlobalHeaderIntegrativeProducts_Page_Feature-1 @Sprint2 @Regression @TC145 @TC146 @TC147 
  Scenario: Verify categories are displayed under Products mega menu
  Given "Products" Mega menu should be displayed
  When Hover on the 'Products' Mega menu
  Then Verify the below categories are displayed
  |BY TYPE|
  |BY HEALTH INTEREST|
  |VIEW ALL PRODUCTS|
  
  
  @GlobalHeaderIntegrativeProducts_Page_Feature-2 @Sprint2 @Regression @TC148 @TC149 @TC150 @TC151 @specific
  Scenario: Verify user can navigate to appropriate sub category page of 'BY TYPE'
  Given "Products" Mega menu should be displayed
  When Hover on the 'Products' Mega menu
  And Hover on the 'BY TYPE' category
  Then Verify the below sub category list of 'BY TYPE'
  When click anyone subcategory of 'BY TYPE'
  Then Verify user can navigate to appropriate subcategory page of 'BY TYPE'


  @GlobalHeaderIntegrativeProducts_Page_Feature-3 @Sprint2 @Regression @TC155
  Scenario: Verify 'Featured Product' details for 'BY TYPE' category
  Given "Products" Mega menu should be displayed
  When Hover on the 'Products' Mega menu
  And Hover on the 'BY TYPE' category
  Then Verify the "Featured Product" title of 'BY TYPE'
  And Verify the featured product image of 'BY TYPE'
  And Verify the featured product name of 'BY TYPE'
  And Verify the 'View Product details' button is displayed and enabled of 'BY TYPE'


  @GlobalHeaderIntegrativeProducts_Page_Feature-4 @Sprint2 @Regression @TC148 @TC152 @TC153 @TC154 @TC156
  Scenario: Verify user can navigate to appropriate sub category page of 'BY HEALTH INTEREST'
  Given "Products" Mega menu should be displayed
  When Hover on the 'Products' Mega menu
  And Hover on the 'BY HEALTH INTEREST' category
  Then Verify the below sub category list of 'BY HEALTH INTEREST'
  When click anyone subcategory of 'BY HEALTH INTEREST'
  Then Verify user can navigate to appropriate subcategory page of 'BY HEALTH INTEREST'

 
  @GlobalHeaderIntegrativeProducts_Page_Feature-5 @Sprint2 @Regression @TC155
  Scenario: Verify 'Featured Product' details for 'BY HEALTH INTEREST' category
  Given "Products" Mega menu should be displayed
  When Hover on the 'Products' Mega menu
  And Hover on the 'BY HEALTH INTEREST' category
  Then Verify the "Featured Product" title of 'BY HEALTH INTEREST'
  And Verify the featured product image of 'BY HEALTH INTEREST'
  And Verify the featured product name of 'BY HEALTH INTEREST'
  And Verify the 'View Product Details' button is displayed and enabled of 'BY HEALTH INTEREST'

   
  #@GlobalHeaderIntegrativeProducts_Page_Feature-6 @Regression @TC157 @TC158 @TC159 @TC160
  #Scenario: Verify user can navigate to appropriate sub category page of 'PROTOCOL KITS'
  #Given 'Products' Mega menu should be displayed
  #When Hover on the 'Products' Mega menu
  #And Hover on the "PROTOCOL KITS' category
  #Then Verify the below sub categoery list of 'PROTOCOL KITS'
  #When click anyone subcategory
  #Then Verify user can navigate to appropriate subcategory page
  
 
  #@GlobalHeaderIntegrativeProducts_Page_Feature-7 @Regression  @TC155
  #Scenario: Verify 'Featured Product' details for 'PROTOCOL KITS' category
  #Given 'Products' Mega menu should be displayed
  #When Hover on the 'Products' Mega menu
  #And Hover on the 'PROTOCOL KITS' category
  #Then Verify the 'Featured Product' title
  #And Verify the featured product image
  #And Verify the featured product name
  #And Verify the 'View Product Details' button is displayed and enabled
  
  
  @GlobalHeaderIntegrativeProducts_Page_Feature-8 @Sprint2 @Regression @TC161 @TC162
  Scenario: Verify user can navigate to PLP by clicking 'VIEW ALL PRODUCTS'
  Given "Products" Mega menu should be displayed
  When Hover on the 'Products' Mega menu
  And click on "VIEW ALL PRODUCTS" link
  Then verify user can navigate to Product listing page