@Pagination @Sprint4
Feature: Pagination This feature deals with the Pagination functionality

  Background: 
    Given I am in home page of integrative pro
    When promo pop up is displayed
    When close ConfirmMessage
    
   @Pagination_01 @Sprint4
   Scenario: To verify the next page nagination using icon
    And click on mega menu products
    And click view all products
    Then verify that current active page is highlighted
    And verify the next page nagination using '>' icon
      
    @Pagination_02 @Sprint4
    Scenario: To verify the previous page nagination using icon
    And click on mega menu products
    And click view all products
    And verify the next page nagination using '<' icon
    
    
   @Pagination_03 @Sprint4
   Scenario: To verify the pagination functionality using number 
   And click on mega menu products
   And click view all products
   Then verify the pagination 
    
    
    @Pagination_04 @Sprint4
   Scenario: To verify the pagination functionality for last page and first page 
   And click on mega menu products
   And click view all products
   Then verify the pagination for firstpage
   And verify the pagination for lastpage
   
    