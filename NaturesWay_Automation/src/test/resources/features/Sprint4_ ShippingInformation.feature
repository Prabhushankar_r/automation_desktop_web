@ShippinginformationPage
Feature: ShippinginformationPage: This feature deals with the Shippinginformation_Page functionality of the application on Home page

  Background: 
    Given I am in home page of integrative pro
    And close ConfirmMessage

  @Shippinginformation_Page_Feature-1 @Cadence4 
  Scenario: Verify the display of Address Drop Down
    And click headercart
    And click proceed to checkout in cartpage
    And click shipping address Drop Down
    And close browser

  @Shippinginformation_Page_Feature-2 @Cadence4 
  Scenario: Verify the display of shipping address Page
    And click headercart
    And click proceed to checkout in cartpage
    And click click on New Shipping Address CTA Text Link
    And Enter Company Name "products"
    And Enter First Name "bala"
    And Enter Last Name "krishna"
    And Enter Street Address "banjarahills"
    And Enter Street Address "nandhamuri colany"
    And Enter City "hyderabad"
    And click on State
    And click Checkbox if primary shipping address
    And click save and Continue
    And close browser

  @Shippinginformation_Page_Feature-3 @Cadence4 
  Scenario: Verify the display of shipping address Page
    And click click on New Shipping Address Edit
    And click headercart
    And click proceed to checkout in cartpage
    And click edit
    And close browser
