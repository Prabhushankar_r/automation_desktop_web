@Resources_GlobalHeaderSearchPage @Sprint2
Feature: GlobalHeaderSearchPage: This feature deals with the GlobalHeaderSearch_Page functionality of the application on Home page

  Background: 
    Given I am in home page of integrative pro
    When promo pop up is displayed
    And close ConfirmMessage
   
   
   @GlobalHeaderSearch_Page_Feature-1 @Cadence2 @Regression @TC163 @Tc164 
    Scenario: Verify the display of search icon and search pane
    Given verify search icon is displayed
    When click on search icon
    Then verify the search pane is displayed
    And Verify the search icon is displayed besides of search pane
    
    @GlobalHeaderSearch_Page_Feature-2 @Cadence2 @Regression @Tc165 @TC167 @TC168 @TC169 @TC176
    Scenario: Verify user can navigate to appropriate page by clicking any products on 'Popular products' under search pan
    Given verify search icon is displayed
    When click on search icon
    Then verify the "POPULAR PRODUCTS" label
    And verify the display of 'Poupular Products' name and images
    When click on anyone product listed
    Then verify user should navigate to approrpiate Product details page

    
    @GlobalHeaderSearch_Page_Feature-3 @Cadence2 @Regression @Tc165 @TC167 @TC170 @TC176 @specific
    Scenario: Verify user can navigate to appropriate page by clicking any articles on 'Popular articles' under search pan
    Given verify search icon is displayed
    When click on search icon
    Then verify "POPULAR ARTICLES" label
    And verify the display of 'Poupular Articles' name
    When click on anyone article listed
    Then verify user should navigate to approrpiate article details page
   
    
    @GlobalHeaderSearch_Page_Feature-4 @Cadence2 @Regression @TC166 @TC172 @TC175 
    Scenario: Verify the display of searched products
    Given verify search icon is displayed
    When click on search icon
    And Enter the first three letter of product "buf" in the search pane
    And Click the keyboard enter
    Then verify the appropriate search product is listed
   
  
   @GlobalHeaderSearch_Page_Feature-5 @Cadence2 @Regression  @TC173 @TC174 
    Scenario: Verify user can close the search pan by clicking the close icon
    Given verify search icon is displayed
    When click on search icon
    Then verify search pan is displayed with search icon
    When click the close icon of the search
    Then verify the search pan is disappeared

    
    @GlobalHeaderSearch_Page_Feature-6 @Cadence2 @Regression @TC177
    Scenario: Verify user can close the search pan by clicking outside of the search
    Given verify search icon is displayed
    When click on search icon
    Then verify search pan is displayed with search icon
    When click outside of search 
    Then verify the search pan is disappeared
  