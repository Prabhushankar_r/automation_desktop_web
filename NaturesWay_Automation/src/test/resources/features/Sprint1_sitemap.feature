@sitemap @Sprint1
Feature: sitemap: This feature deals with the sitemap_Page functionality of the application on Home page

  Background: 
   Given I am in home page of integrative pro
     When promo pop up is displayed  
    And close ConfirmMessage

  @sitemap_Page_Feature-1 @Tc170 @Sprint1 @Regression
  Scenario: verify the display of items under sitemap
    And click on sitemap link in globel footer
    And verify the display of products
    Then verify the display of resources
    And verify the display of about us
    Then verfy the display of general links
    Then user should able to view the bread crumbs
    
