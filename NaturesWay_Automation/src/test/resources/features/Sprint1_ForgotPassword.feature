@ForgotPassword  @Sprint1
Feature: ForgotPassword
  Description: This feature deals with the forgot password functionality of the application

  Background: 
    Given I am in home page of integrative pro
     When promo pop up is displayed 
     And close ConfirmMessage

  @Forgot-1 @Sprint1 @Regression @Tc140 @Tc142 @Tc143 @Tc155 @Tc88 @Tc105 @Tc146 @Tc147
  Scenario: Verify forgot password and its components
    And click on sigin button
    Then click on forgot password
    Then verify the display of forgot password title
    Then verify the display of forgot password description
    When An email address text box
    And verify submit button
    

  @Forgot-2 @Tc141 @Tc152 @Tc153 @Sprint1 @Regression
  Scenario: Verify the message when email address is given
    And click on sigin button
    Then click on forgot password
    And I enter email "new@yopmail.com"
    And I should click on submit button
    And verify the success message
    


  @Forgot-3 @Sprint1 @Regression
  Scenario: Verify submit button is enabled when invalid email is entered
    And click on sigin button
    Then click on forgot password
    When I enter email "new@yopmai"  
    Then verify error message for invalid email
    And verify submit button is disabled in forgot password 
    
    

  @Forgot-4 @Tc150 @Tc149 @Sprint1 @Regression
  Scenario: Verify the cancel button
    And click on sigin button
    Then click on forgot password
    And click on cancel button
    

  @Forgot-5 @Tc158 @Tc159 @Sprint1 @Regression
  Scenario: Verify the close icon
    And click on sigin button
    Then click on forgot password
    And click on close icon in forgot password screen
    

  @Forgot-6 @Tc148 @Sprint1 @Regression
  Scenario: click on the forgot text box
    And click on sigin button
    Then click on forgot password
    And click on the forgot text box
    
