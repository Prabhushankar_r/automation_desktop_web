@SignIn_Feature
Feature: SignIn: This feature deals with the SignIn functionlity of the apllication

  Background:
 Given I am in home page of integrative pro
  When promo pop up is displayed
  And close ConfirmMessage
	
	@SignIn_Feature-1 @TC1 @TC2 @TC3 @TC4 @TC5 @TC6 @TC7 @TC8 @TC9 @TC10 @Sprint1 @Regression @Smoke @SignedIn_User
	Scenario: Verify user is able to log-in to the application
  When I tap on Sign In
  Then I enter valid login credentials
   And I click on sign in button in modal window  
   Then verify user is logged successfully
   Then verify user icon for sign out
   Then I Should be logout succesfully 
   #And user logs out
	

  @SignIn_Feature-2 @TC7 @TC9 @TC10 @Sprint1 @Regression @UI
  Scenario: Verify the label and placeholders on Email Address and Password textbox
	When I tap on Sign In
	Then I should see SignIn Model window
	When I click on email address
	Then watermark should become smaller 
	And I Enter your email address text change as Email address
	When I click on password
	Then I see password text become smaller 	
	
  @SignIn_Feature-3 @TC11 @TC12 @TC13 @TC14 @TC15 @TC16 @Sprint1 @Regression @UI
  Scenario: Verify the visiblity and behavoir of show icon or hide icon
	When I tap on Sign In
	Then I should see SignIn Model window
	Then I see the show icon as eye icon with striked off mark on the right side of password field	
	When I enter the password
	When I click on show icon, hide eye icon should change without striked off mark
	And I should see the entered characters
	When I click on hide icon, show eye icon should change with striked off mark
	And I see the masked characters
	When I double click show icon 
	Then I should see the masked characters
	When I double click Hide icon
	Then I should see the characters	
	
  @SignIn_Feature-4 @TC17 @TC18 @TC19 @Sprint1 @Regression
  Scenario: Verify the visiblity and behavoir of Forgot password
	When I tap on Sign In
	Then I should see SignIn Model window
	Then I see the Forgot Password hyperlinked text below to password field
	When I click on Forgot password link
	Then I should see the Forgot password content window
	
  @SignIn_Feature-5 @TC20 @TC21 @TC22 @Sprint1 @Regression 
  Scenario: Verify the visiblity and behavoir of Sign In tab valid data entry
	When I tap on Sign In
	Then I should see SignIn Model window
	Then I enter valid login credentials
	And I click on sign in button in modal window  
     Then verify user is logged successfully
	#Then I am succesfully signed in to the site on valid input

	
  @SignIn_Feature-6 @TC23 @TC24 @Sprint1 @Regression 
  Scenario: Verify the visiblity and behavoir of Sign In tab for invalid data entry
	When I tap on Sign In
	Then I should see SignIn Model window
	When I do Sign In with Invalid email address "aa@bb.cc" and the Password "testing"
	Then I see the field with red color
	Then I see the explanation of error message as Information provided doesn't match
	

  @SignIn_Feature-7 @TC25 @TC26 @TC27 @Sprint1 @Regression
  Scenario: Verify the create an Account tab
	When I tap on Sign In
	Then I should see SignIn Model window
	Then I see the Create an Account hyperlinked text below to Sign In tab
	When I click create an account hyperlinked text
	Then I should be navigated to Create an Account page
	
  @SignIn_Feature-8 @TC28 @TC29 @Sprint1 @Regression
  Scenario: Verify the close icon
	When I tap on Sign In
	Then I should see SignIn Model window
	Then I see the close icon on top right corner of the Model window
	When I click on close icon
	Then Sign In model closed and I see the previous page
	
  @SignIn_Feature-9 @TC30 @Sprint1 @Regression 
  Scenario: Verify the close icon on outside mouse click 
	When I tap on Sign In
	Then I should see SignIn Model window
	Then I Click on outside model window
	Then Sign In model closed and I see the previous page
	
	
	# @SignIn_Feature-2 @TC1 @TC2 @TC3 @TC4 @TC5 @TC6 @TC8 @Sprint1 @Regression @UI 
# Scenario: Verify the sequence of components and it's visiblity in Sign In Model
#	When I tap on Sign In
#	Then I should see SignIn Model window
#	And I should able to see components of signin model
#		|SignIn Components	| 
#		|Brand Box 					|
#		|Integrative logo		|
#		|Brand Image				|
#		|Model window header|
#		|Text Fields				|
#		|Forgot password		|
#		|Submit							|
#		|Create an acoount	|
#		|Close icon					|
#	Then I see model window header Sign In in top of the window and below to Brandbox
	