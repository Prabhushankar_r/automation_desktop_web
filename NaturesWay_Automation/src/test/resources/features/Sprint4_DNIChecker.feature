@Sprint4_DNIChecker
Feature: DNI_Checker: This feature deals with the DNI checker functionality of the application 

  Background: 
   Given I am in home page of integrative pro
    When promo pop up is displayed
    When close ConfirmMessage
 
   @DNIChecker_Feature_01 @Cadence4 
    Scenario: Verify drug interaction section in home page
    And scroll till Drug- Nutrient interaction
    And Click on Check for Interaction button 
    Then display Drug-Nutrient Interaction Landing Page 
    And close browser
    
   Scenario: Verify form on DNI checker landing page
    And click on resources
    And click Drug-Nutrient Interaction
    Then verify the banner image 
    Then verify breadcrumbs
    And verify the form  section
    
    Scenario: To verify Info Collect Form section
    And click on resources
    And click Drug-Nutrient Interaction
    And enter invalid email "email"
    Then display error in DNI page 
    Then verify whether submit button is disabled 
    Then verify and enter values in below components 
    |Email address|
    |Practitioner Type|
    |Number of Practitioners|
    |Submit button|
    Then verify submit button is enabled
    And Click on submit button
   
    
    
    