@Cartpage @Sprint4
Feature: CartPage: This feature deals with the Cartdrawer_Page functionality of the application on Home page

  Background: 
    Given I am in home page of integrative pro
    When promo pop up is displayed
    When close ConfirmMessage

  @CartPage_Feature-1   @TC_01,@TC_02,@TC_03,@TC_04,@TC_15 @Sprint4
  Scenario: Verify the display of product components in shopping cart section
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    When click on search icon
    Then verify the search pane is displayed
    And search for "Esberitox" and select it
    And click Add to Cart button
    #And click Add to Cart button and verify the count
    And click proceed to checkout
    Then display cart page
    Then verify the product details in shopping cart section
    And verify subtotal and price is displayed 
    When click on proceed to checkout in cart page
    Then display checkout page
    


 @CartPage_Feature-2   @TC_06,@TC_07,@TC_08,@TC_13,@TC_26 @Sprint4
 Scenario: To verify recently added product and delete functionality for shopping cart
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    And verify the cart icon in header is empty
    When click on search icon
    Then verify the search pane is displayed
    And search for "Esberitox" and select it
    And click Add to Cart button
    And click proceed to checkout
    Then display cart page
    Then verify new product is displayed first in shopping cart
    When delete link is selected in cart section
    And verify proceed to checkout button is in disabled state
    
   
    
 @CartPage_Feature-3   @TC_09,@TC_10,@TC_16,@TC_25, @Sprint4
 Scenario: To verify save for later functionality in cart page after relogin
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    When click on cart icon in header
    And verify save for later section is empty in cart page
    When click on search icon
    Then verify the search pane is displayed
    And search for "Esberitox" and select it
    And click Add to Cart button
    And click proceed to checkout
    Then display cart page
    Then verify save for later functionality in cart page
    And change the qty and units values for save for later products
    Then user should able to click on welcome user
    Then I Should be logout succesfully  
    Then verify app launch is success  
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    When click on cart icon in header
    Then display cart page
    And verify save for later is displayed after relogin
    When delete link is selected in save for later section
    
    
@CartPage_Feature-4   @TC_11, @Sprint4
  Scenario: Verify the display of product components in saveforlater section
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    And verify the cart icon in header is empty
    When click on search icon
    Then verify the search pane is displayed
    And search for "Esberitox" and select it
    And click Add to Cart button and verify the count
    #And click Add to Cart button
    And click proceed to checkout
    Then verify save for later functionality in cart page
    And verify the product details in saveforlater section
    And verify subtotal and price is displayed 
    
    
  @CartPage_Feature-5   @TC_12, @Sprint4
  Scenario: Verify product moved from saveforlater section to cart section
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    And verify the cart icon in header is empty
    When click on search icon
    Then verify the search pane is displayed
    And search for "Esberitox" and select it
    And click Add to Cart button and verify the count
    #And click Add to Cart button
    And click proceed to checkout
    Then display cart page
    Then verify new product is displayed first in shopping cart
    Then verify save for later functionality in cart page
    And verify new product is displayed first in saveforlater
    When click Add to Cart button in saveforlater section
    Then move the products into cart section
    
    
    
#   #Yet to implement
#     @CartPage_Feature-7  
#  Scenario: Verify the default values of Qty and Units 
#    And I click on sign-in
#    Then I enter valid "june27@yopmail.com" and valid "Photon@123"
#    And I click on sign in button in modal window
#    Then verify user is logged successfully
#    #And verify the cart icon in header is empty
#    When click on search icon
#    Then verify the search pane is displayed
#    And search for "Esberitox" and select it
#    #And click Add to Cart button and verify the count
#    Then verify the default values of Qty and Units fields 
#    And click Add to Cart button
#    And click proceed to checkout
#    Then display cart page
    

    
  @CartPage_Feature-6   @TC_20,@TC_21,@TC_22 @Sprint4
  Scenario: Verify the behaviour of Need Help and Return Policy
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    And verify the cart icon in header is empty
    When click on cart icon in header
    Then display cart page
    And verify proceed to checkout button is in disabled state
    Then display need help button in cart page 
    #When click on need help button in cart page 
    #Then display Need help screen
    And display return policy button in cart page 
    When click on return policy button in cart page 
    #Then display Return Policy screen
    
  
  	@CartPage_Feature-7   @TC_24, @Sprint4
  	Scenario: Verify the system remove the product when delete is selected from cart drawer
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    #And verify the cart icon in header is empty
    When click on search icon
    Then verify the search pane is displayed
    And search for "Esberitox" and select it
    And click Add to Cart button and verify the count
    #And click Add to Cart button
    When delete link is selected in cart drawer screen
   

   @CartPage_Feature-8  @Regression @TC_17 @TC_18 @Sprint4
   Scenario: To verify system display message when products are not available in Shopping cart and Save for later section
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    And verify the cart icon in header is empty
    When click on cart icon in header  
    Then verify save for later section is empty
    Then display message for empty cart section "Your Cart is Empty"
    And display message for empty saveforlater section "No Items in Saved for Later"
    
    
