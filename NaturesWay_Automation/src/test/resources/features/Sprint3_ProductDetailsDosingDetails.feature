@ProductDetailsDosingDetails @Sprint3
Feature: ProductDetailsDosingDetails This feature deals with the displaying of dosage and direction for the products

  Background: 
   Given I am in home page of integrative pro
    When promo pop up is displayed
    When close ConfirmMessage
    
   @ProductDetailsDosingDetails_Feature-1 @Sprint3 @Regression
  Scenario: verify the display of Product Details-Dosing Details
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    And click on mega menu products
    And click view all products
    And click product title 
    When navigate to direction menu in product detail page
    Then verify the title 'Direction' in pdp
    Then verify Direction & Dosage information available
    
    
    
  
  