@HowToOrder @Sprint4
Feature: HowToOrderPage: This feature deals with the HowToOrder_Page functionality of the application on Home page

  Background: 
    Given I am in home page of integrative pro
    When promo pop up is displayed
    When close ConfirmMessage


  @HowToOrder_Feature-1 @Sprint4  @TC_01,@TC_02,@TC_03,@TC_04,@TC_05,@TC_08,@TC_09
  Scenario: verify the display of Hero Banner in how to order page
    And click on mega menu products
    And click view all products
    And click product image
    When click how to order
    Then verify how to order page    
    And verify Title and hero banner
    And verify content area
    And click Find a Practitioner button
    Then navigate to Find a Practitioner page  
    

  @HowToOrder_Page_Feature-2 @Sprint4  @TC_06,@TC_10,@TC_11
  Scenario: verify the display of More Ways to Order
    And click on mega menu products
    And click view all products
    And click product image
    And click how to order
    And verify amazon logo
    And verify Title of amazon
    And verify Shop Amazon Now button  
    And click Amazon link
    

  @HowToOrder_Page_Feature-3 @Sprint4  @TC_12
  Scenario: To click on Shop Amazon Now button
    And click on mega menu products
    And click view all products
    And click product image
    And click how to order
    And click Shop Amazon now
    

  @HowToOrder_Page_Feature-4 @Sprint4  @TC_13
  Scenario: verify the display of More Ways to Order
    And click on mega menu products
    And click view all products
    And click product image
    And click how to order
    And verify amazon logo
    And verify Title of amazon
    And click Integrative practitioner
   Then navigate to Find a Practitioner page  
    
    

  @HowToOrder_Page_Feature-5 @Sprint4  @TC_07,@TC_14,@TC_15,@TC_16,@TC_17
  Scenario: verify the display of Frequently asked questions
    And click on mega menu products
    And click view all products
    And click product image
    And click how to order
    And verify question and answer section in how to order page  
    Then verify multiple question and answer section can be viewed
    
    
