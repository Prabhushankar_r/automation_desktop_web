@Sprint4_Checkout
Feature: Verifying the scenarios related to the checkout page 

 Background: 
    Given I am in home page of integrative pro
    When promo pop up is displayed
    When close ConfirmMessage
    
    #################
    # Review Order  # 
    ################# 
    @Checkout-Scenario-1 @TC15036 @Sprint4
    Scenario: Verify system updating the product price automatically by changing the 'Qty' and 'units'
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    And click on mega menu products
    And click view all products
    Then verify user can navigate to Product listing page
    When Click on product '0' and get the product title
    Then Verify user is navigated to PDP
    And click Add to Cart button
    And click proceed to checkout
    Then display cart page
    When click on proceed to checkout in cart page
    Then display checkout page
    And verify the line item count
    When change the product Qty as "2" and verify the product price got updated properly
    When change the unit as "Case (12 Units)" and verify the product price got updated properly
    
    @Checkout-Scenario-2 @TC15052 @TC15053 @Sprint4
	  Scenario: Verify authenticated user can land on the checkout page
 	  And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    And click on mega menu products
    And click view all products
    Then verify user can navigate to Product listing page
    When Click on product '0' and get the product title
    Then Verify user is navigated to PDP
    And click Add to Cart button
    And click proceed to checkout
    Then display cart page
    When click on proceed to checkout in cart page
    Then display checkout page
    
    @Checkout-Scenario-3 @TC15040 @Sprint4
    Scenario: Verify user is navigated to PLP by clicking the continue shopping link
 	  And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    And click on mega menu products
    And click view all products
    Then verify user can navigate to Product listing page
    When Click on product '0' and get the product title
    Then Verify user is navigated to PDP
    And click Add to Cart button
    And click proceed to checkout
    Then display cart page
    When click on proceed to checkout in cart page
    Then display checkout page
    When click the 'Coninue shopping' link
    Then verify user can navigate to Product listing page
    
    ##################################
    #                                #
    # Shipping Information Scenarios #
    #                                #
    ##################################
    #Bug Id: P11486-17248 (Failed in validating the address)
    @Checkout-Scenario-4 @TC15062 @Sprint4
    Scenario: Verify user can add the new shipping address by providing valid details
 	  And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    And click on mega menu products
    And click view all products
    Then verify user can navigate to Product listing page
    When Click on product '0' and get the product title
    Then Verify user is navigated to PDP
    And click Add to Cart button
    And click proceed to checkout
    Then display cart page
    When click on proceed to checkout in cart page
    Then display checkout page
    When click on Add new shipping address 
    Then verify company name is disabled in shipping address
    Then enter first name in shipping address "Prabhu"
    And enter last name in shipping address "Shankar"
    Then enter address in shipping information "1650 Grand Concourse"
    And enter zipcode in shipping information "10457"
    Then state and city fields are autopopulated in shipping address
    And enable checkbox for primary shipping address
    When click on CONTINUE button in add new shipping address
    Then Verify added shipping address is displayed for user with edit link in checkoutPage
    
    @Checkout-Scenario-5 @TC15054 @TC15060 @Sprint4
    Scenario: Verify the 'Shipping Information' and 'Add new shipping information' componenets
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    And click on mega menu products
    And click view all products
    Then verify user can navigate to Product listing page
    When Click on product '0' and get the product title
    Then Verify user is navigated to PDP
    And click Add to Cart button
    And click proceed to checkout
    Then display cart page
    When click on proceed to checkout in cart page
    Then display checkout page
    Then Verify the components in the shipping information section
    Then verify the components in the Add new shipping information section
    
    @Checkout-Scenario-6 @TC15057 @Sprint4
    Scenario: Verify default shipping address is displayed under shipping address dropdown
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    And click on mega menu products
    And click view all products
    Then verify user can navigate to Product listing page
    When Click on product '0' and get the product title
    Then Verify user is navigated to PDP
    And click Add to Cart button
    And click proceed to checkout
    Then display cart page
    When click on proceed to checkout in cart page
    Then display checkout page
    Then verify default shipping address is displayed in shipping address dropdown
    
    #########################
    # Payment Information   # 
    ######################### 
    #@Checkout-PaymentInformation-1 @Sprint4
    #Scenario: Verify system displays error message when enter the invalid card number and submit it in add new payment
    #And I click on sign-in
    #Then I enter valid login credentials
    #And I click on sign in button in modal window
    #Then verify user is logged successfully
    #And click on mega menu products
    #And click view all products
    #Then verify user can navigate to Product listing page
    #When Click on product '0' and get the product title
    #Then Verify user is navigated to PDP
    #And click Add to Cart button
    #And click proceed to checkout
    #Then display cart page
    #When click on proceed to checkout in cart page
    #Then display checkout page
    #When click on Add new shipping address 
    #Then verify company name is disabled in shipping address
    #Then enter first name in shipping address "Prabhu"
    #And enter last name in shipping address "Shankar"
    #Then enter address in shipping information "1650 Grand Concourse"
    #And enter zipcode in shipping information "10457"
    #Then state and city fields are autopopulated in shipping address
    #When click on CONTINUE button in add new shipping address
    #When click on 'Add new payment method' link
    #And Enter the cardno, name on card, Exp Month, Exp year and CVV
    
     ####################
     # Billing Address  # 
     #################### 
    @Checkout-Scenario-7 @TC15026 @TC15025 @Sprint4
    Scenario: Verify the components of 'New billing address' and 'Billing Address same as Shipping Address' is displayed with checkbox
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    And click on mega menu products
    And click view all products
    Then verify user can navigate to Product listing page
    When Click on product '0' and get the product title
    Then Verify user is navigated to PDP
    And click Add to Cart button
    And click proceed to checkout
    Then display cart page
    When click on proceed to checkout in cart page
    Then display checkout page
    When click on Add new shipping address 
    Then verify company name is disabled in shipping address
    Then enter first name in shipping address "Prabhu"
    And enter last name in shipping address "Shankar"
    Then enter address in shipping information "1650 Grand Concourse"
    And enter zipcode in shipping information "10457"
    Then state and city fields are autopopulated in shipping address
    When click on CONTINUE button in add new shipping address
    Then Verify added shipping address is displayed for user with edit link in checkoutPage
    Then verify Billing address same as shipping address is displayed with checkbox
    When uncheck the 'Billing address same as shipping address' checkbox
    Then verify the componenets of the 'New billing address'
    
    @Checkout-Scenario-8 @TC15033 @Sprint4
    Scenario: Verify system closes the 'new billing information' section, when click cancel link after enterted all informations
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    And click on mega menu products
    And click view all products
    Then verify user can navigate to Product listing page
    When Click on product '0' and get the product title
    Then Verify user is navigated to PDP
    And click Add to Cart button
    And click proceed to checkout
    Then display cart page
    When click on proceed to checkout in cart page
    Then display checkout page
    When click on Add new shipping address 
    Then verify company name is disabled in shipping address
    Then enter first name in shipping address "Prabhu"
    And enter last name in shipping address "Shankar"
    Then enter address in shipping information "1650 Grand Concourse"
    And enter zipcode in shipping information "10457"
    Then state and city fields are autopopulated in shipping address
    When click on CONTINUE button in add new shipping address
    Then Verify added shipping address is displayed for user with edit link in checkoutPage
    When uncheck the 'Billing address same as shipping address' checkbox
    When enter 'company name','firstname','lastname','address', 'zipcode' 
    Then verify city and state is autopopulated in the new billing address section 
    When click 'cancel' link in new billing address section
    Then verify new billing address section is disappeared
    
    #################
    # Order Summary # 
    ################# 
    @Checkout-Scenario-9 @TC15049 @TC15065 @Sprint4
    Scenario: Verify the components and product price is updated in "Order Summary" by deleting any one product
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    And click on mega menu products
    And click view all products
    Then verify user can navigate to Product listing page
    When Click on product '0' and get the product title
    Then Verify user is navigated to PDP
    And click Add to Cart button
    And click proceed to checkout
    Then display cart page
    When click on proceed to checkout in cart page
    Then display checkout page
    And verify the line item count
    Then verify the componenets are displayed under order summary section
    When Delete the '0' index product in the review order section
    Then verify the respective product is disappeared from the lineitem
    Then verify the price is updated properly on the order summary section
    
    #################
    # Review Order  # 
    ################# 
    @Checkout-Scenario-10 @TC15038 @Sprint4
    Scenario: Verify the respective product is disappeared when click 'Save for later' link
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    And click on mega menu products
    And click view all products
    Then verify user can navigate to Product listing page
    When Click on product '0' and get the product title
    Then Verify user is navigated to PDP
    And click Add to Cart button
    Then display continue shopping and proceed to checkout CTA button
    And click on mega menu products
    And click view all products
    Then verify user can navigate to Product listing page
    When Click on product '1' and get the product title
    Then Verify user is navigated to PDP
    And click Add to Cart button
    And click proceed to checkout
    Then display cart page
    When click on proceed to checkout in cart page
    Then display checkout page
    And verify the line item count
    When clicks save for later link for the product in '0'th index in the lineitem
    Then verify the respective product is disappeared from the lineitem