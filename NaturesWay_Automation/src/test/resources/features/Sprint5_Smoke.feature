@Sprint5_smoke
Feature: Smoke Related feature 

 Background: 
    Given I am in home page of integrative pro
    When promo pop up is displayed
    When close ConfirmMessage
    
 @Sprint5_smoke_01 @TC_01 @TC_02
 Scenario: To Edit the personal information for authenticated user
  And I click on sign-in
  Then I enter valid login credentials
  And I click on sign in button in modal window
  Then verify user is logged successfully
  And verify username is displayed in header
  When click on Account Information from dropdown
  Then display the "Account Details" Page 
  Then Edit the information and save it
   
   
   
@Sprint5_smoke_02 @TC_03  @Smoke_TC
Scenario: To verify components of patient resources page
    And click on resources
    And click Patient Resource
    And verify the display of resource header
    And verify the display of resource hero banner title
    And verify the display of resource hero banner description
    And verify the display of two filters
    And click product information
    Then verify the count for product information
    And verify the display of title for product information tab
    Then verify the display of images for product information tab
    And verify the display of Patient Resource PDFs
    And click on Health Conditions 
    Then verify the count for Health Conditions
    And verify the display of title for health condition tab
    Then verify the display of images for health condition tab
    Then verify the display of Patient Resource PDFs related to Health Conditions
    And verify the display of back to home button
    
    
  @Sprint5_smoke_03 @TC_03 @Smoke_TC
	Scenario: To verify checkout page
 	And I click on sign-in
 	Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    And click on mega menu products
    And click view all products
    Then verify user can navigate to Product listing page
    And click product title
    Then Verify user is navigated to PDP
    And click Add to Cart button
    And click proceed to checkout
    Then display cart page
    When click on proceed to checkout in cart page
    Then display checkout page
    When click on Add new shipping address 
    Then verify company name is disabled in shipping address
    Then enter first name in shipping address "Navya"
    And enter last name in shipping address "Sampath"
    Then enter address in shipping information "1650 Grand Concourse"
    And enter zipcode in shipping information "10457"
    Then state and city fields are autopopulated in shipping address
    And enable checkbox for primary shipping address
    When click on CONTINUE button in add new shipping address
     
    @Sprint5_smoke @TC_06 @TC_07 @Tc10
	Scenario: To verify the Order summary and billing address in order confirmation page
 	And I click on sign-in
 	Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    And click on mega menu products
    And click view all products
    Then verify user can navigate to Product listing page
    When Click on product '0' and get the product title
    Then Verify user is navigated to PDP
    And click Add to Cart button
    And click proceed to checkout
    Then display cart page
    When click on proceed to checkout in cart page
    Then display checkout page
    When click on Add new shipping address 
    Then verify company name is disabled in shipping address
    Then enter first name in shipping address "Navya"
    And enter last name in shipping address "Sampath"
    Then enter address in shipping information "1650 Grand Concourse"
    And enter zipcode in shipping information "10457"
    Then state and city fields are autopopulated in shipping address
    And enable checkbox for primary shipping address
    When click on CONTINUE button in add new shipping address
    Then Verify added shipping address is displayed for user with edit link in checkoutPage
    When click on 'Add new payment method' link
    And Enter the cardno, name on card, Exp Month, Exp year and CVV
    Then verify 'Billing address same as shipping address' and 'Save payment method for future use' is enabled
    When click the 'Save&continue' button
    Then verify the 'card no' and 'billing address'
    And Verify the 'Place Order' button is enabled
    When Get the details of 'Items','Shipping & Handling','Discounts','subtotal', 'tax' and 'Order total' from order summary 
    And Click the 'Place order' button
    Then verify user is navigated to order confirmation page and order is placed successfully
    And verify the 'Order summary' details which is fetched from checkout page
    And verify the given billing address is displayed in order confirmation page
    
    @Sprint5_smoke @TC_08
    Scenario: Verify the sales representative information in My Account page
    And I click on sign-in
    Then I enter valid login credentials
    And I click on sign in button in modal window
    Then verify user is logged successfully
    When click on Account Information from dropdown
    Then display the "Account Details" Page 
    And Verify the information of sales representative 'Photo Image', 'Name' , 'Job title', 'Address', 'Email Address' , 'Care Email Address' , 'Phone No'
      
    @Sprint5_smoke @TC_09
    Scenario: Verify user is able to see the favorite products in favorite product page
    And I click on sign-in
    Then I enter valid login credentials
    #Then I enter valid "prabhuHCP2@gmail.com" and valid "Photon@123"
    And I click on sign in button in modal window
    Then verify user is logged successfully
    And click on mega menu products
    And click view all products
    Then verify user can navigate to Product listing page
    When Click on product '0' and get the product title
    Then Verify user is navigated to PDP
    And click favorites
    Then verify that 'favorite' icon is highlighted
    When verify the favorite Icon and click the favorite menu under MyAccount dropdown 
    Then Verify user is landed in favorites page
    And Verify that Favorite product is displayed in favorites page