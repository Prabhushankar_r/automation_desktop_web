@legal @Sprint1
Feature: legal: This feature deals with the legal_Page functionality of the application on Home page

  Background: 
   Given I am in home page of integrative pro
     When promo pop up is displayed 
     And close ConfirmMessage

  @legal_Page_Feature-1 @Tc166 @Sprint1 @Regression
  Scenario: verify the display of items under legal
    And click on legal link in globel footer
    And verify the view of legal page title
    Then verify the display of tabs
    Then user should able to view the bread crumbs
    

  @legal_Page_Feature-2 @Tc167 @Tc168 @Tc169 @Sprint1 @Regression
  Scenario: verify the display of Tabs under legal
    And click on legal link in globel footer
    And verify the view of legal page title
    And User able to click on Terms Of Use Icon and close
    And User able to click on Resale Policy Icon and close
    And User able to click on Privacy Policy Icon and close
    And User able to click on HIPPA Icon and close
    Then user should able to view the bread crumbs
    
