@GlobelHeader  @Sprint1
Feature: GlobelHeader: This feature deals with the GlobelHeader_Page functionality of the application on Home page

  Background: 
    Given I am in home page of integrative pro
     When promo pop up is displayed 
     And close ConfirmMessage

  @GlobelHeaderWeb_Page_Feature-1 @Smoke @Tc6 @Tc7 @Tc8 @Tc9 @Tc10 @Tc11 @Tc12 @Tc13 @Tc16 @Tc17 @Tc20 @Tc21 @Sprint1 @Regression
  Scenario: verify the display OF GlobelHeader Icons
    Then Verify the display of Integrativepro logo
    When I should see mega menu
      | MMC       |
      | Products  |
      | Resources |
      | About Us  |
    And verify the display of cart icon
    And verify the display of search icon
    And I click on sign-in
    And click on close icon
    And I click on create an account
    #And click on go to integrative pro
    
