@Megamenu @Sprint1
Feature: MegaMenu
  Description: This feature deals with the Mega menu functionality of the application

  Background: 
    Given I am in home page of integrative pro
     When promo pop up is displayed 
    And close ConfirmMessage

  @Mega_Menu_Feature-1 @TC7 @TC8 @Tc23 @Tc24 @Tc25 @Tc236 @Sprint1 @Regression
  Scenario: Verify the sequence of Global mega menu components for Non signed in user
    When I should see mega menu
      | MMC       |
      | Products  |
      | Resources |
      | About Us  |
      | Cart      |
    

  @Mega_Menu_Feature-2 @Smoke @TC12 @Tc17 @TC18 @Tc19 @Tc26 @Tc27 @Tc28 @Tc29 @Tc88 @Tc91 @102 @Tc107 @Tc108 @Sprint1 @Regression
  Scenario: I Verify the sequence of Global mega menu components for signed in user
    And I click on sign-in
    Then I enter valid login credentials
    And click on password eye icon
    And I click on sign in button in modal window
    Then verify user is logged successfully
    When I Verify the sequence of global mega menu components for signed in user
      | MMC       |
      | Products  |
      | Resources |
      | About Us  |
      | Cart      |
    And User should see the quickorder
    Then user should able to click on welcome user
    Then I Should be logout succesfully
    

  @Mega_Menu_Feature-4 @Sprint1 @Tc90 @Tc88 @Tc114 @Regression
  Scenario: I Verify the close model in sign page
    And I click on sign-in
    And click on close icon
    

  @Mega_Menu_Feature-5 @Tc238 @Sprint1 @Regression
  Scenario: I Verify the Drug Nutrient Interaction
    And click on resources
    And click Drug Nutrient Interaction
    

  @Mega_Menu_Feature-6 @Tc240 @Sprint1 @Regression
  Scenario: I Verify the Patient Resources
    And click on resources
    And click Patient Resource
    

  @Mega_Menu_Feature-7 @242 @Sprint1 @Regression
  Scenario: verify the Article
    And click on resources
    And click on article tile
    

  @Mega_Menu_Feature-8 @Tc246 @Sprint1 @Regression
  Scenario: I Verify the Find a Practitioner
    And click on resources
    And click Find a Practitioner
    
