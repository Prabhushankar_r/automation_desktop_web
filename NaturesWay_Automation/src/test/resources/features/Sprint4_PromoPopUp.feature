@PromoPopUp
Feature: PromoPopUp This feature deals with the PromoCode_Page functionality of the application on Home page

  Background: 
    Given I am in home page of integrative pro
   
    
  @PromoCode_Page_Feature-1 @Cadence4 
  Scenario: Verify the display of promo pop up
  Then verify promo pop up is displayed 
  And verify the components in promo screen
  And verify email field is displayed in pop up screen
  When email field is empty in promo screen
  Then display "The email address is required." for empty email field in promo screen
  And verify the Claim my offer button is disabled
  When enter email address "june26@yopmail.com" in promo screen
  Then verify the Claim my offer button is enabled
  And click on Claim my offer button
  When click on No Thanks button
  Then promo pop up should disappear
  
  
  
  @PromoCode_Page_Feature-2 @Cadence4 
  Scenario: Verify the functionality of close promo pop up
  Then verify promo pop up is displayed 
  When click on close icon ins promo screen
  Then promo pop up should disappear
  
  
  
  
  
  


  