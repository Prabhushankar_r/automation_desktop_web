@PaymentInformationPage
Feature: PaymentInformationPage: This feature deals with the PaymentInformation_Page functionality of the application on Home page

  Background: 
    Given I am in home page of integrative pro
    And close ConfirmMessage

  @PaymentInformation_Page_Feature-1 @Cadence4 
  Scenario: Verify the display of Payment drop down
    And click headercart
    And click proceed to checkout in cartpage
    And click payment drop down to choose from existing cards
    And Add a New Payment Method
    And Enter Name on Card ""
    And Enter Expiry Date ""
    And Enter User types ""
    And Select Save Payment Method
    And Validate the error message
    And close browser

  @PaymentInformation_Page_Feature-2 @Cadence4 
  Scenario: Verify the display of Billing Information
    And click payment drop down to choose from existing cards
    And Add a New Payment Method
    And Enter Name on Card ""
    And Enter Expiry Date ""
    And Enter User types ""
    And Select Save Payment Method
    And Validate the error message
    And close browser

  @PaymentInformation_Page_Feature-3 @Cadence4 
  Scenario: Verify the display of Billing Address different from Shipping Address
    And click headercart
    And click proceed to checkout in cartpage
    And Select Billing information
    And Enter Company Name "products"
    And Enter First Name "bala"
    And Enter Last Name "krishna"
    And Enter Street Address "banjarahills"
    And Enter Street Address "nandhamuri colany"
    And Enter City "hyderabad"
    And click on State
    And click Checkbox if primary shipping address
    And click save and Continue
    And close browser

  @PaymentInformation_Page_Feature-4 @Cadence4 
  Scenario: Verify the display of Billing Address different from Shipping Address
    And click headercart
    And click proceed to checkout in cartpage
    And Select Billing information
    And Enter Company Name "products"
    And Enter First Name "bala"
    And Enter Last Name "krishna"
    And Enter Street Address "banjarahills"
    And Enter Street Address "nandhamuri colany"
    And Enter City "hyderabad"
    And click on State
    And click Checkbox if primary shipping address
    And click save and Continue
    And close browser

  @PaymentInformation_Page_Feature-5 @Cadence4 
  Scenario: Verify the display of Cancel nagitive case
    And click headercart
    And click proceed to checkout in cartpage
    And Select Billing information
    And Enter Company Name "products"
    And Enter First Name "bala"
    And Enter Last Name "krishna"
    And Enter Street Address "banjarahills"
    And Enter Street Address "nandhamuri colany"
    And Enter City "hyderabad"
    And click on State
    And click Checkbox if primary shipping address
    And click save and Continue
    And click Cancel
    And close browser
