package pageobjects.webportal;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import platforms.WebPortal;

public class Sprint5_OrderConfirmationPage extends WebPortal
{
	public WebDriver driver;
	
	public Sprint5_OrderConfirmationPage(WebDriver driver)
	{
		this.driver = driver;
	}
	
	public WebElement itemCount_OrderConfirmation()
	{
		return driver.findElement(By.xpath("//span[@data-bind='text: $root.cartCount']"));
	}
	
	public WebElement totalAmount_OrderConfirmation()
	{
		return driver.findElement(By.xpath("//span[@data-bind='text: cart().totalAmount']"));
	}
	
	public WebElement shippingAmt_OrderConfirmation()
	{
		return driver.findElement(By.xpath("//span[@data-bind='text: cart().shippingTotal']"));
	}
	
	public WebElement savingsOrDiscountAmt_OrderConfirmation()
	{
		return driver.findElement(By.xpath("//span[@data-bind='text: cart().discount']"));
	}
	
	public WebElement subTotal_OrderConfirmation()
	{
		return driver.findElement(By.xpath("//span[@data-bind='text: cart().subTotal']"));
	}
	
	public WebElement tax_OrderConfirmation()
	{
		return driver.findElement(By.xpath("//span[@data-bind='text: cart().taxTotal']"));
	}
	
	public WebElement orderToatl_OrderConfirmation()
	{
		return driver.findElement(By.xpath("//span[@data-bind='text: cart().total']"));
	}

	
	public WebElement firstNameShipping_Orderconfirmation()
	{
		return driver.findElement(By.xpath(""));
	}
	
	public WebElement lastNameShipping_Orderconfirmation()
	{
		return driver.findElement(By.xpath(""));
	}
	
	public WebElement address1Shipping_Orderconfirmation()
	{
		return driver.findElement(By.xpath(""));
	}
	
	public WebElement zipcodeShipping_Orderconfirmation()
	{
		return driver.findElement(By.xpath(""));
	}
	

}
