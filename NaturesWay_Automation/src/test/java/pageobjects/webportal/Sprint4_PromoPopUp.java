package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint4_PromoPopUp {
	public WebDriver driver;

	public Sprint4_PromoPopUp(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement LogoImage_PromoPopUp() throws Exception {
		return driver.findElement(By.cssSelector("div#promo-popup .modal-logo img"));
	}

	public WebElement HeaderTitle_PromoPopUp() throws Exception {
		return driver.findElement(By.cssSelector("div#promo-popup .module-title .promo-title-text"));
	}
	
	public WebElement Description_PromoPopUp() throws Exception {
		return driver.findElement(By.cssSelector(".promo-pop-up-content .module-description"));
	}
	
	public WebElement EmailField_PromoPopUp() throws Exception {
		return driver.findElement(By.cssSelector("input#promo-email-address"));
	}
	
	public WebElement ErrMsgEmailField_PromoPopUp() throws Exception {
		return driver.findElement(By.cssSelector("p#promo-email-field"));
	}
	
	public WebElement ClaimMyOffer_PromoPopUp() throws Exception {
		return driver.findElement(By.cssSelector("div.promo-btn button.btn-primary"));
	}
	
	public WebElement NoThanks_PromoPopUp() throws Exception {
		return driver.findElement(By.cssSelector("a.promo-no-thanks-text"));
	}
	
	public WebElement Close_Icon_Promo_Popup() throws Exception {
		return driver.findElement(By.cssSelector("div.promo-pop-up-close a"));
	}
	
	public WebElement PromoPopUp() throws Exception {
		return driver.findElement(By.cssSelector("div#promo-popup"));
	}
	
}