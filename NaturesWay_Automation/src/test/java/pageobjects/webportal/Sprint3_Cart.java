package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Sprint3_Cart {
//	public WebDriver driver;
//
//	public Sprint3_Cart(WebDriver driver) {
//		this.driver = driver;
//	}
	
		public WebDriver driver;

		public Sprint3_Cart(WebDriver driver) {
			PageFactory.initElements(driver, this);
			this.driver = driver;
		}
		
		
		@FindBy(css="div.saved-shopping-cart-wrapper .cart-item-info .cart-item-title")
		private List<WebElement> ProductTitle_SaveforLater;
		
		public List<WebElement> ProductTitle_SaveforLater() {
			return ProductTitle_SaveforLater;
		}
		
	public List<WebElement> productslist_cartdrawer() throws Exception {
		return driver.findElements(By.cssSelector("drawer-sub-details"));
	}
	
	public List<WebElement> saveforlater_list_cartpage() throws Exception {
		return driver.findElements(By.cssSelector("div#saveforlater-cart-lines div.cart-item-cont"));
	}
	
	public List<WebElement> shoppingcart_productlist_cartpage() throws Exception {
		return driver.findElements(By.cssSelector("div.shopping-cart-lines .cart-item-cont"));
	}
	
	public List<WebElement> saveforlater_productlist_cartpage() throws Exception {
		return driver.findElements(By.cssSelector("div#saveforlater-cart-lines .cart-item-wrapper .cart-item-cont"));
	}
	
	public WebElement cartpage() throws Exception {
		return driver.findElement(By.cssSelector("div.shopping-cart"));
	}
	
	public WebElement CartDrawerScreen() throws Exception {
		return driver.findElement(By.cssSelector("div.cart-drawer"));
	}
	
	public WebElement ProceedToCheckout_cartdrawer() throws Exception {
		return driver.findElement(By.cssSelector("div.drawer-btn-group a[href='/cart']"));
	}
	
	public WebElement ProceedToCheckout_cartpage() throws Exception {
		return driver.findElement(By.cssSelector("div.component-content a.btn-sub-total"));
	}
	
	public WebElement ContinueShopping_cartpage() throws Exception {
		return driver.findElement(By.cssSelector("div.field-link a.cart-cont-shop"));
	}

	public List<WebElement> DeleteLink_ShoppingCart() throws Exception {
		return driver.findElements(By.cssSelector("div.shopping-cart-lines div.btn-wrapper a:nth-child(1) span"));
	}
	
	
	public WebElement DeleteLinkAProduct_ShoppingCart() throws Exception {
		return driver.findElement(By.cssSelector("div.shopping-cart-lines div.btn-wrapper a:nth-child(1) span"));
	}
	
	public WebElement DeleteLinkAProduct_SaveforLater() throws Exception {
		return driver.findElement(By.cssSelector("div.saved-shopping-cart-wrapper div.btn-wrapper a:nth-child(1) span"));
	}
	
	public List<WebElement> DeleteLink_saveforlater() throws Exception {
		return driver.findElements(By.cssSelector("div.saved-shopping-cart-wrapper div.saved-btn a:nth-child(1) span"));
	}
	
	public List<WebElement> addtocartlink_saveforlatersection() throws Exception {
		return driver.findElements(By.cssSelector("div.saved-shopping-cart-wrapper div.saved-btn a:nth-child(2) span"));
	}
	
	
	
	public WebElement SaveForLaterLinkAProduct_ShoppingCart() throws Exception {
		return driver.findElement(By.cssSelector("div.shopping-cart-lines div.btn-wrapper a:nth-child(2) span"));
	}
	
	public List<WebElement> SaveForLaterLink_ShoppingCart() throws Exception {
		return driver.findElements(By.cssSelector("div.shopping-cart-lines div.btn-wrapper a:nth-child(2) span"));
	}
	
	public WebElement ContinueShopping_cartdrawer() throws Exception {
		return driver.findElement(By.cssSelector("div.drawer-btn-group a.close-drawer"));
	}

	public WebElement delete_cartdrawer() throws Exception {
		return driver.findElement(By.cssSelector(".prod-del"));
	}

	public WebElement saveforlater_cartdrawer() throws Exception {
		return driver.findElement(By.cssSelector(".prod-save"));
	}
	
	public WebElement Proceed_To_Checkout_Btn() throws Exception {
		return driver.findElement(By.cssSelector("a[title*='proceed to checkout']"));
	}
	
	public WebElement ContinueShopping_Btn() throws Exception {
		return driver.findElement(By.cssSelector("a[title*='continue shopping']"));
	}
	
	public WebElement cartcount_header() throws Exception {
		return driver.findElement(By.cssSelector("p.cart-items-count"));
	}
	
	public List<WebElement> CartCount_Header() throws Exception {
		return driver.findElements(By.cssSelector("p.cart-items-count"));
	}
	
	public List<WebElement> emptycart_header() throws Exception {
		return driver.findElements(By.cssSelector("div[data-bind='if: cartCount'] p"));
	}
	
	public WebElement cartIcon_header() throws Exception {
		return driver.findElement(By.cssSelector("a.top-cart"));
	}
	

	public List<WebElement> firstproductImage_shoppingcart() throws Exception {
		return driver.findElements(By.cssSelector("div.shopping-cart-lines .cart-item-wrapper div.cart-item-cont a img"));
	}
	
	public List<WebElement> saveforlaterfirstproductImage() throws Exception {
		return driver.findElements(By.cssSelector("div.saved-shopping-cart-wrapper .cart-item-wrapper div.cart-item-cont a img"));
	}
	
	public List<WebElement> ProductTitle_ShoppingCart() throws Exception {
		return driver.findElements(By.cssSelector("div.shopping-cart-lines .cart-item-info .cart-item-title"));
	}
	
//	public List<WebElement> ProductTitle_SaveforLater() throws Exception {
//		return driver.findElements(By.cssSelector("div.saved-shopping-cart-wrapper .cart-item-info .cart-item-title"));
//	}
	
	public List<WebElement> firstproductTitle_shoppingcart() throws Exception {
		return driver.findElements(By.cssSelector("div.shopping-cart-lines .cart-item-wrapper div.cart-item-cont .cart-item-info p.cart-item-title"));
	}
	
	public List<WebElement> saveforlaterfirstproductTitle() throws Exception {
		return driver.findElements(By.cssSelector("div.saved-shopping-cart-wrapper .cart-item-wrapper div.cart-item-cont .cart-item-info p.cart-item-title"));
	}
	
	public List<WebElement> firstproductSKU_shoppingcart() throws Exception {
		return driver.findElements(By.cssSelector("div.shopping-cart-lines .cart-item-wrapper div.cart-item-cont .cart-item-info p.cart-item-number-label"));
	}
	
	public List<WebElement> saveforlaterfirstproductSKU() throws Exception {
		return driver.findElements(By.cssSelector("div.saved-shopping-cart-wrapper .cart-item-wrapper div.cart-item-cont .cart-item-info p.cart-item-number-label"));
	}
	
	public List<WebElement> firstproductSize_shoppingcart() throws Exception {
		return driver.findElements(By.cssSelector("div.shopping-cart-lines .cart-item-wrapper div.cart-item-cont .cart-item-info p.cart-item-size-label"));
	}
	
	public List<WebElement> saveforlaterfirstproductSize() throws Exception {
		return driver.findElements(By.cssSelector("div.saved-shopping-cart-wrapper .cart-item-wrapper div.cart-item-cont .cart-item-info p.cart-item-size-label"));
	}
	
	public List<WebElement> firstproductFlavor_shoppingcart() throws Exception {
		return driver.findElements(By.cssSelector("div.shopping-cart-lines .cart-item-wrapper div.cart-item-cont .cart-item-info p.cart-item-flavour-label"));
	}
	
	public List<WebElement> saveforlaterfirstproductFlavor() throws Exception {
		return driver.findElements(By.cssSelector("div.saved-shopping-cart-wrapper .cart-item-wrapper div.cart-item-cont .cart-item-info p.cart-item-flavour-label"));
	}
	
	public List<WebElement> firstproductQty_shoppingcart() throws Exception {
		return driver.findElements(By.cssSelector("div.shopping-cart-lines .cart-item-wrapper div.cart-item-cont .cart-selct-box .quantity-input-text"));
	}
	
	public List<WebElement> saveforlaterfirstproductQty() throws Exception {
		return driver.findElements(By.cssSelector("div.saved-shopping-cart-wrapper .cart-item-wrapper div.cart-item-cont .cart-selct-box .quantity-input-text"));
	}
	
	public List<WebElement> firstproductUnits_shoppingcart() throws Exception {
		return driver.findElements(By.cssSelector("div.shopping-cart-lines .cart-item-wrapper div.cart-item-cont .cart-selct-box .dropdown .unit-text"));
	}
	
	public List<WebElement> saveforlaterfirstproductUnits() throws Exception {
		return driver.findElements(By.cssSelector("div.saved-shopping-cart-wrapper .cart-item-wrapper div.cart-item-cont .cart-selct-box .unit-text"));
	}
	
	public List<WebElement> firstproduct_actualpriceOfProduct() throws Exception {
		return driver.findElements(By.cssSelector("div.shopping-cart-lines div.price-wrapper p span"));
	}
	
	public List<WebElement> saveforlaterfirstproduct_actualpriceOfProduct() throws Exception {
		return driver.findElements(By.cssSelector("div.saved-shopping-cart-wrapper div.price-wrapper p span"));
	}
	
	public List<WebElement> firstproductTotalSum_shoppingcart() throws Exception {
		return driver.findElements(By.cssSelector("div.shopping-cart-lines .cart-item-wrapper div.cart-item-cont .cart-selct-box .cart-qty-unit-sum"));
	}
	
	public List<WebElement> saveforlaterfirstproductTotalSum_shoppingcart() throws Exception {
		return driver.findElements(By.cssSelector("div.saved-shopping-cart-wrapper .cart-item-wrapper div.cart-item-cont .cart-selct-box .cart-qty-unit-sum"));
	}
	
	public WebElement firstproductTotalSubTotal_shoppingcart() throws Exception {
		return driver.findElement(By.cssSelector("div.cart-sub-total-item-wrap p label.cart-sub-total-item-price"));
	}
	
//	public WebElement firstproductPrice_shoppingcart() throws Exception {
//		return driver.findElement(By.cssSelector("div.shopping-cart-lines .cart-item-wrapper div.cart-item-cont .price-wrapper"));
//	}
	
	public List<WebElement> shoppingcartlist_cartpage() throws Exception {
		return driver.findElements(By.cssSelector("div.shopping-cart-lines .cart-item-wrapper div.cart-item-cont"));
	}
	
	public List<WebElement> saveforlater_List_cartpage() throws Exception {
		return driver.findElements(By.cssSelector("div.saved-shopping-cart-wrapper .cart-item-wrapper div.cart-item-cont"));
	}
	
	public WebElement itemsCount_CartPage() throws Exception {
		return driver.findElement(By.cssSelector("div.shopping-cart-lines p.sub-total-items span"));
	}
	
	public WebElement Subtotal_itemsCount_CartPage() throws Exception {
		return driver.findElement(By.cssSelector("div.shopping-cart-lines p.cart-sub-total span"));
	}
	
	public WebElement Subtotal_PriceDetails_CartPage() throws Exception {
		return driver.findElement(By.cssSelector("div.cart-sub-total-item-wrap p.cart-sub-total label"));
	}

	public List<WebElement> actualpriceOfProduct_List() throws Exception {
		return driver.findElements(By.cssSelector("div.price-wrapper p span"));
	}
	
	public WebElement emptycart_message_shoppingcart() throws Exception {
		return driver.findElement(By.cssSelector("div.shopping-cart-lines div .cart-empty-text"));
	}
	
	public WebElement emptycart_message_saveforlater() throws Exception {
		return driver.findElement(By.cssSelector("div.saved-shopping-cart-wrapper div .cart-empty-text"));
	}
	
	public WebElement Qty_DropdownIcon_saveforlater() throws Exception {
		return driver.findElement(By.cssSelector("div#saveforlater-cart-lines div.quantity em.quantity-icon"));
	}
	
	public WebElement Units_DropdownIcon_saveforlater() throws Exception {
		return driver.findElement(By.cssSelector("div#saveforlater-cart-lines div.unit em.quantity-icon"));
	}
	
	public List<WebElement> QtyValuesList_safeforlater() throws Exception {
		return driver.findElements(By.cssSelector("div#saveforlater-cart-lines div.quantity ul li"));
	}
	
	public List<WebElement> UnitsValuesList_safeforlater() throws Exception {
		return driver.findElements(By.cssSelector("div#saveforlater-cart-lines div.unit ul li"));
	}
	
	public WebElement NeedHelpBtn_CartPage() throws Exception {
		return driver.findElement(By.xpath("//div[@class='component-content']/p/a[contains(text(),'Help')]"));
	}
	
	public WebElement ReturnPolicyBtn_CartPage() throws Exception {
		return driver.findElement(By.xpath("//div[@class='component-content']/p/a[contains(text(),'Return')]"));
	}
	
	public List<WebElement> deletelink_Cartdrawer() throws Exception {
		return driver.findElements(By.cssSelector("span.prod-del"));
	}
	
	public List<WebElement> saveforlater_Cartdrawer() throws Exception {
		return driver.findElements(By.cssSelector("span.prod-save"));
	}
	
	public List<WebElement> productslistTitle_cartdrawer() throws Exception {
		return driver.findElements(By.cssSelector("div.drawer-sub-details .prod-title a"));
	}
	
}
