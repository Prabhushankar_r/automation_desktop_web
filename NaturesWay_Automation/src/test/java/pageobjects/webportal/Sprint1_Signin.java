package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint1_Signin {
	
	public WebDriver driver;

	public Sprint1_Signin(WebDriver driver) {
		this.driver = driver;
		
	}
	
	public String getPageTitle() {
		return driver.getTitle();
	}
	
	public WebElement signInInHomePage() throws Exception {
		return driver.findElement(By.linkText("Sign-In"));
	}
	
	public WebElement signInModalWindow() throws Exception {
		return driver.findElement(By.id("login-modal-content"));
	}

	
	public WebElement signInLogo() throws Exception {
		//return driver.findElement(By.className("sign-in-logo"));
		return driver.findElement(By.xpath("//img[@class='img-responsive modal-logo']"));
		
}

	public WebElement signInBrandImage() throws Exception {
		return driver.findElement(By.cssSelector(".sign-in-logo img"));
	}
	
	public WebElement signInTitle() throws Exception {
		return driver.findElement(By.cssSelector(".sign-in-title"));
	}
	
	public WebElement usernameField() throws Exception {
		return driver.findElement(By.id("UserName"));
	}
	
	public WebElement passwordField() throws Exception {
		return driver.findElement(By.id("Password"));
	}
	
	public WebElement confirmPasswordField() throws Exception {
		return driver.findElement(By.id("ConfirmPassword"));
	}
	
	public WebElement usernameLabel() throws Exception {
		return driver.findElement(By.cssSelector(".login-username label"));
	}
	
	public WebElement passwordLabel() throws Exception {
		return driver.findElement(By.cssSelector(".login-password label"));
	}
	
	public WebElement submitSignIn() throws Exception {
		return driver.findElement(By.id("SignInButton"));
	}
	
	public WebElement signInforgotPassword() throws Exception {
		return driver.findElement(By.linkText("Forgot password?"));
	}
	
	public WebElement hideIconInPasswordField() throws Exception {
		return driver.findElement(By.cssSelector(".icon-password"));
}
	
	public WebElement showIconInPasswordField() throws Exception {
		return driver.findElement(By.cssSelector(".icon-hide-password"));
	}
	
	public List<WebElement> hideIconInPasswordFieldList() throws Exception {
		return driver.findElements(By.cssSelector(".icon-password"));
	}
	
	public List<WebElement> showIconInPasswordFieldList() throws Exception {
		return driver.findElements(By.cssSelector(".icon-hide-password"));
	}	
	
	public WebElement signInErrorMessage() throws Exception {
		return driver.findElement(By.cssSelector("#validation-error"));
	}
	
	public WebElement signInWindowCloseIcon() throws Exception {
		return driver.findElement(By.cssSelector(".sign-in-close .icon-small"));
}
	
	public WebElement welcomeUserText() throws Exception {
		return driver.findElement(By.cssSelector(".username-text"));
	}
	
	public WebElement signOut() throws Exception {
		return driver.findElement(By.cssSelector(".sign-out-top"));
	}
	
	public WebElement signInPageCreateAccount() throws Exception {
		return driver.findElement(By.cssSelector(".sign-in-register a"));
	}
	
	public WebElement registerUser() throws Exception {
		return driver.findElement(By.id("sign-up-button"));
}
	
	public WebElement createAccountPageEle()
	{
		return driver.findElement(By.xpath("//h1[text()= 'Create an Account ']"));
	}
	
	public WebElement continueAsGuest() throws Exception {
		return driver.findElement(By.linkText("Continue as guest"));
	}
	
	public WebElement goToIntegrative() throws Exception {
		return driver.findElement(By.linkText("Go to IntegrativePro.com"));
	}
	
}
