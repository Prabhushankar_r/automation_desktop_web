package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import net.sf.ehcache.util.FindBugsSuppressWarnings;

public class Sprint4_CheckoutPage {
	public WebDriver driver;

	public Sprint4_CheckoutPage(WebDriver driver) 
	{
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	
	@FindBy(css = ".btn-sub-total")
	private WebElement cartpageCheckOut;
	
	@FindBy(xpath = "(//a[text()= 'Add New Payment Method'])[1]")
	private WebElement addNewPaymentLink;
	
	@FindBy(xpath = "//input[@id='c-cardnumber']")
	private WebElement cardNumber_Payment;
	
	@FindBy(css = "#c-cardname")
	private WebElement cardName_Payment;
	
	@FindBy(css = "#c-exmth")
	private WebElement cardExpMonth_Payment;
	
	@FindBy(css = "#c-exyr")
	private WebElement cardExpYear_Payment;
	
	@FindBy(css = ".cvv-field-component")
	private WebElement cardCVV_Payment;
	
	@FindBy(css = "#savepayment")
	private WebElement savePaymentCheckbox_Payment;
	
	@FindBy(xpath = "(//input[@id='billing-add'])[1]")
	private WebElement billingAddSameAsShippingAddCheckbox_Payment;
	
	@FindBy(xpath = "//label[@for='billing-add']")
	private WebElement billingAddSameAsShippingAddressText;

	@FindBy(xpath = "//button[text()='Save & Continue']")
	private WebElement saveAndContinue_Button;
	
	@FindBy(xpath = "//button[text()='Place Order']")
	private WebElement placeOrder_Button;
	
	@FindBy(xpath = "//span[@data-bind='text: $root.cartCount']")
	private WebElement itemCount_Checkout;
	
	@FindBy(css = ".sub-total-price")
	private WebElement totalAmount_Checkout;
	
	@FindBy(css = ".shipping-total-price")
	private WebElement shippingAmt_Checkout;
	
	@FindBy(css = ".discount-price")
	private WebElement savingsOrDiscountAmt_Checkout;
	
	@FindBy(css = ".total-price")
	private WebElement subTotal_Checkout;
	
	@FindBy(css = ".tax-price")
	private WebElement tax_Checkout;
	
	@FindBy(css = ".item-price")
	private WebElement orderTotal_Checkout;
	
	@FindBy(css = ".cart-cont-shop")
	private WebElement continueShoppingLink;

	@FindBy(css = "#companyname")
	private WebElement companyName_billingAddress;
	
	@FindBy(css = "#firstname")
	private WebElement firstName_billingAddress;
	
	@FindBy(css = "#lastname")
	private WebElement lastName_billingAddress;
	
	@FindBy(xpath = "//input[@id='address']")
	private WebElement address1_billingAddress;

	@FindBy(css = "#Zipcode")
	private WebElement zipcode_billingAddresss;
	
	@FindBy(css = "#City")
	private WebElement city_billingAddress;

	@FindBy(css = "#State")
	private WebElement stateDropdown_billingAddress;
	
	@FindBy(xpath = "(//a[text() = 'Cancel'])[2]")
	private WebElement cancelLink_billingAddres;
	
	@FindBy(css = ".billing-address-container div[data-bind='validationElement: addCity'] label.input-out-focus")
	private WebElement cityAutoPopulated_BillingAddress;
	
	@FindBy(css = ".billing-component div[data-bind='validationElement: state'] label.select-out-focus")
	private WebElement stateAutoPopulated_BillingAddress;
	
	@FindBy(css = ".cart-item-cont")
	private List<WebElement> lineItem_List;
	
	@FindBy(xpath = "//span[contains(text(),'Save for later')]")
	private List<WebElement> saveForLater_List;
	
	@FindBy(xpath = "//span[contains(text(),'Delete')]")
	private List<WebElement> deleteLink_List;
	
	@FindBy(css = ".cart-item-title")
	private List<WebElement> productName_List;
	
	@FindBy(css = ".cart-price-txt")
	private List<WebElement> productPrice_List;
	
	@FindBy(css = ".cart-qty-unit-price")
	private List<WebElement> productUnitPrice_List;

	@FindBy(xpath = "//div[@class='quantity']//button/em")
	private List<WebElement> qtyArrow_List;
	
	@FindBy(css = ".unit em")
	private List<WebElement> unitArrow_List;

	@FindBy(xpath = "//div[@class='dropdown open']//a")
	private List<WebElement> QtyAndUnitDropdownValue_list;
	

	public WebElement cartpageCheckOut() {
		return cartpageCheckOut;
	}

	public WebElement addNewPaymentLink() {
		return addNewPaymentLink;
	}

	public WebElement cardNumber_Payment() {
		return cardNumber_Payment;
	}

	public WebElement cardName_Payment() {
		return cardName_Payment;
	}

	public WebElement cardExpMonth_Payment() {
		return cardExpMonth_Payment;
	}

	public WebElement cardExpYear_Payment() {
		return cardExpYear_Payment;
	}

	public WebElement cardCVV_Payment() {
		return cardCVV_Payment;
	}

	public WebElement savePaymentCheckbox_Payment() {
		return savePaymentCheckbox_Payment;
	}

	public WebElement billingAddSameAsShippingAddCheckbox_Payment() {
		return billingAddSameAsShippingAddCheckbox_Payment;
	}
	
	public WebElement billingAddSameAsShippingAddressText() {
		return billingAddSameAsShippingAddressText;
	}

	public WebElement saveAndContinue_Button() {
		return saveAndContinue_Button;
	}

	public WebElement placeOrder_Button() {
		return placeOrder_Button;
	}

	public WebElement itemCount_Checkout() {
		return itemCount_Checkout;
	}

	public WebElement totalAmount_Checkout() {
		return totalAmount_Checkout;
	}

	public WebElement shippingAmt_Checkout() {
		return shippingAmt_Checkout;
	}

	public WebElement savingsOrDiscountAmt_Checkout() {
		return savingsOrDiscountAmt_Checkout;
	}

	public WebElement subTotal_Checkout() {
		return subTotal_Checkout;
	}

	public WebElement tax_Checkout() {
		return tax_Checkout;
	}

	public WebElement orderTotal_Checkout() {
		return orderTotal_Checkout;
	}

	public WebElement continueShoppingLink() {
		return continueShoppingLink;
	}
	
	public WebElement companyName_billingAddress() {
		return companyName_billingAddress;
	}

	public WebElement firstName_billingAddress() {
		return firstName_billingAddress;
	}

	public WebElement lastName_billingAddress() {
		return lastName_billingAddress;
	}

	public WebElement zipcode_billingAddresss() {
		return zipcode_billingAddresss;
	}

	public WebElement stateDropdown_billingAddress() {
		return stateDropdown_billingAddress;
	}

	public WebElement cancelLink_billingAddres() {
		return cancelLink_billingAddres;
	}
	
	public WebElement city_billingAddress() {
		return city_billingAddress;
	}
	
	public WebElement address1_billingAddress() {
		return address1_billingAddress;
	}
	
	public WebElement cityAutoPopulated_BillingAddress() {
		return cityAutoPopulated_BillingAddress;
	}

	public WebElement stateAutoPopulated_BillingAddress() {
		return stateAutoPopulated_BillingAddress;
	}
	
	public List<WebElement> lineItem_List() {
		return lineItem_List;
	}

	public List<WebElement> saveForLater_List() {
		return saveForLater_List;
	}
	
	public List<WebElement> deleteLink_List() {
		return deleteLink_List;
	}

	public List<WebElement> productName_List() {
		return productName_List;
	}

	public List<WebElement> productPrice_List() {
		return productPrice_List;
	}
	
	public List<WebElement> productUnitPrice_List() {
		return productUnitPrice_List;
	}

	public List<WebElement> qtyArrow_List() {
		return qtyArrow_List;
	}
	
	public List<WebElement> unitArrow_List() {
		return unitArrow_List;
	}
	
	public List<WebElement> qtyAndUnitDropdownValue_list() {
		return QtyAndUnitDropdownValue_list;
	}

	
}
