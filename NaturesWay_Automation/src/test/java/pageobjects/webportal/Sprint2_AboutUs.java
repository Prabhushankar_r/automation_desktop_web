package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import net.sf.ehcache.util.FindBugsSuppressWarnings;

public class Sprint2_AboutUs {
	public WebDriver driver;

	public Sprint2_AboutUs(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	@FindBy(xpath="//div[@class='module-wrapper img-right-one']/div/div/h2")
	private WebElement Title_rightimages;
	
	@FindBy(css = "div.img-left-one .content .module-title")
	private WebElement Title_leftimages;
	
	@FindBy(css = ".banner-header")
	private WebElement AuHeader;
	
	public WebElement Title_leftimages() {
		return Title_leftimages;
	}

	public WebElement Title_rightimages() {
		return Title_rightimages;
	}
	
	public WebElement AuHeader() {
		return AuHeader;
	}

	public WebElement clickOnAboutUs() throws Exception {
		return driver.findElement(By.cssSelector(".mega-dropdown >a#dropdown-about-us"));
	}

	public WebElement WhoWeAreMenu_Header() throws Exception {
		return driver.findElement(By.cssSelector(".dropdown:nth-child(3) a[title*='Who']"));
	}

	@FindBy(css = ".banner-title")
	private WebElement Aboutus_heroBannertitle;

	public WebElement Aboutus_heroBannertitle()  {
		return Aboutus_heroBannertitle;
	}

	@FindBy(css = ".banner-top-description")
	private WebElement Aboutus_heroBannerDescription;
	
	public WebElement Aboutus_heroBannerDescription()  {
		return Aboutus_heroBannerDescription;
	}

	public WebElement AuIMTitle() throws Exception {
		return driver.findElement(By.cssSelector(".col-sm-6:nth-child(1) .module-title"));
	}
	
	@FindBy(css = "div.img-left-one .content .module-description")
	private WebElement Description_leftimages;
	
	public WebElement Description_leftimages()  {
		return Description_leftimages;
	}
	
	@FindBy (css = "div.img-left-one .content .btn-primary")
	private List<WebElement> ReadMoreBtn_leftimages;
	
	public List<WebElement> ReadMoreBtn_leftimages() {
		return ReadMoreBtn_leftimages;
	}
	
	@FindBy(css = "div.img-right-one .content .module-description")
	private WebElement Description_rightimages;
	
	public WebElement Description_rightimages() {
		return Description_rightimages;
	}
	
	@FindBy(css = "div.img-right-one .content .btn-primary")
	private List<WebElement> ReadMoreBtn_rightimages;
	
	public List<WebElement> ReadMoreBtn_rightimages() {
		return ReadMoreBtn_rightimages;
	}
	
	public WebElement AuIMDesc() throws Exception {
		return driver.findElement(By.cssSelector(".col-sm-6:nth-child(1) .module-description"));
	}

	public WebElement clickOnIMRMButton() throws Exception {
		return driver.findElement(By.cssSelector(".module-content:nth-child(1) .btn"));
	}

	public WebElement AuOHTitle() throws Exception {
		return driver.findElement(By.cssSelector(".container > .module-wrapper .module-title"));
	}

	public WebElement AuOHImages() throws Exception {
		return driver.findElement(By.cssSelector(".center-block"));
	}

	public WebElement AuOHDesc() throws Exception {
		return driver.findElement(By.cssSelector(".container .module-content:nth-child(1) .module-description"));
	}
	
	public WebElement AuOHDesc2() throws Exception {
		return driver.findElement(By.cssSelector(".module-description:nth-child(1)"));
	}

	public WebElement AuOPTitle() throws Exception {
		return driver.findElement(By.cssSelector(".module-content:nth-child(2) .module-title"));
	}

	public WebElement AuOPDesc() throws Exception {
		return driver.findElement(By.cssSelector(".module-content:nth-child(2) .module-description"));
	}

	public WebElement clickOnOPRMButton() throws Exception {
		return driver.findElement(By.cssSelector(".module-content:nth-child(2) .btn"));
	}

	public WebElement AuFBTitle() throws Exception {
		return driver.findElement(By.cssSelector(".container > .module-title"));
	}

	public WebElement AuFBDesc() throws Exception {
		return driver.findElement(By.cssSelector(".banner-description"));
	}
}
