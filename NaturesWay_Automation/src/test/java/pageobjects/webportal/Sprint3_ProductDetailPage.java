package pageobjects.webportal;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint3_ProductDetailPage {

	public WebDriver driver;

	public Sprint3_ProductDetailPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement favSucessMessage() throws Exception {
		return driver.findElement(By.cssSelector("strong"));
	}

	public WebElement sigInlinkPDP() throws Exception {
		return driver.findElement(By.cssSelector(".pdp-signin-text"));
	}

	public WebElement AddToCartButton() throws Exception {
		return driver.findElement(By.cssSelector("div.prod-det-add-cart > .btn"));
	}

	public WebElement CartDrawer() throws Exception {
		return driver.findElement(By.cssSelector("div.drawer-list"));
	}
	
	public WebElement Subtotal_text() throws Exception {
		return driver.findElement(By.xpath("(//*[contains(text(), 'Subtotal (')]"));
	}
	
	public WebElement No_Of_Items() throws Exception {
		return driver.findElement(By.xpath("(//p[@class='sub-total-item']/span)[1]"));
	}
	
	public WebElement TotalPrice_CartDrawer() throws Exception {
		return driver.findElement(By.xpath("(//p[@class='sub-total-item']/span)[2]"));
	}
	
	public WebElement CartDrawerScreen() throws Exception {
		return driver.findElement(By.cssSelector("div.drawer-list"));
	}
	
	

}
