package pageobjects.webportal;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint1_ForgotPassword {

	public WebDriver driver;

	public Sprint1_ForgotPassword(WebDriver driver) {
		this.driver = driver;
	}

	public String getPageTitle() {
		return driver.getTitle();
	}
	
	public WebElement forgotPasswordTitle() throws Exception {
		return driver.findElement(By.cssSelector(".forgot-password-content > .module-title"));
	}

	public WebElement signIn() throws Exception {
		return driver.findElement(By.cssSelector(".sign-in-top:nth-child(1)"));
		// .actionText
	}

	public WebElement ForgotPasswordScreen_Title() throws Exception {
		return driver.findElement(By.cssSelector(".forgot-password-content .module-title"));
		// .actionText
	}
	
	public WebElement forgotpassword_link() throws Exception {
		return driver.findElement(By.xpath("//a[text()='Forgot password?']"));
	}

	public WebElement email() throws Exception {
		return driver.findElement(By.xpath("//input[@id='EmailAddress']"));
	}

	public WebElement submit() throws Exception {
		return driver.findElement(By.xpath("//div[@class='email-entry-buttons']//button[@type='submit']"));
	}

	public WebElement enterRongMail() throws Exception {
		return driver.findElement(By.cssSelector("#EmailAddress"));
	}

	public WebElement verifyErrorMessage() throws Exception {
		return driver.findElement(By.cssSelector(".field-validation-error > span"));
	}

	public WebElement TickIcon_ForgotPasswordSuccessscreen() throws Exception {
		return driver.findElement(By.cssSelector("a[title*='Forgot Password Success'] img"));
	}
	
	public WebElement Message_ForgotPasswordSuccessscreen() throws Exception {
		return driver.findElement(By.cssSelector(".forgot-password-success p.module-description"));
	}
	
	public WebElement Closelink_ForgotPasswordSuccessscreen() throws Exception {
		return driver.findElement(By.cssSelector(".forgot-password-success div.field-link a"));
	}
	
	
	public WebElement forgotCancel() throws Exception {
		return driver.findElement(By.cssSelector("#forgot-password-cancel"));
	}

	public WebElement forgotPassTitle() throws Exception {
		return driver.findElement(By.cssSelector(".forgot-password-content > .module-title"));
	}

	public WebElement forgotPassDes() throws Exception {
		return driver.findElement(By.cssSelector(".module-description:nth-child(2)"));
	}

}
