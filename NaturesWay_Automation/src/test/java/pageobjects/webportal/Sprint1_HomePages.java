package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Sprint1_HomePages {

	public WebDriver driver;

	public Sprint1_HomePages(WebDriver driver) 
	{
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	public String getPageTitle() {
		return driver.getTitle();
	}

	public WebElement title() throws Exception {
		return driver.findElement(By.cssSelector(".col-sm-7:nth-child(1) .module-header"));
	}

	public WebElement description() throws Exception {
		return driver.findElement(By.cssSelector(".module-description:nth-child(4)"));
	}

	public WebElement video() throws Exception {
		return driver.findElement(By.cssSelector(".video-init"));
	}
	
	public WebElement Image_OurStorysection() throws Exception {
		return driver.findElement(By.xpath("//div[contains(@class,'module-image col-sm-5')]/picture/img"));
	}

	public WebElement healthIcon() throws Exception {
		return driver.findElement(By.cssSelector(".field-header-icon > img"));
	}
	
	public List<WebElement> Title_FeaturedProduct() throws Exception {
		return driver.findElements(By.cssSelector(".prod-item-name"));
	}

	public List<WebElement> imageCarouselBanner() throws Exception {
		return driver.findElements(By.cssSelector("ul.slides li"));
	}
	
	public List<WebElement> titleCarouselBanner() throws Exception {
		return driver.findElements(By.cssSelector("div.carousel-content .slide-title"));
	}

	public List<WebElement> descriptionCarouselBanner() throws Exception {
		return driver.findElements(By.cssSelector("div.carousel-content p.slide-description"));
	}
	
	@FindBy(css = "div.carousel-content a.slide-link")
	private List<WebElement> ReadMoreButtonCarouselBanner;
	
	public List<WebElement> ReadMoreButtonCarouselBanner() throws Exception {
		return ReadMoreButtonCarouselBanner;
	}

	@FindBy(css = "div.nav-items div")
	private List<WebElement> carousellisticons;
	
	public List<WebElement> carousellisticons()
	{
		return carousellisticons;
	}
	
	
	public WebElement scrollhorizontalbar() throws Exception {
		return driver.findElement(By.cssSelector("div.featured-prdcts div.simplebar-horizontal div.simplebar-scrollbar"));
	}
	
	
	
	public List<WebElement> imageCarouselBannerTitle() throws Exception {
		return driver.findElements(By.cssSelector(".text-left h2"));
	}

	public List<WebElement> imageCarouselNavigationButton() throws Exception {
		return driver.findElements(By.cssSelector("div.nav-items div"));
	}

	public List<WebElement> imageCarouselReadMoreButton() throws Exception {
		return driver.findElements(By.cssSelector(".text-left a"));
	}

	public List<WebElement> featureProductimages() throws Exception {
		return driver.findElements(By.cssSelector(".featured-prdcts-image-section img.featured-prdcts-front-image"));
	}

	public List<WebElement> featureProductBackGroundImage() throws Exception {
		return driver.findElements(By.cssSelector(".featured-prdct-article .featured-prdcts-bg-image"));
	}

	public List<WebElement> featureProductDesc() throws Exception {
		return driver.findElements(By.cssSelector("p.prod-item-description"));
	}

	public List<WebElement> viewproductdetails_Button() throws Exception {
		return driver.findElements(By.cssSelector(".featured-prdct-article .btn"));
	}

	public WebElement checkIterations() throws Exception {
		return driver.findElement(By.cssSelector(".content > .btn:nth-child(4)"));
	}

	
	
	public WebElement clickOurStoryReadMore() throws Exception {
		return driver.findElement(By.cssSelector(".btn:nth-child(5)"));
	}

	public WebElement featureProductIcon() throws Exception {
		return driver.findElement(By.cssSelector(".icon-feature"));
	}

	public WebElement featureProductTitle() throws Exception {
		return driver.findElement(By.cssSelector(".prod-head-title"));
	}

	public WebElement hct() throws Exception {
		return driver.findElement(By.cssSelector(".module-content .module-title"));
	}

	public WebElement hcd() throws Exception {
		return driver.findElement(By.cssSelector(".content > .module-description:nth-child(3)"));
	}
	
	public WebElement fphDesc() throws Exception {
		return driver.findElement(By.cssSelector(".prod-head-description"));
	}
}
