package pageobjects.webportal;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint3_FindAPractitioner {
	public WebDriver driver;

	public Sprint3_FindAPractitioner(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement findapraHeader() throws Exception {
		return driver.findElement(By.cssSelector(".banner-title"));
	}

	public WebElement findapraDesc() throws Exception {
		return driver.findElement(By.cssSelector(".banner-top-description"));
	}

	public WebElement findapraSearch() throws Exception {
		return driver.findElement(By.cssSelector("#PractitionerSearch"));
	}

	public WebElement findapraZipCode() throws Exception {
		return driver.findElement(By.cssSelector("#PractitionerZipcode"));
	}

	public WebElement findapraClickOnSearch() throws Exception {
		return driver.findElement(By.cssSelector(".col-md-2:nth-child(4)"));
	}

	public WebElement findapraPragraph() throws Exception {
		return driver.findElement(By.cssSelector(".find-a-practitioner"));
	}

}
