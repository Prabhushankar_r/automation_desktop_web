package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint1_MegaMenu {
	public WebDriver driver;

	public Sprint1_MegaMenu(WebDriver driver) {
		this.driver = driver;
	}

	public String getPageTitle() {
		return driver.getTitle();
	}

	public WebElement products() throws Exception {
		return driver.findElement(By.cssSelector(".dropdown:nth-child(1)"));
	}

	public List<WebElement> productsList() throws Exception {
		return driver.findElements(By.cssSelector("ul.mega-menu__column li"));
	}

	public WebElement resources() throws Exception {
		return driver.findElement(By.cssSelector(".dropdown:nth-child(2)"));
	}

	public List<WebElement> resourcesList() throws Exception {
		return driver.findElements(By.cssSelector(".dropdown:nth-child(2) span"));
	}

	public WebElement aboutus() throws Exception {
		return driver.findElement(By.cssSelector(".dropdown:nth-child(3)"));
	}

	public List<WebElement> aboutusList() throws Exception {
		return driver.findElements(By.cssSelector(".dropdown:nth-child(3) span"));
	}

	public WebElement Cart() throws Exception {
		return driver.findElement(By.cssSelector(".cartCTA"));
	}

	public WebElement un() throws Exception {
		return driver.findElement(By.cssSelector("#UserName"));
	}

	public WebElement pw() throws Exception {
		return driver.findElement(By.cssSelector("#Password"));
	}

	public WebElement cp() throws Exception {
		return driver.findElement(By.cssSelector("#ConfirmPassword"));
	}

	public WebElement username() throws Exception {
		return driver.findElement(By.cssSelector("input#UserName"));
	}

	public WebElement password() throws Exception {
		return driver.findElement(By.cssSelector("input#Password"));
	}

	public WebElement Sign_In_Button_ModalWindow() throws Exception {
		return driver.findElement(By.cssSelector("button#SignInButton"));
	}

	public WebElement signoutbutton_welcomedropdown() throws Exception {
		return driver.findElement(By.cssSelector("a.sign-out-top"));
	}

	public WebElement verifyQuickOrderPresent() throws Exception {
		return driver.findElement(By.cssSelector("li.mega-dropdown a#dropdown-quick-order"));
	}

	public WebElement createAnAccount() throws Exception {
		return driver.findElement(By.cssSelector(".user-login [title*='Create']"));
	}

	public WebElement clickOnCreateNewAccount() throws Exception {
		return driver.findElement(By.cssSelector("#registerButton"));
	}

	public WebElement clickOnPasswordEyeIcon() throws Exception {
		return driver.findElement(By.cssSelector(".icon-hide-password"));
		// i.fa-eye-slash
	}

	public WebElement DrugNutrientMenu_Header() throws Exception {
		return driver.findElement(By.cssSelector("a[title*='Drug-Nutrient']"));
		// i.fa-eye-slash
	}
	
	public WebElement PatientResorceMenu_Header() throws Exception {
		return driver.findElement(By.cssSelector("a[title*='Patient Resources']"));
		// i.fa-eye-slash
	}
	
	public WebElement ArticlesMenu_Header() throws Exception {
		return driver.findElement(By.cssSelector("a[title*='Articles']"));
		// i.fa-eye-slash
	}
	public List<WebElement> drugNutrientInteraction() throws Exception {
		return driver.findElements(By.cssSelector(".dropdown:nth-child(2) span"));
	}

	/*public List<WebElement> patientResource() throws Exception {
		return driver.findElements(By.cssSelector(".dropdown:nth-child(2) span"));
	}
  */
	public List<WebElement> patientResource() throws Exception {
		return driver.findElements(By.cssSelector(".dropdown-menu--item >a[title*='Patient']"));
	}
	
	
	public List<WebElement> conditions() throws Exception {
		return driver.findElements(By.cssSelector(".dropdown:nth-child(2) span"));
	}

	public List<WebElement> findAPractitioner() throws Exception {
		return driver.findElements(By.cssSelector(".dropdown:nth-child(2) span"));
	}

	public WebElement siginModelWindowClosed() throws Exception {
		return driver.findElement(By.cssSelector(".sign-in-close .img-responsive"));
	}

	public WebElement Closeicon_ModelWindow() throws Exception {
		return driver.findElement(By.cssSelector("a[title*='Close Icon']"));
	}
	
	public WebElement Closeicon_ForgotPasswordModelWindow() throws Exception {
		return driver.findElement(By.cssSelector("div.forgot-password-close a[title*='Close Icon']"));
	}
	
	public WebElement clickOnWelcomeUser() throws Exception {
		return driver.findElement(By.cssSelector("div.username-label a.username-text"));
	}
	
	public WebElement clickOnWelcomeUserIcon() throws Exception {
		return driver.findElement(By.cssSelector("div.username-label em.icon-arrow-down"));
	}
	
	public WebElement signinbtn_header() throws Exception {
		return driver.findElement(By.partialLinkText("Sign"));
	}
}
