package pageobjects.webportal;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint3_MiniCartPage {

	public WebDriver driver;

	public Sprint3_MiniCartPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement ClickOnHc() throws Exception {
		return driver.findElement(By.cssSelector(".top-cart"));
	}

}
