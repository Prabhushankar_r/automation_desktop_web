package pageobjects.webportal;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint1_PromotionBanner {
	
	public WebDriver driver;

	public Sprint1_PromotionBanner(WebDriver driver) {
		this.driver = driver;
	}
     public WebElement PromotionBanner_Text() throws Exception {
		return driver.findElement(By.cssSelector(".promo-subline"));
	}
     
     public WebElement clickOnPB() throws Exception {
 		return driver.findElement(By.cssSelector(".promo-subline"));
 	}

}
