package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint1_ContactUs {
	public WebDriver driver;

	public Sprint1_ContactUs(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement contactUsLink() throws Exception {
		return driver.findElement(By.cssSelector(".link-list .item0 > a "));
	}

	public WebElement integrativeProLogo() throws Exception {
		return driver.findElement(By.cssSelector("a[title='Integrative Pro Logo']"));
	}

	public WebElement contactUsHeroBanner() throws Exception {
		//return driver.findElement(By.cssSelector("img[alt='header-logo']"));
		return driver.findElement(By.id("page-banner-contact-us"));
	}

	public WebElement contactUsTitle() throws Exception {
		return driver.findElement(By.cssSelector(".banner-title"));
	}

	public WebElement contactUsDescription() throws Exception {
		//return driver.findElement(By.cssSelector(".banner-top-description"));
		return driver.findElement(By.xpath("//p[contains(@class, 'banner-top-description')]"));
	}

	public WebElement contactUsCallUsSymbol() throws Exception {
		//return driver.findElement(By.cssSelector("div.content-block:nth-child(1) img"));
		return driver.findElement(By.cssSelector(".icon-call"));
	}

	public WebElement contactUsCallUsTitle() throws Exception {
		return driver.findElement(By.cssSelector(".col-sm-10 .item-title"));
	}

	public WebElement contactUsCallUsDescription() throws Exception {
		return driver.findElement(By.cssSelector("div.content-block:nth-child(1) p"));
	}

	public WebElement contactUsCallUsFaxNo() throws Exception {
		//return driver.findElement(By.cssSelector("p.item-footer"));
		return driver.findElement(By.cssSelector(".customer-service-fax-no"));
		
	}

	public WebElement contactUsChatWithUsSymbol() throws Exception {
		//return driver.findElement(By.cssSelector("img[alt='Chat With Us']"));
		return driver.findElement(By.cssSelector(".icon-chat-circle"));
	}

	public WebElement contactUsChatWithUsTitle() throws Exception {
		//return driver.findElement(By.cssSelector(".content-block:nth-child(1) h2"));
		return driver.findElement(By.xpath("//h2[text() = 'Chat Hours']"));
	}

	public WebElement contactUsChatWithUsDescription() throws Exception {
		return driver.findElement(By.cssSelector(".col-lg-4:nth-child(2) .item-description"));
	}

	public WebElement contactUsChatWithUsButton() throws Exception {
		//return driver.findElement(By.cssSelector(".col-lg-4:nth-child(2) button"));
		return driver.findElement(By.xpath("//button[text() = 'Chat Now']"));
	}

	public WebElement contactUsEmailSymbol() throws Exception {
		//return driver.findElement(By.cssSelector(".col-lg-4:nth-child(3) img"));
		return driver.findElement(By.cssSelector(".icon-email"));
	}

	public WebElement contactUsEmailTitle() throws Exception {
		return driver.findElement(By.cssSelector(".col-lg-4:nth-child(3) .item-title"));
	}

	public WebElement contactUsEmailDescription() throws Exception {
		return driver.findElement(By.cssSelector(".col-lg-4:nth-child(3) .item-description"));
	}

	public WebElement contactUsEmailButton() throws Exception {
		return driver.findElement(By.cssSelector(".contact-email-us"));
	}

	public WebElement backToHomeButton() throws Exception {
		return driver.findElement(By.cssSelector(".home a"));
	}

	public WebElement contactUsMapLocation() throws Exception {
		return driver.findElement(By.cssSelector("#mapDiv .gm-style > div > div:nth-child(3)"));
	}

	public WebElement contactUsMapIntegrativeTherapeutics() throws Exception {
		return driver.findElement(By.cssSelector(".map-heading:nth-child(1)"));
	}
	
	public WebElement contactUsIntegrativeTherapeuticsTitle()
	{
		return driver.findElement(By.cssSelector(".map-header-title"));
	}

	public WebElement contactUsMapCustomerCareHours() throws Exception {
		return driver.findElement(By.cssSelector("div.component-content h3:nth-child(3)"));
	}

	public WebElement contactUsMapPhone() throws Exception {
		return driver.findElement(By.xpath("//h3[text() ='Phone']"));
	}

	public WebElement contactUsMapFax() throws Exception {
		return driver.findElement(By.cssSelector("div.component-content h3:nth-child(7)"));
	}

	public WebElement contactUsEmailFormTitle() throws Exception {
		return driver.findElement(By.cssSelector(".contactus-form > h2"));
	}

	public WebElement contactUsEmailFormDescription() throws Exception {
		return driver.findElement(By.cssSelector(".contactus-form > p"));
	}

	public WebElement contactUsEmailFormTextBox1() throws Exception {
		return driver.findElement(By.cssSelector("div.nameSection :nth-child(4)"));
	}

	public WebElement contactUsEmailFormTextBox2() throws Exception {
		return driver.findElement(By.cssSelector("div.email-section :nth-child(4)"));
	}

	public List<WebElement> contactUsEmailFormPractitionerDd1() throws Exception {
		return driver.findElements(By.cssSelector("select[data-val-required='Practitioner type is required.'] option"));
	}

	public List<WebElement> contactUsEmailFormInquiryDd2() throws Exception {
		return driver.findElements(By.cssSelector("select[data-val-required='Inquiry is required.'] option"));
	}

	public WebElement contactUsEmailFormMessage() throws Exception {
		return driver.findElement(By.cssSelector("textarea[data-val-required='Message is required.']"));
	}

	public WebElement contactUsEmailFormSubmitButton() throws Exception {
		return driver.findElement(By.cssSelector(".btn-submit"));
	}

	public WebElement contactUsSucessMessage() throws Exception {
		return driver.findElement(By.cssSelector("span.thank-you-message"));
	}

	public WebElement phoneNumber() throws Exception {
		return driver.findElement(By.cssSelector(".customer-service-no"));
	}

	public WebElement contactUsMessageTextBoxErrorMessage() throws Exception {
		return driver.findElement(By.cssSelector(".field-validation-error span"));
	}
	
	public WebElement contactUsEmailFieldError()
	{
		return driver.findElement(By.xpath("//div[contains(@class,'email-section input-field')]/span/span"));
	}
	
}