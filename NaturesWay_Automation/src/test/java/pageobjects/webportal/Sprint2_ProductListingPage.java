package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint2_ProductListingPage {
	public WebDriver driver;

	public Sprint2_ProductListingPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement enteredcharacter_text() throws Exception {
		return driver.findElement(By.cssSelector("div.result-heading .result-title"));
	}

	public WebElement productsmenu_header() throws Exception {
		return driver.findElement(By.cssSelector("li.mega-dropdown a#dropdown-products"));
	}

	public WebElement viewallproducts_header() throws Exception {
		return driver.findElement(By.cssSelector("ul.mega-menu__column li a[title*='View All Products']"));
	}

	public WebElement selectaproductbyTitle() throws Exception {
		return driver.findElement(By.cssSelector("a.product-title"));
	}
	
	//Need to use when we need a list
	public List<WebElement> productTitleList()
	{
		return driver.findElements(By.cssSelector("a.product-title"));
	}

	public WebElement selectaproductbyImage() throws Exception {
		return driver.findElement(By.cssSelector("figure img.product-img"));
	}
	
	public WebElement selectaproductbyViewDetails() throws Exception {
		return driver.findElement(By.cssSelector("a.view-more-btn"));
	}
	
	public List<WebElement> clickOnMMVAP() throws Exception {
		return driver.findElements(By.cssSelector("ul.mega-menu__column li"));
	}

	public List<WebElement> clickOnPI() throws Exception {
		return driver.findElements(By.cssSelector(".col-lg-3 .product-img"));
	}

	public void navigateBack() throws Exception {
		driver.navigate().back();
	}

	public List<WebElement> clickOnProdTitle() throws Exception {
		return driver.findElements(By.cssSelector(".col-lg-3 h4 > .a-link"));
	}

	public List<WebElement> clickOnFilterCheckBox() throws Exception {
		return driver.findElements(By.xpath("//div[@class='simplebar-mask']//input"));
	}

	public WebElement clickOnClearAllFilter() throws Exception {
		return driver.findElement(By.cssSelector("div.selected-categories li.clear-all-selection a"));
	}

	public WebElement NowFilterBySection() throws Exception {
		return driver.findElement(By.cssSelector(".plp-filter-title"));
	}
	
	public WebElement sortTheProd() throws Exception {
		return driver.findElement(By.cssSelector("#sortBy"));
	}
	
	public WebElement DropDown_SortingProducts() throws Exception {
		return driver.findElement(By.cssSelector(".dropdown >button"));
	}
	
	public List<WebElement> DropDown_Value_SortingProducts() throws Exception {
		return driver.findElements(By.cssSelector(".select-dd-list >li"));
	}
	
	public WebElement ProductSelected_Title() throws Exception {
		return driver.findElement(By.xpath("(//a[@class='a-link product-title'])[1]"));
	}
	

	public WebElement clickOnSMR() throws Exception {
		return driver.findElement(By.cssSelector(".showmore-btn > .btn"));
	}

	public WebElement clickOnBreadCrumb() throws Exception {
		return driver.findElement(By.cssSelector(".breadcrumb-item:nth-child(2) a"));
	}

	public List<WebElement> clickOnViewDetails() throws Exception {
		return driver.findElements(By.cssSelector(".col-lg-3 .btn"));
	}

	public List<WebElement> clickOnForm() throws Exception {
		return driver.findElements(By.cssSelector(".pull-right-sm h3"));
	}

	public List<WebElement> clickOnKeyIngredients() throws Exception {
		return driver.findElements(By.cssSelector(".pull-right-sm h3"));
	}

	public List<WebElement> clickOnProductType() throws Exception {
		return driver.findElements(By.cssSelector(".pull-right-sm h3"));
	}

	public List<WebElement> clickOnHealthFilter() throws Exception {
		return driver.findElements(By.cssSelector(".pull-right-sm h3"));
	}

	public List<WebElement> clickOnWomensFreeForm() throws Exception {
		return driver.findElements(By.cssSelector(".pull-right-sm h3"));
	}

	public List<WebElement> clickOnSortProducts() throws Exception {
		return driver.findElements(By.cssSelector(" .select-dd-list li"));
	}

	
	public WebElement Firstproduct_PDP() throws Exception {
		return driver.findElement(By.cssSelector("(//a[@class='a-link product-title'])[1]"));
	}

	

	/*public List<WebElement> clicksOnFormsFilter() throws Exception {
	return driver.findElements(By.xpath("//div[@id='0']//div[@class='simplebar-mask']//div[@class='check']"));
    }
*/
	public List<WebElement> clicksOnFormsFilter() throws Exception {
		return driver.findElements(By.xpath("//div[@id='0']//div[@class='simplebar-mask']//div[@class='checkbox-wrapper']/input"));
	    }
	
	
	public List<WebElement> clicksonDietaryNeedsFilter() throws Exception {
		return driver.findElements(By.xpath("//div[@id='1']//div[@class='simplebar-mask']//div[@class='checkbox-wrapper']/input"));
	}

	public List<WebElement> clicksOnProductTypeFilter() throws Exception {
		return driver.findElements(By.xpath("//div[@id='3']//div[@class='simplebar-mask']//div[@class='checkbox-wrapper']/input"));
	}

	public List<WebElement> clicksOnHealthFilter() throws Exception {
		return driver.findElements(By.xpath("//div[@id='4']//div[@class='simplebar-mask']//div[@class='checkbox-wrapper']/input"));
	}

	public List<WebElement> clicksOnFormulationFilter() throws Exception {
		return driver.findElements(By.xpath("//div[@id='2']//div[@class='simplebar-mask']//div[@class='checkbox-wrapper']/input"));
	}
}
