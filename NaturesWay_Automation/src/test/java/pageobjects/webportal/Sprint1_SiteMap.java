package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint1_SiteMap {

	public WebDriver driver;

	public Sprint1_SiteMap(WebDriver driver) {
		this.driver = driver;
	}

	public List<WebElement> products_ByType_list() {
		return driver.findElements(By.cssSelector("div.col-lg-3 ul.sitemap-content li"));
	}

	public List<WebElement> products_ByHealthInterest_list() {
		return driver.findElements(By.cssSelector("div.sitemap-by-health li"));
	}
	
	public List<WebElement> ResourceSection_list() {
		return driver.findElements(By.cssSelector("div.col-md-6:nth-child(1) ul li"));
	}
	
	public List<WebElement> AboutUs_list() {
		return driver.findElements(By.xpath("(//div[@class='col-md-6'])[2]/ul/li/a"));
	}
	
	public List<WebElement> GeneralLinks_list() {
		return driver.findElements(By.xpath("(//div[@class='col-md-12'])[2]/ul/li/a"));
	}
	
	
	
	public WebElement siteMapLink() {
		return driver.findElement(By.cssSelector("li.component-list a[href='/sitemap']"));
	}
	

	public WebElement siteMapByType() {
		return driver.findElement(By.cssSelector(".col-xs-12:nth-child(1) > .sitemap-prod-item-title"));
	}

	public List<WebElement> siteMapByHealthInterest() {
		return driver.findElements(By.cssSelector("div.sitemap-by-health ul li"));
	}

	public List<WebElement> siteMapProtocolKits() {
		return driver.findElements(By.cssSelector(".col-xs-12:nth-child(3) ul:nth-child(2) li"));
	}

	public List<WebElement> siteMapResources() {
		return driver.findElements(By.cssSelector(".sitemap-color:nth-child(2) ul:nth-child(2) li"));
	}

	public List<WebElement> siteMapAboutUs() {
		return driver.findElements(By.cssSelector(".sitemap-color:nth-child(3) ul:nth-child(2) li"));
	}

	public List<WebElement> siteMapGeneralLinks() {
		return driver.findElements(By.cssSelector(".sitemap-color:nth-child(4) ul:nth-child(2) li"));
	}

	public WebElement siteMapBacktohome() {
		return driver.findElement(By.cssSelector(".home a"));
	}

}
