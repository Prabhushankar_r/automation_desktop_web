package pageobjects.webportal;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Cadences_ConformMessage {
	public WebDriver driver;

	public Cadences_ConformMessage(WebDriver driver) {
		this.driver = driver;
	}

//	public WebElement confirmMessage() throws Exception {
//		return driver.findElement(By.cssSelector("div.submit a"));
//	}
	
	public WebElement confirmMessage() throws Exception {
		return driver.findElement(By.cssSelector("button.btn-min-width"));
	}
}
