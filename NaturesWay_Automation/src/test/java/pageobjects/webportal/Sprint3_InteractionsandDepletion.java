package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint3_InteractionsandDepletion {
	public WebDriver driver;

	public Sprint3_InteractionsandDepletion(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement Interaction_Depletion_Menu() throws Exception {
		return driver.findElement(By.cssSelector(".text-list a[title*='Interaction']"));
	}
	
	
	public List<WebElement> MenuTitle_Submenus() throws Exception {
		return driver.findElements(By.cssSelector(".text-title")) ;
	}

	public WebElement ClickInteraction_Avl() throws Exception {
		return driver.findElement(By.cssSelector(".accordion-title a"));
	}
	
	public WebElement LoadingSpinner() throws Exception {
		return driver.findElement(By.cssSelector(".hni_spinner"));
	}
	
	public List<WebElement> ExpandInteraction() throws Exception {
		return driver.findElements(By.cssSelector(".hni_interaction__header .hni_interaction__title-container"));
	}
	
	public List<WebElement> CollapseInteraction() throws Exception {
		return driver.findElements(By.cssSelector(".hni_interaction__header > button.hni_hidden"));
	}
	
//	public List<WebElement> ExpandedContent() throws Exception {
//		return driver.findElements(By.cssSelector("section.hni_interaction__content div"));
//	}
	
	public WebElement ExpandedContent() throws Exception {
		return driver.findElement(By.cssSelector("section.hni_visible div.hni_interaction__content--container"));
	}
}




