package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint2_Resources_PatientResourcePage {
	
	public WebDriver driver;

	public Sprint2_Resources_PatientResourcePage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement Resources_Header() throws Exception {
		return driver.findElement(By.cssSelector(".field-banner-header-text"));
	}
	
	public WebElement PatientResource_Title() throws Exception {
		return driver.findElement(By.cssSelector(".field-banner-title"));
	}
	
	public WebElement PatientResource_Description() throws Exception {
		return driver.findElement(By.cssSelector(".field-banner-description"));
	}
	
	public WebElement patientRTHead() throws Exception {
		return driver.findElement(By.cssSelector(".resource-type"));
	}

	public List<WebElement> patientresource_filters() throws Exception {
		return driver.findElements(By.cssSelector("div.patient-resource-header li a"));
	}
	
	public List<WebElement> patientRTResourcesAlpha() throws Exception {
		return driver.findElements(By.cssSelector(".col-md-3 .patient-section-header"));
	}

	public List<WebElement> patientResource_tabslist() throws Exception {
		return driver.findElements(By.cssSelector(".tabs-section li"));
	}
	public List<WebElement> patientRVerifyPRPdfs() throws Exception {
		return driver.findElements(By.cssSelector(".col-md-3 .img-responsive"));
	}

	public WebElement patientRPIR() throws Exception {
		return driver.findElement(By.cssSelector(".count-style"));
	}
	public List<WebElement> patientRTClickOnHc() throws Exception {
		return driver.findElements(By.cssSelector(".tabs-section li"));
	}

	public WebElement patientRHCR() throws Exception {
		return driver.findElement(By.cssSelector(".count-style"));
	}
	
	public List<WebElement> PDF_TitleValue() throws Exception {
		return driver.findElements(By.cssSelector("div.patient-info a"));
	}
	
	public List<WebElement> PDF_Image() throws Exception {
		return driver.findElements(By.cssSelector("div.patient-info a"));
	}
	
	public List<WebElement> patientresourcePDFsList() throws Exception {
		return driver.findElements(By.cssSelector(".col-md-3 .img-responsive"));
	}
	
	public WebElement count_details_PatientResouce() throws Exception {
		return driver.findElement(By.cssSelector(".count-style"));
	}


}
