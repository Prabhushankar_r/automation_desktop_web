package pageobjects.webportal;

import java.sql.Driver;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import net.sf.ehcache.util.FindBugsSuppressWarnings;
import platforms.WebPortal;

public class Sprint2_GlobelHeaderIntegrativeProducts  {
	private WebDriver driver;

	public Sprint2_GlobelHeaderIntegrativeProducts(WebDriver driver) 
	{
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	@FindBy(id = "dropdown-products")
	private WebElement productMegamenu_element;

	public WebElement getProductMegamenu_element() {
		return productMegamenu_element;
	}

	public List<WebElement> getProductMegamenuList() {
		return productMegamenuList;
	}

	public WebElement getViewProductDetails_ByType() {
		return viewProductDetails_ByType;
	}

	public WebElement getViewProductDetails_ByHealthInterest() {
		return viewProductDetails_ByHealthInterest;
	}

	public WebElement getFeaturedProductLabel_ByType() {
		return featuredProductLabel_ByType;
	}

	public WebElement getFeaturedProductLabel_ByHealthInterest() {
		return featuredProductLabel_ByHealthInterest;
	}

	public WebElement getFeaturedProductImage_ByType() {
		return featuredProductImage_ByType;
	}

	public WebElement getFeaturedProductImage_ByHealthInterest() {
		return featuredProductImage_ByHealthInterest;
	}

	public WebElement getFeaturedProductName_ByType() {
		return featuredProductName_ByType;
	}

	public WebElement getFeaturedProductName_ByHealthInterest() {
		return featuredProductName_ByHealthInterest;
	}

	public List<WebElement> getCategoryList_ByType() {
		return categoryList_ByType;
	}

	public List<WebElement> getCategoryList_ByHealthInterest() {
		return categoryList_ByHealthInterest;
	}
	
	public WebElement getCategoryClicked_Title() {
		return categoryClicked_title;
	}

	public List<WebElement> getViewProductDetails_PLP() {
		return viewProductDetails_PLP;
	}
	
	public WebElement getSearchResultCount_PLP() {
		return searchResultCount_PLP;
	}
	
	@FindBy(css = "ul.mega-menu__column li")
	private List<WebElement> productMegamenuList;
    
	@FindBy(xpath = "(//a[contains(@class,'featured-product-btn')])[2]")
	private WebElement viewProductDetails_ByType;
	
	@FindBy(xpath = "(//a[contains(@class,'featured-product-btn')])[1]")
	private WebElement viewProductDetails_ByHealthInterest;
	
	@FindBy(xpath = "(//p[@class='featured-product-heading'])[2]")
	private WebElement featuredProductLabel_ByType;
	
	@FindBy(xpath = "(//p[@class='featured-product-heading'])[1]")
	private WebElement featuredProductLabel_ByHealthInterest;
	
    @FindBy(xpath = "(//img[@class='featured-products-img'])[2]")
    private WebElement featuredProductImage_ByType;
    
    @FindBy(xpath = "(//img[@class='featured-products-img'])[1]")
    private WebElement featuredProductImage_ByHealthInterest;
    
    @FindBy(xpath = "(//a[@class='featured-caption-title'])[2]")
    private WebElement featuredProductName_ByType;
   
    @FindBy(xpath = "(//a[@class='featured-caption-title'])[1]")
    private WebElement featuredProductName_ByHealthInterest;
    
    @FindBy(xpath ="//div[@id='by-type']//ul//a")
    private List<WebElement> categoryList_ByType;
    
    @FindBy(xpath = "//div[@id='by-health-interest']//ul//a")
    private List<WebElement> categoryList_ByHealthInterest;
    
    @FindBy(css = ".field-banner-title")
    private WebElement categoryClicked_title;
    
    @FindBy(xpath = "//a[text()= 'View Details']")
    private List<WebElement> viewProductDetails_PLP;
 
    @FindBy(css = ".result-title")
    private WebElement searchResultCount_PLP;

	
	
	
	/*
	 * private WebElement clickOnRFPI() throws Exception { return
	 * driver.findElement(By.cssSelector(".featured-product-btn")); }
	 * 
	 * private WebElement clickOnRFPVD() throws Exception { return
	 * driver.findElement(By.cssSelector(".featured-product-btn")); }
	 * 
	 * private List<WebElement> clickOnByType() throws Exception { return
	 * driver.findElements(By.cssSelector("ul.mega-menu__column li")); }
	 * 
	 * private List<WebElement> clickOnAntioxidants() throws Exception { return
	 * driver.findElements(By.cssSelector(".tab-pane ul a")); }
	 * 
	 * private List<WebElement> clickOnHI() throws Exception { return
	 * driver.findElements(By.cssSelector("ul.mega-menu__column li")); }
	 * 
	 * private List<WebElement> clickOnAS() throws Exception { return
	 * driver.findElements(By.cssSelector(".tab-pane ul a")); }
	 * 
	 * private List<WebElement> clickOnPK() throws Exception { return
	 * driver.findElements(By.cssSelector("ul.mega-menu__column li")); }
	 * 
	 * private List<WebElement> clickOnSubitems() throws Exception { return
	 * driver.findElements(By.cssSelector(".tab-pane ul a"));
	 * 
	 * }
	 */
	 
}
