package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint4_Pagination {
	public WebDriver driver;

	public Sprint4_Pagination(WebDriver driver) {
		this.driver = driver;
	}

	public List<WebElement> NavigationBar() throws Exception {
		return driver.findElements(By.cssSelector(".pagination-navbar ul button"));
	}
	
	public WebElement Page1PLP()  throws Exception {
	return driver.findElement(By.xpath("//li/button[@id='previous-page']/parent::li//following-sibling::li[1]/button"));

	}
	
	public WebElement fistpagePLP()  throws Exception {
		return driver.findElement(By.cssSelector(".pagination-navbar ul button#first-page"));

		}
	
	public WebElement previouspagePLP()  throws Exception {
		return driver.findElement(By.cssSelector(".pagination-navbar ul button#previous-page"));

		}
	
	public WebElement lastPageNumber_ListingPage()throws Exception {
		return driver.findElement(By.xpath("//li/button[@id='last-page']/parent::li//preceding-sibling::li[2]/button"));

		}
	
	public WebElement nextpagePLP()  throws Exception {
		return driver.findElement(By.cssSelector(".pagination-navbar ul button#next-page"));

		}
	
	public WebElement lastpagePLP()  throws Exception {
		return driver.findElement(By.cssSelector(".pagination-navbar ul button#last-page"));

		}
	
	public WebElement Page3_PLP()  throws Exception {
		return driver.findElement(By.xpath("//li/button[@id='previous-page']/parent::li/following-sibling::li[3]/button"));

		}
}