package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint1_Faqs {
	public WebDriver driver;

	public Sprint1_Faqs(WebDriver driver) {
		this.driver = driver;
	}

	public String getPageTitle() {
		return driver.getTitle();
	}

	public WebElement integrativeProLogo() throws Exception {
		return driver.findElement(By.xpath("//img[@alt='header-logo']"));
	}

	public WebElement faqlink_footer() throws Exception {
		return driver.findElement(By.xpath("//a[contains(text(),'FAQs')]"));
	}

	public WebElement Logo_FAQpage() throws Exception {
		return driver.findElement(By.cssSelector("h1.page-title"));

	}
	
	public WebElement Description_FAQpage() throws Exception {
		return driver.findElement(By.xpath("//div[@class='component-content']/p[@class='module-description']"));

	}
	
	public WebElement contactuslink_FAQdescription() throws Exception {
		return driver.findElement(By.cssSelector("div.component-content p.module-description a"));

	}

	public WebElement fContactUs() throws Exception {
		return driver.findElement(By.cssSelector(".component-content > .btn"));
	}

	public WebElement fDescription() throws Exception {
		return driver.findElement(By.cssSelector("p.module-description:nth-child(1)"));
	}

	public WebElement fTag() throws Exception {
		return driver.findElement(By.cssSelector(".component:nth-child(4) .module-title"));
	}

	public WebElement fTag0() throws Exception {
		return driver.findElement(By.cssSelector(".component:nth-child(5) .module-title"));
	}
	
	public WebElement ContactUsButton_FaqPage() throws Exception {
		return driver.findElement(By.xpath("//div[@class='component-content']/a[contains(text(),'Contact')]"));
	}
	
	

	public List<WebElement> fQuestionSessionAns() throws Exception {
		return driver.findElements(By.cssSelector(".toggle-header"));
	}
	
	public List<WebElement> Questionsesstion_FaqPage() throws Exception {
		return driver.findElements(By.cssSelector("div.field-heading"));
	}
	
	public List<WebElement> Answersesesstion_FaqPage() throws Exception {
		return driver.findElements(By.cssSelector("div.field-content p"));
	}
	

}