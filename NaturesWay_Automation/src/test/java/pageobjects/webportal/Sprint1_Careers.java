package pageobjects.webportal;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint1_Careers {
	public WebDriver driver;

	public Sprint1_Careers(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement Promo_PopUp() throws Exception {
		return driver.findElement(By.cssSelector("div#promo-popup"));
	}
	
	
	
	public WebElement carrerLink() throws Exception {
		return driver.findElement(By.cssSelector(".link-list .item3 > a"));
	}

	public WebElement integrativeProLogo() throws Exception {
		return driver.findElement(By.cssSelector("a[title='Integrative Pro Logo']"));
	}

	public WebElement carrerBannerTitle() throws Exception {
		return driver.findElement(By.cssSelector(".banner-title"));
	}

	public WebElement carrerBannerDescription() throws Exception {
		return driver.findElement(By.cssSelector("p.banner-description:nth-child(2)"));
	}

	public WebElement carrerBannerButton() throws Exception {
		return driver.findElement(By.cssSelector(".vertical-center > .btn"));
	}

	public WebElement carrerOurEmployeesImage() throws Exception {
		return driver.findElement(By.cssSelector(".module-image:nth-child(1) .img-responsive"));
	}

	public WebElement carrerOurEmployeesTitle() throws Exception {
		return driver.findElement(By.cssSelector(".col-sm-7:nth-child(2) .module-title"));
	}

	public WebElement carrerOurEmployeesDescription() throws Exception {
		return driver.findElement(By.cssSelector("p.module-description:nth-child(3)"));
	}

	public WebElement carrerOurEmployeesLearnOurStoryButton() throws Exception {
		return driver.findElement(By.cssSelector(".btn:nth-child(4)"));
	}

	public WebElement carrerJoinourteamBanner() throws Exception {
		return driver.findElement(By.cssSelector(".banner-container > .col-xs-12 > picture > .img-responsive"));
	}

	public WebElement carrerJoinourteamTitle() throws Exception {
		return driver.findElement(By.cssSelector(".container > .module-title"));
	}

	public WebElement carrerBenefitsTitle() throws Exception {
		return driver.findElement(By.cssSelector(".col-sm-6:nth-child(2) h3"));
	}

	public WebElement carrerBenefitsMedicalInsurance() throws Exception {
		return driver.findElement(By.cssSelector("p.field-title:nth-child(2)"));
	}

	public WebElement carrerBenefitsDentalInsurance() throws Exception {
		return driver.findElement(By.cssSelector(".component:nth-child(2) .item1 > .field-title"));
	}

	public WebElement carrerBenefitsVoluntaryVisionInsurance() throws Exception {
		return driver.findElement(By.cssSelector(".component:nth-child(2) .item2 > .field-title"));
	}

	public WebElement carrerBenefitslifeinsurance() throws Exception {
		return driver.findElement(By.cssSelector(".component:nth-child(2) .item3 > .field-title"));
	}

	public WebElement carrerBenefitsprofitsharing() throws Exception {
		return driver.findElement(By.cssSelector(".component:nth-child(2) .item4 > .field-title"));
	}

	public WebElement carrerPerksTitle() throws Exception {
		return driver.findElement(By.cssSelector(".col-sm-6:nth-child(3) h3"));
	}

	public WebElement carrerPerksPaidTimeOff() throws Exception {
		return driver.findElement(By.cssSelector(".col-sm-6:nth-child(3) .item0 > .field-title"));
	}

	public WebElement carrerPerksPaidHolidays() throws Exception {
		return driver.findElement(By.cssSelector(".col-sm-6:nth-child(3) .item1 > .field-title"));
	}

	public WebElement carrerPerksWellnessincentives() throws Exception {
		return driver.findElement(By.cssSelector(".col-sm-6:nth-child(3) .item2 > .field-title"));
	}

	public WebElement carrerPerksGenerousproductdiscountsforouandyourfamily() throws Exception {
		return driver.findElement(By.cssSelector(".col-sm-6:nth-child(3) .item3 > .field-title"));
	}

	public WebElement carrerPerksEducationalReimbursement() throws Exception {
		return driver.findElement(By.cssSelector(".col-sm-6:nth-child(3) .item4 > .field-title"));
	}

	public WebElement careerpagejot_viewcareeropportunities() throws Exception {
		return driver.findElement(By.cssSelector("div#career-banner a.btn-primary"));
	}

	public WebElement carrerPageJoinOurTDes() throws Exception {
		return driver.findElement(By.cssSelector("div#career-banner .banner-description"));
	}

}
