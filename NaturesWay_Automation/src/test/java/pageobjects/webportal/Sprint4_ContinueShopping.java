package pageobjects.webportal;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint4_ContinueShopping {
	public WebDriver driver;

	public Sprint4_ContinueShopping(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement cartpageClickOnConShopping() throws Exception {
		return driver.findElement(By.cssSelector(".cart-cont-shop"));
	}
}
