package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import net.sf.ehcache.util.FindBugsSuppressWarnings;

public class Sprint2_AboutUsQuality {

	public WebDriver driver;

	public Sprint2_AboutUsQuality(WebDriver driver) 
	{
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	public WebElement clickOnAboutUs() throws Exception {
		return driver.findElement(By.cssSelector(".dropdown:nth-child(3)"));
	}

	public WebElement Qualitysubmenu_header() throws Exception {
		return driver.findElement(By.cssSelector("li.dropdown-menu--item a[title*='Quality']"));
	}

	public WebElement AbUsQualityTitle() throws Exception {
		return driver.findElement(By.cssSelector("p.field-banner-title"));
	}

	public WebElement AbUsQualityDesc() throws Exception {
		return driver.findElement(By.cssSelector("p.field-banner-description"));
	}
	
	@FindBy(css = ".video-init")
	private WebElement AbUsClickOnVedio;

	public WebElement AbUsClickOnVedio() {
		return AbUsClickOnVedio;
	}


	public List<WebElement> QualityTitle_leftimages() throws Exception {
		return driver.findElements(By.cssSelector("div.img-left-one .content .module-title"));
	}
	
	public List<WebElement> QualityDescription_leftimages() throws Exception {
		return driver.findElements(By.cssSelector("div.img-left-one .content .module-description"));
	}

	@FindBy(css = "div.img-left-one div.content .module-title")
	private List<WebElement> QualityPage_Title_leftsection;
	
	public List<WebElement> QualityPage_Title_leftsection() throws Exception {
		return QualityPage_Title_leftsection;
	}
	@FindBy(css = "div.img-left-one div.content .module-description")
	private List<WebElement> QualityPage_Description_leftsection;
	
	public List<WebElement> QualityPage_Description_leftsection()  {
		return QualityPage_Description_leftsection;
	}
	
	public List<WebElement> QualityPage_Images_leftsection() throws Exception {
		return driver.findElements(By.cssSelector("div.img-left-one picture img"));
	}
	
	@FindBy(css = "div.img-right-one div.content .module-title")
	private List<WebElement> QualityPage_Title_rightsection;

	public List<WebElement> QualityPage_Title_rightsection() {
		return QualityPage_Title_rightsection;
	}
	
	@FindBy(css = "div.img-right-one div.content .module-description")
	private List<WebElement> QualityPage_Description_rightsection;
	
	public List<WebElement> QualityPage_Description_rightsection() 
	{
		return QualityPage_Description_rightsection;
	}
	
	public List<WebElement> QualityPage_Images_rightsection() throws Exception {
		return driver.findElements(By.cssSelector("div.img-right-one picture img"));
	}
	
	
	
	public WebElement AbUsISTitle() throws Exception {
	return driver.findElement(By.xpath("//div[@class='content']/h2[contains(text(),'Ingredient')]"));
   }  
	
	
	public WebElement AbUsISDesc() throws Exception {
		return driver.findElement(By.cssSelector(".component:nth-child(6) .module-description"));
	}

	
	
	public WebElement AbUsISImage() throws Exception {
		return driver.findElement(By.xpath("//picture/img[@alt='Ingredient Sourcing']"));
	}

	
	public WebElement AbUsFDTitle() throws Exception {
		return driver.findElement(By.xpath("//div[@class='content']/*[contains(text(),'Formulation')]"));
	}

	/*public WebElement AbUsFDDesc() throws Exception {
		return driver.findElement(By.cssSelector(".component:nth-child(7) > .module-wrapper .module-description"));
	}
	*/
	public WebElement AbUsFDDesc() throws Exception {
		return driver.findElement(By.xpath("//div[@class='content']//p[contains(text(),'developing formulations')]"));
	}

	/*public WebElement AbUsFDImage() throws Exception {
		return driver.findElement(By.cssSelector(".component:nth-child(7) > .module-wrapper .img-responsive"));
	}
*/
	public WebElement AbUsFDImage() throws Exception {
		return driver.findElement(By.xpath("(//picture/img[@alt='Formulation Development'])[1]"));
	}
	public WebElement AbUsMTitle() throws Exception {
		return driver.findElement(By.cssSelector(".component:nth-child(8) .module-title"));
	}

	public WebElement AbUsMDesc() throws Exception {
		return driver.findElement(By.cssSelector(".component:nth-child(8) .module-description"));
	}

	public WebElement AbUsMImage() throws Exception {
		return driver.findElement(By.cssSelector(".component:nth-child(8) .img-responsive"));
	}

	public WebElement AbUsLTitle() throws Exception {
		return driver.findElement(By.cssSelector(".component:nth-child(9) .module-title"));
	}

	public WebElement AbUsLDesc() throws Exception {
		return driver.findElement(By.cssSelector(".component:nth-child(9) .module-description"));
	}

	public WebElement AbUsLImage() throws Exception {
		return driver.findElement(By.cssSelector(".component:nth-child(9) .img-responsive"));
	}

	public WebElement AbUsQuality_FooterBannerTitle() throws Exception {
		return driver.findElement(By.xpath("//div[@id='quality']//*[@class='module-title']"));
	}

	public WebElement AbUsQuality_FooterBannerDescription() throws Exception {
		return driver.findElement(By.xpath("//div[@id='quality']//*[@class='module-description']"));
	}

	public List<WebElement> AbUsQuality_FooterBannerImages() throws Exception {
		return driver.findElements(By.cssSelector(".qualityinfo-icons"));
	}

	public WebElement AbUsQuality_FooterAjaxBannerTitle() throws Exception {
		return driver.findElement(By.cssSelector(".component > div > .content > .module-title"));
	}

	public WebElement AbUsQuality_FooterAjaxBannerDescription() throws Exception {
		return driver.findElement(By.cssSelector(".component > div > .content > .module-description"));
	}

	public List<WebElement> AbUsQuality_FooterAjaxBannerImages() throws Exception {
		return driver.findElements(By.cssSelector("li.slide"));
	}

}
