package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint2_ProductDetailPage {
	public WebDriver driver;

	public Sprint2_ProductDetailPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement signinlink() throws Exception {
		return driver.findElement(By.cssSelector(".how-to-order a.sign-in-top"));
	}
	
	public WebElement LeftMenu_PDP() throws Exception {
		return driver.findElement(By.cssSelector("ul.left-sidemenu"));
	}
	
	public WebElement productTitle_PDP() throws Exception {
		return driver.findElement(By.cssSelector("div.hidden-xs div.content .product-title"));
	}
	
	public WebElement productImage_PDP() throws Exception {
		return driver.findElement(By.cssSelector("div#photo"));
	}
	
	
	public List<WebElement> smallpreviewimages_PDP() throws Exception {
		return driver.findElements(By.cssSelector(".thumbnail-image .img-responsive"));
	}

	public WebElement shareIcon_PDP() throws Exception 
	{ 
		return driver.findElement(By.xpath("(//div[@id='social-share-icon'])[2]//a/em[@class='icon-share']"));
	}
	
	
	public WebElement activevariant_PDP() throws Exception {
		return driver.findElement(By.cssSelector("div.product-flavor button.active"));
	}
	
	public List<WebElement> variants_list() throws Exception {
		return driver.findElements(By.cssSelector("div.product-flavor button"));
	}
	
	public List<WebElement> size_list() throws Exception {
		return driver.findElements(By.cssSelector("div.product-size button"));
	}
	
	
	public List<WebElement> clickOnSV() throws Exception {
		return driver.findElements(By.cssSelector(".left-sidemenu li a"));
	}
	
	public WebElement FAQMenu_PDPage() throws Exception {
		return driver.findElement(By.cssSelector(".quantity-unit .quantity em"));
	}
	
	public List<WebElement> ProductVariants() throws Exception {
		return driver.findElements(By.cssSelector(".product-flavor >button"));
	}
	
	public List<WebElement> clickOnPQuality() throws Exception {
		return driver.findElements(By.cssSelector("ul#quantity li"));
	}

	public List<WebElement> quantity_value() throws Exception {
		return driver.findElements(By.cssSelector("ul#quantity li a"));
	}
	
	public List<WebElement> units_value() throws Exception {
		return driver.findElements(By.cssSelector("ul#unit li a"));
	}

	public WebElement qualitydropdown() throws Exception {
		return driver.findElement(By.cssSelector(".quantity-unit .quantity em"));
	}

	public WebElement unitsdropdown() throws Exception {
		return driver.findElement(By.cssSelector(".quantity-unit .unit em"));
	}

	public WebElement clickOnFavorites() throws Exception {
		return driver.findElement(By.cssSelector("div.share-favorite .hidden-xs  .product-detail-favorite a.fav-icon em.icon-favorite"));
	}
	
	
	public List<WebElement> Fav_Marked() throws Exception {
		return driver.findElements(By.cssSelector("div.share-favorite .hidden-xs  .product-detail-favorite a.fav-icon em.icon-favcopy"));
	}

	public WebElement FavoriteAlert() throws Exception {
		return driver.findElement(By.cssSelector("div.fav-alert strong"));
	}
	
	public WebElement breadcrumb_AllProducts() throws Exception {
		return driver.findElement(By.cssSelector("li.breadcrumb-item:nth-child(2) a"));
	}
	
	
	
	public List<WebElement> pDPBC() throws Exception {
		return driver.findElements(By.cssSelector(".breadcrumb-item a"));
	}

	public List<WebElement> navFaqs() throws Exception {
		return driver.findElements(By.cssSelector(".left-sidemenu li"));
	}

	public WebElement clickOnQues() throws Exception {
		return driver.findElement(By.cssSelector(".accordion-link"));
	}

	public WebElement clickOnContactUs() throws Exception {
		return driver.findElement(By.cssSelector(".text-center > .btn"));
	}

	public List<WebElement> ClickOnpImageRA() throws Exception {
		return driver.findElements(By.cssSelector(".col-lg-3 .product-img"));
	}

	public WebElement Title_RelatedProduct_PDP() throws Exception {
		return driver.findElement(By.cssSelector(".related-product-title"));
	}

	public WebElement relatedproduct_selectaimage() throws Exception {
		return driver.findElement(By.cssSelector(".related-product-article:nth-child(1) .products-list-item"));
	}
	
	public WebElement relatedproduct_selectaproducttitle() throws Exception {
		return driver.findElement(By.cssSelector(".related-product-article:nth-child(1) a.related-products-link"));
	}

	public WebElement relatedproduct_selectviewdetailsbtn() throws Exception {
		return driver.findElement(By.cssSelector(".related-product-article:nth-child(1) a.related-btn"));
	}
	
	public List<WebElement> clickOnRAMT() throws Exception {
		return driver.findElements(By.cssSelector(".item .related-products-link"));
	}

	public List<WebElement> clickOnVDRA() throws Exception {
		return driver.findElements(By.cssSelector(".item .btn"));
	}

	public List<WebElement> clickOnFPA() throws Exception {
		return driver.findElements(By.cssSelector(".col-lg-3 .product-img"));
	}

	public List<WebElement> clickOnFPI() throws Exception {
		return driver.findElements(By.cssSelector(".article-item-section .article-image"));
	}

	public List<WebElement> verifyFPT() throws Exception {
		return driver.findElements(By.cssSelector(".article-item-section > .article-item-title > a"));
	}

	public WebElement FeaturedArticle_Icon_PDP() throws Exception {
		return driver.findElement(By.cssSelector("em.icon-article"));
	}
	
	public WebElement selectFeaturedArticleImage() throws Exception {
		return driver.findElement(By.cssSelector("div.article-item-section:nth-child(1) .article-image"));
	}
	
	public WebElement selectFeaturedArticleTitle() throws Exception {
		return driver.findElement(By.cssSelector("div.article-item-section:nth-child(1) .article-item-title a"));
	}
	
	public WebElement selectFeaturedArticleReadMore() throws Exception {
		return driver.findElement(By.cssSelector("div.article-item-section:nth-child(1) a.btn-primary"));
	}
	
	public WebElement FeaturedArticle_Title_PDP() throws Exception {
		return driver.findElement(By.cssSelector(".featured-article-title"));
	}

	public WebElement FeaturedArticle_Description_PDP() throws Exception {
		return driver.findElement(By.cssSelector("div.featured-subheading .module-title"));
	}
	
	public List<WebElement> clickOnRMFP() throws Exception {
		return driver.findElements(By.cssSelector(".article-item-section > .btn"));
	}

	public WebElement DetailsMenu_LeftPane() throws Exception {
		return driver.findElement(By.cssSelector(".text-list >a[title*='Details']"));
	}
	
	public WebElement Detailsofproduct_LeftPane() throws Exception {
		return driver.findElement(By.cssSelector(".pdp-tab-overlay div#details p"));
	}
	
	public WebElement Ingredientstitle_LeftPane() throws Exception {
		return driver.findElement(By.cssSelector(".pdp-tab-overlay div#ingredients .text-title"));
	}
	
	public WebElement Ingredientsofproduct_LeftPane() throws Exception {
		return driver.findElement(By.cssSelector(".pdp-tab-overlay div#ingredients .ingredients-table"));
	}
	
	public WebElement IngredientsMenu_LeftPane() throws Exception {
		return driver.findElement(By.cssSelector(".text-list >a[title*='Ingredients']"));
	}
	
	public WebElement DirectionsorDosageTitle_LeftPane() throws Exception {
		return driver.findElement(By.xpath("(//div[@id='directions']/div[@class='pad-xs-15']//*[@class='text-sub-title'])[1]"));
	}
	
	public WebElement DirectionsorDosageMessage_LeftPane() throws Exception {
		return driver.findElement(By.xpath("(//div[@id='directions']/div[@class='pad-xs-15']//p)[1]"));
	}
	
	public WebElement WarningTitle_LeftPane() throws Exception {
		return driver.findElement(By.xpath("(//div[@id='directions']/div[@class='pad-xs-15']//*[@class='text-sub-title'])[1]"));
	}
	
	public WebElement WarningMessage_LeftPane() throws Exception {
		return driver.findElement(By.xpath("(//div[@id='directions']/div[@class='pad-xs-15']//p)[1]"));
	}
	
	
	public WebElement Directionstitle_LeftPane() throws Exception {
		return driver.findElement(By.cssSelector(".pdp-tab-overlay div#directions .text-title"));
	}
	
	public WebElement DirectionsMenu_LeftPane() throws Exception {
		return driver.findElement(By.cssSelector(".text-list >a[title*='Directions']"));
	}
	
	public WebElement Interactions_DepletionsMenu_LeftPane() throws Exception {
		return driver.findElement(By.cssSelector(".text-list >a[title*='Interactions']"));
	}
	
	public WebElement Interactionstitle_LeftPane() throws Exception {
		return driver.findElement(By.cssSelector(".pdp-tab-overlay div#interactions-depletions .text-title"));
	}
	
	public List<WebElement> InteractionsdetailsEmpty_LeftPane() throws Exception {
		return driver.findElements(By.cssSelector("div#interactions-depletions div#accordion p"));
	}
	

	public WebElement PatientSupportMenu_LeftPane() throws Exception {
		return driver.findElement(By.cssSelector(".text-list >a[title*='Patient']"));
	}
	
	public WebElement PatientSupportMenu_Title() throws Exception {
		return driver.findElement(By.cssSelector("div#patientsupport .text-title"));
	}
	
	public List<WebElement> PatientSupportEmpty_LeftPane() throws Exception {
		return driver.findElements(By.cssSelector("div.patient-support-block p"));
	}
	
	public WebElement PatientSupportMenu_PDFLink() throws Exception {
		return driver.findElement(By.cssSelector("div.patient-support-block a"));
	}
	
	public WebElement PatientSupportMenu_PDFImg() throws Exception {
		return driver.findElement(By.cssSelector("div.patient-support-block a img"));
	}
	
	public WebElement FAQsMenu_LeftPane() throws Exception {
		return driver.findElement(By.cssSelector(".text-list >a[title*='Faq']"));
	}
	
	public WebElement FAQstitle_LeftPane() throws Exception {
		return driver.findElement(By.cssSelector(".pdp-tab-overlay div#faqs .text-title"));
	}
	
	public WebElement QuestionAnswerSection_FAQSMenu() throws Exception {
		return driver.findElement(By.cssSelector("div#faqs div.accordion-section"));
	}

	
	public WebElement ExpandQuestion_FAQSMenu() throws Exception {
		return driver.findElement(By.cssSelector("div#faqs a.collapsed"));
	}
	
	public WebElement AnswerSection_FAQSMenu() throws Exception {
		return driver.findElement(By.cssSelector("div#faqs div#collapseOne .panel-body"));
	}
	
	public WebElement Title_FAQsMenu_LeftPane() throws Exception {
		return driver.findElement(By.cssSelector("div#faqs .text-title"));
	}
	
	public WebElement PriceDetails_PDP() throws Exception {
		return driver.findElement(By.cssSelector("div.product-custom-selection span#product-price"));
	}
	
	
	
	
}
