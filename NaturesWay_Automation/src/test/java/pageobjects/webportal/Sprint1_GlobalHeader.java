package pageobjects.webportal;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint1_GlobalHeader {
	public WebDriver driver;

	public Sprint1_GlobalHeader(WebDriver driver) {
		this.driver = driver;
	}

	public String getPageTitle() {
		return driver.getTitle();
	}

	public WebElement headerintegrativeProLogo() throws Exception {
		return driver.findElement(By.cssSelector(".int-logo a"));
	}

	public WebElement headerCart() throws Exception {
		return driver.findElement(By.cssSelector(".icon-cart"));
	}

	public WebElement headerSearch() throws Exception {
		return driver.findElement(By.cssSelector("a#searchIcon"));
	}
	
	public WebElement clickOnIp() throws Exception {
		return driver.findElement(By.cssSelector(".field-link > a"));
	}

}
