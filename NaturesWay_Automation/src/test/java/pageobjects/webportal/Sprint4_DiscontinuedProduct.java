package pageobjects.webportal;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint4_DiscontinuedProduct {
	public WebDriver driver;

	public Sprint4_DiscontinuedProduct(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement discontinued_Label() throws Exception {
		return driver.findElement(By.cssSelector(".product-details-discontinued p"));
	}
}
