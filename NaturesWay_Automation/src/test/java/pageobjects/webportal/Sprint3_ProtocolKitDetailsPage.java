package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint3_ProtocolKitDetailsPage {
	public WebDriver driver;

	public Sprint3_ProtocolKitDetailsPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement pcKitProPrice() throws Exception {
		return driver.findElement(By.cssSelector("#product-price"));
	}

	public List<WebElement> pcKitProSize() throws Exception {
		return driver.findElements(By.cssSelector(".btn-secondary"));
	}

	public WebElement pcKitProImgPane() throws Exception {
		return driver.findElement(By.cssSelector("#photo"));
	}

	public WebElement pcKitProHea() throws Exception {
		return driver.findElement(By.cssSelector(".col-12 > .protocol-subtitle"));
	}

	public WebElement pcKitProTitle() throws Exception {
		return driver.findElement(By.cssSelector(".col-12 > .protocol-title"));
	}

	public List<WebElement> pcKitProImage() throws Exception {
		return driver.findElements(By.cssSelector(".article-item-section .img-responsive"));
	}

	public List<WebElement> pcKitProImageTitle() throws Exception {
		return driver.findElements(By.cssSelector(".article-item-section > .text-center > .article-item-title"));
	}

	public List<WebElement> pcKitProImageDes() throws Exception {
		return driver.findElements(By.cssSelector(".article-item-section > .text-center > .article-item-description"));
	}

	public List<WebElement> pcKitProNurInfo() throws Exception {
		return driver.findElements(By.cssSelector(".row > .col-sm-6 > .center-block"));
	}

	public List<WebElement> pcKitProLabel() throws Exception {
		return driver.findElements(By.cssSelector(".row > .col-sm-6 .protocol-subtitle"));
	}

	public List<WebElement> pcKitProPT() throws Exception {
		return driver.findElements(By.cssSelector(".row > .col-sm-6 .protocol-title"));
	}

	public List<WebElement> pcKitProPD() throws Exception {
		return driver.findElements(By.cssSelector(".row > .col-sm-6 .protocol-spaces"));
	}

	public List<WebElement> pcKitProPI() throws Exception {
		return driver.findElements(By.cssSelector(".row > .col-sm-6 > .about-protocol-kit .btn"));
	}

	public WebElement pcFeaArt() throws Exception {
		return driver.findElement(By.cssSelector(".icon-article"));
	}

	public WebElement pcShaArt() throws Exception {
		return driver.findElement(By.cssSelector(".col-xs-12:nth-child(2) .icon-share"));
	}

	public WebElement pcClickOnProKits() throws Exception {
		return driver.findElement(By.cssSelector(".hidden-xs .icon-favorite"));
	}

}
