package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint3_UsersAccountPage {
	public WebDriver driver;

	public Sprint3_UsersAccountPage(WebDriver driver) {
		this.driver = driver;
	}

/*	public List<WebElement> clickOnAD() throws Exception {
		return driver.findElements(By.cssSelector(".user-submenu-link> .user-list"));
	}

	public List<WebElement> clickOnMO() throws Exception {
		return driver.findElements(By.cssSelector(".user-submenu-link> .user-list"));
	}

	public List<WebElement> clickOnPO() throws Exception {
		return driver.findElements(By.cssSelector(".user-submenu-link> .user-list"));
	}

	public List<WebElement> clickOnCP() throws Exception {
		return driver.findElements(By.cssSelector(".user-submenu-link> .user-list"));
	}

	public List<WebElement> clickOnFP() throws Exception {
		return driver.findElements(By.cssSelector(".user-submenu-link> .user-list"));
	}
*/
	
	
  public WebElement activeLeftMenu() throws Exception {
		
		return driver.findElement(By.cssSelector("ul.nav-tab-left li.active a"));
	}
	

	public WebElement chevron_username() throws Exception {
		
		return driver.findElement(By.cssSelector("div.username-label em.icon-arrow-down"));
	}
	
	
   public WebElement username_header() throws Exception {
		
		return driver.findElement(By.cssSelector("div.username-label a#login-user-full-name"));
	}

// Account Information
	
		public WebElement UpdatedName_EditInfo() throws Exception {
			
			return driver.findElement(By.xpath("(//*[@class='sub-section-value'])[1]"));
		}
		
	public WebElement UpdatedPhNo_EditInfo() throws Exception {
			
			return driver.findElement(By.xpath("(//*[@class='sub-section-value'])[3]"));
		}

		public WebElement PersonalInfoSection() throws Exception {
			
			return driver.findElement(By.cssSelector("div#personal-view-only-container"));
		}
	
	public WebElement EditPersonalInfoSection() throws Exception {
			
			return driver.findElement(By.cssSelector("div#personal-secondary-container"));
		}

		public WebElement accountDetails_menu() throws Exception {
			
			return driver.findElement(By.cssSelector("li.user-submenu-link a[title*='Account']"));
		}
		
		public WebElement EditLink_PersonalInfoaccountDetail() throws Exception {
			
			return driver.findElement(By.cssSelector("a#profile-edit"));
		}
		
        public WebElement Prefix_EditInfo() throws Exception {
			
			return driver.findElement(By.cssSelector("select#prefixID"));
		}
        
    public List<WebElement> Title_EditInformation() throws Exception {
			
			return driver.findElements(By.cssSelector("p.sub-section-title"));
		}
 
    public List<WebElement> Value_EditInformation() throws Exception {
		
		return driver.findElements(By.cssSelector("p.sub-section-value"));
	}
					
       public WebElement FirstName_EditInfo() throws Exception {
			
			return driver.findElement(By.cssSelector("input#first-name"));
		}
   		
       public WebElement LastName_EditInfo() throws Exception {
			
			return driver.findElement(By.cssSelector("input#last-name"));
		}
   		
       public WebElement PostNominals_EditInfo() throws Exception {
			
			return driver.findElement(By.cssSelector("input#post-nominals"));
		}
       
       public WebElement PersonalPhNo_EditInfo() throws Exception {
			
			return driver.findElement(By.cssSelector("input#personal-phone-number"));
		}
       
       public WebElement Email_EditInfo() throws Exception {
			
			return driver.findElement(By.cssSelector("input#email-address"));
		}
       
		 public WebElement usericon_Accountinfo() throws Exception {		
		
			 return driver.findElement(By.cssSelector("li.user-submenu-link a[title*='Account'] em[class*='user-icon']"));
		 }
		
		 public WebElement accountinfo_section() throws Exception {
				
				return driver.findElement(By.cssSelector("div.profile-list"));
			}
		 
		 public WebElement saveButton_EditInfo() throws Exception {
				
				return driver.findElement(By.cssSelector("section#personal-view-section div.form-input button.btn-primary"));
			}
		 
		 public WebElement Cancelink_EditInfo() throws Exception {
				
				return driver.findElement(By.cssSelector("section#personal-view-section div.form-input a.cancel-btn"));
			}
// Favorites
		 
     public WebElement favorites_menu() throws Exception {
			
			return driver.findElement(By.cssSelector("li.user-submenu-link a[title*='Favorites']"));
		}
     
     public WebElement usericon_Favorite() throws Exception {
			
			return driver.findElement(By.cssSelector("li.user-submenu-link a[title*='Favorites'] em[class*='user-icon']"));
		}
     
     public WebElement breadcrumb_Favorite()
     {
    	return driver.findElement(By.xpath("//li[@class='breadcrumb-item last']/div/a"));
     }
     
     public List<WebElement> favoriteProducts()
     {
    	 return driver.findElements(By.cssSelector(".product-title"));
     }
     
 
     
 // Payment Methods
     
     public WebElement paymentmethods_menu() throws Exception {
			
			return driver.findElement(By.cssSelector("li.user-submenu-link a[title*='Payment']"));
		}
     
     public WebElement usericon_PaymentMethods() throws Exception {
			
			return driver.findElement(By.cssSelector("li.user-submenu-link a[title*='Payment'] em[class*='user-icon']"));
		}
     
     
     
 // Shipping Addess
     
     public WebElement shippingaddresses_menu() throws Exception {
			
			return driver.findElement(By.cssSelector("li.user-submenu-link a[title*='Shipping']"));
		}
     
     public WebElement usericon_shippingaddressess() throws Exception {
			
			return driver.findElement(By.cssSelector("li.user-submenu-link a[title*='Shipping Addresses'] em[class*='user-icon']"));
		}
     
 // Find A Practitioner
     
     public WebElement findAPractitionerProfile_menu() throws Exception {
			
			return driver.findElement(By.cssSelector("li.user-submenu-link a[title*='Practitioner']"));
		}
     
     public WebElement usericon_findAPractitionerProfile() throws Exception {
			
			return driver.findElement(By.cssSelector("li.user-submenu-link a[title*='Practitioner'] em[class*='user-icon']"));
		}
     
 // Sign out
	
     public WebElement signout_menu() throws Exception {
			
			return driver.findElement(By.cssSelector("li.user-submenu-link a.sign-out-top"));
		}
     
     public WebElement usericon_SignOut() throws Exception {
			
			return driver.findElement(By.cssSelector("li.user-submenu-link a[title*='Sign'] em[class*='user-icon']"));
		}
     
     
// My Orders
     
     public WebElement myorders_menu() throws Exception {
			
			return driver.findElement(By.cssSelector("li.user-submenu-link a[title*='Order']"));
		}
     
     public WebElement usericon_MyOrders() throws Exception {
			
			return driver.findElement(By.cssSelector("li.user-submenu-link a[title*='Order'] em[class*='user-icon']"));
		}
     
   
     //Sales representative
     
     public WebElement salesRepresentative_Photo()
     {
    	 return driver.findElement(By.xpath(""));
     }
     
     public WebElement salesRepresentative_Name()
     {
    	 return driver.findElement(By.xpath(""));
     }
     
     public WebElement salesRepresentative_JobTitle()
     {
    	 return driver.findElement(By.xpath(""));
     }
     
     public WebElement salesRepresentative_Address()
     {
    	 return driver.findElement(By.xpath(""));
     }
     
     public WebElement salesRepresentative_Email()
     {
    	 return driver.findElement(By.xpath(""));
     }
     
     public WebElement salesRepresentative_PhoneNo()
     {
    	 return driver.findElement(By.xpath(""));
     }
     
     
     
    
     
}



