package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint5_MyAccount_Favorites {
	public WebDriver driver;

	public Sprint5_MyAccount_Favorites(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement WelcomeMessage_Header_FavoritePage() throws Exception {
		return driver.findElement(By.cssSelector("div.my-account-header .page-sub-heading:nth-child(1)"));
	}
	
	public WebElement display_username_FavoritePage() throws Exception {
		return driver.findElement(By.cssSelector("div.my-account-header .page-sub-heading span.user-name"));
	}
	
	public WebElement accountnumber_Section_FavoritePage() throws Exception {
		return driver.findElement(By.cssSelector("div.my-account-header .account-number-heading"));
	}
	
	public WebElement display_accountnumber_FavoritePage() throws Exception {
		return driver.findElement(By.cssSelector("div.my-account-header .account-number-heading .account-number"));
	}
	
	public List<WebElement> productcounts_FavoritePage() throws Exception {
		return driver.findElements(By.cssSelector("div.product-item"));
	}
	
	public List<WebElement> productcounts_title_FavoritePage() throws Exception {
		return driver.findElements(By.cssSelector("div.product-item a.product-title"));
	}
	
	public List<WebElement> removefavorite_links_FavoritePage() throws Exception {
		return driver.findElements(By.cssSelector("div.product-item a.remove-favorite-link"));
	}
	
	public List<WebElement> productTitleList()
	{
		return driver.findElements(By.cssSelector("a.product-title"));
	}
	
	public List<WebElement> viewDetailsButton_FavoritePage()
	{
		return driver.findElements(By.cssSelector("a.view-more-btn"));
	}
	
}