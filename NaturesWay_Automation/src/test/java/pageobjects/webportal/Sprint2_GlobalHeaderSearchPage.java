package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint2_GlobalHeaderSearchPage {
	
	public WebDriver driver;

	public Sprint2_GlobalHeaderSearchPage(WebDriver driver) 
	{
		this.driver = driver;
	}

	public WebElement searchIcon() throws Exception 
	{
		return driver.findElement(By.cssSelector(".icon-search"));
	}
	
	public WebElement searchCloseIcon()
	{
		return driver.findElement(By.xpath("//a[@id='searchIcon']//em[@class='icon-small']"));
	}

	public WebElement popularProductLabel()
	{
		return driver.findElement(By.id("product-heading"));
	}
	
	public WebElement popularArticleLabel()
	{
		return driver.findElement(By.id("article-heading"));
	}

	public List<WebElement> popularProductsImageList() throws Exception 
	{
		return driver.findElements(By.xpath("//li[@class='search-result-list']/img"));
	}
	
	public List<WebElement> suggestedpopularproducts_Search() throws Exception 
	{
		return driver.findElements(By.cssSelector("ul.sugested-products li a"));
	}
	
	public List<WebElement> popularProductsNameList() throws Exception 
	{
		return driver.findElements(By.xpath("//li[@class='search-result-list']/a"));
	}

	public List<WebElement> popularArticlesList() throws Exception
	{
		return driver.findElements(By.cssSelector("ul.sugested-articles li a"));
	}
	
	public WebElement searchPane() throws Exception 
	{
		return driver.findElement(By.cssSelector("#searchInput"));
	}

	public List<WebElement> searchPaneList() throws Exception 
	{
		return driver.findElements(By.cssSelector("#searchInput"));
	}

	public WebElement viewAllProducts_Search() throws Exception
	{
		return driver.findElement(By.cssSelector(".viewall-products-link"));
	}
	
	public WebElement clickedProduct_Name()
	{
		return driver.findElement(By.xpath("//div[contains(@class,'hidden-xs')]//ancestor::h1[@class='product-title']"));
	}

	public WebElement clickedArticle_Name()
	{
		return driver.findElement(By.cssSelector(".page-title"));
	}
	
	public WebElement outsideOfSearch()
	{
		return driver.findElement(By.cssSelector(".header-user-nav"));
			
	}
	
	public WebElement enterThreecCharSP() throws Exception {
		return driver.findElement(By.cssSelector("#searchInput"));
	}
	
	public WebElement clickOnSearchPane() throws Exception {
		return driver.findElement(By.cssSelector(".icon-search"));
	}
	
	
	public WebElement viewdetailsAProduct_PLP() throws Exception {
		return driver.findElement(By.cssSelector("a.view-more-btn"));
	}

}
