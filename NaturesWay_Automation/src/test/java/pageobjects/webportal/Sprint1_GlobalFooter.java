package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint1_GlobalFooter {
	public WebDriver driver;

	public Sprint1_GlobalFooter(WebDriver driver) {
		this.driver = driver;
	}

	public String getPageTitle() {
		return driver.getTitle();
	}

	public List<WebElement> footerLinks() throws Exception {
		return driver.findElements(By.cssSelector("ul.list-component-field li a[data-variantfieldname]"));
	}
	
	public List<WebElement> socialLinks() throws Exception {
		return driver.findElements(By.cssSelector("ul.list-component-field li a.social-icons-link"));
	}
	
	public WebElement newslettersection_footer() throws Exception {
		return driver.findElement(By.cssSelector("div.center-news"));
	}
	
	public WebElement copyrights_footer() throws Exception {
		return driver.findElement(By.cssSelector("p.copyrights-txt"));
	}
	
	
	public WebElement newsLetterSection() throws Exception {
		return driver.findElement(By.cssSelector(".newsletter"));
	}
	
	public WebElement subText() throws Exception {
		return driver.findElement(By.cssSelector(".newsletter-help-text"));
	}

	public WebElement email() throws Exception {
		return driver.findElement(By.cssSelector("input[type='email']"));
	}

	public WebElement submit() throws Exception {
		return driver.findElement(By.cssSelector("button.newsletter-submit-btn "));
	}

	public WebElement sucessFullMessage() throws Exception {
		return driver.findElement(
				By.cssSelector("#fxb_04a18d25-aa16-4f2b-84e7-b7002af139c0_9d732801-7b61-4061-b118-d120f699cdde > p"));
	}

	public WebElement instagram() throws Exception {
		return driver.findElement(By.cssSelector(".icon-insta"));
	}

	public WebElement linkedin() throws Exception {
		return driver.findElement(By.cssSelector(".icon-linkedin"));
	}

	public WebElement facebook() throws Exception {
		return driver.findElement(By.cssSelector(".icon-facebook"));
	}

	public WebElement contact() throws Exception {
		return driver.findElement(By.cssSelector(".link-list .item0 > a "));
	}

	public WebElement sitemap() throws Exception {
		return driver.findElement(By.cssSelector(".link-list .item1 > a"));
	}

	public WebElement legal() throws Exception {
		return driver.findElement(By.cssSelector(".link-list .item2 > a"));
	}

	public WebElement careers() throws Exception {
		return driver.findElement(By.cssSelector(".link-list .item3 > a"));
	}

	public WebElement ic() throws Exception {
		return driver.findElement(By.cssSelector(".link-list .item4 > a"));
	}

	public WebElement faqs() throws Exception {
		return driver.findElement(By.cssSelector(".link-list .item5 > a"));
	}

	public WebElement ks() throws Exception {
		return driver.findElement(By.cssSelector("div.sna-footer-bottom-text"));
	}

	public WebElement cr() throws Exception {
		return driver.findElement(By.cssSelector(".copyrights-txt"));
	}
	public WebElement instagramText() throws Exception {
		return driver.findElement(By.cssSelector("div.cq2ai img"));
	}
	public WebElement linkedinText() throws Exception {
		return driver.findElement(By.cssSelector("header.nav img"));
	}
	public WebElement facebookText() throws Exception {
		return driver.findElement(By.cssSelector("div.lfloat i"));
	}
	

}
