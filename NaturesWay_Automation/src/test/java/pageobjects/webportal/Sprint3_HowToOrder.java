package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint3_HowToOrder {
	public WebDriver driver;

	public Sprint3_HowToOrder(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement howtoorderTitle() throws Exception {
		return driver.findElement(By.cssSelector(".banner-title"));
	}
	
	public WebElement clickOnHowToOrder() throws Exception {
		return driver.findElement(By.xpath("//div[@class='how-to-order']/a"));
	}
	
	public WebElement BannerImg_HowToOrderPg() throws Exception {
		return driver.findElement(By.cssSelector("div#page-banner-how-to-order"));
	}

	public WebElement howtoorderDesc() throws Exception {
		return driver.findElement(By.cssSelector(".banner-top-description"));
	}

	public WebElement clickOnFindAprac() throws Exception {
		return driver.findElement(By.cssSelector("div#page-banner-how-to-order a.btn-primary"));
	}

	public WebElement verifyAmazonLogo() throws Exception {
		return driver.findElement(By.cssSelector(".int-order-image"));
	}

	public WebElement verifyAmazonTitle() throws Exception {
		return driver.findElement(By.cssSelector(".content > .module-title"));
	}

	public WebElement AmazonLink() throws Exception {
		return driver.findElement(By.cssSelector(".order-link:nth-child(1)"));
	}

	public WebElement ShopAmazonNow_Btn() throws Exception {
		return driver.findElement(By.cssSelector(".content > .btn"));
	}

	public WebElement IntergrativeMedicineLink() throws Exception {
		return driver.findElement(By.cssSelector(".module-description a[href='/find-practitioner']"));
	}

	public List<WebElement> clickExpandIcon_HowToOrder() throws Exception {
		return driver.findElements(By.cssSelector(".faq-accord-title"));
	}
	
	public List<WebElement> verifyCollapseIcon_HowToOrder() throws Exception {
		return driver.findElements(By.cssSelector("div.field-content p"));
	}

}
