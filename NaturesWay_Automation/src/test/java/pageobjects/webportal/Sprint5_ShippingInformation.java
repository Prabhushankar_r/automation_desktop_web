package pageobjects.webportal;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class Sprint5_ShippingInformation {
	public WebDriver driver;

	public Sprint5_ShippingInformation(WebDriver driver) 
	{
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	
	@FindBy(xpath = "//h3[text() ='Choose Your Shipping Address']")
	private WebElement shippingInfo_Header;
	
	@FindBy(xpath = "//select[@id='address']")
	private WebElement addressDropdown;
	
	@FindBy(css = ".radio-wrapper")
	private WebElement upsGround_radiobutton;

	@FindBy(css = "div[data-bind='with: shippingAddress'] p.add-shipping-link a")
	private WebElement AddNewShippingAddress_Link_Checkout;
	
	@FindBy(css = "div[data-bind='with: shippingAddress'] button.btn-primary")
	private WebElement ContinueButton_ShippingInformation;
	
	@FindBy(css = "input#CompanyName")
	private WebElement CompanyName_ShippingInfo;
	
	@FindBy(css = "input#FirstName")
	private WebElement FirstName_ShippingInfo;
	
	@FindBy(css = "input#LastName")
	private WebElement LastName_ShippingInfo;
	
	@FindBy(css = "input#Address")
	private WebElement Address_ShippingInfo;

	@FindBy(css = "input#zipcode")
	private WebElement Zipcode_ShippingInfo;
	
	@FindBy(css = "input#primary-shipping")
	private WebElement EnableCheckBox_ShippingInfo;
	
	@FindBy(css = "input#primary-shipping")
	private WebElement Cancel;

	@FindBy(css = ".add-shipping div[data-bind='validationElement: addCity'] label.input-out-focus")
	private WebElement cityAutoPopulated_shippingAddress;
	
	@FindBy(css = ".add-shipping div[data-bind='validationElement: addCity']")
	private WebElement cityEmptyField;
	
	@FindBy(css = ".add-shipping div#practioner-state label.select-out-focus")
	private WebElement stateAutoPopulated_shippingAddress;
	
	@FindBy(css = ".add-shipping div#practioner-state")
	private WebElement stateEmptyField;

	@FindBy(css = ".add-shipping div#practioner-state label.select-out-focus")
	private WebElement stateAutoPopulate;
	
	@FindBy(xpath = "(//span[@data-bind='text: firstName'])[1]")
	private WebElement firstName_Updated;
	
	@FindBy(xpath = "(//span[@data-bind='text: lastName'])[1]")
	private WebElement lastName_Updated;
	
	@FindBy(xpath = "(//span[@data-bind='text: address1'])[1]")
	private WebElement address1_Updated;
	
	@FindBy(xpath = "(//li[@data-bind='text: addZipcode'])[1]")
	private WebElement zipcode_Updated;
	
	@FindBy(xpath = "(//a[@class='ship-addr-edit'])[1]")
	private WebElement editLink_ShippingInfo;

	
	public WebElement AddNewShippingAddress_Link_Checkout() 
	{
		return AddNewShippingAddress_Link_Checkout;
	}	
	public WebElement ContinueButton_ShippingInformation()  
	{
		return ContinueButton_ShippingInformation;
	}
	public WebElement CompanyName_ShippingInfo()  
	{
		return CompanyName_ShippingInfo;
	}
	public WebElement FirstName_ShippingInfo()  
	{
		return FirstName_ShippingInfo;
	}
	public WebElement LastName_ShippingInfo()  
	{
		return LastName_ShippingInfo;
	}
	public WebElement Address_ShippingInfo()  
	{
		return Address_ShippingInfo;
	}
	public WebElement Zipcode_ShippingInfo()  
	{
		return Zipcode_ShippingInfo;
	}
	public WebElement EnableCheckBox_ShippingInfo()  {
		return EnableCheckBox_ShippingInfo;
	}
	public WebElement Cancel()  {
		return Cancel;
	}
	public WebElement cityAutoPopulated_shippingAddress()  {
		return cityAutoPopulated_shippingAddress;
	}
	public WebElement stateAutoPopulated_shippingAddress()  {
		return stateAutoPopulated_shippingAddress;
	}
	public WebElement stateAutoPopulate()  {
		return stateAutoPopulate;
	}
	public WebElement firstName_Updated()
	{
		return firstName_Updated;
	}	
	public WebElement lastName_Updated()
	{
		return lastName_Updated;
	}

	public WebElement address1_Updated()
	{
		return address1_Updated;
	}
	
	public WebElement zipcode_Updated()
	{
		return zipcode_Updated;
	}
	
	public WebElement editLink_ShippingInfo()
	{
		return editLink_ShippingInfo;
	}
	
	public WebElement shippingInfo_Header() {
		return shippingInfo_Header;
	}

	public WebElement addressDropdown() {
		return addressDropdown;
	}

	public WebElement upsGround_radiobutton() {
		return upsGround_radiobutton;
	}
	
	public WebElement cityEmptyField() {
		return cityEmptyField;
	}
	public WebElement stateEmptyField() {
		return stateEmptyField;
	}
}
