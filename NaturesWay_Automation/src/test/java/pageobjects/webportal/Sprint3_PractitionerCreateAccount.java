package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint3_PractitionerCreateAccount {
	public WebDriver driver;

	public Sprint3_PractitionerCreateAccount(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement CreateAccount_Header() throws Exception {
		return driver.findElement(By.cssSelector(".page-heading"));
	}
	
	public WebElement CreateAccount_SubHeading() throws Exception {
		return driver.findElement(By.cssSelector(".page-sub-heading"));
	}
	
	public List<WebElement> RadioBtns() throws Exception {
		return driver.findElements(By.cssSelector(".about-you-radio"));
	}
	
	public WebElement HealthCare_RadioBtns() throws Exception {
		return driver.findElement(By.xpath("//input[@id='healthcare-practitioner']"));
	}
	
	public WebElement MedicalStudent_RadioBtns() throws Exception {
		return driver.findElement(By.xpath("//input[@id='medical-student']"));
	}
	
	
	public List<WebElement> Prefix_DropDown_Values() throws Exception {
		return driver.findElements(By.cssSelector("select#prefix > option"));
	}
	
	public WebElement CreateAccount_Btn() throws Exception {
		return driver.findElement(By.cssSelector("p.user-login >a[title*='Create']"));
	}
	
	
	public WebElement Prefix_Field() throws Exception {
		return driver.findElement(By.cssSelector("select#prefix"));
	}
	
	public WebElement FirstName_Field() throws Exception {
		return driver.findElement(By.cssSelector("input#first-name"));
	}
	
	public WebElement LastName_Field() throws Exception {
		return driver.findElement(By.cssSelector("input#last-name"));
	}
	
	public WebElement CollegeName_Field() throws Exception {
		return driver.findElement(By.cssSelector("input#college-name"));
	}
	
	public WebElement GraduationMonth_Field() throws Exception {
		return driver.findElement(By.cssSelector("select#graduation-month"));
	}
	
	public WebElement GraduationMonthDropDown_Field() throws Exception {
		return driver.findElement(By.cssSelector("select#graduation-month >option"));
	}
	
	public WebElement GraduationMonthDropDownValue_Field() throws Exception {
		return driver.findElement(By.xpath("(//select[@id='graduation-month']/option)[2]"));
	}

	
	public WebElement GraduationYear_Field() throws Exception {
		return driver.findElement(By.cssSelector("select#graduation-year"));
	}
	
	public WebElement GraduationYearDropDown_Field() throws Exception {
		return driver.findElement(By.cssSelector("select#graduation-year > option"));
	}
	
	public WebElement DegreeObtained() throws Exception {
		return driver.findElement(By.cssSelector("input#degree-obtained"));
	}
	
	
	public WebElement Program() throws Exception {
		return driver.findElement(By.cssSelector("input#program"));
	}
	public WebElement Err_Msg_LastName() throws Exception {
		return driver.findElement(By.xpath("//p[@data-valmsg-for='LastName']"));
	}
	
	public WebElement PostNominals_Field() throws Exception {
		return driver.findElement(By.cssSelector("input#post-nominals"));
	}
	
	public WebElement PostNominalsValuecheck_Field() throws Exception {
		return driver.findElement(By.cssSelector("label[for='post-nominals']"));
	}
	
	public WebElement OtherPractitionerValuecheck_Field() throws Exception {
		return driver.findElement(By.cssSelector("label[for='other-practitioner-type']"));
	}
	
	public WebElement BusinessNameValuecheck_Field() throws Exception {
		return driver.findElement(By.cssSelector("label[for='business-name']"));
	}
	
	public WebElement BusinessAdd1Valuecheck_Field() throws Exception {
		return driver.findElement(By.cssSelector("label[for='business-address1']"));
	}
	
	public WebElement StudentAdd1Valuecheck_Field() throws Exception {
		return driver.findElement(By.cssSelector("label[for='student-address1']"));
	}
	
	public WebElement LicenseValuecheck_Field() throws Exception {
		return driver.findElement(By.cssSelector("label[for='license-number']"));
	}
	
	public WebElement ReEnterPasswordValuecheck_Field() throws Exception {
		return driver.findElement(By.cssSelector("label[for='registration-confirmation-password']"));
	}
	
	
	
	public WebElement PractitionerType_Field() throws Exception {
		return driver.findElement(By.cssSelector("select#practitioner-type"));
	}
	
	public WebElement OtherPractitionerType_Field() throws Exception {
		return driver.findElement(By.cssSelector("input#other-practitioner-type"));
	}
	
	public WebElement PhoneNo_Field() throws Exception {
		return driver.findElement(By.cssSelector("input#phone-number"));
	}
	
	public WebElement EmailAddress() throws Exception {
		return driver.findElement(By.cssSelector("input#practioner-email"));
	}
	
	public WebElement StudentEmailAddress() throws Exception {
		return driver.findElement(By.cssSelector("input#student-email-address"));
	}
	
	public WebElement LicenseNo() throws Exception {
		return driver.findElement(By.cssSelector("input#license-number"));
	}
	
	public WebElement UsageTypes() throws Exception {
		return driver.findElement(By.cssSelector("select#intend-products"));
	}
	
	public WebElement Password() throws Exception {
		return driver.findElement(By.cssSelector("input#security-password"));
	}
	
	public WebElement ReEnter_Password() throws Exception {
		return driver.findElement(By.cssSelector("input#registration-confirmation-password"));
	}
	
	public WebElement BusinessName() throws Exception {
		return driver.findElement(By.cssSelector("input#business-name"));
	}
	
	public WebElement businessaddress1() throws Exception {
		return driver.findElement(By.cssSelector("input#business-address1"));
	}
	

	public WebElement Studentaddress1() throws Exception {
		return driver.findElement(By.cssSelector("input#student-address1"));
	}
	
	public WebElement Studentaddress2() throws Exception {
		return driver.findElement(By.cssSelector("input#student-address2"));
	}
	
	public WebElement businessaddress2() throws Exception {
		return driver.findElement(By.cssSelector("input#business-address2"));
	}
	
	public WebElement businesszipcode() throws Exception {
		return driver.findElement(By.cssSelector("input#business-zipcode"));
	}
	
	public WebElement studentzipcode() throws Exception {
		return driver.findElement(By.cssSelector("input#student-zipcode"));
	}
	
	public WebElement studentcity() throws Exception {
		return driver.findElement(By.cssSelector("input#student-city"));
	}
	
	public WebElement studentstate() throws Exception {
		return driver.findElement(By.cssSelector("select#student-state"));
	}
	
	public WebElement businesscity() throws Exception {
		return driver.findElement(By.cssSelector("input#business-city"));
	}
	
	public WebElement businessstate() throws Exception {
		return driver.findElement(By.cssSelector("select#business-state"));
	}
	
	public WebElement PractitionersCount() throws Exception {
		return driver.findElement(By.cssSelector("select#practitioners-count"));
	}
	
	public WebElement BusinessPhone() throws Exception {
		return driver.findElement(By.cssSelector("input#business-phone"));
	}
	
	public WebElement Oues_Optional1() throws Exception {
		return driver.findElement(By.cssSelector("textarea#HearAboutUs"));
	}
	
	public WebElement Oues_Optional2() throws Exception {
		return driver.findElement(By.cssSelector("textarea#AnyQuestions"));
	}
	
	
	public WebElement Err_Msg_Prefix() throws Exception {
		return driver.findElement(By.xpath("//p[@data-valmsg-for='Prefix']"));
	}
	
	public WebElement Err_Msg_FirstName() throws Exception {
		return driver.findElement(By.xpath("//p[@data-valmsg-for='FirstName']"));
	}
	
	public WebElement Err_Msg_PostNominal() throws Exception {
		return driver.findElement(By.xpath("//p[@data-valmsg-for='PostNominal']"));
	}
	
	public WebElement Err_Msg_PractitionerType() throws Exception {
		return driver.findElement(By.xpath("//p[@data-valmsg-for='PractitionerType']"));
	}
	
	public WebElement Err_Msg_OtherPractitionerType() throws Exception {
		return driver.findElement(By.xpath("//p[@data-valmsg-for='OtherPractitionerType']"));
	}
	
	public WebElement Err_Msg_BusinessEmail() throws Exception {
		return driver.findElement(By.xpath("//p[@data-valmsg-for='BusinessEmail']"));
	}
	
	public WebElement Err_Msg_StudentEmail() throws Exception {
		return driver.findElement(By.xpath("//p[@data-valmsg-for='StudentEmailAddress']"));
	}
	
	public WebElement Err_Msg_PhoneNumber() throws Exception {
		return driver.findElement(By.xpath("//p[@data-valmsg-for='PhoneNumber']"));
	}
	
	public WebElement Err_Msg_LicenseNumber() throws Exception {
		return driver.findElement(By.xpath("//p[@data-valmsg-for='LicenseNumber']"));
	}
	
	public WebElement Err_Msg_UsageTypes() throws Exception {
		return driver.findElement(By.xpath("//p[@data-valmsg-for='UsageTypes']"));
	}
	
	public WebElement Err_Msg_Password() throws Exception {
		return driver.findElement(By.xpath("//p[@data-valmsg-for='Password']"));
	}
	
	public WebElement Err_Msg_ConfirmPassword() throws Exception {
		return driver.findElement(By.xpath("//p[@data-valmsg-for='ConfirmPassword']"));
	}
	
	public WebElement Err_Msg_BussinessName() throws Exception {
		return driver.findElement(By.cssSelector("p#business-name-error"));
	}
	
	public WebElement Err_Msg_Address1_HealthCare() throws Exception {
		return driver.findElement(By.cssSelector("p#business-address1-error"));
	}
	
	
	public WebElement Err_Msg_Address1_Student() throws Exception {
		return driver.findElement(By.cssSelector("p#student-address1-error"));
	}
	
	public WebElement Err_Msg_Address2_Student() throws Exception {
		return driver.findElement(By.xpath("//div[@class='row student-form']//p[@data-valmsg-for='Address2']"));
	}
	
	public List<WebElement> Err_Msg_Address2_HealthCare() throws Exception {
		return driver.findElements(By.xpath("//section[@class='practioner-form']//p[@data-valmsg-for='Address2']"));
	}
	
	public WebElement Err_Msg_Zipcode() throws Exception {
		return driver.findElement(By.xpath("(//p[@data-valmsg-for='Zipcode'])[2]"));
	}
	
	public WebElement Err_Msg_Zipcode_MedicalStudent() throws Exception {
		return driver.findElement(By.cssSelector("p#student-zipcode-error"));
	}
	
	public WebElement Err_Msg_City() throws Exception {
		return driver.findElement(By.xpath("(//p[@data-valmsg-for='City'])[2]"));
	}
	
	public WebElement Err_Msg_City_MedicalStudent() throws Exception {
		return driver.findElement(By.cssSelector("p#student-city-error"));
	}
	
	public WebElement Err_Msg_State_MedicalStudent() throws Exception {
		return driver.findElement(By.cssSelector("p#student-state-error"));
	}
	
	public WebElement Err_Msg_State() throws Exception {
		return driver.findElement(By.xpath("(//p[@data-valmsg-for='State'])[2]"));
	}
	
	public WebElement Err_Msg_TotalNumberofPractitioners() throws Exception {
		return driver.findElement(By.xpath("//p[@data-valmsg-for='TotalNumberofPractitioners']"));
	}
	
	public WebElement Err_Msg_BusinessPhone() throws Exception {
		return driver.findElement(By.xpath("//p[@data-valmsg-for='BusinessPhone']"));
	}
	
	public WebElement Err_Msg_CollegeName() throws Exception {
		return driver.findElement(By.xpath("//p[@data-valmsg-for='CollegeName']"));
	}
	
	public WebElement Err_Msg_GraduationMonths() throws Exception {
		return driver.findElement(By.xpath("//p[@data-valmsg-for='GraduationMonths']"));
	}
	
	public WebElement Err_Msg_GraduationYears() throws Exception {
		return driver.findElement(By.xpath("//p[@data-valmsg-for='GraduationYears']"));
	}
	
	
	
	public WebElement Err_Msg_DegreeObtained() throws Exception {
		return driver.findElement(By.xpath("//p[@data-valmsg-for='DegreeObtained']"));
	}
	
	public WebElement Err_Msg_Program() throws Exception {
		return driver.findElement(By.xpath("//p[@data-valmsg-for='Program']"));
	}
	
	public WebElement SubmitBtn() throws Exception {
		return driver.findElement(By.cssSelector("button.form-button"));
	}
	
	public WebElement UseSelectedAdd_Btn() throws Exception {
		return driver.findElement(By.cssSelector("a[title*='Use Selected Address']"));
	}
	
	public WebElement WelcomeUsername_header() throws Exception {
		return driver.findElement(By.cssSelector("username-text"));
	}
			
	public WebElement ResalePolicy() throws Exception {
		return driver.findElement(By.cssSelector("input#resale-policy"));
	}
	
	public WebElement ResalePolicyLink() throws Exception {
		return driver.findElement(By.cssSelector("label[for='resale-policy'] a"));
	}
	
	public WebElement HearAboutUsTxtBox() throws Exception {
		return driver.findElement(By.cssSelector("#HearAboutUs"));
	}
	
	public WebElement AnyQnsTxtBox() throws Exception {
		return driver.findElement(By.cssSelector("#AnyQuestions"));
	}
	
	public WebElement CheckSubscribe() throws Exception {
		return driver.findElement(By.cssSelector("input#check-subscribe"));
	}
	
	public WebElement createaccount_success_pageheading() throws Exception {
		return driver.findElement(By.cssSelector(".page-heading"));
	}
}
