package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint1_Legal {
	public WebDriver driver;

	public Sprint1_Legal(WebDriver driver) {
		this.driver = driver;
	}

	public List<WebElement> legalTabs() throws Exception {
		return driver.findElements(By.cssSelector("ul.tabset-list li a"));
	}

	public WebElement legalLink_Footer() throws Exception {
		return driver.findElement(By.cssSelector("li.component-list a[href='/legal']"));
	}

	public WebElement legalTitle() throws Exception {
		return driver.findElement(By.cssSelector(".page-title"));
	}

	public WebElement legalTermsOfUse() throws Exception {
		return driver.findElement(By.cssSelector("ul.tabset-list a#terms-of-use"));
	}

	public WebElement legalResalePolicy() throws Exception {
		return driver.findElement(By.cssSelector("ul.tabset-list a#resale-policy"));
	}

	public WebElement legalPrivacyPolicy() throws Exception {
		return driver.findElement(By.cssSelector("ul.tabset-list a#privacy-policy"));
	}

	public WebElement legalHippa() throws Exception {
		return driver.findElement(By.cssSelector("ul.tabset-list a#hipaa"));
	}

}
