package pageobjects.webportal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Sprint1_ArticleListeingAndDetails {
	public WebDriver driver;

	public Sprint1_ArticleListeingAndDetails(WebDriver driver) {
		this.driver = driver;
	}

	public String getPageTitle() {
		return driver.getTitle();
	}

	public WebElement resource_header() throws Exception {
		return driver.findElement(By.cssSelector("a#dropdown-resources"));
	}

/*	public WebElement articles() throws Exception {
		return driver.findElement(By.cssSelector(".dropdown-menu--item:nth-child(3) .dropdown-menu-txt"));
	}
*/	
	public WebElement articles() throws Exception {
		return driver.findElement(By.cssSelector("[title*='Articles'] .dropdown-menu-txt"));
	}

	
	public WebElement FindAPractitoner() throws Exception {
		return driver.findElement(By.cssSelector(".dropdown-menu a[title*='Practitioner']"));
	}
	
	public WebElement articleTitle() throws Exception {
		return driver.findElement(By.cssSelector(".banner-title"));
	}

	public List<WebElement> readMoreButton() throws Exception {
		return driver.findElements(By.xpath("//a[@class='moreBtn']"));
	}

	public WebElement articlesPageHeroBanner() throws Exception {
		return driver.findElement(By.cssSelector(".page-banner"));

	}

	public List<WebElement> articlesPageAuthorName() throws Exception {
		return driver.findElements(By.className(".col-md-10"));

	}

	public WebElement articleDateSection() throws Exception {
		return driver.findElement(By.cssSelector(".article-box:nth-child(1) .article-published-date"));

	}

	public WebElement articlesSectionimage() throws Exception {
		return driver.findElement(By.cssSelector(".article-box:nth-child(1) .col-xs-12 img"));
		//return driver.findElement(By.xpath("(//div[@class='col-xs-12 col-lg-6 col-md-6 col-sm-6 article-img'])[1]"));

	}

	public WebElement articlesPageCategorySection() throws Exception {
		return driver.findElement(By.cssSelector(".article-box:nth-child(1) .article-category-desc"));

	}

	public WebElement bth() throws Exception {
		return driver.findElement(By.cssSelector(".home a"));

	}

	public WebElement articlepage_shareicon() throws Exception {
		return driver.findElement(By.cssSelector("a.share-modal-popup"));

	}
	
	public WebElement clickOnShareIcon() throws Exception {
		return driver.findElement(By.xpath("(//a[@class='icon-link share-modal-popup'])[2]"));

	}

	public WebElement enabledEmailTextbox() throws Exception {
		return driver.findElement(By.cssSelector("input#share-email-address"));

	}

	public WebElement clickOnEmailButton() throws Exception {
		return driver.findElement(By.cssSelector("button.btn"));

	}

	public WebElement emailSucessMessageAd() throws Exception {
		return driver.findElement(By.cssSelector(".success-mesg"));

	}

	public List<WebElement> iterationOfSocialMediaIcons() throws Exception {
		return driver.findElements(By.cssSelector("ul.art-listing-icons-group-content img"));

	}

	public WebElement clickOnCopyRightLink() throws Exception {
		return driver.findElement(By.cssSelector("a#copyLink"));

	}

	public WebElement enabledWhiteColourTickMark() throws Exception {
		return driver.findElement(By.cssSelector("a#copyLink"));

	}

	public List<WebElement> iterationOfFeatureProducts() throws Exception {
		return driver.findElements(By.cssSelector("img.featured-prdcts-bg-image"));

	}
	
	public List<WebElement> Title_FeaturedProduct() throws Exception {
		return driver.findElements(By.cssSelector(".prod-item-name"));

	}
	
	

	public WebElement fpButton() throws Exception {
		return driver.findElement(By.cssSelector(".featured-prdct-article:nth-child(2) .btn"));

	}

	public WebElement fpButtonClick() throws Exception {
		return driver.findElement(By.cssSelector(".featured-prdct-article:nth-child(2) .btn"));

	}

	public List<WebElement> articleCount() throws Exception {
		return driver.findElements(By.cssSelector(".article-img img"));

	}

	public WebElement backToArticles() throws Exception {
		return driver.findElement(By.cssSelector(".home a"));

	}

	public WebElement relatedArticlesLogo() throws Exception {
		return driver.findElement(By.cssSelector(".related-articles-logo >em"));

	}

	public WebElement relatedArticlesTitle() throws Exception {
		return driver.findElement(By.cssSelector(".related-article-header"));

	}

	public List<WebElement> noOfrelatedArticles() throws Exception {
		//return driver.findElements(By.cssSelector(".field-image"));
		return driver.findElements(By.xpath("//a[text()='Read More']"));

	}

	public List<WebElement> relatedArticlesDesc() throws Exception {
		return driver.findElements(By.cssSelector("p.module-title"));

	}

	public List<WebElement> relatedArticlesReadMore() throws Exception {
		return driver.findElements(By.cssSelector(".rel-article-btn"));

	}
	
	public WebElement articlebc() throws Exception {
		return driver.findElement(By.cssSelector(".breadcrumb-item:nth-child(2) a"));

	}
}
