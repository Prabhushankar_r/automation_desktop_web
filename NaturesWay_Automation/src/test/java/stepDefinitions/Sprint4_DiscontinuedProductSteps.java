package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.testng.Assert;
import cucumber.api.java.en.Then;
import platforms.WebPortal;

public class Sprint4_DiscontinuedProductSteps extends WebPortal {

	public Sprint4_DiscontinuedProductSteps() throws FileNotFoundException, IOException, ParseException {
		super();
	
	}
	@Then("^verify discontinued label in PDP$")
	public void verify_Discontinued_Label_PDP() throws Throwable {
	  // platform.cartpageClickOnConShopping();
		Assert.assertTrue(verifyElementIsPresent(discontinuedproduct.discontinued_Label()));
		
	}


}