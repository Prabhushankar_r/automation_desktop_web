package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;
import org.json.simple.parser.ParseException;
import org.testng.Assert;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import platforms.WebPortal;

public class Sprint1_MegaMenuSteps<must> extends WebPortal {

	public Sprint1_MegaMenuSteps() throws FileNotFoundException, IOException, ParseException {
		super();
	}

	@When("^I should see mega menu$")
	public void i_should_see_mega_menu(DataTable smicons) throws Throwable {
		verifyMegamenuElement(smicons);
	}

	@And("^I click on sign-in$")
	public void I_click_on_signin() throws Throwable {	
		waitTillElementExist(forgotpassword.signIn());
		verifyElementIsDisplayed(forgotpassword.signIn());
		clickOnLinkWithOutScroll(forgotpassword.signIn());
		//platform.button();
	}

	@When("^user enters valid \"(.*?)\" and pass \"(.*?)\" and comfirm \"(.*?)\"$")
	public void i_enter_valid_and_pass_and_confirm(String username, String pass, String confirm) throws Throwable {
		//platform.createAccount(username, pass, confirm);
	}
	
	@Then("^display signin modal$")
	public void Display_SignIn_Modal() throws Throwable {
		Assert.assertTrue(verifyElementIsPresent(megamenu.Sign_In_Button_ModalWindow()));
	}


	@Then("^I enter valid login credentials$")
	public void Enter_Valid_Login_Credentials() throws Throwable {
		waitForPageLoaded();
		EnterValidLoginCredentials();	
	}
	
	
	
	@And("^I click on submit$")
	public void I_click_on_submit() throws Throwable {
		clickButtonWithOutScroll(megamenu.Sign_In_Button_ModalWindow());
	}
	
	@And("^I click on sign in button in modal window$")
	public void I_click_on_sign_in_button_modal_Window() throws Throwable {
		clickButtonWithOutScroll(megamenu.Sign_In_Button_ModalWindow());
	}

	
	@When("^I Verify the sequence of global mega menu components for signed in user$")
	public void i_Verify_the_sequence_of_global_mega_menu_components_for_signed_in_user(DataTable smicons)
			throws Throwable {
		verifyMegamenuElement(smicons);
	}

	@And("^User should see the quickorder$")
	public void user_should_see_the_quickorder() throws Throwable {
		waitForPageLoaded();
		explicitWait(megamenu.verifyQuickOrderPresent());
		verifyElementIsDisplayed(megamenu.verifyQuickOrderPresent());
		
	}

	@Then("^I Should be logout succesfully$")
	public void i_Should_be_logout_succesfully() throws Throwable {
		signOut();
	}
	
	
	@Then("^verify app launch is success$")
	public void verify_App_Is_launched_Successfully() throws Throwable {
		waitForPageLoaded();
		waitTillElementExist(megamenu.signinbtn_header());
	}
	
	

	@And("^I click on create an account$")
	public void I_click_on_create_an_account() throws Throwable {
		//platform.createAnAccount();
	}

	@And("^click on continue as create the new Account$")
	public void click_on_continue_as_guest_user() throws Throwable {
		//platform.clickOnCreateNewAccount();
	}

	@When("^click on password eye icon$")
	public void click_on_password_eye_icon() throws Throwable {
		clickOnLinkWithOutScroll(megamenu.clickOnPasswordEyeIcon());
	}

	@And("^click Drug Nutrient Interaction$")
	public void click_Drug_Nutrient_Interaction() throws Throwable {
		holdAndClick(articlelisteing.resource_header(),megamenu.DrugNutrientMenu_Header());
		//platform.drugNutrientInteraction();
	}

	@And("^click Patient Resource$")
	public void click_Patient_Resource() throws Throwable {
		holdAndClick(articlelisteing.resource_header(),megamenu.PatientResorceMenu_Header());

	}


	@And("^click Find a Practitioner$")
	public void click_Find_a_Practitioner() throws Throwable {
		holdAndClick(articlelisteing.resource_header(),articlelisteing.FindAPractitoner());
		//platform.findAPractitioner();

	}

	@And("^click on close icon$")
	public void click_on_close_icon() throws Throwable {
		//verifyElementIsDisplayed(megamenu.Closeicon_ModelWindow());
		clickOnLinkWithOutScroll(megamenu.Closeicon_ModelWindow());
			}
	
	@And("^click on close icon in forgot password screen$")
	public void click_on_close_icon_ForgotPassword_ModalWindow() throws Throwable {
		//verifyElementIsDisplayed(megamenu.Closeicon_ModelWindow());
		clickOnLinkWithOutScroll(megamenu.Closeicon_ForgotPasswordModelWindow());
			}

	@Then("^user should able to click on welcome user$")
	public void user_should_able_to_click_on_welcome_user() throws Throwable {
		verifyElementIsDisplayed(megamenu.clickOnWelcomeUser());
		//platform.clickOnWelcomeUser();
	}

}