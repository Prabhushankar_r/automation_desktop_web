package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;

import cucumber.api.java.en.And;
import platforms.WebPortal;

public class Sprint3_MiniCartSteps extends WebPortal {

	public Sprint3_MiniCartSteps() throws FileNotFoundException, IOException, ParseException {
		super();
	}

	@And("^click headercart$")
	public void click_headercart() throws Throwable {
     ClickOnHc();
	}

}
