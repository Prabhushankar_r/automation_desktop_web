

package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import platforms.WebPortal;

public class Sprint4_HowToOrderSteps extends WebPortal {

	public Sprint4_HowToOrderSteps() throws FileNotFoundException, IOException, ParseException {
		super();
		}
	
	@When("^click how to order$")
	public void click_how_to_order() throws Throwable {
		 waitForPageLoaded();
		 Assert.assertTrue(verifyElementIsDisplayed(productdetailpage.shareIcon_PDP()));
		//scrollToElement(productdetailpage.signinlink());
		 Assert.assertTrue(verifyElementIsDisplayed(howtoorder.clickOnHowToOrder()));
		 clickButtonWithOutScroll(howtoorder.clickOnHowToOrder());
	}

@And("^verify Title and hero banner$")
public void verify_Title_hero_banner_how_to_order() throws Throwable {
	Assert.assertTrue(verifyElementIsPresent(howtoorder.BannerImg_HowToOrderPg()));
	Assert.assertTrue(verifyTextIsDisplayed(howtoorder.howtoorderTitle()));
}

@Then("^verify how to order page$")
public void verify_app_navigates_HowToOrderPage() throws Throwable {
	 verifyExtendedUrl("how-to-order");
	//platform.favSucessMessage();
}

@And("^verify content area$")
public void verify_content_area() throws Throwable {
Assert.assertTrue(verifyTextIsDisplayed(howtoorder.howtoorderDesc()));

}
//
@And("^click Find a Practitioner button$")
public void click_Find_a_Practitioner() throws Throwable {
	Assert.assertTrue(verifyElementIsPresent(howtoorder.clickOnFindAprac()));
	clickButtonWithOutScroll(howtoorder.clickOnFindAprac());
}

@And("^navigate to Find a Practitioner page$")
public void Navigate_FindAPractitionerPage() throws Throwable {
	verifyExtendedUrl("find-practitioner");
}


@And("^verify amazon logo$")
public void verify_amazon_logo() throws Throwable {
 Assert.assertTrue(verifyElementIsPresent(howtoorder.verifyAmazonLogo()));
 
}

@And("^verify Title of amazon$")
public void verify_Title_of_amazon() throws Throwable {
Assert.assertTrue(verifyElementIsPresent(howtoorder.verifyAmazonTitle())
		
);
}

@And("^verify Shop Amazon Now button$")
public void Verify_ShopAmazonNow_Button() throws Throwable {
	Assert.assertTrue(verifyElementIsPresent(howtoorder.ShopAmazonNow_Btn()));

}


@And("^click Amazon link$")
public void click_Amazon_com_Link() throws Throwable {
 Assert.assertTrue(verifyElementIsPresent(howtoorder.AmazonLink()));
 clickButtonWithOutScroll(howtoorder.AmazonLink());
 waitForPageLoaded();
 verifyExtendedUrl_NewTab("https://www.amazon.com/");
}

@And("^click Shop Amazon now$")
public void click_Shop_Amazon_now() throws Throwable {
	   clickButtonWithOutScroll(howtoorder.ShopAmazonNow_Btn());
	   waitForPageLoaded();
	   verifyExtendedUrl_NewTab("https://www.amazon.com/");

}

@And("^click Integrative practitioner$")
public void click_Integrative_practitioner() throws Throwable {
	 Assert.assertTrue(verifyElementIsPresent(howtoorder.IntergrativeMedicineLink()));
	 clickButtonWithOutScroll(howtoorder.IntergrativeMedicineLink());
	 waitForPageLoaded();
	 verifyExtendedUrl("find-practitioner");
}

@And("^verify question and answer section in how to order page$")
public void verify_Question_Answer_Section_HowToOrderPage() throws Throwable {
	question_answerAccordian_HowToOrder();
}

@Then("^verify multiple question and answer section can be viewed$")
public void verify_Multiple_Question_Answer_Section_CanbeViewed_At_Time() throws Throwable {
	multiplequestion_answerAccordian_HowToOrder();
}

}
