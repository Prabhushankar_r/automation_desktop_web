package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;

import cucumber.api.java.en.And;

public class Sprint3_ProtocolKitDetailsSteps extends AbstractStepDefinition {

	public Sprint3_ProtocolKitDetailsSteps() throws FileNotFoundException, IOException, ParseException {
		super();

	}

	@And("^verify the display of Price$")
	public void verify_the_display_of_Price() throws Throwable {
		//platform.pcKitProPrice();
	}

	@And("^click Size$")
	public void click_Size() throws Throwable {
		//platform.pcKitProSize();
	}

	@And("^Clicking the image within the main image pane, should open a light box$")
	public void clicking_the_image_within_the_main_image_pane_should_open_a_light_box() throws Throwable {
		//platform.pcKitProImgPane();
	}

	@And("^verify header$")
	public void verify_header() throws Throwable {
		//platform.pcKitProHea();
	}

	@And("^verify title$")
	public void verify_title() throws Throwable {
		//platform.pcKitProTitle();
	}

	@And("^verify image$")
	public void verify_image() throws Throwable {
		//platform.pcKitProImage();
	}

	@And("^verify image title$")
	public void verify_image_title() throws Throwable {
		//platform.pcKitProImageTitle();
	}

	@And("^verify image description$")
	public void verify_image_description() throws Throwable {
		//platform.pcKitProImageDes();
	}

	@And("^Nutrition Information should be visible in this section$")
	public void nutrition_Information_should_be_visible_in_this_section() throws Throwable {
		//platform.pcKitProNurInfo();
	}

	@And("^User should be able to see Label$")
	public void user_should_be_able_to_see_Label() throws Throwable {
		//platform.pcKitProLabel();
	}

	@And("^User should be able to see the Product Title$")
	public void user_should_be_able_to_see_the_Product_Title() throws Throwable {
		//platform.pcKitProPT();
	}

	@And("^View Product Details$")
	public void view_Product_Details() throws Throwable {
		//platform.pcKitProPD();
	}

	@And("^User should be able to view Product information$")
	public void user_should_be_able_to_view_Product_information() throws Throwable {
		//platform.pcKitProPI();
	}

	@And("^verify feature Articles icon$")
	public void verify_feature_Articles_icon() throws Throwable {
		//platform.pcFeaArt();
	}

	@And("^click on share article$")
	public void click_on_share_article() throws Throwable {
		//platform.pcShaArt();
	}

	@And("^click protokol kit favorites$")
	public void click_protokol_kit_favorites() throws Throwable {
		//platform.pcClickOnProKits();
	}

}
