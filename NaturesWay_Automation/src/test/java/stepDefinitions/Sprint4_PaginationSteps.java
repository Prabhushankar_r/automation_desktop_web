package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import platforms.WebPortal;

public class Sprint4_PaginationSteps extends WebPortal {

	public Sprint4_PaginationSteps() throws FileNotFoundException, IOException, ParseException {
		super();
	
	}
	@Then("^verify the pagination$")
	public void verify_Pagination_PLP() throws Throwable {
		pagination();
	}


	@Then("^verify the pagination for firstpage$")
	public void verify_Pagination_PLP_FirstPage() throws Throwable {
		firstpageFunctionality();	
	}

	@And("^verify the pagination for lastpage$")
	public void verify_Pagination_PLP_LastPage() throws Throwable {
		lastpageFunctionality();
	}

	
	@Then("^verify that current active page is highlighted$")
	public void verify_CurrentActivePage_Highlighted() throws Throwable {
		highlightActivePage();

	}
	
	@Then("^verify the next page nagination using '>' icon$")
	public void verify_NextPageNavigationUsingIcon() throws Throwable {
		nextpageUsingIconPageNavigation();	
	}

	@Then("^verify the next page nagination using '<' icon$")
	public void verify_PreviousPageNavigationUsingIcon() throws Throwable {
		previouspageUsingIconPageNavigation();	
	}
	
}
