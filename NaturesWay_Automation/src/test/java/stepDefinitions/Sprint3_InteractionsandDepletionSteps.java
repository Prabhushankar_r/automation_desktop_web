package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import platforms.WebPortal;

public class Sprint3_InteractionsandDepletionSteps extends WebPortal {

	public Sprint3_InteractionsandDepletionSteps() throws FileNotFoundException, IOException, ParseException {
		super();
	}
	
	@And("^search for \"(.*?)\" and select it$")
	public void searchadrug_SearchPane(String drugname) throws Throwable {
	    searchADrug(drugname);
		//navigatetourl();
		
	}
	
	@And("^navigate to InteractionsandDepletion$")
	public void Naviagate_To_Interaction_Depletion_InPDP() throws Throwable {
		NagiateToInteraction();
	
	}
	
	@And("^Expand all interactios listed$")
	public void ClickInteractions_ToExpand() throws Throwable {
		ExpandInteraction_listed();
	
	}
	
	@And("^verify the below menus in left pane$")
	public void verify_leftpanemenus_PDP(DataTable leftpanemenus) throws Throwable {
		verify_LeftpaneMenus(leftpanemenus);
	
	}
	
	
	
}