package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.testng.Assert;

import cucumber.api.java.en.*;
import platforms.WebPortal;

public class Sprint2_ProductDetailSteps extends WebPortal {

	public Sprint2_ProductDetailSteps() throws FileNotFoundException, IOException, ParseException {
		super();

	}
	
	@Then("^Verify user is navigated to PDP$")
	public void verify_user_is_navigated_to_PDP() throws Exception 
	{
		implicitWait(20);
		waitTillElementExist(productdetailpage.clickOnFavorites());
		verifyExpectedText(Sprint2_ProductListingSteps.productTitle, globelheadersearch.clickedProduct_Name());
	}
	
	@Then("^verify the share icon in PDP$")
	public void verify_Share_Icon_PDP() throws Throwable {
		Assert.assertTrue(verifyElementIsDisplayed(productdetailpage.shareIcon_PDP()));

	}
		
	
	@And("^click on share icon in PDP$")
	public void Click_ShareIcon_ProductDetailPage() throws Throwable {
		waitTillElementExist(productdetailpage.shareIcon_PDP());
		clickButtonWithOutScroll(productdetailpage.shareIcon_PDP());			
	}
	
	@And("^clicking on the small preview images$")
	public void clicking_on_the_small_preview_images() throws Throwable {
		verifyElementsAreDisplayed(productdetailpage.smallpreviewimages_PDP());
	}

	@And("^selecting a variant, the page should be updated accordingly$")
	public void selecting_a_variant_the_page_should_be_updated_accordingly() throws Throwable {
		variantChange_PDP();
		sizeChange_PDP();
	}
	
	@And("^selecting a size the page should be updated accordingly$")
	public void selecting_a_size_the_page_should_be_updated_accordingly() throws Throwable {
		sizeChange_PDP();
	}

	@And("^verify Qty and Units dropdown are displayed$")
	public void verify_Quanity_Units_Dropdown_Displayed() throws Throwable {
		clickButtonWithOutScroll(productdetailpage.qualitydropdown());
		verifyElementsAreDisplayed(productdetailpage.quantity_value());

		clickButtonWithOutScroll(productdetailpage.unitsdropdown());
		verifyElementsAreDisplayed(productdetailpage.units_value());

	}
	
	@And("^verify the price change when units and quanity values are changed$")
	public void Price_Change_When_Quanity_Units_Are_Changed() throws Throwable {
		pricechange_OnChanging_Quantity_Units();
	}
	
	
	
	@Then("^verify the suggested retail price displayed in PDP$")
	public void verify_Suggested_RetailPrice_Displayed_PDP() throws Throwable {
		Assert.assertTrue(verifyElementIsDisplayed(productdetailpage.PriceDetails_PDP()));

	}
	
	@Then("^verify the mandatory components in detail page$")
	public void verify_Mandatoy_Components_PDP_Page() throws Throwable {
		Assert.assertTrue(verifyTextIsDisplayed(productdetailpage.productTitle_PDP()));
		Assert.assertTrue(verifyElementIsDisplayed(productdetailpage.productImage_PDP()));
		Assert.assertTrue(verifyTextIsDisplayed(productdetailpage.PriceDetails_PDP()));
		Assert.assertTrue(verifyElementsAreDisplayed(productdetailpage.smallpreviewimages_PDP()));


	}
	
	@And("^verify the mandatory button and links in PDP for Unauthenticated user$")
	public void verify_Mandatory_Button_Link_UnauthenticatedUser() throws Throwable {
		Assert.assertTrue(verifyElementIsDisplayed(productdetailpage.signinlink()));
		Assert.assertTrue(verifyElementIsDisplayed(howtoorder.clickOnHowToOrder()));

	}
	
	
	
	@And("^select the quality$")
	public void select_the_quality() throws Throwable {
		//clickButtonWithOutScroll(productdetailpage.quantity_value());
		clickOnList_Index(productdetailpage.quantity_value(), 1);
	}

	@And("^select the unit$")
	public void select_the_unit() throws Throwable {
		//clickButtonWithOutScroll(productdetailpage.units_value());
		clickOnList_Index(productdetailpage.quantity_value(), 1);

	}

	@And("^select the quality drop down$")
	public void select_the_quality_drop_down() throws Throwable {
		clickButtonWithOutScroll(productdetailpage.qualitydropdown());
	}

	@And("^select the unit drop down$")
	public void select_the_unit_drop_down() throws Throwable {
		clickButtonWithOutScroll(productdetailpage.unitsdropdown());
	//	platform.cDP();
	}

	
	@Then("^verify the favorites icon in PDP$")
	public void verify_favorites_Icon_PDP() throws Exception 
	{
		Assert.assertTrue(verifyElementIsDisplayed(productdetailpage.clickOnFavorites()));
	}
	
	@And("^click favorites$")
	public void click_favorites() throws Throwable {
		favoritefunctionality_PDP();
   }
	
	@When("^favorite icon is clicked for unauthenticated user$")
	public void FavoriteIcon_selected_UnauthenticatedUser() throws Throwable {
		clickButtonWithOutScroll(productdetailpage.clickOnFavorites());
	}
	
	@When("^display the product detail page with favorite$")
	public void Display_ProductDetailPage_afterlogin_Favorite() throws Throwable {
		verifyExtendedUrl("products/");
		Assert.assertTrue(verifyElementIsPresent(productdetailpage.FavoriteAlert()));
	}
	
	
	@Then("^verify that 'favorite' icon is highlighted$")
	public void verify_that_favorite_icon_is_highlighted() throws Exception 
	{
		waitVisibilityOfElement(productdetailpage.FavoriteAlert());
		Assert.assertTrue(productdetailpage.FavoriteAlert().getText().equalsIgnoreCase("Added to favorites"),"Added to Favorites alert is not displayed ");
		//String favoriteColor = productdetailpage.clickOnFavorites().getCssValue("background-color");
		//System.out.println("After Favorite Click  "+favoriteColor);
		
	}

	@And("^click ProductDetail Page Bread Crumbs$")
	public void click_ProductDetail_Page_Bread_Crumbs() throws Throwable {
		waitTillElementExist(productdetailpage.breadcrumb_AllProducts());
		clickOnLinkWithOutScroll(productdetailpage.breadcrumb_AllProducts());
		waitTillElementExist(contactus.backToHomeButton());
		clickOnLinkWithOutScroll(contactus.backToHomeButton());

   }

	@And("^navigate to faqs$")
	public void navigate_to_faqs() throws Throwable {
		navigateFAQ_PDP();
		waitForPageLoaded();
	}

	@And("^click on question$")
	public void click_on_question() throws Throwable {
	
		Assert.assertTrue(verifyElementIsPresent(productdetailpage.clickOnQues()),"Question section under FAQ - PDP is not displayed");
		javascriptExecutorClick(productdetailpage.clickOnQues());
		
	}

	@And("^click on contact-us$")
	public void click_on_contact_us() throws Throwable {
		Assert.assertTrue(verifyElementIsPresent(productdetailpage.clickOnContactUs())," Contact Us under FAQ - PDP is not displayed");
		clickButtonWithOutScroll(productdetailpage.clickOnContactUs());
	}

	@And("^click product image related articles$")
	public void click_product_image_related_articles() throws Throwable {
		//platform.ClickOnpImageRA();
	}

	@And("^verify the display of related article header$")
	public void verify_the_display_of_related_article_header() throws Throwable {
		verifyExpectedText("RELATED PRODUCTS",productdetailpage.Title_RelatedProduct_PDP());
	}

	@And("^click on related articles images$")
	public void click_on_related_articles_images() throws Throwable {
		clickOnLinkWithOutScroll(productdetailpage.relatedproduct_selectaimage());

	}

	@And("^click on related articles module titles$")
	public void click_on_related_articles_module_titles() throws Throwable {
		clickOnLinkWithOutScroll(productdetailpage.relatedproduct_selectaproducttitle());
	}

	@And("^click on view details button$")
	public void click_on_view_details_button() throws Throwable {
		clickButtonWithOutScroll(productdetailpage.relatedproduct_selectviewdetailsbtn());
	}

	@And("^click product feature product articles$")
	public void click_product_feature_product_articles() throws Throwable {
		//platform.clickOnFPA();
	}

	@And("^verify the display of featured Article title in product detail page$")
	public void verify_the_display_of_featured_article_title() throws Throwable {
		verifyElementIsDisplayed(productdetailpage.FeaturedArticle_Icon_PDP());
		Assert.assertTrue(verifyTextIsDisplayed(productdetailpage.FeaturedArticle_Title_PDP()));
	}
	
	@And("^verify the display of featured Article description in product detail page$")
	public void verify_the_display_of_featured_article_description() throws Throwable {
		Assert.assertTrue(verifyTextIsDisplayed(productdetailpage.FeaturedArticle_Description_PDP()));
	}

	@And("^click on featured article images in PDP$")
	public void click_on_featured_article_images() throws Throwable {
		clickButtonWithOutScroll(productdetailpage.selectFeaturedArticleImage());
    }

	@And("^click on tiles in featured articles of PDP$")
	public void click_on_feature_products_titles() throws Throwable {
		clickButtonWithOutScroll(productdetailpage.selectFeaturedArticleTitle());
	}

	@And("^click on read more featured article of PDP$")
	public void click_on_read_more_feature_products() throws Throwable {
		clickButtonWithOutScroll(productdetailpage.selectFeaturedArticleReadMore());
		}
	
	@When("^navigate to patient support menu in pdp$")
	public void navigate_PatientSupport_LeftMenu_PDP() throws Throwable {
		scrollToElement(productdetailpage.PatientSupportMenu_LeftPane());
		clickButtonWithOutScroll(productdetailpage.PatientSupportMenu_LeftPane());
		}
	
	
	@When("^navigate to direction menu in product detail page$")
	public void navigate_Directions_LeftMenu_PDP() throws Throwable {
		scrollToElement(productdetailpage.DirectionsMenu_LeftPane());
		clickButtonWithOutScroll(productdetailpage.DirectionsMenu_LeftPane());
	}
	
	
	@When("^verify Direction & Dosage information available$")
	public void verify_Direction_Dosage_Information_displayed_PDP() throws Throwable {
		verifyElementIsDisplayed(productdetailpage.DirectionsorDosageTitle_LeftPane());
		verifyElementIsDisplayed(productdetailpage.DirectionsorDosageMessage_LeftPane());

		verifyElementIsDisplayed(productdetailpage.WarningTitle_LeftPane());
		verifyElementIsDisplayed(productdetailpage.WarningMessage_LeftPane());

	}
	
	
	@When("^navigate to FAQs menu in pdp$")
	public void navigate_FAQs_LeftMenu_PDP() throws Throwable {
		scrollToElement(productdetailpage.LeftMenu_PDP());
		clickButtonWithOutScroll(productdetailpage.FAQsMenu_LeftPane());
		}
	
	@Then("^display FAQs section in PDP$")
	public void display_FAQs_Section_ProductDetailPage() throws Throwable {
		Assert.assertTrue(verifyTextIsDisplayed(productdetailpage.Title_FAQsMenu_LeftPane()));
		
	}
	
	@Then("^verify Q&A section are displayed in FAQ$")
	public void verify_Question_Answer_Section_Displayed() throws Throwable {
		Assert.assertTrue(verifyElementIsDisplayed(productdetailpage.QuestionAnswerSection_FAQSMenu()),"FAQs Question & Answer Section are not displayed");
		Assert.assertTrue(verifyElementIsDisplayed(productdetailpage.clickOnContactUs()));
	
	}
	
	@When("^expand question section in FAQ$")
	public void Expand_Question_In_FAQs() throws Throwable {
		Assert.assertTrue(verifyElementIsDisplayed(productdetailpage.ExpandQuestion_FAQSMenu()));
		clickButtonWithOutScroll(productdetailpage.ExpandQuestion_FAQSMenu());
	
	}
	
	@When("^display answer under FAQ$")
	public void display_Answer_Section_FAQs() throws Throwable {
		Assert.assertTrue(verifyElementIsDisplayed(productdetailpage.AnswerSection_FAQSMenu()));
	
	}
	
	@Then("^verify the title 'Patient Support' in pdp$")
	public void verify_Title_PatientSupport_PDP() throws Throwable {
		Assert.assertTrue(verifyTextIsDisplayed(productdetailpage.PatientSupportMenu_Title()));
		
	}
	
	@Then("^verify the title 'Direction' in pdp$")
	public void verify_Title_Direction_PDP() throws Throwable {
		Assert.assertTrue(verifyTextIsDisplayed(productdetailpage.Directionstitle_LeftPane()));
		
	}
	
	@Then("^verify the \"Details\" section in PDP $")
	public void verify_Details_Section() throws Throwable {
		Assert.assertTrue(verifyTextIsDisplayed(productdetailpage.PatientSupportMenu_Title()));
		
	}
	
	
	@And("^verify the PDF available under patient support in pdp$")
	public void verify_PDP_Section_Available_PatientSupprt_Menu() throws Throwable {
		Assert.assertTrue(verifyElementIsDisplayed(productdetailpage.PatientSupportMenu_PDFImg()));
		
	}
	
	@Then("^verify the redirection for PDF under patient support$")
	public void verify_Redirection_Of_PDF_PatientSupportMenu() throws Throwable {
		clickButtonWithOutScroll(productdetailpage.PatientSupportMenu_PDFLink());
		//redirectionOfPDF_PatientSupport();
		
	}
	
	
}

