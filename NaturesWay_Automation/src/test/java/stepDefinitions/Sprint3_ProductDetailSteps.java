package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import platforms.WebPortal;

public class Sprint3_ProductDetailSteps extends WebPortal {

	public Sprint3_ProductDetailSteps() throws FileNotFoundException, IOException, ParseException {
		super();

	}

	@And("^verify the sucessmessage in product detail page$")
	public void verify_the_sucessmessage_in_product_detail_page() throws Throwable {
		//platform.favSucessMessage();
	}
	

	@And("^click on sign-in product detail page$")
	public void click_on_sign_in_product_detail_page() throws Throwable {
		clickOnLinkWithOutScroll(productdetailpage_3.sigInlinkPDP());
		//platform.sigInPDP();
	}

	@And("^click Add to Cart button$")
	public void click_Add_to_Cart_button() throws Throwable {
		 waitTillElementExist(productdetailpage_3.AddToCartButton());
		 verifyElementIsPresent(productdetailpage_3.AddToCartButton());
		 Assert.assertTrue(productdetailpage_3.AddToCartButton().isEnabled());
		 clickButtonWithOutScroll(productdetailpage_3.AddToCartButton());
		 verifyElementIsPresent(productdetailpage_3.CartDrawer());
		//platform.ATCB();
	}

	@And("^click Find in Store button$")
	public void click_Find_in_Store_button() throws Throwable {

	}
	
	@And("^verify recently added product is displayed in cart drawer$")
	public void verify_RecentlyAddedProduct_Displayed_CartDrawer() throws Throwable {
		displayRecentlyAddedProduct_CartDrawer();

	}

	
	
}
