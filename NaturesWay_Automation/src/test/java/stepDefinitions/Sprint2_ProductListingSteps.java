package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.testng.Assert;

import cucumber.api.java.en.*;
import platforms.WebPortal;

public class Sprint2_ProductListingSteps extends WebPortal 
{
	public static String productTitle;

	public Sprint2_ProductListingSteps() throws FileNotFoundException, IOException, ParseException 
	{
		super();
	}

	@And("^click on mega menu products$")
	public void click_on_mega_menu_products() throws Throwable {
		clickButtonWithOutScroll(productlisting.productsmenu_header());
		//platform.clickOnMMenuProducts();
	}

	@And("^click view all products$")
	public void click_on_view_all_products() throws Throwable {
		holdAndClick(productlisting.productsmenu_header(),productlisting.viewallproducts_header());
		waitFor(2000);
		waitForPageLoaded();
		verifyExtendedUrl("products");
		//platform.clickOnMMVAP();
	}

	@And("^click product image$")
	public void click_product_image() throws Throwable {
		clickOnLinkWithOutScroll(productlisting.selectaproductbyImage());
		waitFor(2000);
		waitForPageLoaded();
		verifyExtendedUrl("products/");	}

	@And("^user navigate back to the page$")
	public void user_navigate_back_to_the_page() throws Throwable {
		navigateBack();
	}

	@And("^click product title$")
	public void click_product_title() throws Throwable {
		productTitle=productlisting.selectaproductbyTitle().getText().toLowerCase();
		clickOnLinkWithOutScroll(productlisting.selectaproductbyTitle());
		 waitForPageLoaded();
		 verifyExtendedUrl("products/");
		 Assert.assertEquals(productTitle,productdetailpage.productTitle_PDP().getText().toLowerCase());
	}

	
	@When("^Click on product '(\\d+)' and get the product title$")
	public void click_on_product_and_get_the_product_title(int index)  
	{
		productTitle = clickOnListReturnText_Index(productlisting.productTitleList(), index);
	   
	}

	@And("^user clicks on a filter clicking on the checkbox applied filters are Now Filter By$")
	public void user_clicks_on_a_filter_clicking_on_the_checkbox_applied_filters_are_Now_Filter_By() throws Throwable {
		clickOnFilterCheckBox();
	}

	@And("^click Clear All Filters$")
	public void click_Clear_All_Filters() throws Throwable {
    clickOnLink(productlisting.NowFilterBySection(), productlisting.clickOnClearAllFilter());
	}

	@And("^sort the products from Product Name A-Z, Product Name Z-A, Popularity$")
	public void sort_the_products_from_Product_Name_A_Z_Product_Name_Z_A_Popularity() throws Throwable {
		sortTheProd();
	}

	@And("^click on show more results$")
	public void click_on_show_more_results() throws Throwable {
		clickOnSMR();
	}

	@And("^click on product bread crumb$")
	public void click_on_product_bread_crumb() throws Throwable {
		clickOnBreadCrumb();
	}

	@And("^click on product view details$")
	public void click_on_product_view_details_button() throws Throwable {
		explicitWait(productlisting.selectaproductbyViewDetails());
		clickButtonWithOutScroll(productlisting.selectaproductbyViewDetails());
		waitFor(2000);
		waitForPageLoaded();
		 verifyExtendedUrl("products/");
	}

	@And("^sort products$")
	public void sort_products() throws Throwable {
		//platform.clickOnSortProducts();
	}
	@And("^user selects all items in form section$")
	public void selectAllitems_Byenablingcheckbox_InFormSection() throws Throwable {
		clicksOnFormsFilter();
	}

	@And("^user selects all items in dietary needs section$")
	public void selectAllitems_Byenablingcheckbox_InDietaryNeedsSection() throws Throwable {
	   clicksonDietaryNeedsFilter();
	}

	@And("^user selects all items in product type section$")
	public void selectAllitems_Byenablingcheckbox_InProductTypeSection() throws Throwable {
		clicksOnProductTypeFilter();
	}

	@And("^user selects all items in health section$")
	public void selectAllitems_Byenablingcheckbox_InHealthSection() throws Throwable {
		clicksOnHealthFilter();  
	}

	@And("^user selects all items in formulation section$")
	public void selectAllitems_Byenablingcheckbox_InFormulationSection() throws Throwable {
	  clicksOnformulationFilter();
	}
	
	}
