package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class Sprint1_FaqsSteps extends AbstractStepDefinition {
	public Sprint1_FaqsSteps() throws FileNotFoundException, IOException, ParseException {
		super();
	}

	@Then("^user should able to view global header$")
	public void user_should_able_to_view_global_header() throws Throwable {
		platform.verifyTheDisplayOfGlobelHeader();
	}

	@And("^click on FAQs footer link$")
	public void click_on_FAQs_footer_link() throws Throwable {
		clickButton(FAQS.faqlink_footer(), FAQS.faqlink_footer());
		//platform.clickOnFaqsLink();
	}

	@And("^Check FAQs page content$")
	public void check_FAQs_page_content() throws Throwable {
		verifyElementIsDisplayed(FAQS.Logo_FAQpage());
		verifyElementIsDisplayed(FAQS.Description_FAQpage());
		verifyElementIsDisplayed(FAQS.contactuslink_FAQdescription());
		//platform.checkFaqsPageContent();
	}

	@And("^It should display Question Answer section$")
	public void It_should_display_Question_Answer_section() throws Throwable {
		//platform.shouldDisplayFaqsContent();
		QAsession_FAQPage();
	}
	
	@And("^verify contact us button in FAQ page$")
	public void Verify_ContactUs_Button_FAQPage() throws Throwable {
		verifyElementIsDisplayed(FAQS.ContactUsButton_FaqPage());
		
	}
	
	

	@Then("^user should be able to view FAQs page component sequence$")
	public void user_should_be_able_to_view_FAQs_page_component_sequence() throws Throwable {
		platform.FaqsPageComponentSequence();
	}

	@And("^It should display to the user Sub-text below to FAQs page header$")
	public void it_should_display_to_the_user_Sub_text_below_to_FAQs_page_header() throws Throwable {
		platform.subtextBelowToFAQsPageHeader();
	}

	@Then("^user should able to view Contact Us  CTA button$")
	public void user_should_able_to_view_Contact_Us_CTA_button() throws Throwable {
		platform.contactUsCTAbuttonFAQs();
	}

}