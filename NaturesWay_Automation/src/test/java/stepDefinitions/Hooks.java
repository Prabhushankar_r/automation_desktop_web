package stepDefinitions;

import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import platforms.WebPortal;

public class Hooks extends WebPortal {

	@After
	@And("^close browser$")
	public static void afterScenario(Scenario sc) throws IOException
	{
		if (sc.isFailed()) {
			System.out.println("Scenario Failed");
			takeScreenshot(sc);
			quitBrowser();
		}
		else {
			System.out.println("Scenario Passed");
			quitBrowser();
		}

	}

}


