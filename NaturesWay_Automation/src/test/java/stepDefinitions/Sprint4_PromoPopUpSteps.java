package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import platforms.WebPortal;

public class Sprint4_PromoPopUpSteps extends WebPortal {

	public Sprint4_PromoPopUpSteps() throws FileNotFoundException, IOException, ParseException {
		super();
	
	}
	
	@Then("^verify promo pop up is displayed$")
	public void verify_PromoPooUp_Displayed() throws Throwable {
		verifyElementIsPresent(promopopup.PromoPopUp());
	}
	
	@And("^verify email field is displayed in pop up screen$")
	public void verify_Email_Field_Displayed_PromoPopUp() throws Throwable {
		verifyElementIsPresent(promopopup.EmailField_PromoPopUp());	
	}
	
	@And("^verify the components in promo screen$")
	public void verify_Components_PromoUpScreen() throws Throwable {
		verifyElementIsPresent(promopopup.LogoImage_PromoPopUp());
		verifyTextIsDisplayed(promopopup.HeaderTitle_PromoPopUp());	
		verifyTextIsDisplayed(promopopup.Description_PromoPopUp());	
		verifyElementIsPresent(promopopup.Close_Icon_Promo_Popup());	
		verifyElementIsPresent(promopopup.EmailField_PromoPopUp());	
		verifyElementIsPresent(promopopup.ClaimMyOffer_PromoPopUp());	
		verifyElementIsPresent(promopopup.NoThanks_PromoPopUp());	
	}
	
	@When("^verify the Claim my offer button is disabled$")
	public void verify_ClaimMyOfferButton_Disabled_EmailFieldIsEmpty() throws Throwable {
	 getAttributeValue(promopopup.ClaimMyOffer_PromoPopUp(),"disabled");
	}
	
	@When("^verify the Claim my offer button is enabled$")
	public void verify_ClaimMyOfferButton_Enabled_EmailFieldIsEmpty() throws Throwable {
		promopopup.ClaimMyOffer_PromoPopUp().isEnabled();
	}
	
	@When("^click on Claim my offer button$")
	public void click_Claim_My_Offer_Button() throws Throwable {
		clickButtonWithOutScroll(promopopup.ClaimMyOffer_PromoPopUp());
	}
	
	@When("^click on No Thanks button$")
	public void click_No_Thanks_Button() throws Throwable {
		clickButtonWithOutScroll(promopopup.NoThanks_PromoPopUp());
	}
	
	@When("^click on close icon ins promo screen$")
	public void click_CloseIcon_PromoPopUpScreen() throws Throwable {
		clickButtonWithOutScroll(promopopup.Close_Icon_Promo_Popup());
	}
	
	
	@Then("^promo pop up should disappear$")
	public void PromoPopUp_Should_Disappear() throws Throwable {
		verifyElementIsNotDisplayed(promopopup.PromoPopUp());

	}
	
	@When("^enter email address \"(.*?)\" in promo screen$")
	public void Enter_Email_Address_PromoScreem(String emailaddress) throws Throwable {
		enterText(promopopup.EmailField_PromoPopUp(),emailaddress);		
	}

	@When("^email field is empty in promo screen$")
	public void Email_Address_Field_Empty() throws Throwable {
		enterText(promopopup.EmailField_PromoPopUp(),"");		
	}
	
	@Then("^display \"(.*?)\" for empty email field in promo screen$")
	public void Display_Error_Message_For_EmptyEmailField_Promo(String errormessage) throws Throwable {
		selectATab(promopopup.EmailField_PromoPopUp());	
		verifyExpectedText(errormessage,promopopup.ErrMsgEmailField_PromoPopUp());
		
	}
	
	
}
