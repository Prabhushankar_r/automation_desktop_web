package stepDefinitions;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import platforms.WebPortal;

public class Sprint5_ShippingInformationSteps extends WebPortal 
{
	public static String firstName_ShippingInfo, lastName_ShippingInfo, address_ShippingInfo,zipcode_ShippingInfo;

	public Sprint5_ShippingInformationSteps() throws FileNotFoundException, IOException, ParseException {
		super();
		}
	
	@When("^click on Add new shipping address$")
	public void click_AddNewShippingAddress_Link() throws Throwable {
		waitTillElementExist(shippinginformation.AddNewShippingAddress_Link_Checkout());
		clickButtonWithOutScroll(shippinginformation.AddNewShippingAddress_Link_Checkout());
    	}
	
	@Then("^enter first name in shipping address \"(.*?)\"$")
	public void Enter_FirstName_ShippingAddress(String firstname) throws Throwable 
	{
		firstName_ShippingInfo= firstname;
		waitTillElementExist(shippinginformation.FirstName_ShippingInfo());
		enterText(shippinginformation.FirstName_ShippingInfo(),firstname);
    	}
	
	@And("^enter last name in shipping address \"(.*?)\"$")
	public void Enter_LastName_ShippingAddress(String lastname) throws Throwable 
	{
		lastName_ShippingInfo = lastname;
		waitTillElementExist(shippinginformation.LastName_ShippingInfo());
		enterText(shippinginformation.LastName_ShippingInfo(),lastname);
	}
	
	@Then("^enter address in shipping information \"(.*?)\"$")
	public void Enter_Address_ShippingAddress(String Address) throws Throwable 
	{
		address_ShippingInfo = Address;
		waitTillElementExist(shippinginformation.Address_ShippingInfo());
		enterText(shippinginformation.Address_ShippingInfo(),Address);
    }
	
	@And("^enter zipcode in shipping information \"(.*?)\"$")
	public void Enter_Zipcode_ShippingAddress(String zipcode) throws Throwable 
	{
		zipcode_ShippingInfo = zipcode;
		waitTillElementExist(shippinginformation.Zipcode_ShippingInfo());
		enterText(shippinginformation.Zipcode_ShippingInfo(),zipcode);
    }
	
	@Then("^state and city fields are autopopulated in shipping address$")
	public void Autopopulate_State_City_FieldsValues_ShippingAddress() throws Throwable 
	{
		verifyAutopopulation_ShippingAddress();
	}
	
	@And("^enable checkbox for primary shipping address$")
	public void Enable_CheckBox_PrimaryShippingAddress() throws Throwable {
		clickButtonWithOutScroll(shippinginformation.EnableCheckBox_ShippingInfo());
		
	}
	
	@Then("^verify company name is disabled in shipping address$")
	public void verify_CompanyName_Disabled_ShippingAddress() throws Throwable {
		String attributeValue = getAttributeValue(shippinginformation.CompanyName_ShippingInfo(),"disabled");
		Assert.assertEquals(attributeValue, "true", "Company name field is not disabled");
		
	}	
	
	@And("^click on CONTINUE button in add new shipping address$")
	public void click_ContinueButton_ShippingAddress_AddNewAddress() throws Throwable {
		clickButtonWithOutScroll(shippinginformation.ContinueButton_ShippingInformation());
		
	}
	
	@Then("^Verify added shipping address is displayed for user with edit link in checkoutPage$")
	public void verify_added_shipping_address_is_displayed_for_user_with_edit_link() throws Exception 
	{
		waitVisibilityOfElement(shippinginformation.firstName_Updated());
	    verifyExpectedText(firstName_ShippingInfo, shippinginformation.firstName_Updated());
	    verifyExpectedText(lastName_ShippingInfo, shippinginformation.lastName_Updated());
	    //verifyExpectedText(address_ShippingInfo, shippinginformation.address1_Updated());
	    verifyContainsText(shippinginformation.zipcode_Updated(), zipcode_ShippingInfo);
	    verifyElementIsDisplayed(shippinginformation.editLink_ShippingInfo());
	}
	
	@Then("^Verify the components in the shipping information section$")
	public void verify_the_components_in_the_shipping_information_section() throws Exception  
	{
	    Assert.assertTrue(verifyElementIsDisplayed(shippinginformation.shippingInfo_Header()), "Shipping Information hearder is NOT displayed");
	    Assert.assertTrue(verifyElementIsDisplayed(shippinginformation.addressDropdown()), "Address dropdown is NOT displayed");
	    Assert.assertTrue(verifyElementIsDisplayed(shippinginformation.AddNewShippingAddress_Link_Checkout()), "Add new shipping address link is not displayed");
	    Assert.assertTrue(verifyElementIsDisplayed(shippinginformation.upsGround_radiobutton()), "upsGround radio button is NOT displayed");
        Assert.assertTrue(!shippinginformation.ContinueButton_ShippingInformation().isEnabled(), "Continue button is enabled");
	    
	}

	@Then("^verify the components in the Add new shipping information section$")
	public void verify_the_components_in_the_Add_new_shipping_information_section() throws Exception 
	{
		clickButtonWithOutScroll(shippinginformation.AddNewShippingAddress_Link_Checkout());
		waitTillElementExist(shippinginformation.CompanyName_ShippingInfo());
		String AttributeValue = getAttributeValue(shippinginformation.CompanyName_ShippingInfo(),"disabled");
		Assert.assertEquals(AttributeValue, "true", "Company name field is not disabled");
	    Assert.assertTrue(verifyElementIsDisplayed(shippinginformation.FirstName_ShippingInfo()), "first name field is NOT displayed");
	    Assert.assertTrue(verifyElementIsDisplayed(shippinginformation.LastName_ShippingInfo()), "last name field is NOT displayed");
	    Assert.assertTrue(verifyElementIsDisplayed(shippinginformation.Address_ShippingInfo()), "address1 field is NOT displayed");
	    Assert.assertTrue(verifyElementIsDisplayed(shippinginformation.Zipcode_ShippingInfo()), "Zip code field is NOT displayed");
	    Assert.assertTrue(verifyElementIsDisplayed(shippinginformation.cityEmptyField()), "city field is NOT displayed");
	    Assert.assertTrue(verifyElementIsDisplayed(shippinginformation.stateEmptyField()), "Sate field is NOT displayed");
	    Assert.assertTrue(verifyElementIsDisplayed(shippinginformation.EnableCheckBox_ShippingInfo()), "Primary address checkbox is NOT displayed");
	    Assert.assertTrue(!shippinginformation.ContinueButton_ShippingInformation().isEnabled(), "continue button is NOT displayed");
		
	}
	
	@Then("^verify default shipping address is displayed in shipping address dropdown$")
	public void verify_default_shipping_address_is_displayed_in_shipping_address_dropdown() throws Exception 
	{
		WebElement selectedElement = getDropdownSelectedValue(shippinginformation.addressDropdown());
		waitTillElementExist(selectedElement);
		Assert.assertTrue(verifyTextIsDisplayed(selectedElement), "Default Address is NOT displayed");
		
	 }

	
	
	
	
}