package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import platforms.WebPortal;

public class Sprint1_LegalSteps extends WebPortal {

	public Sprint1_LegalSteps() throws FileNotFoundException, IOException, ParseException {
		super();

	}

	@And("^verify the view of legal page title$")
	public void verify_the_display_of_title() throws Throwable {
		verifyTextIsDisplayed(legal.legalTitle());
	}

	@Then("^verify the display of tabs$")
	public void verify_the_display_of_tabs() throws Throwable {
	   lTabs();

	}

	@And("^click on legal link in globel footer$")
	public void click_on_legal_link_in_globel_footer() throws Throwable {
		clickOnLink(legal.legalLink_Footer(), legal.legalLink_Footer());
	}

	@And("^User able to click on Terms Of Use Icon and close$")
	public void user_able_to_click_on_Terms_Of_Use_Icon_and_close() throws Throwable {
		clickButtonWithOutScroll(legal.legalTermsOfUse());
		//platform.legalTermsOfUse();
	}

	@And("^User able to click on Resale Policy Icon and close$")
	public void user_able_to_click_on_Resale_Policy_Icon_and_close() throws Throwable {
		clickButtonWithOutScroll(legal.legalResalePolicy());

		//platform.legalResalePolicy();

	}

	@And("^User able to click on Privacy Policy Icon and close$")
	public void user_able_to_click_on_Privacy_Policy_Icon_and_close() throws Throwable {
		clickButtonWithOutScroll(legal.legalPrivacyPolicy());

		//platform.legalPrivacyPolicy();
	}

	@And("^User able to click on HIPPA Icon and close$")
	public void user_anle_to_click_on_HIPPA_Icon_and_close() throws Throwable {
		clickButtonWithOutScroll(legal.legalHippa());

		//platform.legalHippa();

	}

}
