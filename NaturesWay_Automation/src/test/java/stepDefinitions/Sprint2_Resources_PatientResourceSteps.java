package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import platforms.WebPortal;

public class Sprint2_Resources_PatientResourceSteps extends WebPortal {

	public Sprint2_Resources_PatientResourceSteps() throws FileNotFoundException, IOException, ParseException {
		super();
		}
	
	@And("^verify the display of resource header$")
	public void verify_display_of_resource_header() throws Throwable {
		verifyExpectedText("RESOURCES", patientresourcepage.Resources_Header());
	}
	
	@And("^verify the display of resource hero banner title$")
	public void verify_display_of_resource_herobanner_title() throws Throwable {
		verifyTextIsDisplayed(patientresourcepage.PatientResource_Title());
		}
	
	@And("^verify the display of resource hero banner description$")
	public void verify_display_of_resource_herobanner_description() throws Throwable {
		verifyTextIsDisplayed(patientresourcepage.PatientResource_Description());
	}
	
	
	@And("^verify the display Resource Type Header$")
	public void verify_the_display_Resource_Type_Header() throws Throwable {
	     //platform.patientRTHead();
	}

	@And("^verify the display of two filters$")
	public void verify_the_display_of_two_filters() throws Throwable {
		patientResourceTwoFilters();
	}

	@And("^verify the display of Resources should be displayed alphabetically$")
	public void verify_the_display_of_Resources_should_be_displayed_alphabetically() throws Throwable {
		patientRTResourcesAlpha();
	}

	@And("^click product information$")
	public void click_product_information() throws Throwable {
		clickOnList_Index(patientresourcepage.patientResource_tabslist(),0);
	}

	@And("^verify the display of Patient Resource PDFs$")
	public void verify_the_display_of_Patient_Resource_PDFs() throws Throwable {
		productinformation_PDFs();
	}

	@And("^verify the display of Product Information resources$")
	public void verify_the_display_of_Product_Information_resources() throws Throwable {
		patientRPIR();
	}

	@And("^click on Health Conditions$")
	public void click_on_Health_Conditions() throws Throwable {
		clickOnList_Value(patientresourcepage.patientResource_tabslist(),"HEALTH CONDITIONS");
	}


	@Then("^verify the display of Patient Resource PDFs related to Health Conditions$")
	public void verify_the_display_of_Patient_Resource_PDFs_related_to_Health_Conditions() throws Throwable {
		healthconditions_PDFs();
	}

	@And("^verify the display of title for product information tab$")
	public void verify_displayof_Title_In_ProductInformation_Section() throws Throwable {
		verifyTextAreDisplayed(patientresourcepage.PDF_TitleValue());
	}
	
	@And("^verify the display of title for health condition tab$")
	public void verify_displayof_Title_In_HealthCondition_Section() throws Throwable {
		verifyTextAreDisplayed(patientresourcepage.PDF_TitleValue());
	}
	
	@Then("^verify the display of images for product information tab$")
	public void verify_displayof_images_In_ProductInformation_Section() throws Throwable {
		verifyElementsAreDisplayed(patientresourcepage.PDF_Image());
		}
	
	@Then("^verify the display of images for health condition tab$")
	public void verify_displayof_images_In_healthcondition_Section() throws Throwable {
		verifyElementsAreDisplayed(patientresourcepage.PDF_Image());
	}
	
	@Then("^verify the count for product information$")
	public void verify_Count_ProductInformation() throws Throwable {
		verifyElementIsPresent(patientresourcepage.count_details_PatientResouce());
	}
	
	@Then("^verify the count for Health Conditions$")
	public void verify_Count_Health_Conditions() throws Throwable {
		verifyElementIsPresent(patientresourcepage.count_details_PatientResouce());
	}
}
