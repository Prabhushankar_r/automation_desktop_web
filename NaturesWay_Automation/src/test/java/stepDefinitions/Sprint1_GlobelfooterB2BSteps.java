package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Sprint1_GlobelfooterB2BSteps extends AbstractStepDefinition {

	public Sprint1_GlobelfooterB2BSteps() throws FileNotFoundException, IOException, ParseException {
		super();
	}

	@Then("^user should able to view the Newsletter section$")
	public void user_should_able_to_view_the_Newsletter_section() throws Throwable {
		platform.verifyTheNewsLetterSection();
	}

	@Then("^user enters email$")
	public void user_enters_email() throws Throwable {
		platform.enterTheEmailInTextBox();
	}

	@Then("^click on submit$")
	public void click_on_submit() throws Throwable {
		platform.click();
	}

	@Then("^user should able to view social media icons$")
	public void user_should_able_to_view_social_media_icons(DataTable smicons) throws Throwable {
		platform.funSocialMediaIcons(smicons);
	}
	
	@Then("^user should be able to view the following components$")
	public void user_should_be_able_to_view_following_components(DataTable components) throws Throwable {
		verifyFooterComponents(components);
	}

	@Then("^user should able to view the anchor Tags$")
	public void user_should_able_to_view_the_anchor_Tags(DataTable anchorTags) throws Throwable {
		platform.displayOfAnchorTags(anchorTags);
	}

	@When("^user click on anchor Tags$")
	public void user_click_on_anchor_Tags(DataTable Tags) throws Throwable {
		platform.clickOnAnchorTags(Tags);
	}

	@Then("^user should able to view the Key statement$")
	public void user_should_able_to_view_the_Key_statement() throws Throwable {
		platform.keyStatement();
	}

	@And("^user should able to see the copyright notices$")
	public void user_should_able_to_view_the_Key_and_copyright_notices() throws Throwable {
		platform.copyRight();
	}

}
