package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class Sprint2_AboutUsQualitySteps extends AbstractStepDefinition {
	
	public Sprint2_AboutUsQualitySteps() throws FileNotFoundException, IOException, ParseException {
		super();
		
	}

	@And("^click on Quality$")
	public void click_on_Quality() throws Throwable {
		holdAndClick(aboutus.clickOnAboutUs(),aboutusquality.Qualitysubmenu_header());
	}
	
	@And("^verify the display of quality hero banner title$")
	public void verify_the_display_of_quality_title() throws Throwable {
		Assert.assertTrue(verifyTextIsDisplayed(aboutusquality.AbUsQualityTitle()));
	}
	

	/*@And("^verify the display of quality title$")
	public void verify_the_display_of_quality_title() throws Throwable {
		verifyTextIsDisplayed(aboutusquality.AbUsQualityTitle());
	}
*/
	@And("^verify the display of quality hero banner description$")
	public void verify_the_display_of_quality_description() throws Throwable {
		Assert.assertTrue(verifyTextIsDisplayed(aboutusquality.AbUsQualityDesc()));
	}

	@And("^click on video$")
	public void click_on_vedio() throws Throwable {
		waitFor(2000);
		waitForPageLoaded();
		clickButtonWithOutScroll(aboutusquality.AbUsClickOnVedio());
		
	}
	
	@Then("^verify the title for contents with left image in qualitypage$")
	public void verify_Title_ContentsWithLeftImages() throws Throwable {
		Assert.assertTrue(verifyTextAreDisplayed(aboutusquality.QualityPage_Title_leftsection()));
	}

	@And("^verify the description for contents with left image in qualitypage$")
	public void verify_Description_ContentsWithLeftImages() throws Throwable {
		Assert.assertTrue(verifyTextAreDisplayed(aboutusquality.QualityPage_Description_leftsection()));
	}

	@Then("^verify the title for contents with right image in qualitypage$")
	public void verify_Title_ContentsWithRightImages() throws Throwable {
		Assert.assertTrue(verifyTextAreDisplayed(aboutusquality.QualityPage_Title_rightsection()));
	}

	@And("^verify the description for contents with right image in qualitypage$")
	public void verify_Description_ContentsWithRightImages() throws Throwable {
		Assert.assertTrue(verifyTextAreDisplayed(aboutusquality.QualityPage_Description_rightsection()));
	}

	


	@And("^verify the display of quality footer banner title$")
	public void verify_the_display_of_quality_footer_banner_title() throws Throwable {
		waitFor(2000);
		waitForPageLoaded();
		Assert.assertTrue(verifyElementIsDisplayed_WithOutMoveToEle(aboutusquality.AbUsQuality_FooterBannerTitle()));
	}

	@And("^verify the display of quality footer banner description$")
	public void verify_the_display_of_quality_footer_banner_description() throws Throwable {
		Assert.assertTrue(verifyElementIsDisplayed_WithOutMoveToEle(aboutusquality.AbUsQuality_FooterBannerDescription()));
	}

	@And("^verfy the display of quality footer banner images$")
	public void verfy_the_display_of_quality_footer_banner_images() throws Throwable {
		Assert.assertTrue(verifyElementsAreDisplayed(aboutusquality.AbUsQuality_FooterBannerImages()));
	}

	@And("^verify the display of quality quality footer Ajax component title$")
	public void verify_the_display_of_quality_quality_footer_Ajax_component_title() throws Throwable {
		waitFor(2000);
		waitForPageLoaded();
		Assert.assertTrue(verifyElementIsDisplayed_WithOutMoveToEle(aboutusquality.AbUsQuality_FooterAjaxBannerTitle()));
	}

	@And("^verify the display of quality quality footer Ajax component description$")
	public void verify_the_display_of_quality_quality_footer_Ajax_component_description() throws Throwable {
		Assert.assertTrue(verifyElementIsDisplayed_WithOutMoveToEle(aboutusquality.AbUsQuality_FooterAjaxBannerDescription()));
	}

	@And("^verfy the display of quality quality footer Ajax component images$")
	public void verfy_the_display_of_quality_quality_footer_Ajax_component_images() throws Throwable {
		Assert.assertTrue(verifyElementsAreDisplayed_WithOutMoveToEle(aboutusquality.AbUsQuality_FooterAjaxBannerImages()));
	}

}
