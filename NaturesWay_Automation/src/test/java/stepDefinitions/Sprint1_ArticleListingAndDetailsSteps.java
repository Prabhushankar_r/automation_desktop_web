package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.junit.Assert;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import platforms.WebPortal;

public class Sprint1_ArticleListingAndDetailsSteps extends WebPortal {

	public Sprint1_ArticleListingAndDetailsSteps() throws FileNotFoundException, IOException, ParseException {
		super();
	}

	@And("^click on resources$")
	public void click_on_resources() throws Throwable {
		clickOnLinkWithOutScroll(articlelisteing.resource_header());
		//navigateToTheResourceAndClick();
	}

	@And("^click on article tile$")
	public void click_on_article_tile() throws Throwable {
		holdAndClick(articlelisteing.resource_header(),articlelisteing.articles());
		//ClickonArticleTile();
	}

	@Then("^user should able to view the article title$")
	public void user_should_able_to_view_the_article_title() throws Throwable {
		verifyTextIsDisplayed(articlelisteing.articleTitle());
		//articleTitle();
	}

	@And("^click on readmore button$")
	public void click_on_readmore_button() throws Throwable {
		
		readMore();
	}

	@Then("^user should able to view the components$")
	public void user_should_adle_to_view_the_components(DataTable tiledescription) throws Throwable {
		verifyArticlePageDetails(tiledescription);
	}

	@Then("^user should able to see the bread crumb$")
	public void user_should_able_to_see_the_bread_crumb() throws Throwable {
		backToHome();
	}

	@Then("^user should able to view the authour name$")
	public void user_should_able_to_view_the_authour_name() throws Throwable {
		authourName();
	}

	@When("^I click on article image$")
	public void i_click_on_article_image() throws Throwable {
		clickOnArticleImage();
	}

	@And("^click on share icon$")
	public void click_on_share_icon() throws Throwable {
		clickOnShareIcon();
	}

	@And("^I should see textbox for providing Email ID$")
	public void i_should_see_textbox_for_providing_Email_ID() throws Throwable {
		enabledEmailTextbox();
	}

	@And("^provide email id and click on send button$")
	public void provide_email_id_and_click_on_send_button() throws Throwable {
		clickOnEmailButton();
	}

	@And("^It should display Email sent successfully success message$")
	public void it_should_display_Email_sent_successfully_success_message() throws Throwable {
		emailSucessMessageAd();
	}

	@And("^I should see social media icons$")
	public void i_should_see_social_media_icons() throws Throwable {
		iterationOfSocialMediaIcons();
	}

	@And("^click on copy right link$")
	public void click_on_copy_right_link() throws Throwable {
		clickOnCopyRightLink();
	}

	@And("^It should display white colour tick mark$")
	public void it_should_display_white_colour_tick_mark() throws Throwable {
		enabledWhiteColourTickMark();
	}

	@And("^I should see Featured products$")
	public void i_should_see_Featured_products() throws Throwable {
		iterationOfFeatureProducts();
	}

	@And("^I should see View Product Details$")
	public void i_should_see_View_Product_Details() throws Throwable {
		fpButton();
	}

	@And("^I Click on View Product Details button$")
	public void i_Click_on_View_Product_Details_button() throws Throwable {
		fpButtonClick();
	}


	@Then("^user should able to view the scroll bar and should be able to drag down the scroll bar successfully$") 
	public void user_should_able_to_view_the_scroll_bar_and_should_be_able_to_drag_down_the_scroll_bar_successfully () throws Throwable 
	{ 
		scrollAllTheImages();
	}


	@Then("^User should able to darg left and right of the horizontal scroll bar$")
	public void user_should_able_to_darg_left_and_right_of_the_horizontal_scroll_bar()
	{
		Assert.assertTrue(false);

	}

	@Then("^user should see minimum (\\d+) articles count on article listing page$")
	public void user_should_see_minimum_articles_count_on_article_listing_page(int arg1) throws Throwable {
		articleCount();
	}

	@And("^click on BACK TO ARTICLES Bread Crumb$")
	public void click_on_BACK_TO_ARTICLES_Bread_Crumb() throws Throwable {
		backToArticles();
	}

	@Then("^verify the logo$")
	public void verify_the_logo() throws Throwable {
		verifyElementIsDisplayed(articlelisteing.relatedArticlesLogo());
	}

	@And("^verify the title$")
	public void verify_the_title() throws Throwable {
		verifyTextIsDisplayed(articlelisteing.relatedArticlesTitle());
	}

	@And("^no of articles$")
	public void no_of_articles() throws Throwable {
		noOfrelatedArticles();
	}

	@Then("^verify the display of related articles discription$")
	public void verify_the_display_of_related_articles_discription() throws Throwable {
		relatedArticlesDesc();
	}

	@And("^click on related articles readmore button$")
	public void click_on_related_articles_readmore_button() throws Throwable {
		clickOnList_Index(articlelisteing.relatedArticlesReadMore(),1);
		//relatedArticlesReadMore();
	}

	@Then("^Appropriate article should be displayed$")
	public void appropriate_article_should_be_displayed()
	{

	}


	@And("^I click on article bread crumb$")
	public void I_click_on_article_bread_crumb() throws Throwable 
	{
		articlebc();
	}

}
