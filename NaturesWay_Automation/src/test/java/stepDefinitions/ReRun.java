package stepDefinitions;
import org.junit.Test;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@CucumberOptions(
    monochrome = true,
    features = {"@target/rerun.txt",},
    plugin= {"pretty","html:target1/site/cucumber-pretty", "json:target1/cucumber/cucumber.json", "junit:target1/cucumber.xml","com.cucumber.listener.ExtentCucumberFormatter:target1/html/rerun.html"}
)

public class ReRun {

	

}
