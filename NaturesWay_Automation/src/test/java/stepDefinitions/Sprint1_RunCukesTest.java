package stepDefinitions;

import java.io.File;
import java.io.IOException;

import helpers.CommunicationHelper;
import platforms.WebPortal;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;

import org.json.simple.parser.ParseException;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

//@RunWith(ExtendedCucumber.class)
@RunWith(ExtendedCucumber.class)
@ExtendedCucumberOptions(
		jsonReport= "target/cucumber.json",
		    retryCount = 0, //To rerun the scenario ,if the scenario fails
	        detailedReport = true,
	        detailedAggregatedReport = true,
	        overviewReport = true,
	        coverageReport = true,
	        //screenShotLocation="ScreenShots/",t
	        //screenShotSize="300px",
	        jsonUsageReport = "target/cucumber-usage.json",
	        usageReport = true,
	        toPDF = true,	
	        //excludeCoverageTags = {""},
	        //includeCoverageTags = {"","@failedCase"},
	        outputFolder = "target")

@CucumberOptions(features = "src/test/resources/features", monochrome = true, dryRun = false, plugin = {
		"html:target/cucumber-html-report", "json:target/cucumber.json", "usage:target/cucumber-usage.json", "pretty",
		"com.cucumber.listener.ExtentCucumberFormatter:target/report/report.html"}, tags = {"@Sprint4_Checkout"})


public class Sprint1_RunCukesTest {

	@AfterClass 
	public static void generateReport() throws IOException, ParseException 
	{
		Reporter.loadXMLConfig(new File("src/test/resources/extent-config.xml"));
		Reporter.setSystemInfo("user", System.getProperty("user.name"));
		Reporter.setSystemInfo("os", "windows");
		Reporter.setTestRunnerOutput("Nature's Way test runner output message");
		CommunicationHelper.closeApp();
	}
	
	@AfterClass
	public static void TearDownClass() throws IOException, ParseException {
		WebPortal.getDriver().quit();
     
	}
	
}
