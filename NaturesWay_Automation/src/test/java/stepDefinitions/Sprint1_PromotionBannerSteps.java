package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import platforms.WebPortal;

public class Sprint1_PromotionBannerSteps extends WebPortal {

	public Sprint1_PromotionBannerSteps() throws FileNotFoundException, IOException, ParseException {
		super();

	}

	@Then("^verify the display of promotionBanner$")
	public void verify_the_display_of_promotionBanner() throws Throwable {
		verifyElementIsDisplayed(promotionbanner.PromotionBanner_Text());
		//platform.pBVerified();
	}

	@And("^I click on PromotionBanner$")
	public void i_click_on_PromotionBanner() throws Throwable {
		//platform.clickOnPB();
	}

}
