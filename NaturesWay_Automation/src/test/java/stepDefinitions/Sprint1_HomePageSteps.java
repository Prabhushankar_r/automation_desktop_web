package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import platforms.WebPortal;

import org.json.simple.parser.ParseException;

public class Sprint1_HomePageSteps extends WebPortal {

	public Sprint1_HomePageSteps() throws FileNotFoundException, IOException, ParseException {
		super();
	}

	@Then("^I should see the Our story title$")
	public void i_should_see_the_Our_story_title() throws Throwable {
		scrollToElement(homePages.title());
		verifyTextIsDisplayed(homePages.title());
			}

	@Then("^I should see the Our story short description$")
	public void i_should_see_the_Our_story_short_description() throws Throwable {
		scrollToElement(homePages.description());
		verifyTextIsDisplayed(homePages.description());
		//platform.verifyOurStoryDescription();
	}

	@Then("^I should see Our Story section should show an image or a video component to user$")
	public void i_should_see_Our_Story_section_should_show_an_image_or_a_video_component_to_user() throws Throwable {
		verifyImage_Or_Video();
		//platform.clickOnVedio();
	}

	@And("^I click on homepage \"(.*?)\" Button$")
	public void I_click_on_homepage_Button(String label) throws Throwable {
		clickButtonWithOutScroll(homePages.clickOurStoryReadMore());
		//platform.clickOurStoryReadMore();
	}

	@Then("^user should able to scroll the feature products$")
	public void user_should_able_to_scroll_the_feature_products() throws Throwable {
		scrollTheImage();
	}

	@Then("^I should see the Health Condition icon$")
	public void i_should_see_the_Health_Condition_icon() throws Throwable {
		//platform.verifyTheDisplayOfHealthConditionIcon();

	}

	@Then("^I should see checkiterations button of Health Condition$")
	public void i_should_see_Read_More_button_of_Health_Condition() throws Throwable {
		//platform.checkIterations();

	}

	@Then("^I should see the image carousel$")
	public void i_should_see_the_image_carousel() throws Throwable {	
		//verifyimages_Carousel();
		//ReadMore_Carousel();
		ReadMore_CarouselOne();
	}

	@And("^I should see the carousel title$")
	public void i_should_see_the_carousel_text() throws Throwable {
		verifytitle_Carousel();
	}

	@And("^I should see the carousel description$")
	public void i_should_see_the_carousel_links() throws Throwable {
		verifydescription_Carousel();	
	}

	@Then("^I Click on \"(.*?)\" button$")
	public void I_Click_on_read_more_button(String label) throws Throwable {
		click_OurStory_ReadMore_button();
	}

	@And("^I click Navigation items in Image carousel$")
	public void it_should_goto_page_defined_by_the_author() throws Throwable {
		//platform.verifyB2B_Image_slider_navigation();
	}

	@Then("^I should see the Product images$")
	public void i_should_see_the_Product_images() throws Throwable {
		verifyElementsAreDisplayed(homePages.featureProductimages());
		
		//platform.featureProductSymbol();
	}

	@Then("^I should see the background images$")
	public void i_should_see_the_background_images() throws Throwable {
		verifyElementsAreDisplayed(homePages.featureProductBackGroundImage());
	}

	@And("^I should see Featured products title$")
	public void i_should_see_Featured_products_Title() throws Throwable {
		verifyTextAreDisplayed(homePages.Title_FeaturedProduct());
	}
	
	
	@Then("^I should see Short description of product$")
	public void i_should_see_Short_description_of_product() throws Throwable {
		verifyTextAreDisplayed(homePages.featureProductDesc());

	}

	@And("^click on view product details button$")
	public void i_should_see_View_Product_Details_button() throws Throwable {
		clickOnList_Index(homePages.viewproductdetails_Button(),0);
		//platform.featureProductButton2();
	}

	@Then("^I should see the Featured Products icon$")
	public void i_should_see_the_Featured_Products_icon() throws Throwable {
		verifyElementIsDisplayed(homePages.featureProductIcon());
		//platform.featureProductIcon();
	}

	@Then("^verify the display of feature product title$")
	public void verify_the_display_of_feature_product_title() throws Throwable {
		verifyTextIsDisplayed(homePages.featureProductTitle());
		//platform.featureProductTitle();
	}

	@Then("^verify the display of health condition Title$")
	public void verify_the_display_of_health_condition_Title() throws Throwable {
		//platform.hct();
	}

	@Then("^verify the display of health condition description$")
	public void verify_the_display_of_health_condition_description() throws Throwable {
		//platform.hcd();
	}

	@Then("^verify the display of feature product description$")
	public void verify_the_display_of_fph_description() throws Throwable {
		verifyTextIsDisplayed(homePages.fphDesc());
		//platform.fphDesc();
	}

}
