package stepDefinitions;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;

import cucumber.api.java.en.*;
import platforms.WebPortal;

public class Sprint2_GlobalHeaderSearchSteps extends WebPortal {
	
	public String ClickedProduct_Search;
	public String ClickedArticle_Search,enteredcharacter;

	public Sprint2_GlobalHeaderSearchSteps() throws FileNotFoundException, IOException, ParseException 
	{
		super();
		
	}

	@Given("^verify search icon is displayed$")
	public void verify_search_icon_is_displayed() throws Exception  
	{
		Assert.assertTrue(verifyElementIsDisplayed(globelheadersearch.searchIcon()));
	    
	}

	@When("^click on search icon$")
	public void click_on_search_icon() throws Exception  
	{
	    clickButtonWithOutScroll(globelheadersearch.searchIcon());
	}
	
	@When("^select the product from suggested search list$")
	public void select_Product_From_Search_SggestedList() throws Exception  
	{
		verifyElementsAreDisplayed(globelheadersearch.suggestedpopularproducts_Search());
		ClickedProduct_Search = clickOnListReturnText_Index(globelheadersearch.suggestedpopularproducts_Search(), 0);
		Assert.assertEquals(ClickedProduct_Search.toLowerCase(),productdetailpage.productTitle_PDP().getText().toLowerCase());	
	}
	
	

	@Then("^verify the search pane is displayed$")
	public void verify_the_search_pane_is_displayed() throws Exception  
	{
		Assert.assertTrue(verifyElementIsDisplayed(globelheadersearch.searchPane()));
	}

	@Then("^Verify the search icon is displayed besides of search pane$")
	public void verify_the_search_icon_is_displayed_besides_of_search_pane() throws Exception  
	{ 
		Assert.assertTrue(verifyElementIsDisplayed(globelheadersearch.searchIcon()));
	    
	}	

	@Then("^verify the \"([^\"]*)\" label$")
	public void verify_the_label(String text) 
	{
		waitVisibilityOfElement(globelheadersearch.popularProductLabel());
	    verifyExpectedText(text, globelheadersearch.popularProductLabel());
	}

	@Then("^verify the display of 'Poupular Products' name and images$")
	public void verify_the_display_of_Poupular_Products_name_and_images() throws Exception  
	{
		waitVisibilityOfALLElement(globelheadersearch.popularProductsNameList());
		listcontainsRequiredItems(globelheadersearch.popularProductsNameList(), 1);
		waitVisibilityOfALLElement(globelheadersearch.popularProductsImageList());
		listcontainsRequiredItems(globelheadersearch.popularProductsImageList(),1);
		   
	}

	@When("^click on anyone product listed$")
	public void click_on_anyone_product_listed() throws Exception  
	{
		waitVisibilityOfALLElement(globelheadersearch.popularProductsNameList());
		ClickedProduct_Search = clickOnListReturnText_Index(globelheadersearch.popularProductsNameList(), 1);
	    
	}

	@Then("^verify user should navigate to approrpiate Product details page$")
	public void verify_user_should_navigate_to_approrpiate_Product_details_page()  
	{
	    implicitWait(50);
	    waitVisibilityOfElement(globelheadersearch.clickedProduct_Name());
	    verifyExtendedUrl("products");
	    verifyExpectedText(ClickedProduct_Search, globelheadersearch.clickedProduct_Name());
	    
	}

	@Then("^verify \"([^\"]*)\" label$")
	public void verify_label(String text) 
	{
		waitVisibilityOfElement(globelheadersearch.popularArticleLabel());
	    verifyExpectedText(text, globelheadersearch.popularArticleLabel());
	}
	

	@Then("^verify the display of 'Poupular Articles' name$")
	public void verify_the_display_of_Poupular_Articles_name() throws Exception  
	{
		waitVisibilityOfALLElement(globelheadersearch.popularArticlesList());
		listcontainsRequiredItems(globelheadersearch.popularArticlesList(), 1);
	}

	@When("^click on anyone article listed$")
	public void click_on_anyone_article_listed() throws Exception 
	{
		waitVisibilityOfALLElement(globelheadersearch.popularArticlesList());
		ClickedArticle_Search = clickOnListReturnText_Index(globelheadersearch.popularArticlesList(), 1);
	    
	}

	@Then("^verify user should navigate to approrpiate article details page$")
	public void verify_user_should_navigate_to_approrpiate_article_details_page() 
	{
		waitFor(20000);
		implicitWait(50);
		waitVisibilityOfElement(globelheadersearch.clickedArticle_Name());
	    verifyExtendedUrl("articles");
	    verifyExpectedText(ClickedArticle_Search, globelheadersearch.clickedArticle_Name());
	    
	}

	@When("^Enter the first three letter of product \"([^\"]*)\" in the search pane$")
	public void enter_the_first_three_letter_of_product_in_the_search_pane(String text) throws Exception 
	{
		enteredcharacter=text;
	    implicitWait(50);
	    waitVisibilityOfElement(globelheadersearch.searchPane());
	    enterText(globelheadersearch.searchPane(), text);
	}

	@When("^Click the keyboard enter$")
	public void click_the_keyboard_enter() throws Exception  
	{
		//keyboradActions("enter");
		globelheadersearch.searchPane().sendKeys(Keys.ENTER);
	    
	}

	@Then("^verify the appropriate search product is listed$")
	public void verify_the_appropriate_search_product_is_listed() throws Exception 
	{
		waitForPageLoaded();
		productlisting.enteredcharacter_text().getText().endsWith(enteredcharacter);
			    
	}

	@Then("^verify search pan is displayed with search icon$")
	public void verify_search_pan_is_displayed_with_search_icon() throws Exception 
	{
		waitVisibilityOfElement(globelheadersearch.searchPane());
	    Assert.assertTrue(verifyElementIsDisplayed(globelheadersearch.searchPane()));
	    Assert.assertTrue(verifyElementIsDisplayed(globelheadersearch.searchIcon()));
	    
	}

	@When("^click the close icon of the search$")
	public void click_the_close_icon_of_the_search() 
	{
	    clickButtonWithOutScroll(globelheadersearch.searchCloseIcon());
	    
	}

	@Then("^verify the search pan is disappeared$")
	public void verify_the_search_pan_is_disappeared() throws Exception 
	{
		verifyElementIsNotDisplayed(globelheadersearch.searchPaneList());
	}

	@When("^click outside of search$")
	public void click_outside_of_search() 
	{
	    clickButtonWithOutScroll(globelheadersearch.outsideOfSearch());
	    
	}

}
