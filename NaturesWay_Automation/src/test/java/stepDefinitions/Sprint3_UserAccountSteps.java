package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.junit.Assert;

import cucumber.api.DataTable;
//import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import platforms.WebPortal;

public class Sprint3_UserAccountSteps extends WebPortal {	
	
	public Sprint3_UserAccountSteps() throws FileNotFoundException, IOException, ParseException {
		super();
	}
	
	@Then("^verify user is logged successfully$")
	public void LoginSuccess() throws Throwable {
		waitForPageLoaded();
		explicitWait(megamenu.verifyQuickOrderPresent());
		Assert.assertTrue(verifyElementIsDisplayed(megamenu.verifyQuickOrderPresent()));
		Assert.assertTrue(verifyElementIsDisplayed(megamenu.clickOnWelcomeUser()));
		
    }

	@Then("^verify chevron is displayed$")
	public void verifyChevron_Displayed_Username() throws Throwable {
		Assert.assertTrue(verifyElementIsPresent(myaccount.chevron_username()));	
	}			
	
	@Then("^display the below components in account dropdown$")
	public void Display_dropdownmenu_UserAccount(DataTable submenu) throws Throwable {
		dropdownmenus_Useracct(submenu);
	}
	
 
	@When("^click on Account Information from dropdown$")
	public void clickaccountInformation_Dropdown() throws Throwable {
		ClickOnElement(myaccount.chevron_username());
		Assert.assertTrue(verifyElementIsPresent(myaccount.accountDetails_menu()));
       ClickOnElement(myaccount.accountDetails_menu());	
   }
	
	@When("^click on Payment methods from dropdown$")
	public void clickPaymentMethods_Dropdown() throws Throwable {
		ClickOnElement(myaccount.chevron_username());
		Assert.assertTrue(verifyElementIsPresent(myaccount.paymentmethods_menu()));
       ClickOnElement(myaccount.paymentmethods_menu());	
   }
	
	
	@When("^verify the favorite Icon and click the favorite menu under MyAccount dropdown$")
	public void verify_the_favorite_Icon_and_click_the_favorite_menu_under_MyAccount_dropdown() throws Exception 
	{
		waitVisibilityOfElement(myaccount.chevron_username());
		moveToElement(myaccount.chevron_username());
     	Assert.assertTrue(verifyElementIsPresent(myaccount.usericon_Favorite()));
		Assert.assertTrue("Favorite menu is not available", verifyElementIsPresent(myaccount.favorites_menu()));
	    ClickOnElement(myaccount.favorites_menu());	
	}
	
	
	@When("^click on Favorites from dropdown$")
	public void clickCFavorites_Dropdown() throws Throwable {
		ClickOnElement(myaccount.chevron_username());
		Assert.assertTrue(verifyElementIsPresent(myaccount.favorites_menu()));
	    ClickOnElement(myaccount.favorites_menu());	

   }
	
	
	@When("^click on shipping addresses from dropdown$")
	public void Click_ShippingAddresses_Dropdown() throws Throwable {
		ClickOnElement(myaccount.chevron_username());
		Assert.assertTrue(verifyElementIsPresent(myaccount.shippingaddresses_menu()));
	    ClickOnElement(myaccount.shippingaddresses_menu());	
	}
	
	@When("^click on find a practitioner profile from dropdown$")
	public void Click_FindAPractitionerProfile_Dropdown() throws Throwable {
		ClickOnElement(myaccount.chevron_username());
		Assert.assertTrue(verifyElementIsPresent(myaccount.findAPractitionerProfile_menu()));
	    ClickOnElement(myaccount.findAPractitionerProfile_menu());	
	}
	
	@Then("^verify user icon for account information$")
	public void verify_usericon_Accountinfo() throws Throwable {
		ClickOnElement(myaccount.chevron_username());
		Assert.assertTrue(verifyElementIsPresent(myaccount.usericon_Accountinfo()));	
	}
	
	@Then("^display the \"(.*?)\" Page$")
	public void accountInformationPage(String accountdetails) throws Throwable {
		verifyExtendedUrl("account-details");
		verifyExpectedText(accountdetails,myaccount.activeLeftMenu());
	}
	
//	@Then("^display the \"(.*?)\" Page$")
//	public void DisplayPaymentPage(String payment) throws Throwable {
//		verifyExtendedUrl("payment");
//		verifyExpectedText(payment,myaccount.activeLeftMenu());
//
//	}
	
	
	@Then("^display \"(.*?)\" page$")
	public void FavoritePage(String favorite) throws Throwable {
		verifyExtendedUrl("favorites");
		verifyExpectedText(favorite,myaccount.activeLeftMenu());

	}
	
	@Then("^\"(.*?)\" Page should display$")
	public void FindAPractitionerProfilePage(String findapractitioner) throws Throwable {
		verifyExtendedUrl("find-practitioner-profile");
		verifyExpectedText(findapractitioner,myaccount.activeLeftMenu());
	}
	
	@Then("^display \"(.*?)\" Page$")
	public void PaymentMethodsPage(String paymentinformationpage) throws Throwable {
		verifyExtendedUrl("payment-methods");
		verifyExpectedText(paymentinformationpage,myaccount.activeLeftMenu());
	}
	
	@Then("^Display \"(.*?)\" Page$")
	public void ShippingAddressesPage(String shippingaddress) throws Throwable {
		verifyExtendedUrl("shipping-addresses");
		verifyExpectedText(shippingaddress,myaccount.activeLeftMenu());
	}
	
	@Then("^verify user icon for my order$")
	public void verify_usericon_MyOrder() throws Throwable {
		ClickOnElement(myaccount.chevron_username());
		Assert.assertTrue(verifyElementIsPresent(myaccount.usericon_MyOrders()));	
	}
	
	@Then("^click on My Orders from dropdown$")
	public void clickMyOrder_submenu() throws Throwable {
		ClickOnElement(myaccount.chevron_username());
		Assert.assertTrue(verifyElementIsPresent(myaccount.myorders_menu()));	
		ClickOnElement(myaccount.myorders_menu());	
	}

	@Then("^\"(.*?)\" Page should be displayed$")
	public void verifyMyOrderPage(String myorder) throws Throwable {
		verifyExtendedUrl("myorders");
		verifyExpectedText(myorder,myaccount.activeLeftMenu());

	}
	
	
	@Then("^verify user icon for payment method$")
	public void verify_usericon_PaymentMethods() throws Throwable {
		ClickOnElement(myaccount.chevron_username());
		Assert.assertTrue(verifyElementIsPresent(myaccount.usericon_PaymentMethods()));	
	}
	
	@Then("^verify user icon for Favorites$")
	public void verify_usericon_Favorite() throws Throwable {
		waitVisibilityOfElement(myaccount.chevron_username());
		ClickOnElement(myaccount.chevron_username());
		Assert.assertTrue(verifyElementIsPresent(myaccount.usericon_Favorite()));	
	}
	 
	
	@Then("^verify user icon for practitioner profile$")
	public void verify_usericon_PractitonerProfile() throws Throwable {
		waitVisibilityOfElement(myaccount.chevron_username());
		ClickOnElement(myaccount.chevron_username());
		Assert.assertTrue(verifyElementIsPresent(myaccount.usericon_findAPractitionerProfile()));	
	}
	
	@Then("^verify user icon for shipping address$")
	public void verify_usericon_ShippingAddresses() throws Throwable {
		waitVisibilityOfElement(myaccount.chevron_username());
		ClickOnElement(myaccount.chevron_username());
		Assert.assertTrue(verifyElementIsPresent(myaccount.usericon_shippingaddressess()));	
	}
	
	@Then("^verify user icon for sign out$")
	public void verify_usericon_SignOut() throws Throwable {
		ClickOnElement(myaccount.chevron_username());
		Assert.assertTrue(verifyElementIsPresent(myaccount.usericon_SignOut()));	
		
	}
	
	
	
	@Then("^Edit the information and save it$")
	public void Edit_Information_AccountDetails_Saveit() throws Throwable
	{
		EditInformation_AccountDetails();		
	}
	
	@Then("^Verify the information of sales representative 'Photo Image', 'Name' , 'Job title', 'Address', 'Email Address' , 'Care Email Address' , 'Phone No'$")
	public void verify_the_information_of_sales_representative_Photo_Image_Name_Job_title_Address_Email_Address_Care_Email_Address_Phone_No() throws Exception
	{
		WaitTillElementExist(myaccount.salesRepresentative_Photo(), 10);
		String attributeValue = getAttributeValue(myaccount.salesRepresentative_Photo(), "");
	    Assert.assertTrue(attributeValue.contains("http://"));
	    verifyTextIsDisplayed(myaccount.salesRepresentative_Name());
	    verifyTextIsDisplayed(myaccount.salesRepresentative_JobTitle());
	    verifyTextIsDisplayed(myaccount.salesRepresentative_Address());
	    verifyContainsText(myaccount.salesRepresentative_Email(), "@");
	    verifyTextIsDisplayed(myaccount.salesRepresentative_PhoneNo());
	    
	}

	
}
