package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import platforms.WebPortal;

public class Sprint1_ContactUsSteps extends WebPortal {

	public Sprint1_ContactUsSteps() throws FileNotFoundException, IOException, ParseException {
		super();
	}

	@Then("^user should able to view the herobanner$")
	public void user_should_able_to_view_the_herobanner() throws Throwable {
		//cBanner();
		Assert.assertTrue(verifyElementIsDisplayed(contactus.contactUsHeroBanner()));
		
	}

	@Then("^verify the contactus title$")
	public void verify_the_contactus_title() throws Throwable {
		//cTitle();
		Assert.assertTrue(verifyTextIsDisplayed(contactus.contactUsTitle()));
	}

	@Then("^verify thre contactus description$")
	public void verify_thre_contactus_description() throws Throwable {
		//cDesc();
		Assert.assertTrue(verifyTextIsDisplayed(contactus.contactUsDescription()));
	}

	@Then("^user should able to view call us image and title$")
	public void user_should_able_to_view_call_us_image_and_title() throws Throwable {
		//callUsIT();
		Assert.assertTrue(verifyElementIsDisplayed(contactus.contactUsCallUsSymbol()));
		Assert.assertTrue(verifyTextIsDisplayed(contactus.contactUsCallUsTitle()));
	}

	@Then("^user should able to view call us description$")
	public void user_should_able_to_view_call_us_description() throws Throwable {
		//callUsDesc();
		Assert.assertTrue(verifyTextIsDisplayed(contactus.contactUsCallUsDescription()));
	}

	@Then("^user should able to view the fax$")
	public void user_should_able_to_view_the_fax() throws Throwable {
		//callUsFax();
		Assert.assertTrue(verifyTextIsDisplayed(contactus.contactUsCallUsFaxNo()));

	}

	@Then("^click on phone number$")
	public void click_on_phone_number() throws Throwable {
		//phoneNumber();
		clickButtonWithOutScroll(contactus.phoneNumber());
	}

	@Then("^user should able to view chat with us image and title$")
	public void user_should_able_to_view_chat_with_us_image_and_title() throws Throwable 
	{
		//chatUsIT();
		Assert.assertTrue(verifyElementIsDisplayed(contactus.contactUsChatWithUsSymbol()));
		Assert.assertTrue(verifyTextIsDisplayed(contactus.contactUsChatWithUsTitle()));
	}

	@Then("^user should able to view chat with us days and time$")
	public void user_should_able_to_view_chat_with_us_days_and_time() throws Throwable {
		//chatUsDT();
		Assert.assertTrue(verifyTextIsDisplayed(contactus.contactUsChatWithUsDescription()));
	}

	@When("^user click on chat button$")
	public void user_click_on_chat_button() throws Throwable {
		//chatUsButton();
		clickButtonWithOutScroll(contactus.contactUsChatWithUsButton());
	}

	@Then("^user should able to view email us image and title$")
	public void user_should_able_to_view_email_us_image_and_title() throws Throwable {
		//emailUsIT();
		Assert.assertTrue(verifyElementIsDisplayed(contactus.contactUsEmailSymbol()));
		Assert.assertTrue(verifyTextIsDisplayed(contactus.contactUsEmailTitle()));
	}

	@Then("^user should able to view description$")
	public void user_should_able_to_view_description() throws Throwable {
		//emailUsDesc();
		Assert.assertTrue(verifyTextIsDisplayed(contactus.contactUsEmailDescription()));
	}

	@When("^user click on email us$")
	public void user_click_on_email_us() throws Throwable {
		Thread.sleep(1000);
		//emailUsButton();
		clickButtonWithOutScroll(contactus.contactUsEmailButton());
	}

	@Then("^verify the display of Integrative Therapeutics$")
	public void verify_the_display_of_Integrative_Therapeutics() throws Throwable {
		//locationh1();
		verifyElementIsDisplayed(contactus.contactUsIntegrativeTherapeuticsTitle());
		verifyElementIsDisplayed(contactus.contactUsMapIntegrativeTherapeutics());
	}

	@Then("^verify the display of Customer Care Hours$")
	public void verify_the_display_of_Customer_Care_Hours() throws Throwable {
		//locationh2();
		Assert.assertTrue(verifyElementIsDisplayed(contactus.contactUsMapCustomerCareHours()));
	}

	@Then("^verify the display of Phone$")
	public void verify_the_display_of_Phone() throws Throwable {
		//locationh3();
		Assert.assertTrue(verifyElementIsDisplayed(contactus.contactUsMapPhone()));
	}

	@Then("^verify the display Fax$")
	public void verify_the_display_Fax() throws Throwable {
		//locationh4();
		Assert.assertTrue(verifyElementIsDisplayed(contactus.contactUsMapFax()));
	}

	@Then("^verify the display of title$")
	public void verify_the_display_of_title() throws Throwable {
		//eT();
		waitFor(10000);
		waitElementToBeClickable(contactus.contactUsEmailFormTitle());
		Assert.assertTrue(verifyTextIsDisplayed(contactus.contactUsEmailFormTitle()));
		Assert.assertTrue(verifyTextIsDisplayed(contactus.contactUsEmailFormDescription()));
	}

	@Then("^user should able to enter the text in name text box$")
	public void user_should_able_to_enter_the_text_in_name_text_box() throws Throwable {
		eName();
	}

	@Then("^user should able to enter the mail$")
	public void user_should_able_to_enter_the_mail() throws Throwable {
		userE();
	}

	@When("^user select the value Practitioner$")
	public void user_select_the_value_Practitioner() throws Throwable {
		userSelectionInEMailForm();
	}

	@When("^user selects the value Inquiry$")
	public void user_selects_the_value_Inquiry() throws Throwable {
		userSelectionInEEForm();
	}

	@Then("^verify the display of back to home button$")
	public void verify_the_display_of_back_to_home_button() throws Throwable {
		waitFor(2000);
		waitForPageLoaded();
		Assert.assertTrue(verifyElementIsDisplayed(contactus.backToHomeButton()));
	}
	
	@When("^click on back to home button and display homepage$")
	public void click_BackToHomeButton_DisplayHomePage() throws Throwable {
		backToHome();
	}

	
	@When("^user enters text in email message box$")
	public void user_enters_text_in_email_message_box() throws Throwable {
		eMessage();
	}

	@When("^click on submit button$")
	public void click_on_submit_button() throws Throwable {
		emailFormSubmit();
	}

	@And("^click on contact-us link in globel footer$")
	public void click_on_contact_us_link_in_globel_footer() throws Throwable {
		//contactUsLink();
		clickButton(contactus.contactUsLink(), contactus.contactUsLink());
	}

	@Then("^verify the display of sucess message$")
	public void verify_the_display_of_sucess_message() throws Throwable {
		contactUsSucessMessage();
	}

	@Then("^verify the display of contact us message box error message$")
	public void verify_the_display_of_contact_us_message_box_error_message() throws Throwable {
		contactUsMessageTextBoxErrorMessage();
	}
	
	
	@Then("^Verify submit button is enabled$")
	public void verify_submit_button_is_enabled() throws Exception 
	{
		waitVisibilityOfElement(contactus.contactUsEmailFormSubmitButton());
	    Assert.assertTrue(contactus.contactUsEmailFormSubmitButton().isEnabled());
	}

	@Then("^verify the display of success message$")
	public void verify_the_display_of_success_message() 
	{
		
	   
	}

	@When("^user enter the name as \"([^\"]*)\"$")
	public void user_enter_the_name_as(String text) throws Exception
	{
		enterText(contactus.contactUsEmailFormTextBox1(), text);
	}

	@When("^user enters the invalid email id \"([^\"]*)\"$")
	public void user_enters_the_invalid_email_id(String text) throws Exception
	{
	  enterText(contactus.contactUsEmailFormTextBox2(), text);
	  contactus.contactUsEmailFormTextBox1().click();
	}

	@Then("^verify system provides the error message for invalid email address \"([^\"]*)\"$")
	public void verify_system_provides_the_error_message_for_invalid_email_address(String text) 
	{
		waitVisibilityOfElement(contactus.contactUsEmailFieldError());
		verifyExpectedText(text, contactus.contactUsEmailFieldError());
		
	}

	@Then("^verify the submit button should be in disabled mode\\.$")
	public void verify_the_submit_button_should_be_in_disabled_mode() throws Exception 
	{
		scrollToElement(contactus.contactUsEmailFormSubmitButton());
	    Assert.assertTrue(!contactus.contactUsEmailFormSubmitButton().isEnabled());
	}


}
