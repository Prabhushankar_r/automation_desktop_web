package stepDefinitions;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.junit.Assert;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import platforms.WebPortal;

public class Sprint1_CareersSteps extends WebPortal {
	public Sprint1_CareersSteps() throws FileNotFoundException, IOException, ParseException {
		super();
	}

	@Then("^vefify the display of title$")
	public void vefify_the_display_of_title() throws Throwable 
	{
		waitVisibilityOfElement(carrer.carrerBannerTitle());
		carTitle();
	}
		
		

	@Then("^verify the display of discription$")
	public void verify_the_display_of_discription() throws Throwable {
		carDescription();
	}

	@Then("^click on View Career Opportunities$")
	public void click_on_View_Career_Opportunities() throws Throwable {
		carHbiButton();
	}

	@Then("^verify the image$")
	public void verify_the_image() throws Throwable {
		carOurEImg();
	}

	@Then("^verify the view of description$")
	public void verify_the_view_of_description() throws Throwable {
		carOurEdesc();
	}

	@Then("^click on Learn Our Story$")
	public void click_on_Learn_Our_Story() throws Throwable {
		carOurESecButton();
	}

	@Then("^verify the display of Benefits$")
	public void verify_the_display_of_Benefits(DataTable benifits) throws Throwable {
		beniftsSection(benifits);
	}

	@Then("^verify the display of Perks$")
	public void verify_the_display_of_Perks(DataTable perks) throws Throwable {
		perksSection(perks);

	}

	@And("^verify the display of Join our team title$")
	public void verify_the_display_of_title() throws Throwable {
		JOTSTitle();
	}

	@When("^verify the display of description$")
	public void verify_the_display_of_description() throws Throwable {
		JOTSDescription();
	}

	@When("^user should be able to click View Career Opportunities|$")
	public void user_should_be_able_to_click_View_Career_Opportunities() throws Throwable {
		clickButtonWithOutScroll(carrer.careerpagejot_viewcareeropportunities());
		//JOTSbUTTON();
	}

	@And("^click on carrer link in globel footer$")
	public void click_on_carrer_link_in_globel_footer() throws Throwable 
	{
		clink();
	}

}
