package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import platforms.WebPortal;

public class Sprint3_PractitionerCreateAccountSteps extends WebPortal {

	public static String currentDate,Prefix,Lastname;
	
	public Sprint3_PractitionerCreateAccountSteps() throws FileNotFoundException, IOException, ParseException {
		super();
		}
	
	@When("^select value in prefix field$")
	public void selectValue_In_PrefixField() throws Throwable {
		waitTillElementExist(createaccount.Prefix_Field());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Prefix_Field()));
		selectElement_DropDown(createaccount.Prefix_Field(),2);
		waitFor(1000);
		Prefix=createaccount.Prefix_DropDown_Values().get(2).getText();
		getAndCheckAttributeValue(createaccount.Prefix_Field(),"data-valid","true");
	}
	
	@Then("^enter the First name \"(.*?)\"$")
	public void enter_the_First_name(String firstname) throws Throwable {
		waitTillElementExist(createaccount.FirstName_Field());
		Assert.assertTrue(verifyElementIsPresent(createaccount.FirstName_Field()));
		getAndCheckAttributeValue(createaccount.FirstName_Field(),"placeholder","First Name");
		enterText(createaccount.FirstName_Field(),firstname);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.FirstName_Field(),"data-valid","true");

	}
	@And("^enter the Last name \"(.*?)\"$")
	public void enter_the_Last_name(String lastname) throws Throwable {
		waitTillElementExist(createaccount.LastName_Field());
		Assert.assertTrue(verifyElementIsPresent(createaccount.LastName_Field()));
		getAndCheckAttributeValue(createaccount.LastName_Field(),"placeholder","Last Name");
		enterText(createaccount.LastName_Field(),lastname);
		Lastname=lastname;
		selectTabKey();
		getAndCheckAttributeValue(createaccount.LastName_Field(),"data-valid","true");
	}

	@And("^enter the Post Nominal \"(.*?)\"$")
	public void enter_the_Post_Nominal(String postnominal) throws Throwable {
		waitTillElementExist(createaccount.PostNominals_Field());
		Assert.assertTrue(verifyElementIsPresent(createaccount.PostNominals_Field()));
		getAndCheckAttributeValue(createaccount.PostNominals_Field(),"placeholder","Post Nominals");
		enterText(createaccount.PostNominals_Field(),postnominal);
		waitFor(1000);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.PostNominalsValuecheck_Field(),"class","input-out-focus");
	}
	
	@And("^enter the \"(.*?)\" in other Practitioner Type$")
	public void Enter_Value_In_Others(String OtherPractitionerType) throws Throwable {
		waitTillElementExist(createaccount.OtherPractitionerType_Field());
		Assert.assertTrue(verifyElementIsPresent(createaccount.OtherPractitionerType_Field()));
		enterText(createaccount.OtherPractitionerType_Field(),OtherPractitionerType);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.OtherPractitionerValuecheck_Field(),"class","input-out-focus");
	}
	
	@Then("^select Practitioner Type value$")
	public void click_Practitioner_Type() throws Throwable {
		verifyDefaultValueDropdown(createaccount.PractitionerType_Field());
		waitTillElementExist(createaccount.PractitionerType_Field());
		Assert.assertTrue(verifyElementIsPresent(createaccount.PractitionerType_Field()));
		selectElement_DropDown(createaccount.PractitionerType_Field(),2);
		waitFor(1000);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.PractitionerType_Field(),"data-valid","true");
	}
	
	@Then("^select Practitioner Type value as \"(.*?)\"$")
	public void click_Practitioner_Type_Value_Others(String value) throws Throwable {
		waitTillElementExist(createaccount.PractitionerType_Field());
		Assert.assertTrue(verifyElementIsPresent(createaccount.PractitionerType_Field()));
		selectElement_DropDownBasedOnValue(createaccount.PractitionerType_Field(),value);
		waitFor(1000);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.PractitionerType_Field(),"data-valid","false");
	}
	
	@And("^enter the \"(.*?)\" in other Practitioner Type value$")
	public void Enter_Value_OtherPractitionerType(String other_Practitionertype) throws Throwable {
		waitTillElementExist(createaccount.PhoneNo_Field());
		Assert.assertTrue(verifyElementIsPresent(createaccount.PhoneNo_Field()));
		enterText(createaccount.OtherPractitionerType_Field(),other_Practitionertype);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.OtherPractitionerValuecheck_Field(),"class","input-out-focus");
	}

	@Then("^select Practitioner count value$")
	public void click_Practitioner_count() throws Throwable {
		waitTillElementExist(createaccount.PractitionersCount());
		Assert.assertTrue(verifyElementIsPresent(createaccount.PractitionersCount()));
		verifyDefaultValueDropdown(createaccount.PractitionersCount());
		selectElement_DropDown(createaccount.PractitionersCount(),2);
		waitFor(1000);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.PractitionersCount(),"data-valid","true");
	} 
	
	@And("^enter the Phone number \"(.*?)\"$")
	public void enter_the_Phone_number(String phonenumber) throws Throwable {
		waitTillElementExist(createaccount.PhoneNo_Field());
		Assert.assertTrue(verifyElementIsPresent(createaccount.PhoneNo_Field()));
		enterText(createaccount.PhoneNo_Field(),phonenumber);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.PhoneNo_Field(),"data-valid","true");
	}

	@And("^enter the business Phone number \"(.*?)\"$")
	public void enter_the_Business_Phone_number(String businessphonenumber) throws Throwable {
		waitTillElementExist(createaccount.BusinessPhone());
		Assert.assertTrue(verifyElementIsPresent(createaccount.BusinessPhone()));
		enterText(createaccount.BusinessPhone(),businessphonenumber);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.BusinessPhone(),"data-valid","true");
	} 
	
	@And("^enter the email address$")
	public void enter_the_EmailAddress() throws Throwable {
		
		currentDate=Date();
		waitTillElementExist(createaccount.EmailAddress());
		Assert.assertTrue(verifyElementIsPresent(createaccount.EmailAddress()));
		getAndCheckAttributeValue(createaccount.EmailAddress(),"placeholder","Email Address");
		enterText(createaccount.EmailAddress(),currentDate);
		getAndCheckAttributeValue(createaccount.EmailAddress(),"data-valid","true");
	}
	
	@And("^enter the student email address$")
	public void enter_the_StudentEmailAddress() throws Throwable {
		currentDate=Date();
		waitTillElementExist(createaccount.StudentEmailAddress());
		Assert.assertTrue(verifyElementIsPresent(createaccount.StudentEmailAddress()));
		getAndCheckAttributeValue(createaccount.StudentEmailAddress(),"placeholder","Student Email Address");
		enterText(createaccount.StudentEmailAddress(),currentDate);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.StudentEmailAddress(),"data-valid","true");
	}
	
	@And("^enter college name \"(.*?)\"$")
	public void enter_the_CollegeName(String college_name) throws Throwable {
		waitTillElementExist(createaccount.CollegeName_Field());
		Assert.assertTrue(verifyElementIsPresent(createaccount.CollegeName_Field()));
		enterText(createaccount.CollegeName_Field(),college_name);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.CollegeName_Field(),"data-valid","true");
	}
	
	@Then("^select the graduation month$")
	public void select_Graduation_Month_MedicalStudent() throws Throwable {
		waitTillElementExist(createaccount.GraduationMonth_Field());
		Assert.assertTrue(verifyElementIsPresent(createaccount.GraduationMonth_Field()));
		selectElement_DropDown(createaccount.GraduationMonth_Field(),2);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.GraduationMonth_Field(),"data-valid","true");
	}
	
	@And("^select the graduation year$")
	public void select_Graduation_Year_MedicalStudent() throws Throwable {
		waitTillElementExist(createaccount.GraduationYear_Field());
		Assert.assertTrue(verifyElementIsPresent(createaccount.GraduationYear_Field()));
		selectElement_DropDown(createaccount.GraduationYear_Field(),2);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.GraduationYear_Field(),"data-valid","true");
	}
	
	@Then("^enter the degree obtained \"(.*?)\"$")
	public void enter_the_DegreeObtained(String degree_obtained) throws Throwable {
		waitTillElementExist(createaccount.DegreeObtained());
		Assert.assertTrue(verifyElementIsPresent(createaccount.DegreeObtained()));
		enterText(createaccount.DegreeObtained(),degree_obtained);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.DegreeObtained(),"data-valid","true");
	}

	@Then("^enter the Program \"(.*?)\"$")
	public void enter_the_ProgramObtained(String Program_obtained) throws Throwable {
		waitTillElementExist(createaccount.Program());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Program()));
		enterText(createaccount.Program(),Program_obtained);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.Program(),"data-valid","true");
	}
	
	@And("^enter license number \"(.*?)\"$")
	public void enter_the_LicenseNumber(String licenseNo) throws Throwable {
		waitTillElementExist(createaccount.LicenseNo());
		Assert.assertTrue(verifyElementIsPresent(createaccount.LicenseNo()));
		getAndCheckAttributeValue(createaccount.LicenseNo(),"placeholder","License Number");
		enterText(createaccount.LicenseNo(),licenseNo);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.LicenseValuecheck_Field(),"class","input-out-focus");
	}

	@Then("^select usage Type value$")
	public void selectValue_In_UsageTypeField() throws Throwable {
		waitTillElementExist(createaccount.UsageTypes());
		Assert.assertTrue(verifyElementIsPresent(createaccount.UsageTypes()));
		verifyDefaultValueDropdown(createaccount.UsageTypes());
		selectElement_DropDown(createaccount.UsageTypes(),2);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.UsageTypes(),"data-valid","true");
	}
	
	@And("^enter password \"(.*?)\"$")
	public void enter_the_PasswordValue(String password) throws Throwable {
		waitTillElementExist(createaccount.Password());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Password()));
		getAndCheckAttributeValue(createaccount.Password(),"placeholder","Password");
		enterText(createaccount.Password(),password);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.Password(),"data-valid","true");
	}
	
	@And("^enter re-enter password \"(.*?)\"$")
	public void enter_the_Re_PasswordValue(String reenter_password) throws Throwable {
		waitTillElementExist(createaccount.ReEnter_Password());
		Assert.assertTrue(verifyElementIsPresent(createaccount.ReEnter_Password()));
		getAndCheckAttributeValue(createaccount.ReEnter_Password(),"placeholder","Re-enter your New Password");
		enterText(createaccount.ReEnter_Password(),reenter_password);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.ReEnterPasswordValuecheck_Field(),"class","input-out-focus");
	}
	
	@Then("^enter business name \"(.*?)\"$")
	public void enter_the_BusinessName(String businessname) throws Throwable {
		waitTillElementExist(createaccount.BusinessName());
		Assert.assertTrue(verifyElementIsPresent(createaccount.BusinessName()));
		getAndCheckAttributeValue(createaccount.BusinessName(),"placeholder","Business Name");
		enterText(createaccount.BusinessName(),businessname);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.BusinessNameValuecheck_Field(),"class","input-out-focus");
	}
	
	@And("^enter Address1 value \"(.*?)\"$")
	public void enter_the_Address1(String Address1) throws Throwable {
		waitTillElementExist(createaccount.businessaddress1());
		Assert.assertTrue(verifyElementIsPresent(createaccount.businessaddress1()));
		getAndCheckAttributeValue(createaccount.businessaddress1(),"placeholder","Address 1");
		enterText(createaccount.businessaddress1(),Address1);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.BusinessAdd1Valuecheck_Field(),"class","input-out-focus");
	}
	
	@And("^enter Address1 value for medical student \"(.*?)\"$")
	public void enter_the_Address1_MedicalStudent(String Address1) throws Throwable {
		waitTillElementExist(createaccount.Studentaddress1());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Studentaddress1()));
		getAndCheckAttributeValue(createaccount.Studentaddress1(),"placeholder","Address 1");
		enterText(createaccount.Studentaddress1(),Address1);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.StudentAdd1Valuecheck_Field(),"class","input-out-focus");
	}
	
	@And("^enter zipcode value \"(.*?)\" for medical student$")
	public void enter_the_ZipcodeValue_MedicalStudent(String zipcode) throws Throwable {
		waitTillElementExist(createaccount.studentzipcode());
		Assert.assertTrue(verifyElementIsPresent(createaccount.studentzipcode()));
		getAndCheckAttributeValue(createaccount.studentzipcode(),"placeholder","Zip Code");
		enterText(createaccount.studentzipcode(),zipcode);
		selectTabKey();
		getAndCheckAttributeValue(createaccount.studentzipcode(),"data-valid","true");
	}
	
	
	
	@And("^enter zipcode value \"(.*?)\"$")
	public void enter_the_ZipcodeValue(String zipcode) throws Throwable {
		waitTillElementExist(createaccount.businesszipcode());
		Assert.assertTrue(verifyElementIsPresent(createaccount.businesszipcode()));
		getAndCheckAttributeValue(createaccount.businesszipcode(),"placeholder","Zip Code");
		enterText(createaccount.businesszipcode(),zipcode);
		getAndCheckAttributeValue(createaccount.businesszipcode(),"data-valid","true");

	}
	
	@Then("^prepopulate state and city field")
	public void Prepopulate_State_City_Field_Value() throws Throwable {
		//verifyAutopopulation_City_State();
        getAndCheckAttributeValue(createaccount.businesscity(),"placeholder","City");	
		verifyDefaultValueDropdown(createaccount.businessstate());
		selectTabKey();
		waitForPageLoaded();
		waitFor(3000);	
		getAndCheckAttributeValue(createaccount.businessstate(),"data-valid","true");		
		getAndCheckAttributeValue(createaccount.businesscity(),"data-valid","true");
		
	}
	
	@Then("^prepopulate city and state for medical student")
	public void Prepopulate_State_City_MedicalStudent() throws Throwable {
		verifyDefaultValueDropdown(createaccount.studentstate());
		//getAndCheckAttributeValue(createaccount.studentstate(),"placeholder","State");
		getAndCheckAttributeValue(createaccount.studentcity(),"placeholder","City");
		selectTabKey();
		waitForPageLoaded();
		waitFor(3000);	
		getAndCheckAttributeValue(createaccount.studentstate(),"data-valid","true");
		getAndCheckAttributeValue(createaccount.studentcity(),"data-valid","true");
			
	}
		
			
	@And("^display error message for prefix \"(.*?)\"$")
	public void Validate_ErrMessage_Prefix( String text) throws Throwable {
		verifyExpectedText(text, createaccount.Err_Msg_Prefix());
	}
	
	@And("^verify the header in create account section$")
	public void VerifyHeader_CreateAccountSection() throws Throwable {
		verifyTextIsDisplayed(createaccount.CreateAccount_Header());
	}
	
	@And("^verify both radio buttons should not be selected$")
	public void verifyRadioBtn_selection() throws Throwable {
		verifyRadioBtn();
	}
	
	@And("^click on Medical Student radio button$")
	public void ClickOnMedicalStudentRadioBtn() throws Throwable {
		waitForPageLoaded();
		clickButtonWithOutScroll(createaccount.MedicalStudent_RadioBtns());
		waitForPageLoaded();
	}
	
	@When("^click healthcare practioner radio button$")
	public void ClickOnHealthCarePractitionerRadioBtn() throws Throwable {
		clickButtonWithOutScroll(createaccount.HealthCare_RadioBtns());
	}
	
	@And("^do not enter values in prefix field$")
	public void DoNotEnterValues_PrefixField() throws Throwable {
		selectElement_DropDown(createaccount.Prefix_Field(),2);
		selectElement_DropDown(createaccount.Prefix_Field(),0);
	}
	
	@And("^click on create account button$")
	public void Click_CreateAccountButton_Header() throws Throwable {
		waitTillElementExist(createaccount.CreateAccount_Btn());
		clickButtonWithOutScroll(createaccount.CreateAccount_Btn());
		waitForPageLoaded();
		verifyElementIsPresent(createaccount.CreateAccount_Header());
	}
	
	@And("^do not enter values in firstname field$")
	public void DoNotEnterValues_firstname() throws Throwable {
		enterText(createaccount.FirstName_Field(),"");
		selectTabKey();
		//waitFor(1000);
	}
	
	@And("^display error message for firstname \"(.*?)\"$")
	public void Validate_ErrMessage_First( String text) throws Throwable {
		//waitFor(1000);
		waitTillElementExist(createaccount.Err_Msg_FirstName());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_FirstName()));
		verifyExpectedText(text, createaccount.Err_Msg_FirstName());
	}
	
	@And("^do not enter values in lastname field$")
	public void DoNotEnterValues_lastname() throws Throwable {
		enterText(createaccount.LastName_Field(),"");
		selectTabKey();
	}
	
	@And("^display error message for lastname \"(.*?)\"$")
	public void Validate_ErrMessage_LastName( String text) throws Throwable {
		//waitFor(1000);
		waitTillElementExist(createaccount.Err_Msg_LastName());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_LastName()));
		verifyExpectedText(text, createaccount.Err_Msg_LastName());
	}
	
	@And("^do not enter values in post nominals field$")
	public void DoNotEnterValues_postNominals() throws Throwable {
		enterText(createaccount.PostNominals_Field(),"");
		selectTabKey();
	}
	
	@And("^do not display error message for post nominals \"(.*?)\"$")
	public void Validate_ErrMessage_PostNominals( String text) throws Throwable {
		waitFor(1000);
		verifyElementIsNotDisplayed(createaccount.Err_Msg_PostNominal());
	}
	
	@And("^do not enter values in practitioner field$")
	public void DoNotEnterValues_practitioner() throws Throwable {
		enterText(createaccount.PractitionerType_Field(),"");
		selectTabKey();

		//selectElement_DropDown(createaccount.PractitionerType_Field(),2);
		//selectElement_DropDown(createaccount.PractitionerType_Field(),0);
	}
	
	@And("^display error message for practitioner \"(.*?)\"$")
	public void Validate_ErrMessage_practitioner( String text) throws Throwable {
		//waitFor(1000);
		waitTillElementExist(createaccount.Err_Msg_PractitionerType());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_PractitionerType()));
		verifyExpectedText(text, createaccount.Err_Msg_PractitionerType());
	}
	
	@And("^do not enter values in Phone No field$")
	public void DoNotEnterValues_PhoneNo() throws Throwable {
		enterText(createaccount.PhoneNo_Field(),"");
		waitFor(1000);
		selectTabKey();
		//selectTabKey();
	}
	
	@And("^display error message for Phone No \"(.*?)\"$")
	public void Validate_ErrMessage_PhoneNo( String text) throws Throwable {
		//waitFor(1000);
		waitTillElementExist(createaccount.Err_Msg_PhoneNumber());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_PhoneNumber()));
		verifyExpectedText(text, createaccount.Err_Msg_PhoneNumber());
	}
	
	@And("^do not enter values in email address field$")
	public void DoNotEnterValues_EmailAddress() throws Throwable {
		enterText(createaccount.EmailAddress(),"");
		selectTabKey();
	}
	
	@And("^do not enter values in student email address field$")
	public void DoNotEnterValues_StudentEmailAddress() throws Throwable {
		enterText(createaccount.StudentEmailAddress(),"");
		selectTabKey();
	}
	
	@And("^display error message for student email address \"(.*?)\"$")
	public void Validate_ErrMessage_StudentEmailAddress( String text) throws Throwable {
		//waitFor(1000);
		waitTillElementExist(createaccount.Err_Msg_StudentEmail());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_StudentEmail()));
		verifyExpectedText(text, createaccount.Err_Msg_StudentEmail());

	}
	
	@And("^display error message for email address \"(.*?)\"$")
	public void Validate_ErrMessage_EmailAddress( String text) throws Throwable {
		//waitFor(1000);
		waitTillElementExist(createaccount.Err_Msg_BusinessEmail());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_BusinessEmail()));
		verifyExpectedText(text, createaccount.Err_Msg_BusinessEmail());

	}
	
	@And("^do not enter values in license field$")
	public void DoNotEnterValues_license() throws Throwable {
		enterText(createaccount.LicenseNo(),"");
		selectTabKey();
	}
	
	@And("^do not display error message for license \"(.*?)\"$")
	public void Validate_ErrMessage_license( String text) throws Throwable {
		verifyElementIsNotDisplayed(createaccount.Err_Msg_LicenseNumber());
	}
	
	@And("^do not enter values in intend field$")
	public void DoNotEnterValues_intend() throws Throwable {
		selectElement_DropDown(createaccount.UsageTypes(),1);
		selectElement_DropDown(createaccount.UsageTypes(),0);
	}
	
	@And("^display error message for intend \"(.*?)\"$")
	public void Validate_ErrMessage_lintend( String usagetype) throws Throwable {
		//waitFor(1000);
		waitTillElementExist(createaccount.Err_Msg_UsageTypes());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_UsageTypes()));
		verifyExpectedText(usagetype, createaccount.Err_Msg_UsageTypes());


	}
	
	@And("^verify watermark and enter invalid email \"(.*?)\"$")
	public void Enter_InvalidEmailAddress( String text) throws Throwable {
		getAndCheckAttributeValue(createaccount.EmailAddress(),"placeholder","Email Address");
		enterText(createaccount.EmailAddress(),text);
		selectTabKey();
		
	}
	
	@And("^display error for invalid email \"(.*?)\"$")
	public void ErrorMsg_InvalidEmailAddress( String text) throws Throwable {
		//waitFor(1000);
		waitTillElementExist(createaccount.Err_Msg_BusinessEmail());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_BusinessEmail()));
		verifyExpectedText(text,createaccount.Err_Msg_BusinessEmail());
	}
	
	@And("^verify watermark and enter invalid PhoneNo \"(.*?)\"$")
	public void Enter_InvalidPhoneNo( String text) throws Exception {
		getAndCheckAttributeValue(createaccount.PhoneNo_Field(),"placeholder","(___) ___-____x_____");
		enterText(createaccount.PhoneNo_Field(),text);
		selectTabKey();
	}
	
	@And("^display error for invalid PhoneNo \"(.*?)\"$")
	public void ErrorMsg_InvalidPhoneNo( String text) throws Throwable {
		//waitFor(1000);
		waitTillElementExist(createaccount.Err_Msg_PhoneNumber());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_PhoneNumber()));
		verifyExpectedText(text,createaccount.Err_Msg_PhoneNumber());
	}
	
	@And("^do not enter values in password field$")
	public void DoNotEnterValues_Password() throws Throwable {
		enterText(createaccount.Password(),"");
		selectTabKey();	
	}
	
	@And("^display error message for password \"(.*?)\"$")
	public void ErrorMsg_Password( String text) throws Throwable {
		//waitFor(1000);
		waitTillElementExist(createaccount.Err_Msg_Password());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_Password()));
		verifyExpectedText(text,createaccount.Err_Msg_Password());
	}
	
	@And("^display error message for re-enter password \"(.*?)\"$")
	public void ErrorMsg_Re_EnterPassword( String text) throws Throwable {
	//	waitFor(1000);
		waitTillElementExist(createaccount.Err_Msg_ConfirmPassword());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_ConfirmPassword()));
		verifyExpectedText(text,createaccount.Err_Msg_ConfirmPassword());
	}
	
	
	@And("^do not enter values in reenter password field$")
	public void DoNotEnterValues_ReEnterPassword() throws Throwable {
		enterText(createaccount.ReEnter_Password(),"");
		selectTabKey();	
	}
	
	@And("^verify watermark and enter value in password section \"(.*?)\" and \"(.*?)\"$")
	public void Enter_ValuesInPasswordSection(String Password , String ConfirmPassword) throws Throwable {
		getAndCheckAttributeValue(createaccount.Password(),"placeholder","Password");
		enterText(createaccount.Password(),Password);
		selectTabKey();
		waitFor(1000);
		getAndCheckAttributeValue(createaccount.ReEnter_Password(),"placeholder","Re-enter your New Password");
		enterText(createaccount.ReEnter_Password(),ConfirmPassword);
		selectTabKey();	
		waitFor(1000);
	}
		
	@And("^display error for not matching password \"(.*?)\"$")
	public void ErrMessage_PasswordDoNotMatch( String text) throws Throwable {
	//	waitFor(1000);
		waitTillElementExist(createaccount.Err_Msg_ConfirmPassword());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_ConfirmPassword()));
		verifyExpectedText(text,createaccount.Err_Msg_ConfirmPassword());

	}
	
	@And("^do not enter values in BussinessPhone field$")
	public void DoNotEnterValues_BussinessPhone() throws Throwable {
		enterText(createaccount.BusinessPhone(),"");
		selectTabKey();	
		waitFor(1000);
	}
	
	@And("^do not enter values in NoOfPractitioner field$")
	public void DoNotEnterValues_NoOfPractitioner() throws Throwable {
		selectElement_DropDown(createaccount.PractitionersCount(),1);
		selectElement_DropDown(createaccount.PractitionersCount(),0);
		waitFor(1000);
	}
	
	@And("^do not enter values in State field$")
	public void DoNotEnterValues_State() throws Throwable {
		selectElement_DropDown(createaccount.businessstate(),1);
		selectElement_DropDown(createaccount.businessstate(),0);
		waitFor(1000);
	}
	
	@And("^do not enter values in State field for student$")
	public void DoNotEnterValues_State_Student() throws Throwable {
		selectElement_DropDown(createaccount.studentstate(),1);
		selectElement_DropDown(createaccount.studentstate(),0);
		waitFor(1000);
	}
	
	@And("^do not enter values in City field for student$")
	public void DoNotEnterValues_City_Student() throws Throwable {
		enterText(createaccount.studentcity(),"");
		selectTabKey();	
		//waitFor(1000);
	}
	
	@And("^do not enter values in City field$")
	public void DoNotEnterValues_City() throws Throwable {
		enterText(createaccount.businesscity(),"");
		selectTabKey();	
		//waitFor(1000);
	}
	
	@And("^do not enter values in ZipCode field$")
	public void DoNotEnterValues_ZipCode() throws Throwable {
		enterText(createaccount.businesszipcode(),"");
		selectTabKey();	
		//waitFor(1000);
	}
	
	@And("^do not enter values in ZipCode field for student$")
	public void DoNotEnterValues_ZipCode_Student() throws Throwable {
		enterText(createaccount.studentzipcode(),"");
		selectTabKey();	
		//waitFor(1000);
	}
	
	

	@And("^do not enter values in Address1 field$")
	public void DoNotEnterValues_Address1() throws Throwable {
		enterText(createaccount.businessaddress1(),"");
		selectTabKey();	
		//waitFor(1000);
	}
	
	@And("^do not enter values in Address1 field for student$")
	public void DoNotEnterValues_StudentAddress1() throws Throwable {
		enterText(createaccount.Studentaddress1(),"");
		selectTabKey();	
		//waitFor(1000);
	}
	
	
	
	
	@And("^do not enter values in Address2 field$")
	public void DoNotEnterValues_Address2() throws Throwable {
		enterText(createaccount.businessaddress2(),"");
		selectTabKey();	
		//waitFor(1000);
	}
	
	@And("^do not enter values in Address2 field for student$")
	public void DoNotEnterValues_Address2_MedicalStudent() throws Throwable {
		enterText(createaccount.Studentaddress2(),"");
		selectTabKey();	
		//waitFor(1000);
	}
	
	@And("^do not enter values in BussinessName field$")
	public void DoNotEnterValues_BussinessName() throws Throwable {
		enterText(createaccount.BusinessName(),"");
		selectTabKey();	
		//waitFor(1000);

	}
	
	@And("^display error message for BussinessName \"(.*?)\"$")
	public void ErrMessage_BussinessName( String text) throws Throwable {
		waitTillElementExist(createaccount.Err_Msg_BussinessName());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_BussinessName()));
		verifyExpectedText(text,createaccount.Err_Msg_BussinessName());
	}
	
	@Then("^display error message for Address1 \"(.*?)\"$")
	public void ErrMessage_Address1( String text) throws Throwable {
		waitTillElementExist(createaccount.Err_Msg_Address1_HealthCare());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_Address1_HealthCare()));
		verifyExpectedText(text,createaccount.Err_Msg_Address1_HealthCare());
	}
	
	
	@Then("^display error message for Address1 in student \"(.*?)\"$")
	public void ErrMessage_Address1_MedicalStudent( String text) throws Throwable {
		waitTillElementExist(createaccount.Err_Msg_Address1_Student());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_Address1_Student()));
		verifyExpectedText(text,createaccount.Err_Msg_Address1_Student());
	}
	
	@Then("^do not display error message for Address2 in student \"(.*?)\"$")
	public void ErrMessage_Address2_MedicalStudent( String text) throws Throwable {
		verifyElementIsNotDisplayed(createaccount.Err_Msg_Address2_Student());
	}
	
	@Then("^do not display error message for Address2 \"(.*?)\"$")
	public void ErrMessage_Address2( String text) throws Throwable {
		verifyElementIsNotDisplayed(createaccount.Err_Msg_Address2_HealthCare());
	}
	
	@And("^display error message for ZipCode \"(.*?)\"$")
	public void ErrMessage_ZipCode( String text) throws Throwable {
		waitTillElementExist(createaccount.Err_Msg_Zipcode());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_Zipcode()));
		verifyExpectedText(text,createaccount.Err_Msg_Zipcode());
	}
	
	@And("^display error message for ZipCode in student \"(.*?)\"$")
	public void ErrMessage_ZipCode_MedicalStudent( String text) throws Throwable {
		waitTillElementExist(createaccount.Err_Msg_Zipcode_MedicalStudent());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_Zipcode_MedicalStudent()));
		verifyExpectedText(text,createaccount.Err_Msg_Zipcode_MedicalStudent());
	}
	
	@And("^display error message for City \"(.*?)\"$")
	public void ErrMessage_City( String text) throws Throwable {
		waitTillElementExist(createaccount.Err_Msg_City());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_City()));
		verifyExpectedText(text,createaccount.Err_Msg_City());
	}
	
	@And("^display error message for City in student \"(.*?)\"$")
	public void ErrMessage_City_MedicalStudent( String text) throws Throwable {
		waitTillElementExist(createaccount.Err_Msg_City_MedicalStudent());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_City_MedicalStudent()));
		verifyExpectedText(text,createaccount.Err_Msg_City_MedicalStudent());
	}
	
	@And("^display error message for State in student \"(.*?)\"$")
	public void ErrMessage_State_MedicalStudent( String text) throws Throwable {
		waitTillElementExist(createaccount.Err_Msg_State_MedicalStudent());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_State_MedicalStudent()));
		verifyExpectedText(text,createaccount.Err_Msg_State_MedicalStudent());
	}
	
	@And("^display error message for State \"(.*?)\"$")
	public void ErrMessage_State( String text) throws Throwable {
		waitTillElementExist(createaccount.Err_Msg_State());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_State()));
		verifyExpectedText(text,createaccount.Err_Msg_State());
	}
	
	@And("^display error message for NoOfPractitioner \"(.*?)\"$")
	public void ErrMessage_NoOfPractitioner( String text) throws Throwable {
		waitTillElementExist(createaccount.Err_Msg_TotalNumberofPractitioners());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_TotalNumberofPractitioners()));
		verifyExpectedText(text,createaccount.Err_Msg_TotalNumberofPractitioners());
	}
	
	@And("^display error message for BussinessPhone \"(.*?)\"$")
	public void ErrMessage_BussinessPhone( String text) throws Throwable {
		waitTillElementExist(createaccount.Err_Msg_BusinessPhone());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_BusinessPhone()));
		verifyExpectedText(text,createaccount.Err_Msg_BusinessPhone());
				
	}
	
	@And("^do not enter values in College field$")
	public void DoNotEnterValues_CollegeField() throws Throwable {
		enterText(createaccount.CollegeName_Field(),"");
		selectTabKey();	
		//waitFor(1000);
	}
	
	@And("^display error message for College \"(.*?)\"$")
	public void ErrMessage_College( String text) throws Throwable {
		waitTillElementExist(createaccount.Err_Msg_CollegeName());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_CollegeName()));
		verifyExpectedText(text,createaccount.Err_Msg_CollegeName());
	}
	
	@And("^do not enter values in GraduationMonth field$")
	public void DoNotEnterValues_GraduationMonth() throws Throwable {
		selectElement_DropDown(createaccount.GraduationMonth_Field(),1);
		selectElement_DropDown(createaccount.GraduationMonth_Field(),0);
		waitFor(1000);
	}
	
	@And("^display error message for GraduationMonth \"(.*?)\"$")
	public void ErrMessage_GraduationMonth( String text) throws Throwable {
		waitTillElementExist(createaccount.Err_Msg_GraduationMonths());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_GraduationMonths()));
		verifyExpectedText(text,createaccount.Err_Msg_GraduationMonths());
	}
	
	@And("^do not enter values in GraduationYear field$")
	public void DoNotEnterValues_GraduationYear() throws Throwable {
		selectElement_DropDown(createaccount.GraduationYear_Field(),1);
		selectElement_DropDown(createaccount.GraduationYear_Field(),0);
		waitFor(1000);
	}
	
	@And("^display error message for GraduationYear \"(.*?)\"$")
	public void ErrMessage_GraduationYear( String text) throws Throwable {
		waitTillElementExist(createaccount.Err_Msg_GraduationYears());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_GraduationYears()));
		verifyExpectedText(text,createaccount.Err_Msg_GraduationYears());
	}
	
	@And("^do not enter values in Degree field$")
	public void DoNotEnterValues_Degree() throws Throwable {
		enterText(createaccount.DegreeObtained(),"");
		selectTabKey();	
		waitFor(1000);
	}
	
	@And("^display error message for Degree \"(.*?)\"$")
	public void ErrMessage_Degree( String text) throws Throwable {
		waitTillElementExist(createaccount.Err_Msg_DegreeObtained());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_DegreeObtained()));
		verifyExpectedText(text,createaccount.Err_Msg_DegreeObtained());
	}
	
	@And("^do not enter values in Program field$")
	public void DoNotEnterValues_Program() throws Throwable {
		enterText(createaccount.Program(),"");
		selectTabKey();	
		waitFor(1000);
	}
	
	@And("^display error message for Program \"(.*?)\"$")
	public void ErrMessage_Program( String text) throws Throwable {
		waitTillElementExist(createaccount.Err_Msg_Program());
		Assert.assertTrue(verifyElementIsPresent(createaccount.Err_Msg_Program()));
		verifyExpectedText(text,createaccount.Err_Msg_Program());
	}
	
	@And("^verify submit button is disabled$")
	public void VerifySubmitBtn_Is_Disabled() throws Throwable {
		verifyButtonDisabled(createaccount.SubmitBtn());
	}
	
	@Then("^check the resale policy$")
	public void check_ResalePolicy_CreateAccount() throws Throwable {
		Assert.assertTrue(verifyElementIsPresent(createaccount.ResalePolicy()));
		clickButtonWithOutScroll(createaccount.ResalePolicy());
		verifyElementIsPresent(createaccount.ResalePolicyLink());
		//verifyButtonDisabled(createaccount.SubmitBtn());
	}
	
	@And("^check the subscribe button$")
	public void check_SubscribeButton_CreateAccount() throws Throwable {
		Assert.assertTrue(verifyElementIsPresent(createaccount.CheckSubscribe()));
		clickButtonWithOutScroll(createaccount.CheckSubscribe());

	}
	
	@Then("^click on Submit Account Request$")
	public void Click_Submit_Account_Request() throws Throwable {
		Assert.assertTrue(verifyElementIsPresent(createaccount.SubmitBtn()));
		clickButtonWithOutScroll(createaccount.SubmitBtn());
	}
	
	@Then("^verify account is created successfully$")
	public void Verify_Account_CreatedSuccessfully() throws Throwable {
		waitForPageLoaded();
		Assert.assertTrue(verifyElementIsPresent(createaccount.createaccount_success_pageheading()));
		Assert.assertTrue(verifyElementIsPresent(megamenu.verifyQuickOrderPresent()));
		Assert.assertTrue(verifyElementIsPresent(myaccount.chevron_username()),"Account Creation is not success");
	
	}
	
	@Then("^verify question text box$")
	public void verify_Question_TextBox() throws Throwable {
		waitForPageLoaded();
		Assert.assertTrue(verifyElementIsPresent(createaccount.HearAboutUsTxtBox()));
		Assert.assertTrue(verifyElementIsPresent(createaccount.AnyQnsTxtBox()));
		
	}
	
	@And("^enter the maximum number of accepted value$")
	public void verify_Maximum_limit_Of_Text() throws Throwable {
		checkMaxLength_TextBox_AnyQnsTxtBox();
		checkMaxLength_TextBox_HearAboutUsTxtBox();
	}
	
	@Then("^verify username is displayed in header$")
	public void verify_UserName_Displayed_Header() throws Throwable {
		verifyUserName_Header();		
	}
	
}
