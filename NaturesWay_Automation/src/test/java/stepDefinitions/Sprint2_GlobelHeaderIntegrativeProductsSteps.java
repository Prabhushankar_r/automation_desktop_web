package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.json.simple.parser.ParseException;
import org.junit.Assert;

import cucumber.api.DataTable;
import cucumber.api.java.en.*;
import platforms.WebPortal;

public class Sprint2_GlobelHeaderIntegrativeProductsSteps extends WebPortal {
	
	public static String Category_ByType;
	public static String Category_ByHealthInterest;

	public Sprint2_GlobelHeaderIntegrativeProductsSteps() throws FileNotFoundException, IOException, ParseException 
	{
		super();

	}
	
	@Given("^\"([^\"]*)\" Mega menu should be displayed$")
	public void mega_menu_should_be_displayed(String text) throws Exception
	{  
		waitElementToBeClickable(globalheaderintegrativeproduct.getProductMegamenu_element());
		verifyElementIsDisplayed(globalheaderintegrativeproduct.getProductMegamenu_element());
		verifyExpectedText(text, globalheaderintegrativeproduct.getProductMegamenu_element());
	}

	@When("^Hover on the 'Products' Mega menu$")
	public void hover_on_the_Products_Mega_menu() 
	{
		moveToElement(globalheaderintegrativeproduct.getProductMegamenu_element());
	}

	@Then("^Verify the below categories are displayed$")
	public void verify_the_below_categories_are_displayed(DataTable categories)  
	{
		List<List<String>> raw = categories.raw();
		Assert.assertTrue(verifyTextInList(globalheaderintegrativeproduct.getProductMegamenuList(), raw.get(0).get(0)));
		Assert.assertTrue(verifyTextInList(globalheaderintegrativeproduct.getProductMegamenuList(), raw.get(1).get(0)));
		Assert.assertTrue(verifyTextInList(globalheaderintegrativeproduct.getProductMegamenuList(), raw.get(2).get(0)));
	}

	@When("^Hover on the 'BY TYPE' category$")
	public void hover_on_the_BY_TYPE_category()  
	{
		Assert.assertTrue(moveToElement_List(globalheaderintegrativeproduct.getProductMegamenuList(), "BY TYPE"));  
	}

	@Then("^Verify the below sub category list of 'BY TYPE'$")
	public void verify_the_below_sub_categoery_list_of_BY_TYPE() 
	{
	   Assert.assertTrue(listcontainsRequiredItems(globalheaderintegrativeproduct.getCategoryList_ByType(), 4)); 
	}

	@When("^click anyone subcategory of 'BY TYPE'$")
	public void click_anyone_subcategory_of_BY_TYPE()  
	{
		Category_ByType = clickOnListReturnText_Index(globalheaderintegrativeproduct.getCategoryList_ByType(), 4); 
	}

	@Then("^Verify user can navigate to appropriate subcategory page of 'BY TYPE'$")
	public void verify_user_can_navigate_to_appropriate_subcategory_page_of_BY_TYPE()  
	{
		//waitFor(30000);
		implicitWait(50);
		waitVisibilityOfElement(globalheaderintegrativeproduct.getCategoryClicked_Title());
		verifyExtendedUrl(Category_ByType.toLowerCase());
		verifyExpectedText(Category_ByType, globalheaderintegrativeproduct.getCategoryClicked_Title());
	}
	
	@Then("^Verify the \"([^\"]*)\" title of 'BY TYPE'$")
	public void verify_the_title_of_BY_TYPE(String text)
	{
		waitVisibilityOfElement(globalheaderintegrativeproduct.getFeaturedProductLabel_ByType());
		verifyExpectedText(text, globalheaderintegrativeproduct.getFeaturedProductLabel_ByType());
	}
	

	@Then("^Verify the featured product image of 'BY TYPE'$")
	public void verify_the_featured_product_image_of_BY_TYPE() 
	{
		String attributeValue = getAttributeValue(globalheaderintegrativeproduct.getFeaturedProductImage_ByType(), "src");
		Assert.assertTrue("Verify the expected and actual text", attributeValue.contains("http://"));
	}

	@Then("^Verify the featured product name of 'BY TYPE'$")
	public void verify_the_featured_product_name_of_BY_TYPE() throws Exception  
	{
		Assert.assertTrue(verifyTextIsDisplayed(globalheaderintegrativeproduct.getFeaturedProductName_ByType()));
	    
	}

	@Then("^Verify the 'View Product details' button is displayed and enabled of 'BY TYPE'$")
	public void verify_the_View_Product_details_button_is_displayed_and_enabled_of_BY_TYPE() throws Exception 
	{
	    Assert.assertTrue(verifyElementIsDisplayed(globalheaderintegrativeproduct.getViewProductDetails_ByType()));
	    Assert.assertTrue(globalheaderintegrativeproduct.getViewProductDetails_ByType().isEnabled());
	}


	@Then("^Verify the below sub category list of 'BY HEALTH INTEREST'$")
	public void verify_the_below_sub_categoery_list_of_BY_HEALTH_INTEREST()  
	{
		Assert.assertTrue(listcontainsRequiredItems(globalheaderintegrativeproduct.getCategoryList_ByHealthInterest(), 1)); 
	    
	}

	@When("^click anyone subcategory of 'BY HEALTH INTEREST'$")
	public void click_anyone_subcategory_of_BY_HEALTH_INTEREST()  
	{
		Category_ByHealthInterest = clickOnListReturnText_Index(globalheaderintegrativeproduct.getCategoryList_ByHealthInterest(), 1); 
	    
	}

	@Then("^Verify user can navigate to appropriate subcategory page of 'BY HEALTH INTEREST'$")
	public void verify_user_can_navigate_to_appropriate_subcategory_page_of_BY_HEALTH_INTEREST()  
	{
		//waitFor(30000);
		implicitWait(50);
		waitVisibilityOfElement(globalheaderintegrativeproduct.getCategoryClicked_Title());
		verifyExtendedUrl(Category_ByHealthInterest.toLowerCase());
		verifyExpectedText(Category_ByHealthInterest, globalheaderintegrativeproduct.getCategoryClicked_Title());
	    
	}

	@When("^Hover on the 'BY HEALTH INTEREST' category$")
	public void hover_on_the_BY_HEALTH_INTEREST_category()  
	{
		Assert.assertTrue(moveToElement_List(globalheaderintegrativeproduct.getProductMegamenuList(), "BY HEALTH INTEREST"));
	}

	@Then("^Verify the \"([^\"]*)\" title of 'BY HEALTH INTEREST'$")
	public void verify_the_title_of_BY_HEALTH_INTEREST(String text) 
	{
	    waitVisibilityOfElement(globalheaderintegrativeproduct.getFeaturedProductLabel_ByHealthInterest());
	    verifyExpectedText(text, globalheaderintegrativeproduct.getFeaturedProductLabel_ByHealthInterest());
	}

	@Then("^Verify the featured product image of 'BY HEALTH INTEREST'$")
	public void verify_the_featured_product_image_of_BY_HEALTH_INTEREST()  
	{
		String attributeValue = getAttributeValue(globalheaderintegrativeproduct.getFeaturedProductImage_ByHealthInterest(), "src");
		Assert.assertTrue("Verify the expected and actual text", attributeValue.contains("http://"));
	    
	}

	@Then("^Verify the featured product name of 'BY HEALTH INTEREST'$")
	public void verify_the_featured_product_name_of_BY_HEALTH_INTEREST() throws Exception  
	{
		Assert.assertTrue(verifyTextIsDisplayed(globalheaderintegrativeproduct.getFeaturedProductName_ByHealthInterest()));
	    
	}

	@Then("^Verify the 'View Product Details' button is displayed and enabled of 'BY HEALTH INTEREST'$")
	public void verify_the_View_Product_Details_button_is_displayed_and_enabled_of_BY_HEALTH_INTEREST() throws Exception  
	{
	    
		Assert.assertTrue(verifyElementIsDisplayed(globalheaderintegrativeproduct.getViewProductDetails_ByHealthInterest()));
	    Assert.assertTrue(globalheaderintegrativeproduct.getViewProductDetails_ByHealthInterest().isEnabled());
	    
	}

	@When("^click on \"([^\"]*)\" link$")
	public void click_on_link(String text)
	{
		waitFor(2000);
		Assert.assertTrue(moveToElement_List(globalheaderintegrativeproduct.getProductMegamenuList(), text));
		Assert.assertTrue(clickOnList_Value(globalheaderintegrativeproduct.getProductMegamenuList(), text));
	}

	@Then("^verify user can navigate to Product listing page$")
	public void verify_user_can_navigate_to_Product_listing_page()  
	{
		implicitWait(50);
		waitVisibilityOfElement(globalheaderintegrativeproduct.getSearchResultCount_PLP());
		verifyContainsText(globalheaderintegrativeproduct.getSearchResultCount_PLP(), "Results");
		verifyExtendedUrl("products");
		Assert.assertTrue(listcontainsRequiredItems(globalheaderintegrativeproduct.getViewProductDetails_PLP(), 1));
	}
	
	/*
	 * @And("^click featured product image$") public void
	 * click_featured_product_image()  { platform.clickOnRFPI(); }
	 * 
	 * @And("^click featured product view details$") public void
	 * click_featured_product_view_details()  {
	 * platform.clickOnRFPVD(); }
	 * 
	 * @And("^click bytype$") public void click_bytype()  {
	 * platform.clickOnByType(); }
	 * 
	 * @And("^click Antioxidants$") public void click_Antioxidants() throws
	 * Throwable { platform.clickOnAntioxidants(); }
	 * 
	 * @And("^click Health Interest$") public void click_Health_Interest() throws
	 * Throwable { platform.clickOnHI(); }
	 * 
	 * @And("^click  Adrenal Support$") public void click_Adrenal_Support() throws
	 * Throwable { platform.clickOnAS(); }
	 * 
	 * @And("^click Protocol Kits$") public void click_Protocol_Kits() throws
	 * Throwable { platform.clickOnPK(); }
	 * 
	 * @And("^click sub-items$") public void click_sub_items()  {
	 * platform.clickOnSubitems(); }
	 */
}
