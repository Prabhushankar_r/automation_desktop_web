package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class Sprint1_GlobelHeaderSteps extends AbstractStepDefinition {

	public Sprint1_GlobelHeaderSteps() throws FileNotFoundException, IOException, ParseException {
		super();
	}

	@Then("^Verify the display of Integrativepro logo$")
	public void verify_the_display_of_Integrativepro_logo() throws Throwable {
		verifyElementIsDisplayed(globelheader.headerintegrativeProLogo());
		//platform.headerIntegraiveprologo();
	}

	@And("^verify the display of cart icon$")
	public void verify_the_display_of_cart_icon() throws Throwable {
		verifyElementIsDisplayed(globelheader.headerCart());
		//platform.headerCart();
	}

	@And("^verify the display of search icon$")
	public void verify_the_display_of_search_icon() throws Throwable {
		verifyElementIsDisplayed(globelheader.headerSearch());
		//platform.headerSearch();
	}

	@And("^click on go to integrative pro$")
	public void click_on_go_to_integrative_pro() throws Throwable {
		platform.clickOnIp();
	}

}
