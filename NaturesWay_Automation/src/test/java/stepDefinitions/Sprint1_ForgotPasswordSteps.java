package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import platforms.WebPortal;

public class Sprint1_ForgotPasswordSteps extends WebPortal {

	public Sprint1_ForgotPasswordSteps() throws FileNotFoundException, IOException, ParseException {
		super();
	}

	@And("^click on sigin button$")
	public void click_on_sigin_button() throws Throwable {
		Assert.assertTrue(verifyElementIsDisplayed(forgotpassword.signIn()));
		clickButtonWithOutScroll(forgotpassword.signIn());
		//platform.button();
	}

	@Then("^click on forgot password$")
	public void click_on_forgot_password() throws Throwable {
		Assert.assertTrue(verifyElementIsDisplayed(forgotpassword.forgotpassword_link()));
		clickOnLinkWithOutScroll(forgotpassword.forgotpassword_link());
		//platform.forGetPasswordButton();
	}
	
	@And("^verify submit button is disabled in forgot password$")
	public void Verify_Submit_Button_Disabled_ForgotPassword_Screen() throws Throwable {
		verifyButtonDisabled(forgotpassword.submit());
	}
	
	

	@When("^An email address text box$")
	public void An_email_address_text_box() throws Throwable {
		Assert.assertTrue(verifyElementIsDisplayed(forgotpassword.email()));
		//platform.enterTheTextForTheForgetPassword();
	}

	@When("^verify submit button$")
	public void Verify_submit_Button() throws Throwable {
		Assert.assertTrue(verifyElementIsDisplayed(forgotpassword.submit()));
		//platform.enterTheTextForTheForgetPassword();
	}
	
	@When("^I should click on submit button$")
	public void i_should_not_to_click_submit_button() throws Throwable {
		Assert.assertTrue(verifyElementIsDisplayed(forgotpassword.submit()));
		clickButton(forgotpassword.submit(),forgotpassword.submit());
		//platform.clickOnSubmitTheForgotPassword();
	}

	@Then("^verify error message for invalid email$")
	public void Verify_Error_Message_InvalidEmailAddress() throws Throwable {
		error_Message_ForgotPassword();
		Assert.assertTrue(verifyElementIsDisplayed(forgotpassword.verifyErrorMessage()));
		//platform.clickOnSubmitTheForgotPassword();
	}

	
	
	@When("^I enter email \"(.*?)\"$")
	public void I_enter_invalid_email(String email) throws Throwable {
		Assert.assertTrue(verifyElementIsDisplayed(forgotpassword.email()));
		enterText(forgotpassword.email(),email);
		//platform.enterRongMail(email);
	}

	@And("^verify the success message$")
	public void verify_the_success_message() throws Throwable {
		Assert.assertTrue(verifyElementIsDisplayed(forgotpassword.TickIcon_ForgotPasswordSuccessscreen()));
		Assert.assertTrue(verifyElementIsDisplayed(forgotpassword.Message_ForgotPasswordSuccessscreen()));
		clickOnLinkWithOutScroll(forgotpassword.Closelink_ForgotPasswordSuccessscreen());
	}

	@And("^click on cancel button$")
	public void click_on_cancel_button() throws Throwable {
		Assert.assertTrue(verifyElementIsDisplayed(forgotpassword.forgotCancel()));
		clickOnLinkWithOutScroll(forgotpassword.forgotCancel());
	}

	@And("^click on the forgot text box$")
	public void click_on_the_forgot_text_box() throws Throwable {
		clickButtonWithOutScroll(forgotpassword.email());
		//platform.clickOnForgotPassword();
	}

	@Then("^verify the display of forgot password title$")
	public void verify_the_display_of_forgot_password_title() throws Throwable {
		verifyTextIsDisplayed(forgotpassword.forgotPassTitle());
		//platform.forgotPassTitle();
	}

	@Then("^verify the display of forgot password description$")
	public void verify_the_display_of_forgot_password_description() throws Throwable {
		verifyTextIsDisplayed(forgotpassword.forgotPassDes());
	}

}