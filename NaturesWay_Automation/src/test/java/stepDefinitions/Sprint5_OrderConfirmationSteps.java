package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import cucumber.api.java.en.*;
import platforms.WebPortal;

public class Sprint5_OrderConfirmationSteps extends WebPortal
{
	public Sprint5_OrderConfirmationSteps() throws FileNotFoundException, IOException, ParseException 
	{
		super();
		
	}
	
	@Then("^verify user is navigated to order confirmation page and order is placed successfully$")
	public void verify_user_is_navigated_to_order_confirmation_page_and_order_is_placed_successfully()  
	{
	    verifyExtendedUrl("");
	    
	}
	
	@Then("^verify the 'Order summary' details which is fetched from checkout page$")
	public void verify_the_Order_summary_details_which_is_fetched_from_checkout_page()  
	{
	    verifyExpectedText(Sprint4_CheckoutSteps.itemCount, orderConfirmationPage.itemCount_OrderConfirmation());
	    verifyExpectedText(Sprint4_CheckoutSteps.totalAmt, orderConfirmationPage.totalAmount_OrderConfirmation());
	    verifyExpectedText(Sprint4_CheckoutSteps.shippingAmt, orderConfirmationPage.shippingAmt_OrderConfirmation());
	    verifyExpectedText(Sprint4_CheckoutSteps.discountAmt, orderConfirmationPage.savingsOrDiscountAmt_OrderConfirmation());
	    verifyExpectedText(Sprint4_CheckoutSteps.tax, orderConfirmationPage.tax_OrderConfirmation());
	    verifyExpectedText(Sprint4_CheckoutSteps.subtotal, orderConfirmationPage.subTotal_OrderConfirmation());
	    verifyExpectedText(Sprint4_CheckoutSteps.orderTotal, orderConfirmationPage.orderToatl_OrderConfirmation());
	    
	}
	
	@Then("^verify the given billing address is displayed in order confirmation page$")
	public void verify_the_given_billing_address_is_displayed_in_order_confirmation_page()  
	{
		verifyExpectedText(Sprint5_ShippingInformationSteps.firstName_ShippingInfo, orderConfirmationPage.firstNameShipping_Orderconfirmation());
	    verifyExpectedText(Sprint5_ShippingInformationSteps.lastName_ShippingInfo, orderConfirmationPage.lastNameShipping_Orderconfirmation());
	    verifyExpectedText(Sprint5_ShippingInformationSteps.address_ShippingInfo, orderConfirmationPage.address1Shipping_Orderconfirmation());
	    verifyContainsText(orderConfirmationPage.zipcodeShipping_Orderconfirmation(), Sprint5_ShippingInformationSteps.zipcode_ShippingInfo);
	    
	    
	}

}
