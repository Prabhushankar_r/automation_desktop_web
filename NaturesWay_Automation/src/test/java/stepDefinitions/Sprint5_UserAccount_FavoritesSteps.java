package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;
import cucumber.api.java.en.*;
import pageobjects.webportal.Sprint3_UsersAccountPage;
import platforms.WebPortal;

import org.json.simple.parser.ParseException;
import org.junit.Assert;

import com.google.common.base.VerifyException;

public class Sprint5_UserAccount_FavoritesSteps extends WebPortal
{
	public String ProductTitle;

	public Sprint5_UserAccount_FavoritesSteps() throws FileNotFoundException, IOException, ParseException {
		super();

	}
	
	@Then("^Verify that Favorite product is displayed in favorites page$")
	public void verify_that_Favorite_product_is_displayed()
	{
	    waitVisibilityOfALLElement(myaccount.favoriteProducts());
	    Assert.assertTrue("Favorite Products mismatch", verifyTextInList(myaccount.favoriteProducts(), Sprint2_ProductListingSteps.productTitle));
	}
	
	@Then("^Verify user is landed in favorites page$")
	public void verify_user_is_landed_in_favorites_page()
	{
		waitForPageLoaded();
		waitVisibilityOfElement(myaccount.breadcrumb_Favorite());
		System.out.println(myaccount.breadcrumb_Favorite().getText());
		verifyExpectedText("FAVORITES", myaccount.breadcrumb_Favorite());
	    verifyExtendedUrl("favorites");
	}

	@And("^display the welcome message in favorite page$")
	public void display_Welcome_Message_FavoritePage() throws Exception
	{
		verifyWelcomeScreen_FavoritePage();		
	}
	
	@And("^verify the product list in favorite page$")
	public void verify_ProductList_FavoritePage() throws Exception
	{
		verifyProductList_FavoritePage();
		addProductExceedLimit_FavoritePage();
	}
	
	@And("^verify whether favorite page has a product$")
	public void verify_Atleast_OneProduct_Displayed_FavoritePage() throws Exception
	{
		verifyFavoritePage_HasAtleastOneProduct();	
		
	}
	
	@When("^click on remove from favorite link$")
	public void click_Remove_From_Favorite_Link() throws Exception
	{
		removeFromFavoriteFunctionality();	
		
	}
	
	@When("^click on view details link in favorite page$")
	public void click_View_Details_Link_Favorite_Page() throws Exception
	{
		ProductTitle=productlisting.selectaproductbyTitle().getText();
		clickOnList_Index(myaccount_favorite.viewDetailsButton_FavoritePage(),0);	
		waitForPageLoaded();
		
	}
	
	@Then("^display product detail page on selecting view detail link$")
	public void Redirect_ProductDetailPage_On_Selection_Of_ViewDetail_Link() throws Exception
	{
		 verifyExtendedUrl("products/");
		 Assert.assertEquals(ProductTitle,productdetailpage.productTitle_PDP().getText());
		
	}
	
	
	
	
}
