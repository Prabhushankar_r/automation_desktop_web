package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import platforms.WebPortal;

public class Sprint1_SiteMapSteps extends WebPortal {

	public Sprint1_SiteMapSteps() throws FileNotFoundException, IOException, ParseException {
		super();
	}

	@Then("^user should able to view the bread crumbs$")
	public void user_should_able_to_view_the_bread_crumbs() throws Throwable {
		verifyElementIsDisplayed(sitemap.siteMapBacktohome());
		clickOnLinkWithOutScroll(sitemap.siteMapBacktohome());
	}

	@Then("^verify the display of products$")
	public void verify_the_display_of_products() throws Throwable {
		verifyElementsAreDisplayed(sitemap.products_ByType_list());
		verifyElementsAreDisplayed(sitemap.products_ByHealthInterest_list());

	}

	@Then("^verify the display of resources$")
	public void verify_the_display_of_resources() throws Throwable {
		verifyElementsAreDisplayed(sitemap.ResourceSection_list());

	}

	@Then("^verify the display of about us$")
	public void verify_the_display_of_about_us() throws Throwable {
		verifyElementsAreDisplayed(sitemap.AboutUs_list());

	}

	@Then("^verfy the display of general links$")
	public void verfy_the_display_of_general_links() throws Throwable {
		verifyElementsAreDisplayed(sitemap.GeneralLinks_list());

	}

	@And("^click on sitemap link in globel footer$")
	public void click_on_sitemap_link_in_globel_footer() throws Throwable {
		clickOnLink(sitemap.siteMapLink(),sitemap.siteMapLink());
		
	}

}
