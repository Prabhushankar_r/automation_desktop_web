package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import platforms.WebPortal;

public class Sprint2_AboutUsSteps extends WebPortal {
	
	public Sprint2_AboutUsSteps() throws FileNotFoundException, IOException, ParseException {
		super();
    }
	@When("^click on about us$")
	public void click_on_about_us() throws Throwable {
		clickButtonWithOutScroll(aboutus.clickOnAboutUs());
	}

	@And("^click on who we are$")
	public void click_on_who_we_are() throws Throwable {
		holdAndClick(aboutus.clickOnAboutUs(),aboutus.WhoWeAreMenu_Header());
		
	}

	@And("^verify the display of hero banner title$")
	public void verify_the_display_of_hero_banner_title() throws Throwable {
		verifyTextIsDisplayed(aboutus.Aboutus_heroBannertitle());

	}

	@And("^verify the display of hero banner description$")
	public void verify_the_display_of_hero_banner_description() throws Throwable {
		verifyTextIsDisplayed(aboutus.Aboutus_heroBannerDescription());
	}

	@Then("^verify the title for contents with left image$")
	public void verify_Title_ContentsWithLeftImages() throws Throwable {
		waitForPageLoaded();
		Assert.assertTrue(verifyTextIsDisplayed(aboutus.Title_leftimages()));
	}

	@And("^verify the description for contents with left image$")
	public void verify_Description_ContentsWithLeftImages() throws Throwable {
		Assert.assertTrue(verifyTextIsDisplayed(aboutus.Description_leftimages()));
		// VerifyDescription_LeftImages();
	}

	@And("^click on read more button on left side content$")
	public void Click_ReadMore_Button_LeftSideContents() throws Throwable {
		ClickAllButton_NavigateBack(aboutus.ReadMoreBtn_leftimages());
		 //ClickReadMoreBtn_LeftImages();
	}

	@Then("^verify the title for contents with right image$")
	public void verify_Title_ContentsWithRightImages() throws Throwable {
		waitForPageLoaded();
		Assert.assertTrue(verifyTextIsDisplayed(aboutus.Title_rightimages()));
		 //platform.VerifyTitle_RightImages();
	}

	@And("^verify the description for contents with right image$")
	public void verify_Description_ContentsWithRightImages() throws Throwable {
		Assert.assertTrue(verifyTextIsDisplayed(aboutus.Description_rightimages()));
		 //platform.VerifyDescription_RightImages();
	}

	@And("^click on read more button on right side content$")
	public void Click_ReadMore_Button_RightSideContents() throws Throwable {
		ClickAllButton_NavigateBack(aboutus.ReadMoreBtn_rightimages());

		// platform.ClickReadMoreBtn_RightImages();
	}
	
	@Then("^verify the display of Our History title$")
	public void verify_the_display_of_Our_History_title() throws Throwable {
		// platform.AuOHTitle();
	}

	@Then("^verify the display of Our History images$")
	public void verify_the_display_of_Our_History_images() throws Throwable {
		// platform.AuOHImages();
	}

	@Then("^verify the display of Our History description$")
	public void verify_the_display_of_Our_History_description() throws Throwable {
		// platform.AuOHDesc();
	}

	@Then("^verify the display of Our Process title$")
	public void verify_the_display_of_Our_Process_title() throws Throwable {
		 //platform.AuOPTitle();
	}

	@Then("^verify the display of Our Process description$")
	public void verify_the_display_of_Our_Process_description() throws Throwable {
		// platform.AuOPDesc();
	}

	@And("^click on Our Process read more button$")
	public void click_on_Our_Process_read_more_button() throws Throwable {
		// platform.clickOnOPRMButton();
	}

	@And("^verify the display of fotter banner title$")
	public void verify_the_display_of_fotter_banner_title() throws Throwable {
		// platform.AuFBTitle();
	}

	@And("^verify the display of fotter banner description$")
	public void verify_the_display_of_fotter_banner_description() throws Throwable {
		 //platform.AuFBDesc();
	}
	@Then("^verify the display of about us header$")
	public void verify_the_display_of_about_us_header() throws Throwable {
		waitForPageLoaded();
		verifyTextIsDisplayed(aboutus.AuHeader());
	    //platform.AuHeader();
	}


	
	

}
