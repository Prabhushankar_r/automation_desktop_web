package stepDefinitions;

import static org.junit.Assert.assertFalse;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;

import cucumber.api.java.en.*;
import org.junit.Assert;
import platforms.WebPortal;

public class Sprint4_CheckoutSteps extends WebPortal 
{
	public static String productName, productPrice;;

	public static String itemCount, totalAmt, shippingAmt, discountAmt, tax, subtotal, orderTotal;
	public Sprint4_CheckoutSteps() throws FileNotFoundException, IOException, ParseException {
		super();
		
	}
	
	@When("^display checkout page$")
	public void Display_Checkout_Page() throws Throwable 
	{
		waitForPageLoaded();
		waitTillElementExist(shippinginformation.AddNewShippingAddress_Link_Checkout());
		verifyExtendedUrl("checkout");
	}
	
	@When("^click the 'Coninue shopping' link$")
	public void click_the_Coninue_shopping_link()  
	{
	    waitElementToBeClickable(checkoutpage.continueShoppingLink());
	    clickButtonWithOutScroll(checkoutpage.continueShoppingLink());
	}
	
	@When("^click on 'Add new payment method' link$")
	public void click_on_Add_new_payment_method_link() 
	{
		waitElementToBeClickable(checkoutpage.addNewPaymentLink());
	    clickButtonWithOutScroll(checkoutpage.addNewPaymentLink());
	   
	}

	@When("^Enter the cardno, name on card, Exp Month, Exp year and CVV$")
	public void enter_the_cardno_name_on_card_Exp_Month_Exp_year_and_CVV() throws Exception 
	{
	    waitTillElementExist(checkoutpage.cardNumber_Payment());
	    enterText(checkoutpage.cardNumber_Payment(), "4111 1111 1111 1111");
	    enterText(checkoutpage.cardName_Payment(),"Test");
	    selectElement_DropDown(checkoutpage.cardExpMonth_Payment(), 2);
	    selectElement_DropDown(checkoutpage.cardExpYear_Payment(), 2);
	    enterText(checkoutpage.cardCVV_Payment(), "999");    
	   
	}

	@Then("^verify 'Billing address same as shipping address' and 'Save payment method for future use' is enabled$")
	public void verify_Billing_address_same_as_shipping_address_and_Save_payment_method_for_future_use_is_enabled() 
	{
	    Assert.assertTrue(checkoutpage.billingAddSameAsShippingAddCheckbox_Payment().isEnabled());
	    Assert.assertTrue(checkoutpage.savePaymentCheckbox_Payment().isEnabled());
	    
	   
	}

	@When("^click the 'Save&continue' button$")
	public void click_the_Save_continue_button() 
	{
	    clickButtonWithOutScroll(checkoutpage.saveAndContinue_Button());
	   
	}

	@Then("^verify the 'card no' and 'billing address'$")
	public void verify_the_card_no_and_billing_address() 
	{
	    
	   
	}

	@Then("^Verify the 'Place Order' button is enabled$")
	public void verify_the_Place_Order_button_is_enabled() 
	{
	    Assert.assertTrue(checkoutpage.placeOrder_Button().isEnabled());
	   
	}

	@When("^Get the details of 'Items','Shipping & Handling','Discounts','subtotal', 'tax' and 'Order total' from order summary$")
	public void get_the_details_of_Items_Shipping_Handling_Discounts_subtotal_tax_and_Order_total_from_order_summary() 
	{
	    itemCount = checkoutpage.itemCount_Checkout().getText();
	    totalAmt = checkoutpage.totalAmount_Checkout().getText();
	    shippingAmt = checkoutpage.shippingAmt_Checkout().getText();
	    discountAmt = checkoutpage.savingsOrDiscountAmt_Checkout().getText();
	    tax = checkoutpage.tax_Checkout().getText();
	    subtotal = checkoutpage.subTotal_Checkout().getText();
	    orderTotal = checkoutpage.orderTotal_Checkout().getText();
	}

	@When("^Click the 'Place order' button$")
	public void click_the_Place_order_button()
	{
	   clickButtonWithOutScroll(checkoutpage.placeOrder_Button());
	}
	
	@Then("^verify Billing address same as shipping address is displayed with checkbox$")
	public void verify_Billing_address_same_as_shipping_address_is_displayed_with_checkbox() throws Exception {
		waitTillElementExist(checkoutpage.billingAddSameAsShippingAddCheckbox_Payment());
		Assert.assertTrue("Billing address same as shipping address checbox is not displayed", verifyElementIsDisplayed(checkoutpage.billingAddSameAsShippingAddCheckbox_Payment()));
		verifyExpectedText("Billing address same as shipping address", checkoutpage.billingAddSameAsShippingAddressText());
	}

	@When("^uncheck the 'Billing address same as shipping address' checkbox$")
	public void uncheck_the_Billing_address_same_as_shipping_address_checkbox() {
		clickButtonWithOutScroll(checkoutpage.billingAddSameAsShippingAddCheckbox_Payment());
	    
	}

	@Then("^verify the componenets of the 'New billing address'$")
	public void verify_the_componenets_of_the_New_billing_address() throws Exception {
		waitTillElementExist(checkoutpage.companyName_billingAddress());
		Assert.assertTrue("Company name is not displayed", verifyElementIsDisplayed(checkoutpage.companyName_billingAddress()));
		Assert.assertTrue("First name is not displayed", verifyElementIsDisplayed(checkoutpage.firstName_billingAddress()));
		Assert.assertTrue("last name is not displayed", verifyElementIsDisplayed(checkoutpage.lastName_billingAddress()));
		Assert.assertTrue("address1 is not displayed", verifyElementIsDisplayed(checkoutpage.address1_billingAddress()));
		Assert.assertTrue("zipcode  is not displayed", verifyElementIsDisplayed(checkoutpage.zipcode_billingAddresss()));
		Assert.assertTrue("city is not displayed", verifyElementIsDisplayed(checkoutpage.city_billingAddress()));
		Assert.assertTrue("state is not displayed", verifyElementIsDisplayed(checkoutpage.stateDropdown_billingAddress()));
		Assert.assertTrue("Save&Continue button is not displayed", verifyElementIsDisplayed(checkoutpage.saveAndContinue_Button()));
		Assert.assertTrue("cancel link is not displayed", verifyElementIsDisplayed(checkoutpage.cancelLink_billingAddres()));  
	}
	
	@When("^enter 'company name','firstname','lastname','address', 'zipcode'$")
	public void enter_company_name_firstname_lastname_address_zipcode() throws Exception  {
		waitTillElementExist(checkoutpage.companyName_billingAddress());
		enterText(checkoutpage.companyName_billingAddress(), "Test Company");
		enterText(checkoutpage.firstName_billingAddress(), "Prabhu");
		enterText(checkoutpage.lastName_billingAddress(), "Shankar");
		enterText(checkoutpage.address1_billingAddress(), "1650 Grand Concourse");
		enterText(checkoutpage.zipcode_billingAddresss(), "10457");
		keyboradActions("Tab");
	   
	}

	@Then("^verify city and state is autopopulated in the new billing address section$")
	public void verify_city_and_state_is_autopopulated_in_the_new_billing_address_section() throws Exception 
	{
	    Assert.assertTrue("City is NOT autopulated under billing address", verifyElementIsPresent(checkoutpage.cityAutoPopulated_BillingAddress()));
	    Assert.assertTrue("State is NOT autopulated under billing address", verifyElementIsPresent(checkoutpage.stateAutoPopulated_BillingAddress()));
	}

	@When("^click 'cancel' link in new billing address section$")
	public void click_cancel_link() throws Throwable 
	{
	   clickButtonWithOutScroll(checkoutpage.cancelLink_billingAddres());
	}

	@Then("^verify new billing address section is disappeared$")
	public void verify_new_billing_address_section_is_disappeared() throws Exception 
	{
	   Assert.assertFalse("New Billing address sections is not displayed", checkoutpage.companyName_billingAddress().isDisplayed());
	}
	
	@Then("^verify the line item count$")
	public void verify_the_line_item_count() 
	{
	  waitVisibilityOfALLElement(checkoutpage.lineItem_List());	
	  Assert.assertTrue("Zero products displayed in the line item", listcontainsRequiredItems(checkoutpage.lineItem_List(), 1));  
	}

	@When("^clicks save for later link for the product in '(\\d+)'th index in the lineitem$")
	public void clicks_save_for_later_link_for_the_product_in_th_index_in_the_lineitem(int index) throws InterruptedException 
	{
		productName = getTextOnList_BasedOnGivenIndex(checkoutpage.productName_List(), index);
		clickOnList_Index(checkoutpage.saveForLater_List(), index);
	}

	@Then("^verify the respective product is disappeared from the lineitem$")
	public void verify_the_respective_product_is_disappeared_from_the_lineitem() 
	{
		waitFor(7000);
		System.out.println(verifyTextInList(checkoutpage.productName_List(), productName));
	    Assert.assertFalse("The respective product is NOT disappeared", verifyTextInList(checkoutpage.productName_List(), productName));
	}
	
	@When("^change the product Qty as \"([^\"]*)\" and verify the product price got updated properly$")
	public void change_the_product_Qty_as_and_verify_the_product_price_got_updated_properly(String Qty) throws InterruptedException {
		
		String productPrice_Qty = getTextOnList_BasedOnGivenIndex(checkoutpage.productUnitPrice_List(), 0);
		System.out.println("Before  " + productPrice_Qty);
		clickOnList_Index(checkoutpage.qtyArrow_List(), 0);
		waitFor(2000);
		dropdownWithoutSelect(checkoutpage.qtyAndUnitDropdownValue_list(), Qty);
		waitFor(7000);
		double productQtyTotal = multiplyDoubles(Qty, productPrice_Qty);
		String ProductQtyTotal_String = convertDecimals(doubleToString(productQtyTotal), 2);
		System.out.println("Multiplied Amt "+ ProductQtyTotal_String);
		Assert.assertTrue("Product Price is NOT updated as per the qty", verifyContainsTextInGivenIndex_List(checkoutpage.productPrice_List(), 0, ProductQtyTotal_String));
	}

	@When("^change the unit as \"([^\"]*)\" and verify the product price got updated properly$")
	public void change_the_unit_as_and_verify_the_product_price_got_updated_properly(String units) throws InterruptedException 
	{
		String productPrice_Units = getTextOnList_BasedOnGivenIndex(checkoutpage.productPrice_List(), 0);
		System.out.println("Before  " + productPrice_Units);
		clickOnList_Index(checkoutpage.unitArrow_List(), 0);
		waitFor(2000);
		dropdownWithoutSelect(checkoutpage.qtyAndUnitDropdownValue_list(), units);
		waitFor(7000);
		double productUnitTotal = multiplyDoubles("12", productPrice_Units);
		String ProductUnitTotal_String = convertDecimals(doubleToString(productUnitTotal), 2);
		System.out.println("Multiplied Amt "+ ProductUnitTotal_String);
		Assert.assertTrue("Product Price is NOT updated as per the units", verifyContainsTextInGivenIndex_List(checkoutpage.productPrice_List(), 0, ProductUnitTotal_String));
		
	}
	
	@Then("^verify the componenets are displayed under order summary section$")
	public void verify_the_componenets_are_displayed_under_order_summary_section() throws Exception  {
	  
		Assert.assertTrue("Item Price is not displayed", verifyElementIsDisplayed(checkoutpage.totalAmount_Checkout()));
		Assert.assertTrue("Shipping Price is not displayed", verifyElementIsDisplayed(checkoutpage.shippingAmt_Checkout()));
		Assert.assertTrue("Savings Price is not displayed", verifyElementIsDisplayed(checkoutpage.savingsOrDiscountAmt_Checkout()));
		Assert.assertTrue("Subtotal Price is not displayed", verifyElementIsDisplayed(checkoutpage.subTotal_Checkout()));
		Assert.assertTrue("Tax Price is not displayed", verifyElementIsDisplayed(checkoutpage.tax_Checkout()));
		Assert.assertTrue("Order Total Price is not displayed", verifyElementIsDisplayed(checkoutpage.orderTotal_Checkout()));
		//String attributeValue = getAttributeValue(checkoutpage.placeOrder_Button(),"disabled");
		//System.out.println(attributeValue);
		//Assert.assertEquals(attributeValue, "true", "Place Order field is not disabled");
		Assert.assertFalse("Place Order field is not disabled", checkoutpage.placeOrder_Button().isEnabled());
	}

	@When("^Delete the '(\\d+)' index product in the review order section$")
	public void delete_the_index_product_in_the_review_order_section(int index) throws InterruptedException  {
		
		productName = getTextOnList_BasedOnGivenIndex(checkoutpage.productName_List(), index);
		productPrice = getTextOnList_BasedOnGivenIndex(checkoutpage.productPrice_List(), index);
		orderTotal = replaceStringValue(checkoutpage.orderTotal_Checkout().getText(), ",");
		clickOnList_Index(checkoutpage.deleteLink_List(), index);
	   
	}

	@Then("^verify the price is updated properly on the order summary section$")
	public void verify_the_price_is_updated_properly_on_the_order_summary_section()  {
	    System.out.println("Order Total : "+ orderTotal);
	    waitFor(3000);
	    System.out.println("Product Price : "+ replaceStringValue(productPrice, ","));
		double subtractAmt = subtractDoubles(orderTotal, replaceStringValue(productPrice, ","));
		String SubtractTotal_String = convertDecimals(doubleToString(subtractAmt), 2);
		System.out.println("Subtract Amt "+ SubtractTotal_String);
		//verifyContainsText(checkoutpage.orderTotal_Checkout(), SubtractTotal_String);
		Assert.assertTrue("Order Total is not updated for the deleted product", replaceStringValue(checkoutpage.orderTotal_Checkout().getText(), ",").contains(SubtractTotal_String));
	}

}
