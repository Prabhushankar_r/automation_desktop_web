package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import platforms.WebPortal;

public class Sprint4_CartSteps extends WebPortal {

	public Sprint4_CartSteps() throws FileNotFoundException, IOException, ParseException {
		super();
		}
	
	@And("^click proceed to checkout$")
	public void click_proceed_to_checkout() throws Throwable {
		WaitTillElementExist(cartdrawer.CartDrawerScreen(),30);
		Assert.assertTrue(verifyElementIsDisplayed(cartdrawer.Proceed_To_Checkout_Btn()));
		clickButtonWithOutScroll(cartdrawer.Proceed_To_Checkout_Btn());
	}
	
	@Then("^verify save for later section is empty$")
	public void verify_Saveforlater_Section_Empty_CartPage() throws Throwable{
		waitForPageLoaded();
		verifysaveforlatersection_Empty();
	}

	@When("^click on cart icon in header$")
	public void click_Cart_Icon_In_Header() throws Throwable{
		Assert.assertTrue(verifyElementIsDisplayed(cartdrawer.cartIcon_header()));
		clickButtonWithOutScroll(cartdrawer.cartIcon_header());
		waitForPageLoaded();
	
	}
	
	@And("^verify the cart icon in header is empty$")
	public void verify_the_cartIcon_In_Header_IsEmpty() throws Throwable{
		verify_CartIcon_header_Empty();
	}
	
	@And("^click continue shopping$")
	public void click_continue_shopping() throws Throwable {
		waitTillElementExist(cartdrawer.CartDrawerScreen());
		Assert.assertTrue(verifyElementIsDisplayed(cartdrawer.ContinueShopping_Btn()));
		clickButtonWithOutScroll(cartdrawer.ContinueShopping_Btn());
	}
	
	@Then("^verify the product details in shopping cart section$")
	public void verify_ProductDetails_ShoppingCart_Section_CartPage() throws Throwable{
		waitForPageLoaded();
		verifyProductComponents_Shoppingcart();
	}
	
	@And("^verify the product details in saveforlater section$")
	public void verify_ProductDetails_In_saveforlater_section() throws Throwable {
		verifyProductComponents_saveforlater();
	}
		
	@And("^verify new product is displayed first in shopping cart$")
	public void Verify_NewlyAddedProduct_Is_Displayed_First_InList_CartPage() throws Throwable {
		verifyAddedProduct_DisplayedFirst();
		}
	
	@And("^verify new product is displayed first in saveforlater$")
	public void Verify_NewlyAddedProduct_Is_Displayed_First_InList_SaveforLaterSection() throws Throwable {
		waitForPageLoaded();
		verifyAddedProduct_DisplayedFirst_SaveforLatersection();
		}
	

	@And("^click delete$")
	public void click_delete() throws Throwable {
		deleteproduct_cartdrawer();
		}
	
	@And("^click Add to Cart button and verify the count$")
	public void verify_cartcount_and_select_AddToCartButton() throws Throwable {
		verifyCartCount_and_AddtoCartButton();

	}
	
	@And("^verify product displayed in shopping cart section$")
	public void Verify_ProductDisplayed_shoppingCart_CartPage() throws Throwable {
		verifyproductorder_shoppingcart();

	}
		
	@And("^click Save for later in cart drawer screen$")
	public void click_delete_Save_for_later() throws Throwable {
      //platform.SFL();
	}

	@Then("^verify proceed to checkout button$")
	public void Verify_ProceedToCheckout_Btn() throws Throwable {
     // platform.verify_ProceedToCheckout_Btn();
	}
	
	@And("^verify Continue shopping button$")
	public void Verify_ContinueShopping_Btn() throws Throwable {
	}

	@And("^verify subtotal and price is displayed$")
	public void verify_SubTotal_PriceDetails_CartPage() throws Throwable {
		Assert.assertTrue(verifyTextAreDisplayed(cartdrawer.actualpriceOfProduct_List()));	
		Assert.assertTrue(verifyTextIsDisplayed(cartdrawer.Subtotal_PriceDetails_CartPage()));
	}
	
	@When("^click on proceed to checkout in cart page$")
	public void Click_ProceedToCheckout_CartPage() throws Throwable {
		Assert.assertTrue(verifyElementIsPresent(cartdrawer.ProceedToCheckout_cartpage()));	
		clickButtonWithOutScroll(cartdrawer.ProceedToCheckout_cartpage());
		waitForPageLoaded();
	}

	
	@When("^display cart page$")
	public void Display_Cart_Page() throws Throwable {
		waitForPageLoaded();
		waitTillElementExist(cartdrawer.ProceedToCheckout_cartpage());
		verifyExtendedUrl("cart");

	}
	
	@When("^click on Continue Shopping in cart page$")
	public void Click_ContinueShopping_header_CartPage() throws Throwable {
		clickButtonWithOutScroll(cartdrawer.ContinueShopping_cartpage());
		
	}
	
	@When("^delete link is selected in cart section$")
	public void Select_Delete_Link_InCartPage_ForAProduct() throws Throwable {
		DeleteFunctionality_ShoppingCart_cartPage();
	}
	
	@Then("^display product listing page$")
	public void Display_ProductListingPage() throws Throwable {
			verifyExtendedUrl("products");
	}
	
	
	@Then("^verify save for later functionality in cart page$")
	public void verify_SaveForLater_Functionality_In_ShoppingCart() throws Throwable {
		saveforlaterFunctionality_cartPage();
	}
	
	
	@And("^verify save for later is displayed after relogin$")
	public void verify_SaveforLater_displayed_reLogin() throws Throwable {
		saveforlater_AfterReLogin();
		}
	
	@When("^click Add to Cart button in saveforlater section$")
	public void Click_AddToCart_Link_SaveforLatersection() throws Throwable {
		clickAddToCartLink_Saveforlatersection();
		}
	
	@Then("^move the products into cart section$")
	public void verify_Products_Moved_IntoCartSection_FromSaveforlater() throws Throwable {
		VerifyProducts_moved_FromSaveforLater();
	}
	
	@Then("^delete link is selected in save for later section$")
	public void select_DeleteLink_In_SaveforLatersection() throws Throwable {
		DeleteFunctionality_saveforLater();
	}
	
	@Then("^verify the default values of Qty and Units fields$")
	public void verify_Default_values_Qty_Units_Section() throws Throwable {
		
	}
	
	@And("^change the qty and units values for save for later products$")
	public void Change_Values_Quanity_Units_Saveforlatersection() throws Throwable {
		Assert.assertTrue(verifyElementIsPresent(cartdrawer.Qty_DropdownIcon_saveforlater()));
		clickButtonWithOutScroll(cartdrawer.Qty_DropdownIcon_saveforlater());
		Assert.assertTrue(verifyElementIsDisplayedBasedOnIndex(cartdrawer.QtyValuesList_safeforlater(),0));
		clickOnList_Index(cartdrawer.QtyValuesList_safeforlater(), 0);

		Assert.assertTrue(verifyElementIsPresent(cartdrawer.Units_DropdownIcon_saveforlater()));
		javascriptExecutorClick(cartdrawer.Units_DropdownIcon_saveforlater());
		//clickButtonWithOutScroll(cartdrawer.Units_DropdownIcon_saveforlater());
		Assert.assertTrue(verifyElementIsDisplayedBasedOnIndex(cartdrawer.UnitsValuesList_safeforlater(),0));
		clickOnList_Index(cartdrawer.UnitsValuesList_safeforlater(), 0);
	}

	
	@And("^verify proceed to checkout button is in disabled state$")
	public void Display_ProceedToCheckout_Button_DisabledState() throws Throwable {
		Assert.assertTrue(cartdrawer.ProceedToCheckout_cartpage().getAttribute("class").contains("disabled"), "Proceed To Checkout button is not in disabled state");
	}

	@Then("^display need help button in cart page$")
	public void Display_NeedHelp_Button_CartPage() throws Throwable {
		Assert.assertTrue(verifyElementIsPresent(cartdrawer.NeedHelpBtn_CartPage()));
		}

	@And("^display return policy button in cart page$")
	public void Display_ReturnPolicy_Button_CartPage() throws Throwable {
		Assert.assertTrue(verifyElementIsPresent(cartdrawer.ReturnPolicyBtn_CartPage()));
	}
	
	@When("^click on need help button in cart page$")
	public void Click_NeedHelp_Button() throws Throwable {
		clickButtonWithOutScroll(cartdrawer.NeedHelpBtn_CartPage());
	}
	
	@When("^click on return policy button in cart page$")
	public void Click_ReturnPolicy_Button() throws Throwable {
		clickButtonWithOutScroll(cartdrawer.ReturnPolicyBtn_CartPage());
	}
	
	@When("^delete link is selected in cart drawer screen$")
	public void Click_DeleteLink_In_CartDrawerScreen() throws Throwable {
		DeleteFunctionality_cartDrawer();
	}

	@When("^display continue shopping and proceed to checkout CTA button$")
	public void display_ContinueShopping_Proceedtocheckout_CTAbutton() throws Throwable {
		waitTillElementExist(cartdrawer.CartDrawerScreen());
		Assert.assertTrue(verifyElementIsPresent(cartdrawer.ContinueShopping_cartdrawer()));
		Assert.assertTrue(verifyElementIsPresent(cartdrawer.Proceed_To_Checkout_Btn()));
	}
	
	@Then("^display message for empty cart section \"(.*?)\"$")
	public void display_Message_EmptyCartSection(String text) throws Throwable {
		verifyExpectedText(text, cartdrawer.emptycart_message_shoppingcart());	
	}

	@And("^display message for empty saveforlater section \"(.*?)\"$")
	public void display_Message_EmptySaveForLaterSection(String text) throws Throwable {
		verifyExpectedText(text, cartdrawer.emptycart_message_saveforlater());
	}
	

	@And("^display Cart Drawer screen$")
	public void display_CartDrawer_Screen() throws Throwable {
		waitTillElementExist(cartdrawer.CartDrawerScreen());
		Assert.assertTrue(verifyElementIsPresent(cartdrawer.CartDrawerScreen()));
	}
	
	@And("^verify save for later section is empty in cart page$")
	public void verify_SaveForLater_Section_Empty() throws Throwable {
		verifysaveforlatersection_Empty();
		//checkSaveForLaterSectionEmpty();
    }

}


