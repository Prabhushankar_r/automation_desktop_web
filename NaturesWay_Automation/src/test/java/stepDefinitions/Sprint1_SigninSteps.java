package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;

import cucumber.api.DataTable;
import cucumber.api.java.en.*;
import platforms.WebPortal;

public class Sprint1_SigninSteps extends WebPortal {

	public Sprint1_SigninSteps() throws FileNotFoundException, IOException, ParseException {
		super();
	}
	
	@And("^I should able to see components of signin model$")
	public void i_should_able_to_see_components_of_signin_model(DataTable components) throws Throwable{
		verify_signin_components(components);
	}
	
	@Then("^I see model window header Sign In in top of the window and below to Brandbox$")
	public void i_see_model_window_header_Sign_In_in_top_of_the_window_and_below_to_Brandbox() throws Throwable{
		check_signin_header();
	}
	
	@When("^I click on email address$")
	public void i_click_on_email_address() throws Throwable {
		click_on_signin_field();
	}

	@Then("^watermark should become smaller$")
	public void watermark_should_become_smaller() throws Throwable {
		check_watermark_on_signin_field();
	}

	@And("^I Enter your email address text change as Email address$")
	public void i_Enter_your_email_address_text_change_as_Email_address() throws Throwable {
		check_label_on_signin_field();
	}

	@When("^I click on password$")
	public void i_click_on_password() throws Throwable {
		click_on_password_field();
	}

	@Then("^I see password text become smaller$")
	public void i_see_password_text_become_smaller() throws Throwable {
		check_watermark_on_password_field();
		check_label_on_password_field();
	}
	
	@Then("^I see the show icon as eye icon with striked off mark on the right side of password field$")
	public void i_see_the_show_icon_as_eye_icon_with_striked_off_mark_on_the_right_side_of_password_field() throws Throwable {
		validate_password_eye_icon_with_slash();
	}
	
	@When("^I enter the password$")
	public void i_enter_the_password() throws Throwable {
		enter_password("Sample123$");
	}
	
	@When("^I click on show icon, hide eye icon should change without striked off mark$")
	public void i_click_on_show_icon_hide_eye_icon_should_change_without_striked_off_mark() throws Throwable {
		validate_click_eye_icon_in_password_field();
		validate_password_eye_icon_without_slash();
	}
	
	@And("^I should see the entered characters$")
	public void i_should_see_the_entered_characters() throws Throwable {
		validate_unmasked_characters_in_password_field();
	}
	
	@When("^I click on hide icon, show eye icon should change with striked off mark$")
	public void i_click_on_hide_icon_show_eye_icon_should_change_with_striked_off_mark() throws Throwable {
		validate_click_eye_icon_in_password_field();
		validate_password_eye_icon_with_slash();
	}
	
	@And("^I see the masked characters$")
	public void i_see_the_masked_characters() throws Throwable {
		validate_masked_characters_in_password_field();
	}
	
	@When("^I double click show icon$")
	public void i_double_click_show_icon() throws Throwable {
		validate_double_click_show_icon();
	}
	
	@Then("^I should see the masked characters$")
	public void i_should_see_the_masked_characters() throws Throwable {
		validate_masked_characters_in_password_field();
	}
	
	@When("^I double click Hide icon$")
	public void i_double_click_Hide_icon() throws Throwable {
		validate_double_click_hide_icon();
	}
	
	@Then("^I should see the characters$")
	public void i_should_see_the_characters() throws Throwable {
		validate_unmasked_characters_in_password_field();
	}
	
	@Then("I see the Forgot Password hyperlinked text below to password field")
	public void i_see_the_Forgot_Password_hyperlinked_text_below_to_password_field() throws Throwable {
		check_forget_password_link_in_signin_window();
	}
	
	@When("I click on Forgot password link")
	public void i_click_on_Forgot_password_link() throws Throwable {
		validate_forget_password();
	}
	
	@Then("I should see the Forgot password content window")
	public void i_should_see_the_Forgot_password_content_window() throws Throwable {
		validate_forgot_password_content_window();
	}
	
	@When("Enter the valid email address \"(.*?)\" and the Password \"(.*?)\"")
	public void enter_the_valid_email_address_and_the_Password(String username, String password) throws Throwable {
		signIn(username, password);
	}

	@And("I Click on Submit button")
	public void i_Click_on_Submit_button() throws Throwable {
		tap_signin();
	}

	@Then("I am succesfully signed in to the site on valid input")
	public void i_am_succesfully_signed_in_to_the_site_on_valid_input() throws Throwable {
		validate_user_signed_in();
	}

	@When("I do Sign In with Invalid email address \"(.*?)\" and the Password \"(.*?)\"")
	public void enter_the_Invalid_email_address_and_the_Password(String invalid_username, String invalid_password) throws Throwable {
		signIn(invalid_username, invalid_password);
	}

	@Then("I see the field with red color")
	public void i_see_the_field_with_red_color() throws Throwable {
		validate_password_field_validation_error();
	}

	@Then("I see the explanation of error message as Information provided doesn't match")
	public void i_see_the_explanation_of_error_message_as_Information_provided_does_not_match() throws Throwable {
		validate_error_message_for_invalid_signin();
	}

	@Then("I see the Create an Account hyperlinked text below to Sign In tab")
	public void i_see_the_Create_an_Account_hyperlinked_text_below_to_Sign_In_tab() throws Throwable {
		check_for_create_an_account_link_in_signin_model_window();
	}

	@When("I click create an account hyperlinked text")
	public void i_click_create_an_account_hyperlinked_text() throws Throwable {
		validate_create_an_account_link_in_signin_model_window();
	}

	@Then("I should be navigated to Create an Account page")
	public void i_should_be_navigated_to_Create_an_Account_page() throws Throwable {
		validate_create_an_account_page();
	}

	@Then("Close the Create an Account page")
	public void close_the_Create_an_Account_page() throws Throwable {
		close_create_an_account_page();
	}

	@Then("I see the close icon on top right corner of the Model window")
	public void i_see_the_close_icon_on_top_right_corner_of_the_Model_window() throws Throwable {
		check_close_icon_in_signin_model_window();
	}

	@When("I click on close icon")
	public void i_click_on_close_icon() throws Throwable {
		validate_close_icon_signin_model_window();
	}

	@Then("Sign In model closed and I see the previous page")
	public void sign_In_model_closed_and_I_see_the_previous_page() throws Throwable {
		validate_SignIn_Modal_Window_Closed();
	}

	@Then("I Click on outside model window")
	public void i_Click_on_outside_model_window() throws Throwable {
		validate_signin_model_window_outside_click_functionality();
	}
	
	@Then("^I tap on Sign In$")
	public void tap_sign() throws Throwable {
		tap_signin();
	}
	
	@And("^user logs out$")
	public void logOut() throws Exception {
		logOutApp();
	}
	
	@Then("^I should see SignIn Model window$")
	public void signin_model_window() throws Throwable {
		validate_SignIn_Modal_Window();
	}
	
	@Then("^I do Sign In Default Credentials$")
	public void sign_in_default() throws Throwable {
		signIn_with_default_credentials();
	}
	


}