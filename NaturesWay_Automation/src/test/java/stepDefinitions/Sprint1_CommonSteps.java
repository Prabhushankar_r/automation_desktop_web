package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;

import cucumber.api.Scenario;
import cucumber.api.java.en.And;
import org.json.simple.parser.ParseException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageobjects.webportal.Sprint1_Careers;
import platforms.WebPortal;

public class Sprint1_CommonSteps extends WebPortal {
	
	public Sprint1_CommonSteps() throws FileNotFoundException, IOException, ParseException {
		super();
	}

	// background
	@Given("^I am in home page of integrative pro$")
	public void launch_the_integrative_pro_url_in_the_browser() throws Throwable {
		Thread.sleep(10000);
		launch();
	}

	@Then("^close ConfirmMessage$")
	public void close_confirmmessage() throws Throwable {
		Thread.sleep(2000);
		clickButtonWithOutScroll(conformmessage.confirmMessage());
		//clickButton(conformmessage.confirmMessage(),conformmessage.confirmMessage());
	}

	@When("^promo pop up is displayed$")
	public void Promo_PopUp_Displayed() throws Throwable {
		Verify_Promo_PopUp_Displayed();
	}
	
}