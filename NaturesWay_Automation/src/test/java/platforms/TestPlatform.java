package platforms;

import java.io.File;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebElement;

import cucumber.api.DataTable;

public interface TestPlatform {

	Object userSelectionInE = null;

	void launch() throws Exception;

	//File takeScreenshot() throws Exception;

	void validate_OurStory(String text) throws Exception;
	// Mega menu

	void verifyMegamenuElement(DataTable smicons);

	void verifyQuickOrderPresent() throws Exception;

	void signOut() throws Exception;

	void verifyTheNewsLetterSection() throws Exception;

	void enterTheEmailInTextBox() throws Exception;

	void click() throws Exception;

	void button();

	// void itemsunderNewslettersection(String text);

	void funSocialMediaIcons(DataTable smicons) throws Exception, IOException, ParseException;

	void displayOfAnchorTags(DataTable anchorTags);

	void closeConfirmMessage() throws Exception;

	void signIn(String username, String password) throws Exception;

	void forGetPasswordButton() throws Exception;

	void enterTheTextForTheForgetPassword() throws Exception;

	void keyStatement() throws Exception;

	void copyRight() throws Exception;

	void clickOnSubmitTheForgotPassword() throws Exception;

	void verifyeingOurStroyTitle() throws Exception;

	void verifyOurStoryDescription() throws Exception;

	void clickOnVedio() throws Exception;

	void verifyTheDisplayOfHealthConditionIcon() throws Exception;

	void checkIterations() throws Exception;

	void scrollTheImage() throws Exception;

	void clickOnAnchorTags(DataTable Tags) throws Exception;

	void createAccount(String username, String pass, String confirm) throws Exception;

	void navigateToTheResourceAndClick() throws Exception;

	void ClickonArticleTile() throws Exception;

	void articleTitle() throws Exception;

	void readMore() throws Exception;

	void verifyArticlePageDetails(DataTable tiledescription) throws Exception;

	void backToHome() throws Exception;

	void clickOnArticleImage() throws Exception;

	void authourName() throws Exception;
	// contact us page

	void cBanner() throws Exception;

	void cTitle() throws Exception;

	void cDesc() throws Exception;

	void callUsIT() throws Exception;

	void callUsDesc() throws Exception;

	void callUsFax() throws Exception;

	void chatUsIT() throws Exception;

	void chatUsDT() throws Exception;

	void chatUsButton() throws Exception;

	void emailUsIT() throws Exception;

	void emailUsDesc() throws Exception;

	void emailUsButton() throws Exception;

	void locationh1() throws Exception;

	void locationh2() throws Exception;

	void locationh3() throws Exception;

	void locationh4() throws Exception;

	void eT() throws Exception;

	void eName() throws Exception;

	void userE() throws Exception;

	void userSelectionInEMailForm() throws Exception;

	void userSelectionInEEForm() throws Exception;

	void bToHome() throws Exception;

	void eMessage() throws Exception;

	void emailFormSubmit() throws Exception;

	void contactUsLink() throws Exception;

	void contactUsMessageTextBoxErrorMessage() throws Exception;

	// carrrer page

	void carTitle() throws Exception;

	void carDescription() throws Exception;

	void carHbiButton() throws Exception;

	void carOurEImg() throws Exception;

	void carOurEdesc() throws Exception;

	void carOurESecButton() throws Exception;

	void beniftsSection(DataTable benifits) throws Exception;

	void perksSection(DataTable perks) throws Exception;

	void JOTSTitle() throws Exception;

	void JOTSDescription() throws Exception;

	void JOTSbUTTON() throws Exception;

	void clink() throws Exception;

	// sitemap


	void sLink() throws Exception;

	void siteMapBC() throws Exception;

	// legal

	void lTabs() throws Exception;


	void lTitle() throws Exception;

	void close_browser() throws Exception;
	// Faqs

	void verifyTheDisplayOfGlobelHeader() throws Exception;

	void clickOnFaqsLink() throws Exception;

	void checkFaqsPageContent() throws Exception;


	void subtextBelowToFAQsPageHeader() throws Exception;

	void contactUsCTAbuttonFAQs() throws Exception;

	void FaqsPageComponentSequence() throws Exception;

	// globel header for mobile
	void headerIntegraiveprologo() throws Exception;

	void headerCart() throws Exception;

	void headerSearch() throws Exception;

	void clickOnIp() throws Exception;

	// homepage

	//void verifyB2B_Image_caroulsel_text() throws Exception;

	void verifyB2B_Image_caroulsel_links() throws Exception;

	void click_OurStory_ReadMore_button() throws Exception;

	void verifyB2B_Image_slider_navigation() throws Exception;

	void submit() throws Exception;
	// Al

	void clickOnShareIcon() throws Exception;

	void enabledEmailTextbox() throws Exception;

	void clickOnEmailButton() throws Exception;

	void emailSucessMessageAd() throws Exception;

	void iterationOfSocialMediaIcons() throws Exception;

	void clickOnCopyRightLink() throws Exception;

	void enabledWhiteColourTickMark() throws Exception;

	void iterationOfFeatureProducts() throws Exception;

	void fpButton() throws Exception;

	void fpButtonClick() throws Exception;

	void articleCount() throws Exception;

	void scrollAllTheImages() throws Exception;
	// HOME PAGE

	void featureProductSymbol() throws Exception;

	void featureProductBackGroundImage() throws Exception;

	void featureProductDesc() throws Exception;

	void featureProductButton2() throws Exception;

	// megamenu

	void createAnAccount() throws Exception;

	void clickOnCreateNewAccount() throws Exception;

	void clickOnPasswordEyeIcon() throws Exception;

	// OURsTORY
	void clickOurStoryReadMore() throws Exception;

	// Contact-us
	void contactUsSucessMessage() throws Exception;

	// Legal
	void legalTermsOfUse() throws Exception;

	void legalResalePolicy() throws Exception;

	void legalPrivacyPolicy() throws Exception;

	void legalHippa() throws Exception;

	// promtionbanner
	void pBVerified() throws Exception;

	void clickOnPB() throws Exception;

	// home page
	void featureProductIcon() throws Exception;

	void featureProductTitle() throws Exception;

	void hct() throws Exception;

	void hcd() throws Exception;

	void fphDesc() throws Exception;

	// Article detail page
	void backToArticles() throws Exception;

	void articlebc() throws Exception;

	// Mega menu
	void drugNutrientInteraction() throws Exception;

	void patientResource() throws Exception;

	void conditions() throws Exception;

	void findAPractitioner() throws Exception;

	void clickOnWelcomeUser() throws Exception;

	// contact-us
	void phoneNumber() throws Exception;

	// Related Articles
	void relatedArticlesLogo() throws Exception;

	void relatedArticlesTitle() throws Exception;

	void noOfrelatedArticles() throws Exception;

	void relatedArticlesDesc() throws Exception;

	void relatedArticlesReadMore() throws Exception;

	// sig-in
	void siginModelWindowClosed() throws Exception;

	// Forgot Paaword
	void enterRongMail(String email) throws Exception;

	void verifyErrorMessage() throws Exception;

	void forgotCancel() throws Exception;

	void clickOnForgotPassword() throws Exception;

	void forgotPassTitle() throws Exception;

	void forgotPassDes() throws Exception;

	// About us
	void clickOnAboutUs() throws Exception;

	void clickOnWwa() throws Exception;

	void Auhbtitle() throws Exception;

	void Auhbdescription() throws Exception;

	void AuIMTitle() throws Exception;

	void AuIMDesc() throws Exception;

	void clickOnIMRMButton() throws Exception;

	void AuOHTitle() throws Exception;

	void AuOHImages() throws Exception;

	void AuOHDesc() throws Exception;

	void AuOPTitle() throws Exception;

	void AuOPDesc() throws Exception;

	void clickOnOPRMButton() throws Exception;

	void AuFBTitle() throws Exception;

	void AuFBDesc() throws Exception;

	void AuHeader() throws Exception;
	// About-Us Quality

	void AbusClickOnQuality() throws Exception;

	void AbUsQualityTitle() throws Exception;

	void AbUsQualityDesc() throws Exception;

	void AbUsClickOnVedio() throws Exception;

	void AbUsISTitle() throws Exception;

	void AbUsISDesc() throws Exception;

	void AbUsISImage() throws Exception;

	void AbUsFDTitle() throws Exception;

	void AbUsFDDesc() throws Exception;

	void AbUsFDImage() throws Exception;

	void AbUsMTitle() throws Exception;

	void AbUsMDesc() throws Exception;

	void AbUsMImage() throws Exception;

	void AbUsLTitle() throws Exception;

	void AbUsLDesc() throws Exception;

	void AbUsLImage() throws Exception;

	void AbUsQFBTitle() throws Exception;

	void AbUsQFBDesc() throws Exception;

	void AbUsQBFImage() throws Exception;

	void AbUsQFACTitle() throws Exception;

	void AbUsQFACDesc() throws Exception;

	void AbUsQFACImage() throws Exception;

	// patient resource page

	void patientRTHead() throws Exception;

	void patientRTResourcesAlpha() throws Exception;

	void patientRClickOnPI() throws Exception;


	void patientRPIR() throws Exception;

	void patientRTClickOnHc() throws Exception;

	void patientRHCR() throws Exception;

	void patientRVerifyHCPdfs() throws Exception;
	// search pane

	void clickOnSearchPane() throws Exception;

	void verifyPP() throws Exception;

	void verifyPA() throws Exception;

	void enterThreecCharSP() throws Exception;

	void clickOnSProd() throws Exception;

	void clickOnSAT() throws Exception;

	void clickOnSPVAP() throws Exception;

	// product listeing
	void clickOnMMenuProducts() throws Exception;

	void clickOnMMVAP() throws Exception;

	void clickOnPI() throws Exception;

	void navigateBack() throws Exception;

	void clickOnProdTitle() throws Exception;

	void clickOnFilterCheckBox() throws Exception;

	void clickOnCAF() throws Exception;

	void sortTheProd() throws Exception;

	void clickOnSMR() throws Exception;

	void clickOnBreadCrumb() throws Exception;

	void clickOnViewDetails() throws Exception;

	void clickOnForm() throws Exception;


	void clickOnProductType() throws Exception;

	void clickOnHealthFilter() throws Exception;

	void clickOnWomensFreeForm() throws Exception;

	void clickOnSortProducts() throws Exception;

	void clicksOnFormsFilter() throws Exception;


	void clicksOnProductTypeFilter() throws Exception;

	void clicksOnHealthFilter() throws Exception;

	// Globel header integrative pro products

	void clickOnRFPI() throws Exception;

	void clickOnRFPVD() throws Exception;

	void clickOnByType() throws Exception;

	void clickOnAntioxidants() throws Exception;

	void clickOnHI() throws Exception;

	void clickOnAS() throws Exception;

	void clickOnPK() throws Exception;

	void clickOnSubitems() throws Exception;
	// product details page

	void clickOnSPI() throws Exception;

	void clickOnSV() throws Exception;

	void clickOnPQuality() throws Exception;

	void clickOnPUnit() throws Exception;

	void qDP() throws Exception;

	void cDP() throws Exception;

	void clickOnFavorites() throws Exception;



	void clickOnQues() throws Exception;

	void clickOnContactUs() throws Exception;

	void ClickOnpImageRA() throws Exception;

	void vAH() throws Exception;

	void clickOnRAI() throws Exception;

	void clickOnRAMT() throws Exception;

	void clickOnVDRA() throws Exception;

	void clickOnFPA() throws Exception;

	void clickOnFPI() throws Exception;

	void verifyFPT() throws Exception;

	void verifyFPH() throws Exception;

	void clickOnRMFP() throws Exception;
	// cadence3 product detail page

	void favSucessMessage() throws Exception;

	void clickOnHowToOrder() throws Exception;

	void ATCB() throws Exception;

	// usersAccount
	void clickOnAD() throws Exception;

	void clickOnMO() throws Exception;

	void clickOnPO() throws Exception;

	void clickOnCP() throws Exception;

	void clickOnFP() throws Exception;

	// cart drawer

	void CS() throws Exception;


	void SFL() throws Exception;
	// header cart

	void ClickOnHc() throws Exception;

	void searchADrug(String drugname) throws Exception;
	
	void NagiateToInteraction() throws Exception ;

	void ExpandInteraction_listed() throws Exception;

	void verify_LeftpaneMenus(DataTable leftpanemenus) throws Exception;
	
	//User Account Page
	
	void loginSuccess() throws Exception;

	void chevron_Username() throws Exception;

	void dropdownmenus_Useracct(DataTable submenu) throws Exception;
	
	void clickAccountInformation() throws Exception ;

	void verifyAccountInfo_Page() throws Exception;

	void verifyusericon_Accountinfo() throws Exception;
	
	void verifyusericon_MyOrder() throws Exception;
	
	void clickMyOrder_Dropdown() throws Exception ;
	
	void verifyMyOrder_Page() throws Exception ;
	
	void verifyusericon_PaymentMethod() throws Exception ;
	
	void verifyusericon_CommunicationPreference() throws Exception ;
	
	void verifyusericon_Favorite() throws Exception ;
	
	void verifyusericon_SignOut() throws Exception ;
	
	void click_Signout() throws Exception ;
	
	void signout_success() throws Exception ;
	
	// Practitioner Create Account

	void verifyError_Prefix(String text) throws Exception;

	void PrefixField_Empty() throws Exception ;

	void Click_CreateAccout_Btn() throws Exception;

	void verifyError_FirstName(String text) throws Exception;

	void FirstName_Empty() throws Exception;

	void verifyError_PostNominals(String text) throws Exception;

	void PostNominals_Empty() throws Exception;

	void verifyError_PractitonerType(String text) throws Exception;

	void PractitonerType_Empty() throws Exception;

	void verifyError_PhoneNo(String text) throws Exception;

	void PhoneNo_Empty() throws Exception;

	void verifyError_EmailAdd(String text) throws Exception;

	void EmailAddress_Empty() throws Exception;

	void verifyError_LicenseNo(String text) throws Exception;

	void LicenseNo_Empty() throws Exception;

	void UsageTypes_Empty() throws Exception;

	void verifyError_UsageTypes(String text) throws Exception;

	void LastName_Empty() throws Exception;

	void verifyError_LastName(String text) throws Exception;

	void Enter_InvalidEmail(String text) throws Exception;

	void Enter_PhoneNo(String text) throws Exception;

	void Password_Empty() throws Exception;

	void Re_EnterPassword_Empty() throws Exception;
	
	void VerifyHeader_CreateAcct() throws Exception;

	void verifyRadioBtn() throws Exception;

	void EnterPassword(String Password, String ConfirmPassword) throws Exception;

	void verifyError_InvalidPhoneNo(String text) throws Exception;

	void verifyError_Password(String text) throws Exception;

	void verifyError_Re_EnterPassword(String text) throws Exception;

	void verifyError_BussinessPhone(String text) throws Exception;

	void verifyError_College(String text) throws Exception;

	void verifyError_GraduationMonth(String text) throws Exception;

	void verifyError_GraduationYear(String text) throws Exception;

	void verifyError_Degree(String text) throws Exception;

	void verifyError_Program(String text) throws Exception;

	void verifyError_NoOfPractitioner(String text) throws Exception;

	void verifyError_State(String text) throws Exception;

	void verifyError_City(String text) throws Exception;

	void verifyError_ZipCode(String text) throws Exception;

	void verifyError_BussinessName(String text) throws Exception;

	void verifyError_Address1(String text) throws Exception;

	void verifyError_Address2(String text) throws Exception;

	void verifyError_pwd_DoNotMatch(String text) throws Exception;

	void verifyError_InvalidEmail(String text) throws Exception;

	void BussinessPhone_Empty() throws Exception;

	void BussinessName_Empty() throws Exception;

	void Address2_Empty() throws Exception;

	void Address1_Empty() throws Exception;

	void City_Empty() throws Exception;

	void ZipCode_Empty() throws Exception;

	void State_Empty() throws Exception;

	void NoOfPractitioner_Empty() throws Exception;

	void Enter_InvalidPhoneNo(String text) throws Exception;

	void Click_MedicalStudent_Radio() throws Exception;

	void CollegeField_Empty() throws Exception;

	void GraduationMonthField_Empty() throws Exception;

	void GraduationYearField_Empty() throws Exception;

	void DegreeField_Empty() throws Exception;

	void ProgramField_Empty() throws Exception;

	void VerifySubmitBtn_Disabled() throws Exception;

	
	// How To Order 
	
	void clickExpandIcon() throws Exception;

	void Click_ShopAmazonNow_Btn() throws Exception;

	void click_AmazonLink() throws Exception;

	void Verify_ShopAmazonNow_Btn() throws Exception;

	void verify_AmazonTitle() throws Exception;

	void howtoorderDesc() throws Exception;

	void howtoorderTitle() throws Exception;

	void verifyAmazonLogo() throws Exception;

	void Verify_Promo_PopUp_Displayed() throws Exception;

	void QAsession_FAQPage() throws Exception;

	void verifyimages_Carousel() throws Exception;

	void verifytitle_Carousel() throws Exception;

	void verifydescription_Carousel() throws Exception;

	void listOfProductsByType() throws Exception;

	void navigateFAQ_PDP() throws Exception;

	void clicksOnformulationFilter() throws Exception;

	void clicksonDietaryNeedsFilter() throws Exception;

	

	


}