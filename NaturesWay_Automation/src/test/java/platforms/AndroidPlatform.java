package platforms;

import java.io.File;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import helpers.ConfigurationHelper;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;

import org.testng.Assert;

import cucumber.api.DataTable;

public class AndroidPlatform extends WebPortal {

	private static AndroidDriver driver;
	private static WebDriverWait wait;
	public static AppiumDriverLocalService appiumService;
	public static String appiumServiceUrl;
	public static String macAppiumPath = "/Applications/Appium.app/Contents/Resources/app/node_modules/appium/build/lib/main.js";
	public static final String macNodePath = "/usr/local/bin/node";

	public static AndroidDriver getDriver() {
		return driver;
	}

	@Override
	public void launch() throws Exception {
		String platformName = ConfigurationHelper.getPlatform();
		//if (driver == null) {
		if (platformName.equals("android_Web")) {
			try {
				ConfigurationHelper.init();
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability("deviceName", ConfigurationHelper.getDeviceName());
				capabilities.setCapability("platformVersion", ConfigurationHelper.getPlatformVersion());
				capabilities.setCapability("platformName", ConfigurationHelper.getPlatformName());
				capabilities.setCapability("appPackage", ConfigurationHelper.getAppPackage());
				capabilities.setCapability("appActivity", ConfigurationHelper.getAppActivity());
				capabilities.setCapability("chromedriverExecutable", "C:\\chromedriver69\\chromedriver_win32\\chromedriver.exe");
				capabilities.setCapability("chromedriverExecutable", ConfigurationHelper.getAndroidBrowser());
				capabilities.setCapability("newCommandTimeout", 300);
				String ADB = System.getenv("ANDROID_HOME");
				String cmd = "/platform-tools/adb shell input keyevent 224";

				Runtime run = Runtime.getRuntime();
				Process pr = run.exec(ADB + cmd);
				pr.waitFor();
				driver = new AndroidDriver(new URL(ConfigurationHelper.getDriverUrl()), capabilities);
				//                signUpPage = new AndroidInitialSignUpPage(driver);
				wait = new WebDriverWait(driver, 30);

				driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			} catch(Exception e) {
				throw new Exception("Unable to connect to the Appium server.");
			}
		} else if (platformName.equals("android_ios")) {
			AppiumServiceBuilder serviceBuilder = new AppiumServiceBuilder();
			serviceBuilder.usingAnyFreePort();
			serviceBuilder.usingDriverExecutable(new File(macNodePath));
			serviceBuilder.withAppiumJS(new File(macAppiumPath));
			appiumService = AppiumDriverLocalService.buildService(serviceBuilder);
			appiumService.start();
			appiumServiceUrl = appiumService.getUrl().toString();
			System.out.println("Appium Service Address : - " + appiumServiceUrl);
			ConfigurationHelper.init();
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability("platformName", ConfigurationHelper.getPlatformName());
			capabilities.setCapability("platformVersion", ConfigurationHelper.getPlatformVersion());
			capabilities.setCapability("deviceName", ConfigurationHelper.getDeviceName());
			capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");
			capabilities.setCapability("chromedriverExecutable", "/Users/nadaradjan_si/Documents/Drivers/chromedriver");
			//        	capabilities.setCapability("chromedriverExecutable", otherFolder);
			capabilities.setCapability("newCommandTimeout", 300);

			driver = new AndroidDriver < WebElement > (new URL(appiumServiceUrl), capabilities);
			//            signUpPage = new AndroidInitialSignUpPage(driver);
			wait = new WebDriverWait(driver, 30);

			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			driver.get("https://www.google.com");

		}
	}
}