package platforms;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.cucumber.listener.Reporter;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import helpers.ConfigurationHelper;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;

import pageobjects.webportal.Sprint3_ProductDetailPage;
import pageobjects.webportal.Sprint3_ProtocolKitDetailsPage;
import pageobjects.webportal.Cadences_ConformMessage;
import pageobjects.webportal.Sprint1_ArticleListeingAndDetails;
import pageobjects.webportal.Sprint1_Careers;
import pageobjects.webportal.Sprint1_ContactUs;
import pageobjects.webportal.Sprint1_Faqs;
import pageobjects.webportal.Sprint1_ForgotPassword;
import pageobjects.webportal.Sprint1_GlobalFooter;
import pageobjects.webportal.Sprint1_GlobalHeader;
import pageobjects.webportal.Sprint1_HomePages;
import pageobjects.webportal.Sprint1_Legal;
import pageobjects.webportal.Sprint1_MegaMenu;
import pageobjects.webportal.Sprint1_PromotionBanner;
import pageobjects.webportal.Sprint1_Signin;
import pageobjects.webportal.Sprint1_SiteMap;
import pageobjects.webportal.Sprint2_AboutUs;
import pageobjects.webportal.Sprint2_AboutUsQuality;
import pageobjects.webportal.Sprint2_GlobalHeaderSearchPage;
import pageobjects.webportal.Sprint2_GlobelHeaderIntegrativeProducts;
import pageobjects.webportal.Sprint2_ProductDetailPage;
import pageobjects.webportal.Sprint2_ProductListingPage;
import pageobjects.webportal.Sprint2_Resources_PatientResourcePage;
import pageobjects.webportal.Sprint3_Cart;
import pageobjects.webportal.Sprint3_FindAPractitioner;
import pageobjects.webportal.Sprint3_HowToOrder;
import pageobjects.webportal.Sprint3_InteractionsandDepletion;
import pageobjects.webportal.Sprint3_MiniCartPage;
import pageobjects.webportal.Sprint3_PractitionerCreateAccount;
import pageobjects.webportal.Sprint3_UsersAccountPage;
import pageobjects.webportal.Sprint4_CheckoutPage;
import pageobjects.webportal.Sprint4_ContinueShopping;
import pageobjects.webportal.Sprint4_DiscontinuedProduct;
import pageobjects.webportal.Sprint4_Pagination;
import pageobjects.webportal.Sprint4_PromoPopUp;
import pageobjects.webportal.Sprint5_MyAccount_Favorites;
import pageobjects.webportal.Sprint5_OrderConfirmationPage;
import pageobjects.webportal.Sprint5_ShippingInformation;

public class WebPortal implements TestPlatform {
	//private static final int WAIT_TIME = 10;
	private static WebDriverWait wait;
	private static RemoteWebDriver driver;
	public static AppiumDriverLocalService appiumService;
	public static String appiumServiceUrl;
	//public WebElement highlightedWebElement;
	// public String hightlightedWebElementStyle;
	public static Actions a; 
	public static String DRUG ;
	public static String lastpagenumber_pagination;
	
	public static Actions actions;

	public static JavascriptExecutor js;
	public static Sprint1_ForgotPassword forgotpassword;
	public static Sprint1_GlobalFooter globelfooter;
	public static Sprint1_GlobalHeader globelheader;
	public static Sprint1_MegaMenu megamenu;
	public static Sprint1_ArticleListeingAndDetails articlelisteing;
	public static Sprint1_ContactUs contactus;
	public static Sprint1_Careers carrer;
	public static Sprint1_SiteMap sitemap;
	public static Sprint1_Legal legal;
	public static Sprint1_Signin signInPage;
	public static Sprint1_HomePages homePages;
	public static Cadences_ConformMessage conformmessage;
	public static Sprint1_Faqs FAQS;
	public static String platformName,UserName,Password;
	public static String browserName;
	public static Sprint1_PromotionBanner promotionbanner;
	public static Sprint2_AboutUs aboutus;
	public static Sprint2_AboutUsQuality aboutusquality;
	public static Sprint2_Resources_PatientResourcePage patientresourcepage;
	public static Sprint2_GlobalHeaderSearchPage globelheadersearch;
	public static Sprint2_ProductListingPage productlisting;
	public static Sprint2_GlobelHeaderIntegrativeProducts globalheaderintegrativeproduct;
	public static Sprint2_ProductDetailPage productdetailpage;
	public static Sprint3_ProductDetailPage productdetailpage_3;
	public static Sprint3_UsersAccountPage myaccount;
	public static Sprint3_Cart cartdrawer;
	public static Sprint3_MiniCartPage minicartpage;
	public static Sprint3_InteractionsandDepletion interactiondepletion;
	public static Sprint3_PractitionerCreateAccount createaccount;
	public static Sprint3_ProtocolKitDetailsPage protocolkitdetailpage;
	public static Sprint4_ContinueShopping continueshopping;
	public static Sprint4_CheckoutPage checkoutpage;
	public static Sprint5_OrderConfirmationPage orderConfirmationPage;
	public static Sprint5_ShippingInformation shippinginformation;
	public static Sprint3_HowToOrder howtoorder;
	public static Sprint4_DiscontinuedProduct discontinuedproduct;
	public static Sprint4_Pagination pagination;
	public static Sprint4_PromoPopUp promopopup;
	public static Sprint5_MyAccount_Favorites myaccount_favorite;


	public static String DefaultUserEmail = "shankarQA3@gmail.com";
	public static String DefaultPassword = "Photon@123";
	//public static Sprint3_MiniCartPage mcp;
	/*public static Sprint3_PractitionerCreateAccount ca;
	public static Sprint3_InteractionsandDepletion id;
	public static Sprint3_FindAPractitioner fap;
	public static Sprint4_ContinueShopping csp;
	public static Sprint4_CheckoutPage cop;
	public static Sprint3_HowToOrder hto;
	 */
	public static WebElement previousElement = null;
	// public static String macAppiumPath =
	// "/Applications/Appium.app/Contents/Resources/app/node_modules/appium/build/lib/main.js";
	// public static final String macNodePath = "/usr/local/bin/node";
	/*
	 * public static AndroidDriver getDriver() { return driver; }
	 */

	public static RemoteWebDriver getDriver() {
		return driver;
	}

	public void launch() throws Exception {
		try {

			platformName = ConfigurationHelper.getPlatform();
			browserName = ConfigurationHelper.getBrowserDetails();

			UserName=ConfigurationHelper.getUserName();
			Password=ConfigurationHelper.getPassword();
			
			String path = System.getProperty("user.dir");
			String driver_location = "";
			if (browserName.equals("ie")) {
				driver_location = path + "\\Drivers\\IEDriverServer_32bit.exe";
				System.setProperty("webdriver.ie.driver", driver_location);
				driver = new InternetExplorerDriver();
			} else if (browserName.equals("firefox")) {
				driver_location = path + "\\Drivers\\geckodriver.exe";
				System.setProperty("webdriver.gecko.driver", driver_location);
				driver = new FirefoxDriver();
			} else if (browserName.equals("edge")) {
				driver_location = path + "\\Drivers\\MicrosoftWebDriver.exe";
				System.setProperty("webdriver.edge.driver", driver_location);
				driver = new EdgeDriver();
			} else if (browserName.equals("safari")) {
				//driver_location = path + "\\Drivers\\MicrosoftWebDriver.exe";
				//System.setProperty("webdriver.edge.driver", driver_location);
				driver = new SafariDriver();
				//Thread.sleep(20000);
			}  else {

				driver_location = path + "/Drivers/chromedriver_83_MAC";
				System.setProperty("webdriver.chrome.silentOutput","true");
				System.setProperty("webdriver.chrome.driver", driver_location);
				ChromeOptions options = new ChromeOptions();

				Map<String, Object> prefs = new HashMap<String, Object>();
				prefs.put("credentials_enable_service", false);
				prefs.put("profile.password_manager_enabled", false);
				options.setExperimentalOption("prefs", prefs);
				options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
				options.setExperimentalOption("useAutomationExtension", true);
				driver = new ChromeDriver(options);
			}
			Properties locatorPool;
			// Launching windows machine chrome browser
			if (platformName.equals("mac_web")) {

				ConfigurationHelper.init();
				driver.manage().deleteAllCookies();
				driver.manage().window().maximize();
				driver.get(ConfigurationHelper.getBaseUri());
				driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
				wait = new WebDriverWait(driver, 50);
				driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
				forgotpassword = new Sprint1_ForgotPassword(driver);
				homePages = new Sprint1_HomePages(driver);
				forgotpassword = new Sprint1_ForgotPassword(driver);
				globelfooter = new Sprint1_GlobalFooter(driver);
				globelheader = new Sprint1_GlobalHeader(driver);
				megamenu = new Sprint1_MegaMenu(driver);
				articlelisteing = new Sprint1_ArticleListeingAndDetails(driver);
				contactus = new Sprint1_ContactUs(driver);
				carrer = new Sprint1_Careers(driver);
				sitemap = new Sprint1_SiteMap(driver);
				legal = new Sprint1_Legal(driver);
				signInPage= new Sprint1_Signin(driver);
				FAQS = new Sprint1_Faqs(driver);
				conformmessage = new Cadences_ConformMessage(driver);
				promotionbanner = new Sprint1_PromotionBanner(driver);
				aboutus = new Sprint2_AboutUs(driver);
				aboutusquality = new Sprint2_AboutUsQuality(driver);
				patientresourcepage = new Sprint2_Resources_PatientResourcePage(driver);
				globelheadersearch = new Sprint2_GlobalHeaderSearchPage(driver);
				productlisting = new Sprint2_ProductListingPage(driver);
				globalheaderintegrativeproduct = new Sprint2_GlobelHeaderIntegrativeProducts(driver);
				productdetailpage = new Sprint2_ProductDetailPage(driver);
				productdetailpage_3 = new Sprint3_ProductDetailPage(driver);
				myaccount = new Sprint3_UsersAccountPage(driver);
				cartdrawer = new Sprint3_Cart(driver);
				minicartpage = new Sprint3_MiniCartPage(driver);
				interactiondepletion = new Sprint3_InteractionsandDepletion(driver);
				createaccount = new Sprint3_PractitionerCreateAccount(driver);
				shippinginformation= new Sprint5_ShippingInformation(driver);
				checkoutpage =  new Sprint4_CheckoutPage(driver);
				orderConfirmationPage = new Sprint5_OrderConfirmationPage(driver);
				howtoorder=new Sprint3_HowToOrder(driver);
				discontinuedproduct=new Sprint4_DiscontinuedProduct(driver);
				pagination=new Sprint4_Pagination(driver);
				promopopup=new Sprint4_PromoPopUp(driver);
				myaccount_favorite=new Sprint5_MyAccount_Favorites(driver);
				a = new Actions(driver);

			}
			// Launching chrome browser in mac machine
			else if (platformName.equals("mac_webs")) {
				locatorPool = new Properties();
				ConfigurationHelper.init();
				driver.manage().deleteAllCookies();
				driver.manage().window().maximize();
				driver.get(ConfigurationHelper.getBaseUri());
				driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
				appiumService.stop();
				wait = new WebDriverWait(driver, 30);
				driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
				forgotpassword = new Sprint1_ForgotPassword(driver);
				homePages = new Sprint1_HomePages(driver);
				forgotpassword = new Sprint1_ForgotPassword(driver);
				globelfooter = new Sprint1_GlobalFooter(driver);
				globelheader = new Sprint1_GlobalHeader(driver);
				megamenu = new Sprint1_MegaMenu(driver);
				articlelisteing = new Sprint1_ArticleListeingAndDetails(driver);
				contactus = new Sprint1_ContactUs(driver);
				carrer = new Sprint1_Careers(driver);
				sitemap = new Sprint1_SiteMap(driver);
				legal = new Sprint1_Legal(driver);
				FAQS = new Sprint1_Faqs(driver);
				myaccount = new Sprint3_UsersAccountPage(driver);
				conformmessage = new Cadences_ConformMessage(driver);
				interactiondepletion = new Sprint3_InteractionsandDepletion(driver);


			} else if (platformName.equals("android_Web")) { // Launching
				// Android
				// device chrome
				// browser in
				// windows
				// machine
				AppiumDriverLocalService appiumService;
				String appiumServiceUrl;
				appiumService = AppiumDriverLocalService.buildDefaultService();
				appiumService.start();
				appiumServiceUrl = appiumService.getUrl().toString();
				System.out.println("Appium Service Address : - " + appiumServiceUrl);
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability("platformVersion", ConfigurationHelper.getPlatformVersion());
				capabilities.setCapability("platformName", ConfigurationHelper.getPlatformName());
				capabilities.setCapability("deviceName", ConfigurationHelper.getDeviceName());
				capabilities.setCapability("browserName", ConfigurationHelper.getBrowserName());
				capabilities.setCapability("chromedriverExecutable",
						"C:\\chromedriver69\\chromedriver_win32\\chromedriver69.exe");
				capabilities.setCapability("newCommandTimeout", 300);

				String ADB = System.getenv("ANDROID_HOME");
				String cmd = "/platform-tools/adb shell input keyevent 224";
				Runtime run = Runtime.getRuntime();
				Process pr = run.exec(ADB + cmd);
				pr.waitFor();

				driver = new RemoteWebDriver(new URL(ConfigurationHelper.getDriverUrl()), capabilities);
				wait = new WebDriverWait(driver, 30);
				driver.get(ConfigurationHelper.getBaseUri());
				driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
				appiumService.stop();
				wait = new WebDriverWait(driver, 30);
				driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			} else if (platformName.equals("android_ios")) {
				FileReader reader = new FileReader("src/test/resources/pageobjects/web_pageobjects.properties");
				locatorPool = new Properties();
				locatorPool.load(reader);

				AppiumDriverLocalService appiumService;
				String appiumServiceUrl;
				appiumService = AppiumDriverLocalService.buildDefaultService();
				appiumService.start();
				appiumServiceUrl = appiumService.getUrl().toString();
				System.out.println("Appium Service Address : - " + appiumServiceUrl);

				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability("deviceName", ConfigurationHelper.getDeviceName());
				capabilities.setCapability("platformVersion", ConfigurationHelper.getPlatformVersion());
				capabilities.setCapability("platformName", ConfigurationHelper.getPlatformName());
				capabilities.setCapability("browserName", ConfigurationHelper.getBrowserName());
				capabilities.setCapability("chromedriverExecutable",
						"/Users/nadaradjan_si/Documents/Drivers/chromedriver");
				capabilities.setCapability("newCommandTimeout", 300);
				driver = new AndroidDriver<WebElement>(new URL(appiumServiceUrl), capabilities);
				wait = new WebDriverWait(driver, 30);
				driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
				driver.get("https://www.google.com");

			} else if (platformName.equals("webPortal_ios")) {
				FileReader reader = new FileReader("src/test/resources/pageobjects/web_pageobjects.properties");
				locatorPool = new Properties();
				locatorPool.load(reader);
				ConfigurationHelper.init();
				driver.manage().deleteAllCookies();
				driver.get(ConfigurationHelper.getBaseUri());
				driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Unknown error while user tries to launch the test application");
		}
	}

	public void EnterValidLoginCredentials() throws Exception {		
		Assert.assertTrue(verifyElementIsPresent(megamenu.username()));
		Assert.assertTrue(verifyElementIsPresent(megamenu.password()));
		enterText(megamenu.username(),UserName);
		enterText(megamenu.password(),Password);	
		
		if(!megamenu.Sign_In_Button_ModalWindow().isEnabled())
			Assert.fail("Invalid Login Credentials");
	
	}
	
	@Override
	public void validate_OurStory(String text) {
		Assert.assertTrue(driver.findElement(By.linkText(text)).isDisplayed(), "OurStory label not available");
	}

	public void highLighterMethod(WebElement element, RemoteWebDriver driver) throws IOException, ParseException {

		if (ConfigurationHelper.getPlatform().equals("webPortal_Dev")) {
			if (previousElement != null)
				revertHighLighterMethod(previousElement, driver);
			JavascriptExecutor js = driver;
			js.executeScript("arguments[0].setAttribute('style','background: yellow; border: 2px solid red;');",
					element);
			previousElement = element;
		}
	}

	public void highLightMethod(WebElement element) throws Exception 
	{
		try
		{
			WebElement highlightedWebElement = element;
			String hightlightedWebElementStyle = (String)driver.executeScript("return arguments[0].style.border;", element);
			driver.executeScript("arguments[0].style.border='3px dotted red';", element);
			js.executeScript("arguments[0].style.border='0px'", element);
		} catch (Exception ex) {
			ex.getMessage();

		}
	}


	public void revertHighLighterMethod(WebElement element, RemoteWebDriver driver) throws IOException, ParseException {
		if (ConfigurationHelper.getPlatform().equals("webPortal_Dev")) {
			JavascriptExecutor js = driver;
			js.executeScript("arguments[0].setAttribute('style','background: transparent; border: none');", element);
		}
	}

	private void scrollToElement(WebElement element, RemoteWebDriver driver)
			throws IOException, ParseException, InterruptedException {
		if (ConfigurationHelper.getPlatform().equals("android_Web")) {
			Actions act = new Actions(driver);
			act.moveToElement(element).build().perform();
		} else if (ConfigurationHelper.getPlatform().equals("webPortal_Dev")) {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,100)");
			Thread.sleep(3000);
			js.executeScript("arguments[0].scrollIntoView();", element);
		}
	}

	/*private void verifylement(WebElement element, String errorMsg) throws Exception {
		System.out.println(element.isDisplayed());
		scrollToElement(element, driver);
		highLighterMethod(element, driver);
		Assert.assertTrue(element.isDisplayed(), errorMsg);
	}
	 */

	public boolean WaitTillElementExist(WebElement element , int WaitTime_Seconds) throws Exception {
		boolean Element_Check = false;
		for (int TempVar = 0; TempVar < WaitTime_Seconds; TempVar++) {
			if(element.isDisplayed())
			{  Element_Check = true;
			break;	
			}
			else
				Thread.sleep(500);
		}
		return Element_Check;		
	}

	public void waitTillElementClickable(WebElement element) throws Exception {		
		wait.until(ExpectedConditions.elementToBeClickable(element));		
	}

	public void waitTillElementExist(WebElement element) throws Exception {	
		
		wait.until(ExpectedConditions.visibilityOf(element));		
	}	 

	public boolean waitTillTextExist(String text , List<WebElement> el)throws Exception {		
		waitFor(1000);
		waitForPageLoaded();
		boolean flag = false;
		for (WebElement element:el)
	{
		waitVisibilityOfElement(element);
		Assert.assertTrue(verifyElementIsPresent(element));
		String title = element.getText();
		System.out.println(text);
		if(title ==text)
		 {
			flag = true;
			break;
		 }
	}
		return flag;
	}
	
	@Override
	public void verifyMegamenuElement(DataTable smicons) {
		try {

			for (Map<String, String> row : smicons.asMaps(String.class, String.class)) {
				String tableElement = row.get("MMC");
				switch (tableElement) {
				case "Products":
					ClickOnElement(megamenu.products());
					List<WebElement> pl = megamenu.productsList();
					System.out.println(pl.size());
					Thread.sleep(1000);
					continue;
				case "Resources":
					ClickOnElement(megamenu.resources());
					List<WebElement> rl = megamenu.resourcesList();
					System.out.println(rl.size());
					Thread.sleep(1000);
					ClickOnElement(megamenu.resources());
					continue;
				case "About Us":
					ClickOnElement(megamenu.aboutus());
					List<WebElement> al = megamenu.aboutusList();
					System.out.println(al.size());
					Thread.sleep(1000);
					ClickOnElement(megamenu.aboutus());
					continue;
				case "Cart":
					ClickOnElement(megamenu.Cart());
					break;
				default:
					Assert.fail("The is no footer link in home page to verify");
					break;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public static void clickOn(WebDriver driver, WebElement locator, int timeout) {
		new WebDriverWait(driver, timeout).until(ExpectedConditions.elementToBeClickable(locator));
		locator.click();
	}

	@Override
	public void verifyQuickOrderPresent() throws Exception {
		Thread.sleep(1000);
		ClickOnElement(megamenu.verifyQuickOrderPresent());
	}

	public Boolean verifyElementIsPresentByXPath(String elementLocator) {
		Boolean isElementPresent = false;
		try {
			WebElement element = driver.findElement(By.xpath(elementLocator));
			if (element.isEnabled()) {
				isElementPresent = true;
			}
		} catch (Exception ex) {

		}
		return isElementPresent;
	}

	@Override
	public void verifyTheNewsLetterSection() throws Exception {
		boolean section = true;
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,2000)");
			section = globelfooter.newsLetterSection().isEnabled();
			section = globelfooter.subText().isEnabled();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		Assert.assertTrue(section);

	}

	@Override
	public void copyRight() throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,3000)");
		Thread.sleep(10000);
		WebElement gettext = globelfooter.cr();
		String text = gettext.getText();
		System.out.println(text);
	}

	@Override
	public void keyStatement() throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,3000)");
		Thread.sleep(10000);
		WebElement gettext = globelfooter.ks();
		String text = gettext.getText();
		System.out.println(text);
	}

	@Override
	public void enterTheEmailInTextBox() throws Exception {
		Thread.sleep(1000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,2000)");
		globelfooter.email().sendKeys("natureway@gmail.com");

	}

	@Override
	public void click() throws Exception {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,3000)");
			Thread.sleep(1000);
			ClickOnElement(globelfooter.submit());
			driver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);
			String emailText = globelfooter.sucessFullMessage().getText();
			String actual = emailText;
			String excepted = "Received your request for the Newsletter, Thank you!";
			Assert.assertEquals(actual, excepted);
		} catch (Exception sigin) {
			sigin.printStackTrace();
		}
	}

	// sig in button
	@Override
	public void button() {
		try {
			Thread.sleep(10000);
			ClickOnElement(forgotpassword.signIn());
			driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
		} catch (Exception sigin) {
			sigin.printStackTrace();
		}
	}

	private void closeWindow() {
		String parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		driver.close();
		driver.switchTo().window(parentHandle);
	}



	public void verifyFooterComponents(DataTable components) throws Exception, IOException, ParseException {

		for (Map<String, String> row : components.asMaps(String.class, String.class)) {
			String tableElement = row.get("Footer Components");
			switch (tableElement) {
			case "Home Page Footer Link":
				List<WebElement> links = globelfooter.footerLinks();
				for (WebElement link_footer :links)
				{ verifyElementIsDisplayed(link_footer);
				break;
				}	continue;				
			case "News letter section":
				verifyElementIsDisplayed(globelfooter.newslettersection_footer());		
				continue;			
			case "Social Media navigation":
				List<WebElement> socialmedia_links = globelfooter.socialLinks();
				for (WebElement sociallink_footer : socialmedia_links)
				{   verifyElementIsDisplayed(sociallink_footer);
				break;
				}  continue;	
			case "Key and copyright notices":
				verifyElementIsDisplayed(globelfooter.copyrights_footer());		
				continue;

			default:
				Assert.fail("The is no footer link in home page to verify");
				break;
			}
		}	

	}


	public void funSocialMediaIcons(DataTable smicons) throws Exception, IOException, ParseException {
		String getBaseURL = ConfigurationHelper.getBaseUri();
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,3000)");
			for (Map<String, String> row : smicons.asMaps(String.class, String.class)) {
				String tableElement = row.get("icons");
				switch (tableElement) {
				case "Instagram":
					ClickOnElement(globelfooter.instagram());
					Thread.sleep(10000);
					closeWindow();
					continue;
				case "LinkledIn":
					Thread.sleep(10000);
					ClickOnElement(globelfooter.linkedin());
					closeWindow();
					continue;
				case "Facebook":
					Thread.sleep(10000);
					ClickOnElement(globelfooter.facebook());
					closeWindow();
					continue;
				default:
					Assert.fail("The is no footer link in home page to verify");
					break;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void displayOfAnchorTags(DataTable anchorTags) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,3000)");
			Thread.sleep(10000);
			for (Map<String, String> row : anchorTags.asMaps(String.class, String.class)) {
				String tableElement = row.get("Anchor Tags");
				switch (tableElement) {

				case "Contact":
					Assert.assertTrue(verifyElementIsPresent(globelfooter.contact()));
					break;
				case "Sitemap":
					Assert.assertTrue(verifyElementIsPresent(globelfooter.sitemap()));
					break;
				case "LEGAL":
					Assert.assertTrue(verifyElementIsPresent(globelfooter.legal()));
					break;
				case "CAREERS":
					Assert.assertTrue(verifyElementIsPresent(globelfooter.careers()));
					break;
				case "INTEGRATIVE CANADA":
					Assert.assertTrue(verifyElementIsPresent(globelfooter.ic()));
					break;
				case "FAQs":
					Assert.assertTrue(verifyElementIsPresent(globelfooter.faqs()));
					break;
				default:
					Assert.fail("The is no footer link in home page to verify");
					break;
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void closeConfirmMessage() throws Exception {
		try {
			//js.executeScript("arguments[0].click()", conformmessage.confirmMessage());
			//ClickOnElement(conformmessage.confirmMessage());
			Actions builder = new Actions(driver);
			builder.moveToElement(conformmessage.confirmMessage()).click(conformmessage.confirmMessage());
			builder.perform();
		} catch (Exception ex) {
			throw new Exception("Error in closing confirm alert method");
		}
	}

	@Override
	public void signIn(String username, String password) throws Exception {

		driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
		megamenu.username().sendKeys(username);

		driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
		megamenu.password().sendKeys(password);
		
		signInPage.submitSignIn().click();
	}

	@Override
	public void submit() throws Exception {
		driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
		ClickOnElement(megamenu.Sign_In_Button_ModalWindow());
		Thread.sleep(10000);

	}

	@Override
	public void signOut() throws Exception {
		driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
		ClickOnElement(megamenu.clickOnWelcomeUser());
		ClickOnElement(megamenu.clickOnWelcomeUserIcon());
		Assert.assertTrue(verifyElementIsPresent(megamenu.signoutbutton_welcomedropdown()));
		ClickOnElement(megamenu.signoutbutton_welcomedropdown());	
		waitForPageLoaded();
		waitTillElementExist(forgotpassword.signIn());
		Assert.assertTrue(verifyElementIsPresent(forgotpassword.signIn()), "App is not logged successfully");
	}

	@Override
	public void createAccount(String username, String pass, String confirm) throws Exception {
		driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);

		driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
		megamenu.un().sendKeys(username);

		driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
		megamenu.pw().sendKeys(pass);

		driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
		megamenu.cp().sendKeys(confirm);

	}

	@Override
	public void forGetPasswordButton() throws Exception {
		try {
			driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
			//ClickOnElement(forgotpassword.for());
		} catch (Exception ex) {
			ex.getMessage();
		}
	}

	@Override
	public void enterTheTextForTheForgetPassword() throws Exception {
		try {
			forgotpassword.email().sendKeys("indlamudidevan.p@photoninfotech.net");
		} catch (Exception ex) {
			ex.getMessage();
		}
	}

	@Override
	public void clickOnSubmitTheForgotPassword() throws Exception {
		try {
			driver.manage().timeouts().pageLoadTimeout(70, TimeUnit.SECONDS);
			ClickOnElement(forgotpassword.submit());

		} catch (Exception ex) {
			ex.toString();
		}
	}

	@Override
	public void verifyeingOurStroyTitle() throws Exception {
		try {

			driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
			Actions a = new Actions(driver);
			actions.moveToElement(homePages.title()).build().perform();

			if (!verifyElementIsPresent(homePages.title())) {
				Assert.fail("The element is not there");
			}

		} catch (Exception ex) {
			ex.getMessage();
		}

	}

	@Override
	public void verifyOurStoryDescription() throws Exception {
		try {

			driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
			Assert.assertTrue(verifyElementIsPresent(homePages.description()));
		} catch (Exception ex) {
			ex.getMessage();
		}
	}

	@Override
	public void clickOnVedio() throws Exception {
		try {
			driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
			homePages.video().click();
			;
			driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
		} catch (Exception ex) {
			ex.getMessage();

		}
	}

	@Override
	public void verifyTheDisplayOfHealthConditionIcon() throws Exception {
		try {

			Assert.assertTrue(verifyElementIsPresent(homePages.healthIcon()));
		} catch (Exception ex) {
			ex.getMessage();

		}
	}

	@Override
	public void checkIterations() throws Exception {
		try {
			driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
			ClickOnElement(homePages.checkIterations());

		} catch (Exception ex) {
			ex.getMessage();

		}
	}

	@Override
	public void scrollTheImage() throws Exception {

		WebDriverWait myElement = new WebDriverWait(driver, 10);
		WebElement scrollArea = homePages.scrollhorizontalbar();
		((JavascriptExecutor) driver).executeScript("scroll(0,1000);");

		((JavascriptExecutor) driver).executeScript("window.scrollBy(500000, 0)");
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollLeft = arguments[0].offsetWidth", scrollArea);


		System.out.println("scroll");
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollLeft = arguments[0].offsetWidth", scrollArea);

	}

	@Override
	public void clickOnAnchorTags(DataTable Tags) throws Exception {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,3000)");
			for (Map<String, String> row : Tags.asMaps(String.class, String.class)) {
				String tableElement = row.get("Tags");
				switch (tableElement) {
				case "Contact":
					ClickOnElement(globelfooter.contact());
					driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
					System.out.println("Contact");
					continue;
				case "Sitemap":
					globelfooter.sitemap().click();
					driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
					System.out.println("Sitemap");
					continue;
				case "LEGAL":
					ClickOnElement(globelfooter.legal());
					driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
					System.out.println("LEGAL");
					continue;
				case "CAREERS":
					ClickOnElement(globelfooter.careers());
					driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
					System.out.println("CAREERS");
					continue;
				case "INTEGRATIVE CANADA":
					ClickOnElement(globelfooter.ic());
					driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
					System.out.println("INTEGRATIVE CANADA");
					continue;
				case "FAQs":
					ClickOnElement(globelfooter.faqs());
					driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
					System.out.println("FAQs");

					continue;
				default:
					Assert.fail("The is no footer link in home page to verify");
					break;

				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	// links
	public static boolean BrokenLinkTest() throws MalformedURLException, IOException, Exception {
		boolean statuscode = false;
		String getBaseURL = ConfigurationHelper.getBaseUri();
		List<WebElement> linkList = driver.findElements(By.tagName("a"));
		linkList.addAll(driver.findElements(By.tagName("img")));
		System.out.println("size of full links and images---->" + linkList);
		List<WebElement> activeLinks = new ArrayList<WebElement>();

		for (int i = 0; i < linkList.size(); i++) {
			System.out.println(linkList.get(i).getAttribute("href"));
			if (linkList.get(i).getAttribute("href") != null
					&& (!linkList.get(i).getAttribute("href").contains("javascript"))) {
				activeLinks.add(linkList.get(i));

				System.out.println("size of active links and images---->" + activeLinks.size());

				for (int j = 0; j < activeLinks.size(); j++) {

					HttpURLConnection connection = (HttpURLConnection) new URL(activeLinks.get(j).getAttribute("href"))
							.openConnection();
					connection.connect();
					String response = connection.getResponseMessage();
					connection.disconnect();
					System.out.println(activeLinks.get(j).getAttribute("href") + "---->" + response);
					if (response.equalsIgnoreCase("200")) {
						statuscode = true;
					}
				}

			}
		}
		return statuscode;

	}

	// article listeing

	@Override
	public void navigateToTheResourceAndClick() throws Exception {
		try {
			driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
			ClickOnElement(articlelisteing.resource_header());
			Thread.sleep(10000);
		} catch (Exception ex) {
			ex.getMessage();

		}

	}

	@Override
	public void ClickonArticleTile() throws Exception {
		try {

			driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
			Actions a = new Actions(driver);
			a.clickAndHold(articlelisteing.articles());
			ClickOnElement(articlelisteing.articles());
			waitForPageLoaded();
		} catch (Exception ex) {
			ex.getMessage();

		}

	}

	@Override
	public void articleTitle() throws Exception {
		try {
			Thread.sleep(10000);
			Assert.assertTrue(verifyElementIsPresent(articlelisteing.articleTitle()));
		} catch (Exception ex) {
			ex.getMessage();

		}

	}

	public Boolean verifyElementIsPresent(WebElement element) throws Exception {
		Boolean isElementPresent = false;
		try {
			if (element.isDisplayed()) {
				isElementPresent = true;
			}
		} catch (Exception ex) {
			throw new Exception("Error in verifyElementIsPresent method - " + ex);
		}
		return isElementPresent;
	}

	@Override
	public void readMore() throws Exception {

		try {
			driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
			List<WebElement> wo = articlelisteing.readMoreButton();
			wo.get(1).click();
		} catch (Exception ex) {
			ex.getMessage();

		}

	}

	@Override
	public void verifyArticlePageDetails(DataTable tiledescription) throws Exception {

		try {
			for (Map<String, String> row : tiledescription.asMaps(String.class, String.class)) {
				String tileElements = row.get("TileDetails");
				switch (tileElements) {
				case "Hero Banner":
					Assert.assertTrue(verifyElementIsPresent(articlelisteing.articlesPageHeroBanner()));
					break;

				case "Article Title":
					articlelisteing.articleTitle().getText().contains("Articles");
					break;

				case "Date":
					JavascriptExecutor js = (JavascriptExecutor) driver;
					js.executeScript("window.scrollBy(0,350)");
					highLightMethod(articlelisteing.articleDateSection());
					Assert.assertTrue(verifyElementIsPresent(articlelisteing.articleDateSection()));
					break;

				case "Image":
					Thread.sleep(5000);
					Assert.assertTrue(verifyElementIsPresent(articlelisteing.articlesSectionimage()));
					break;

				case "Article Category":
					highLightMethod(articlelisteing.articlesPageCategorySection());
					Assert.assertTrue(verifyElementIsPresent(articlelisteing.articlesPageCategorySection()));
					break;

				default:
					Assert.fail("The is no bread crum to verify in article page - " + tileElements);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void backToHome() throws Exception {
		try {
			waitForPageLoaded();
			Assert.assertTrue(verifyElementIsDisplayed(contactus.backToHomeButton()));
			clickButtonWithOutScroll(contactus.backToHomeButton());
			waitForPageLoaded();
			String url = driver.getCurrentUrl();
			Assert.assertTrue(driver.getCurrentUrl().endsWith("sitecore.naturesway.com/"));
		} catch (Exception m) {
			m.printStackTrace();
		}

	}

	public static void waitForPageLoaded() {
		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString()
						.equals("complete");
			}
		};
		try {
			Thread.sleep(1000);
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(expectation);
		} catch (Throwable error) {
			Assert.fail("Timeout waiting for Page Load Request to complete.");
		}
	}

	public void ClickOnElement(WebElement element) throws Exception {
		try {
			if (element.isDisplayed()) 
			{
				element.click();
			}
		} catch (Exception ex) {
			throw new Exception("Error in verifyElementIsPresent method - " + ex);
		}
	}

	// sendkeys
	private void sendKeys(WebElement element) throws Exception {
		try {
			if (element.isDisplayed()) {
				element.sendKeys("www@gmail.com");
				Thread.sleep(5000);
			}
		} catch (Exception ex) {
			throw new Exception("Error in verifyElementIsPresent method - " + ex);
		}
	}

	@Override
	public void clickOnArticleImage() throws Exception {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", articlelisteing.articlesSectionimage());
		verifyElementIsPresent(articlelisteing.articlesSectionimage());
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", articlelisteing.articlesSectionimage());
		waitForPageLoaded();
	}



	public void explicitWait(WebElement ele)
	{
		waitForPageLoaded();
		wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(ele));

	}

	@Override
	public void authourName() throws Exception {
		try {
			List<WebElement> an = (articlelisteing.articlesPageAuthorName());
			System.out.println(an.size());
			waitForPageLoaded();

		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	// contact us page
	@Override
	public void contactUsLink() throws Exception {
		waitForPageLoaded();
		waitElementToBeClickable(contactus.contactUsLink());
		moveToElement(contactus.contactUsLink());
		ClickOnElement(contactus.contactUsLink());
	}

	@Override
	public void cBanner() throws Exception {
		try {
			waitFor(1000);
			waitForPageLoaded();
			waitVisibilityOfElement(contactus.contactUsHeroBanner());
			Assert.assertTrue(verifyElementIsPresent(contactus.contactUsHeroBanner()));

		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void cTitle() throws Exception {
		try {
			waitVisibilityOfElement(contactus.contactUsTitle());
			Assert.assertTrue(verifyTextIsDisplayed(contactus.contactUsTitle()));

		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void cDesc() throws Exception 
	{

		waitVisibilityOfElement(contactus.contactUsDescription());
		Assert.assertTrue(verifyTextIsDisplayed(contactus.contactUsDescription()));

	}

	@Override
	public void callUsIT() throws Exception {
		try {

			waitForPageLoaded();
			moveToElement(contactus.contactUsCallUsSymbol());
			waitVisibilityOfElement(contactus.contactUsCallUsSymbol());
			Assert.assertTrue(verifyElementIsPresent(contactus.contactUsCallUsSymbol()));
			/*
			 * if (!verifyElementIsPresent(contactus.contactUsCallUsSymbol())) {
			 * Assert.fail("The element is not there");
			 * 
			 * }
			 */
			waitVisibilityOfElement(contactus.contactUsCallUsTitle());
			Assert.assertTrue(verifyTextIsDisplayed(contactus.contactUsCallUsTitle()));
			waitForPageLoaded();

		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void callUsDesc() throws Exception {
		try {

			waitForPageLoaded();
			moveToElement(contactus.contactUsCallUsDescription());
			waitVisibilityOfElement(contactus.contactUsCallUsDescription());
			Assert.assertTrue(verifyTextIsDisplayed(contactus.contactUsCallUsDescription()));
			/*
			 * if (!verifyElementIsPresent(contactus.contactUsCallUsDescription())) {
			 * Assert.fail("The element is not there"); } waitForPageLoaded();
			 */
		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void callUsFax() throws Exception {
		try {
			waitFor(1000);
			waitForPageLoaded();	
			moveToElement(contactus.contactUsCallUsFaxNo());
			Assert.assertTrue(verifyTextIsDisplayed(contactus.contactUsCallUsFaxNo()));

		} 
		catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void chatUsIT() throws Exception {
		try {
			Thread.sleep(5000);
			waitForPageLoaded();
			moveToElement(contactus.contactUsChatWithUsSymbol());
			if (!verifyElementIsPresent(contactus.contactUsChatWithUsSymbol())) {
				Assert.fail("The element is not there");
			}
			Actions b = new Actions(driver);
			b.moveToElement(contactus.contactUsChatWithUsTitle());
			if (!verifyElementIsPresent(contactus.contactUsChatWithUsTitle())) {
				Assert.fail("The element is not there");
			}
			waitForPageLoaded();

		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void chatUsDT() throws Exception {
		try {
			Thread.sleep(5000);
			waitForPageLoaded();
			Actions b = new Actions(driver);
			b.moveToElement(contactus.contactUsChatWithUsDescription());
			if (!verifyElementIsPresent(contactus.contactUsChatWithUsDescription())) {
				Assert.fail("The element is not there");
			}
			waitForPageLoaded();

		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void chatUsButton() throws Exception {
		try {
			Thread.sleep(5000);
			waitForPageLoaded();
			Actions b = new Actions(driver);
			b.moveToElement(contactus.contactUsChatWithUsButton());
			ClickOnElement(contactus.contactUsChatWithUsButton());
			waitForPageLoaded();

		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void emailUsIT() throws Exception {
		try {
			Thread.sleep(5000);
			waitForPageLoaded();
			Actions b = new Actions(driver);
			b.moveToElement(contactus.contactUsEmailSymbol());
			contactus.contactUsEmailSymbol().isEnabled();
			contactus.contactUsEmailTitle().isEnabled();
		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void emailUsDesc() throws Exception {
		try {
			Thread.sleep(5000);
			waitForPageLoaded();
			Actions b = new Actions(driver);
			b.moveToElement(contactus.contactUsEmailDescription());
			contactus.contactUsEmailDescription().isEnabled();
		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void emailUsButton() throws Exception {
		try {

			waitForPageLoaded();
			Actions b = new Actions(driver);
			b.moveToElement(contactus.contactUsEmailButton());
			ClickOnElement(contactus.contactUsEmailButton());

		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void locationh1() throws Exception {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,650)");
			waitForPageLoaded();
			Assert.assertTrue(verifyElementIsPresent(contactus.backToHomeButton()));
			// Assert.assertTrue(verifyElementIsPresent(contactus.contactUsMapLocation()));
			Assert.assertTrue(verifyElementIsPresent(contactus.contactUsMapIntegrativeTherapeutics()));

		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void locationh2() throws Exception {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,1700)");
			Thread.sleep(1000);
			Assert.assertTrue(verifyElementIsPresent(contactus.contactUsMapCustomerCareHours()));
		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void locationh3() throws Exception {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,1800)");
			Thread.sleep(1000);
			Assert.assertTrue(verifyElementIsPresent(contactus.contactUsMapPhone()));

		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void locationh4() throws Exception {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,1900)");
			Thread.sleep(1000);
			Assert.assertTrue(verifyElementIsPresent(contactus.contactUsMapFax()));

			waitForPageLoaded();

		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void eT() throws Exception {
		try {
			Thread.sleep(5000);
			waitForPageLoaded();
			Actions b = new Actions(driver);
			b.moveToElement(contactus.contactUsEmailFormTitle());
			if (!verifyElementIsPresent(contactus.contactUsEmailFormTitle())) {
				Assert.fail("The element is not there");
			}
			if (!verifyElementIsPresent(contactus.contactUsEmailFormDescription())) {
				Assert.fail("The element is not there");
			}
			waitForPageLoaded();

		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void eName() throws Exception {
		try {
			Thread.sleep(5000);
			waitForPageLoaded();
			sendKeys(contactus.contactUsEmailFormTextBox1());
			waitForPageLoaded();

		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void userE() throws Exception {
		try {
			Thread.sleep(5000);
			waitForPageLoaded();
			sendKeys(contactus.contactUsEmailFormTextBox2());
			waitForPageLoaded();

		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void userSelectionInEMailForm() throws Exception {
		try {
			waitForPageLoaded();
			List<WebElement> an = (contactus.contactUsEmailFormPractitionerDd1());
			System.out.println(an.size());
			for (int i = 0; i <= an.size(); i++) {
				an.get(1).click();
				continue;
			}
			waitForPageLoaded();

		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void userSelectionInEEForm() throws Exception {
		try {
			waitForPageLoaded();
			List<WebElement> an = (contactus.contactUsEmailFormInquiryDd2());
			System.out.println(an.size());
			an.get(2).click();
			waitForPageLoaded();

		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void bToHome() throws Exception {
		try {

			waitForPageLoaded();
			ClickOnElement(contactus.backToHomeButton());

		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void eMessage() throws Exception {
		try {
			Thread.sleep(5000);
			waitForPageLoaded();
			sendKeys(contactus.contactUsEmailFormMessage());
			waitForPageLoaded();

		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	@Override
	public void emailFormSubmit() throws Exception {
		try {
			Actions b = new Actions(driver);
			b.moveToElement(contactus.contactUsEmailFormSubmitButton());
			waitForPageLoaded();
			ClickOnElement(contactus.contactUsEmailFormSubmitButton());
			waitForPageLoaded();

		} catch (Exception a) {
			a.printStackTrace();
		}

	}

	// carrer page
	@Override
	public void carTitle() throws Exception {
		Thread.sleep(1000);
		String gp = driver.getPageSource();
		System.out.println(gp);
		Thread.sleep(5000);
		carrer.carrerBannerTitle().isEnabled();

	}

	@Override
	public void carDescription() throws Exception {
		Thread.sleep(1000);
		Assert.assertTrue(verifyElementIsPresent(carrer.carrerBannerDescription()));

	}

	@Override
	public void carHbiButton() throws Exception {
		Thread.sleep(5000);
		ClickOnElement(carrer.carrerBannerButton());
		driver.navigate().back();

	}

	@Override
	public void carOurEImg() throws Exception {
		Actions b = new Actions(driver);
		b.moveToElement(carrer.carrerOurEmployeesImage());
		carrer.carrerOurEmployeesImage().isEnabled();

	}

	@Override
	public void carOurEdesc() throws Exception {
		Actions b = new Actions(driver);
		b.moveToElement(carrer.carrerOurEmployeesDescription());
		carrer.carrerOurEmployeesDescription().isEnabled();

	}

	@Override
	public void carOurESecButton() throws Exception {
		Actions b = new Actions(driver);
		b.moveToElement(carrer.carrerOurEmployeesLearnOurStoryButton());
		ClickOnElement(carrer.carrerOurEmployeesLearnOurStoryButton());
		waitForPageLoaded();

	}

	@Override
	public void clink() throws Exception {
		ClickOnElement(carrer.carrerLink());
	}

	@Override
	public void beniftsSection(DataTable benifits) throws Exception {

		try {
			for (Map<String, String> row : benifits.asMaps(String.class, String.class)) {
				String bs = row.get("Bs");
				switch (bs) {
				case "Benefits":
					if (!verifyElementIsPresent(carrer.carrerBenefitsTitle())) {
						Assert.fail("The element " + bs + " is not present in carrer page");
					}

					break;
				case "Medical Insurance":
					if (!carrer.carrerBenefitsMedicalInsurance().getAttribute("innerHTML").contains(bs)) {
						System.out.println(carrer.carrerBenefitsMedicalInsurance().getAttribute("innerHTML"));
						Assert.fail("The element " + bs + " is not present in carrer page");
					}

					break;

				case "Dental Insurance":
					if (!verifyElementIsPresent(carrer.carrerBenefitsDentalInsurance())) {
						Assert.fail("The element " + bs + " is not present in carrer page");
					}

					break;
				case "Voluntary Vision Insurance":
					if (!verifyElementIsPresent(carrer.carrerBenefitsVoluntaryVisionInsurance())) {
						Assert.fail("The element " + bs + " is not present in carrer page");
					}

					break;
				case "Company paid short and long term disability and life insurance":
					if (!verifyElementIsPresent(carrer.carrerBenefitslifeinsurance())) {
						Assert.fail("The element " + bs + " is not present in carrer page");
					}

					break;
				case "401(k) with employer match and profit sharing":
					if (!verifyElementIsPresent(carrer.carrerBenefitsprofitsharing())) {
						Assert.fail("The element " + bs + " is not present in carrer page");
					}

					break;

				default:
					Assert.fail("The is no bread crum to verify in article page ");
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void perksSection(DataTable perks) throws Exception {
		try {
			for (Map<String, String> row : perks.asMaps(String.class, String.class)) {
				String ps = row.get("Ps");
				switch (ps) {
				case "Perks":
					if (!verifyElementIsPresent(carrer.carrerPerksTitle())) {
						Assert.fail("The element " + ps + " is not present in carrer page");
					}

					break;
				case "Paid Time Off":
					if (!carrer.carrerPerksPaidTimeOff().getAttribute("innerHTML").contains(ps)) {
						System.out.println(carrer.carrerBenefitsMedicalInsurance().getAttribute("innerHTML"));
						Assert.fail("The element " + ps + " is not present in carrer page");
					}

					break;

				case "Paid Holidays":
					if (!verifyElementIsPresent(carrer.carrerPerksPaidHolidays())) {
						Assert.fail("The element " + ps + " is not present in carrer page");
					}

					break;
				case "Wellness incentives":
					if (!verifyElementIsPresent(carrer.carrerPerksWellnessincentives())) {
						Assert.fail("The element " + ps + " is not present in carrer page");
					}

					break;
				case "Generous product discounts  for you and your family":
					if (!verifyElementIsPresent(carrer.carrerPerksGenerousproductdiscountsforouandyourfamily())) {
						Assert.fail("The element " + ps + " is not present in carrer page");
					}

					break;
				case "Educational Reimbursement":
					if (!verifyElementIsPresent(carrer.carrerPerksEducationalReimbursement())) {
						Assert.fail("The element " + ps + " is not present in carrer page");
					}

					break;

				default:
					Assert.fail("The is no bread crum to verify in article page ");
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void JOTSTitle() throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,3000)");
		Actions b = new Actions(driver);
		b.moveToElement(carrer.carrerJoinourteamTitle());
		Assert.assertTrue(verifyElementIsPresent(carrer.carrerJoinourteamTitle()));

	}

	@Override
	public void JOTSDescription() throws Exception {
		/*JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,3000)");
		Actions b = new Actions(driver);
		b.moveToElement(carrer.carrerPageJoinOurTDes());	
		Assert.assertTrue(verifyElementIsPresent(carrer.carrerPageJoinOurTDes()));
		 */
		verifyElementIsPresent(carrer.carrerPageJoinOurTDes());

	}

	/*	@Override
	public void JOTSbUTTON() throws Exception {
		Actions b = new Actions(driver);
		b.moveToElement(carrer.carrerPageJoinOurTB());
		ClickOnElement(carrer.carrerPageJoinOurTB());
		Thread.sleep(10000);

	}
	 */
	// sitemap

	@Override
	public void listOfProductsByType() throws Exception {
		try {
			sitemap.siteMapByType().isEnabled();
			List<WebElement> sp = sitemap.products_ByType_list();
			for (int i = 0; i < sp.size(); i++) {
				System.out.println(sp.size());
				String text = sp.get(i).getText();
				System.out.println(text);
			}
			List<WebElement> sphi = sitemap.siteMapByHealthInterest();
			for (int i = 0; i < sphi.size(); i++) {
				System.out.println(sphi.size());
				String text = sphi.get(i).getText();
				System.out.println(text);
			}
			List<WebElement> spk = sitemap.siteMapProtocolKits();
			for (int i = 0; i < spk.size(); i++) {
				System.out.println(spk.size());
				String text = spk.get(i).getText();
				System.out.println(text);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override

	public void sLink() throws Exception {
		try {
			Actions b = new Actions(driver);
			b.moveToElement(sitemap.siteMapLink());
			ClickOnElement(sitemap.siteMapLink());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void siteMapBC() throws Exception {
		try {

			ClickOnElement(sitemap.siteMapBacktohome());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//	@Override
	//	public void siteMapRes() throws Exception {
	//		try {
	//			List<WebElement> sr = sitemap.siteMapResources();
	//			for (int i = 0; i < sr.size(); i++) {
	//				System.out.println(sr.size());
	//				String text1 = sr.get(i).getText();
	//				System.out.println(text1);
	//			}
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//
	//	}
	//
	//	@Override
	///*	public void siteMapAbo() throws Exception {
	//		try {
	//			List<WebElement> sau = sitemap.siteMapAboutUs();
	//			for (int i = 0; i < sau.size(); i++) {
	//				System.out.println(sau.size());
	//				String text = sau.get(i).getText();
	//				System.out.println(text);
	//			}
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//
	//	}
	//
	//	
	//
	//	// legal
	//	@Override
	//
	//	public void lTabs() throws Exception {
	//		try {
	//			List<WebElement> lt = legal.legalTabs();
	//			for (int i = 0; i < lt.size(); i++) {
	//				System.out.println(lt.size());
	//				String text = lt.get(i).getText();
	//				System.out.println(text);
	//			}
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//
	//	}
	//
	//	
	//
	//	@Override
	//	public void lTitle() throws Exception {
	//		Assert.assertTrue(verifyElementIsPresent(legal.legalTitle()));
	//
	//	}
	//
	//	@Override
	//	public File takeScreenshot() throws Exception {
	//		return null;
	//	}
	//
	//
	//
	//
	//	// Faqs
	//	@Override
	//	public void verifyTheDisplayOfGlobelHeader() throws Exception {
	//		try {
	//			waitForPageLoaded();
	//			String c = driver.getCurrentUrl();
	//			Assert.assertEquals(c, "https://qa.sitecore.naturesway.com/");
	//			Assert.assertTrue(verifyElementIsPresent(FAQS.integrativeProLogo()), "logo not should be present");
	//		} catch (Exception ex) {
	//			ex.printStackTrace();
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnFaqsLink() throws Exception {
	//		try {
	//			Thread.sleep(1000);
	//			FAQS.faqlink_footer().getAttribute("style");
	//			FAQS.faqlink_footer().getText();
	//			ClickOnElement(FAQS.faqlink_footer());
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//
	//	}
	//
	//	@Override
	//	public void checkFaqsPageContent() throws Exception {
	//		try {
	//			String PageTitle=driver.getCurrentUrl();
	//			if(PageTitle.contains("faq")) {
	//				verifyElementIsPresent(FAQS.Logo_FAQpage());
	//				verifyElementIsPresent(FAQS.fDescription());
	//			}
	//			/*Actions b = new Actions(driver);
	//			b.moveToElement(FAQS.fLogo());
	//			FAQS.fLogo().isEnabled();
	//			Actions C = new Actions(driver);
	//			C.moveToElement(FAQS.fDescription());
	//			Assert.assertTrue(verifyElementIsPresent(FAQS.fDescription()));
	//			Assert.assertEquals(FAQS.getPageTitle(), "FAQs");
	//			 */
	//		
	//		}
	//
	//
	//
	//	@Override
	//	public void shouldDisplayFaqsContent() throws Exception {
	//		try {
	//
	//			JavascriptExecutor js = (JavascriptExecutor) driver;
	//			js.executeScript("window.scrollBy(0,1000)");
	//			Thread.sleep(10000);
	//			List<WebElement> qa = FAQS.fQuestionSessionAns();
	//			for (int i = 1; i <= qa.size() - 1; i++) {
	//				qa.get(i).click();
	//				continue;
	//			}
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//	}
	//
	//	@Override
	//	public void FaqsPageComponentSequence() throws Exception {
	//		try {
	//			Assert.assertTrue(verifyElementIsPresent(FAQS.fTag()));
	//			Assert.assertTrue(verifyElementIsPresent(FAQS.fTag0()));
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//	}
	//
	//	@Override
	//	public void subtextBelowToFAQsPageHeader() throws Exception {
	//		try {
	//			FAQS.fDescription().getText();
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//
	//	}
	//
	//	@Override
	//	public void contactUsCTAbuttonFAQs() throws Exception {
	//		try {
	//			Thread.sleep(1000);
	//			ClickOnElement(FAQS.fContactUs());
	//			String t1 = driver.getTitle();
	//			String t2 = "Contact Us";
	//			Assert.assertEquals(t1, t2);
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//
	//	}
	//
	//	// globel header mobile //web
	//	@Override
	//	public void headerIntegraiveprologo() throws Exception {
	//		/*String a = globelheader.getPageTitle();
	//		String b = "Integrative Therapeutics";
	//		Assert.assertEquals(a, b);
	//		Assert.assertTrue(verifyElementIsPresent(globelheader.headerintegrativeProLogo()));
	//		ClickOnElement(globelheader.headerintegrativeProLogo());
	//		 */
	//		verifyElementIsPresent(globelheader.headerintegrativeProLogo());
	//
	//	}
	//
	//	@Override
	//	public void headerCart() throws Exception {
	//		Assert.assertTrue(verifyElementIsPresent(globelheader.headerCart()));
	//
	//	}
	//
	//	@Override
	//	public void headerSearch() throws Exception {
	//		Assert.assertTrue(verifyElementIsPresent(globelheader.headerSearch()));
	//
	//	}
	//
	// homepage
	@Override
	public void verifyimages_Carousel() throws Exception {	
		List<WebElement> imageslist_carousel = homePages.imageCarouselBanner();
		List<WebElement> carousellisticons = homePages.carousellisticons();
		for (WebElement selectcarousel :carousellisticons)
		{
			selectcarousel.click();
			selectcarousel.getAttribute("class").equalsIgnoreCase("sxa-numbers active");		
			for(WebElement verifyimage_carousel : imageslist_carousel)
			{
				verifyimage_carousel.getAttribute("Style").equalsIgnoreCase("display: none;");
				break;			}
		}	
	}

	@Override
	public void verifytitle_Carousel() throws Exception {	
		List<WebElement> titlelist_carousel = homePages.titleCarouselBanner();
		List<WebElement> carousellisticons = homePages.carousellisticons();
		for (WebElement selectcarousel :carousellisticons)
		{
			selectcarousel.click();
			selectcarousel.getAttribute("class").equalsIgnoreCase("sxa-numbers active");		
			for(WebElement verifytitle_carousel : titlelist_carousel)
			{
				selectcarousel.click();
				if(verifytitle_carousel.getText()!=null)
					//verifyTextIsDisplayed(verifytitle_carousel);
					break;			}
		}	
	}


	public void verifydescription_Carousel() throws Exception {	
		List<WebElement> descriptionlist_carousel = homePages.descriptionCarouselBanner();
		List<WebElement> carousellisticons = homePages.carousellisticons();
		for (WebElement selectcarousel :carousellisticons)
		{
			selectcarousel.click();
			selectcarousel.getAttribute("class").equalsIgnoreCase("sxa-numbers active");		
			for(WebElement verifydescription_carousel : descriptionlist_carousel)
			{
				selectcarousel.click();
				if(verifydescription_carousel.getText()!=null)

					//verifyTextIsDisplayed(verifydescription_carousel);
					break;			}
		}	
	}

	
//	public void ReadMore_CarouselOne() throws Exception {	
//		List<WebElement> readmorelist_carousel = homePages.ReadMoreButtonCarouselBanner();
//		List<WebElement> carousellisticons = homePages.carousellisticons();
//		for(int i=0; i<carousellisticons.size();i++)
//		{
//			waitElementToBeClickable(carousellisticons.get(i));
//			carousellisticons.get(i).click();
//			waitFor(3000);
//			Assert.assertTrue(carousellisticons.get(i).getAttribute("class").equalsIgnoreCase("sxa-numbers active"), "Carousel is not active");
//			for(int j=i; j<readmorelist_carousel.size();j++)
//			{
//				waitElementToBeClickable(readmorelist_carousel.get(j));
//				readmorelist_carousel.get(j).isDisplayed();
//				readmorelist_carousel.get(j).click();
//				driver.navigate().back();
//				((JavascriptExecutor) driver).executeScript("window.focus();");
//				break;
//			}
//		}
//
//	}

	public void ReadMore_CarouselOne() throws Exception {	
		List<WebElement> readmorelist_carousel = homePages.ReadMoreButtonCarouselBanner();
		List<WebElement> carousellisticons = homePages.carousellisticons();
		for(int i=0; i<carousellisticons.size();i++)
		{
			javascriptExecutorClick(carousellisticons.get(i));
			getAndCheckAttributeValue(carousellisticons.get(i),"class","sxa-numbers active");
			javascriptExecutorClick(readmorelist_carousel.get(i));
			driver.navigate().back();
			waitForPageLoaded();
					
		}	
}
	
	////	@Override
	////	public void verifyB2B_Image_caroulsel_text() throws Exception {
	////		List<WebElement> hm = homePages.imageCarouselNavigationButton();
	////		System.out.println(hm.size());
	////		Thread.sleep(3000);
	////		List<WebElement> h = homePages.imageCarouselBannerTitle();
	////		System.out.println(h.size());
	////	}
	//
	//	@Override
	//	public void verifyB2B_Image_caroulsel_links() throws Exception {
	//		List<WebElement> hm = homePages.imageCarouselNavigationButton();
	//		System.out.println(hm.size());
	//	}
	//
	//	@Override
	//	public void click_OurStory_ReadMore_button() throws Exception {
	//		List<WebElement> hm = homePages.imageCarouselReadMoreButton();
	//		System.out.println(hm.size());
	//		for (int i = 0; i <= hm.size(); i++) {
	//			hm.get(i).click();
	//			break;
	//		}
	//	}
	//
	//	@Override
	//	public void verifyB2B_Image_slider_navigation() throws Exception {
	//		List<WebElement> hm = homePages.imageCarouselNavigationButton();
	//		System.out.println(hm.size());
	//		for (int i = 0; i <= hm.size(); i++) {
	//			hm.get(i).click();
	//			break;
	//
	//		}
	//
	//	}
	//
	//	// Al
	//	@Override

	//
	//	@Override
	//	public void enabledEmailTextbox() throws Exception {
	//		articlelisteing.enabledEmailTextbox().sendKeys("abcdef@gmail.com");
	//
	//	}
	//
	//	@Override
	//	public void clickOnEmailButton() throws Exception {
	//		ClickOnElement(articlelisteing.clickOnEmailButton());
	//
	//	}
	//
	//	@Override
	//	public void emailSucessMessageAd() throws Exception {
	//		String s = articlelisteing.emailSucessMessageAd().getText();
	//		System.out.println(s);
	//
	//	}
	//
	//	@Override
	//	public void iterationOfSocialMediaIcons() throws Exception {
	//		List<WebElement> icons = articlelisteing.iterationOfSocialMediaIcons();
	//		System.out.println(icons.size());
	//	}
	//
	//	@Override
	//	public void clickOnCopyRightLink() throws Exception {
	//		ClickOnElement(articlelisteing.clickOnCopyRightLink());
	//
	//	}
	//
	//	@Override
	//	public void enabledWhiteColourTickMark() throws Exception {
	//		articlelisteing.enabledWhiteColourTickMark().isEnabled();
	//
	//	}
	//
	//	@Override
	//	public void iterationOfFeatureProducts() throws Exception {
	//		JavascriptExecutor js = (JavascriptExecutor) driver;
	//		js.executeScript("window.scrollBy(0,1000)");
	//		List<WebElement> FP = articlelisteing.iterationOfFeatureProducts();
	//		System.out.println(FP.size());
	//
	//	}
	//
	//	@Override
	//	public void fpButton() throws Exception {
	//		Assert.assertTrue(verifyElementIsPresent(articlelisteing.fpButton()));
	//
	//	}
	//
	//	@Override
	//	public void fpButtonClick() throws Exception {
	//		Thread.sleep(1000);
	//		ClickOnElement(articlelisteing.fpButtonClick());
	//		Thread.sleep(1000);
	//	}
	//
	//	@Override
	//	public void articleCount() throws Exception {
	//		List<WebElement> Til = articlelisteing.articleCount();
	//		System.out.println(Til.size());
	//	}
	//
	//	@Override
	//	public void scrollAllTheImages() throws Exception {
	//		js = (JavascriptExecutor) driver;
	//		Assert.assertTrue(
	//				(boolean) js.executeScript(
	//						"return document.documentElement.scrollHeight>document.documentElement.clientHeight;"),
	//				"Scroll bar is not present in Article listing page");
	//		js.executeScript("window.scrollTo(0, document.body.scrollHeight);");
	//	}
	//
	//	// HOMEPAGE
	//	@Override
	//	public void featureProductSymbol() throws Exception {
	//		JavascriptExecutor js = (JavascriptExecutor) driver;
	//		js.executeScript("window.scrollBy(0,1000)");
	//		List<WebElement> fp = homePages.featureProductSymbol();
	//		System.out.println(fp.size());
	//
	//	}
	//
	//	@Override
	//	public void featureProductBackGroundImage() throws Exception {
	//		List<WebElement> bg = homePages.featureProductBackGroundImage();
	//		System.out.println(bg.size());
	//	}
	//
	//	@Override
	//	public void featureProductDesc() throws Exception {
	//		List<WebElement> Desc = homePages.featureProductDesc();
	//		System.out.println(Desc.size());
	//
	//	}
	//
	//	@Override
	//	public void featureProductButton2() throws Exception {
	//		List<WebElement> hm = homePages.featureProductButton2();
	//		System.out.println(hm.size());
	//		hm.get(0).click();
	//
	//	}
	//
	//	// megamenu
	//	@Override
	//	public void createAnAccount() throws Exception {
	//		ClickOnElement(megamenu.createAnAccount());
	//
	//	}
	//
	//	@Override
	//	public void clickOnCreateNewAccount() throws Exception {
	//		JavascriptExecutor js = (JavascriptExecutor) driver;
	//		js.executeScript("window.scrollBy(0,1000)");
	//		ClickOnElement(megamenu.clickOnCreateNewAccount());
	//
	//	}
	//
	//	@Override
	//	public void clickOnPasswordEyeIcon() throws Exception {
	//		ClickOnElement(megamenu.clickOnPasswordEyeIcon());
	//
	//	}
	//
	//	// OUR Story
	//	@Override
	//	public void clickOurStoryReadMore() throws Exception {
	//		ClickOnElement(homePages.clickOurStoryReadMore());
	//
	//	}
	//
	//	// contact us
	//	@Override
	//	public void contactUsSucessMessage() throws Exception {
	//		Assert.assertTrue(verifyElementIsPresent(contactus.contactUsSucessMessage()));
	//
	//	}
	//
	//	// legal
	//	@Override
	//	public void legalTermsOfUse() throws Exception {
	//		ClickOnElement(legal.legalTermsOfUse());
	//
	//	}
	//
	//	@Override
	//	public void legalResalePolicy() throws Exception {
	//		ClickOnElement(legal.legalResalePolicy());
	//
	//	}
	//
	//	@Override
	//	public void legalPrivacyPolicy() throws Exception {
	//		ClickOnElement(legal.legalPrivacyPolicy());
	//
	//	}
	//
	//	@Override
	//	public void legalHippa() throws Exception {
	//		ClickOnElement(legal.legalHippa());
	//
	//	}
	//
	//	// promotion banner not there
	//	@Override
	//	public void pBVerified() throws Exception {
	//		Assert.assertTrue(verifyElementIsPresent(promotionbanner.PromotionBanner_Text()), "Not present pb");
	//	}
	//
	//	@Override
	//	public void clickOnPB() throws Exception {
	//		ClickOnElement(promotionbanner.clickOnPB());
	//
	//	}
	//
	//	// Home page
	//	@Override
	//	public void featureProductIcon() throws Exception {
	//		Assert.assertTrue(verifyElementIsPresent(homePages.featureProductIcon()));
	//	}
	//
	//	@Override
	//	public void featureProductTitle() throws Exception {
	//		Assert.assertTrue(verifyElementIsPresent(homePages.featureProductTitle())); // TODO
	//		// Auto-generated
	//		// method
	//		// stub
	//
	//	}
	//
	//	// Article detail page
	//	@Override
	//	public void backToArticles() throws Exception {
	//		ClickOnElement(articlelisteing.backToArticles());
	//	}
	//
	//	// Mega menu
	//	@Override
	//	public void drugNutrientInteraction() throws Exception {
	//		List<WebElement> pl = megamenu.drugNutrientInteraction();
	//		System.out.println(pl.size());
	//		for (int i = 1; i <= pl.size(); i++) {
	//			pl.get(0).click();
	//			break;
	//		}
	//	}
	//
	//	@Override
	//	public void patientResource() throws Exception {
	//		Actions builder = new Actions(driver);
	//		articlelisteing.resour().click();
	//		//builder.clickAndHold(megamenu.patientResource());
	//		builder.moveToElement(megamenu.patientResource()).click().perform();
	//
	//	}
	//
	//	@Override
	//	public void conditions() throws Exception {
	//		List<WebElement> pl = megamenu.conditions();
	//		System.out.println(pl.size());
	//		for (int i = 1; i <= pl.size(); i++) {
	//			pl.get(3).click();
	//			break;
	//		}
	//	}
	//
	//	@Override
	//	public void findAPractitioner() throws Exception {
	//		/*List<WebElement> pl = megamenu.findAPractitioner();
	//		System.out.println(pl.size());
	//		for (int i = 1; i <= pl.size(); i++) {
	//			pl.get(4).click();
	//			break;
	//		}*/
	//		Actions a = new Actions(driver);
	//		a.clickAndHold(articlelisteing.FindAPractitoner());
	//		ClickOnElement(articlelisteing.FindAPractitoner());
	//
	//	}
	//
	//	// contact-us
	//	@Override
	//	public void phoneNumber() throws Exception {
	//		JavascriptExecutor js = (JavascriptExecutor) driver;
	//		js.executeScript("window.scrollBy(0,200)");
	//		Thread.sleep(1000);
	//		ClickOnElement(contactus.phoneNumber());
	//	}
	//
	//	// Related Articles
	//	@Override
	//	public void relatedArticlesLogo() throws Exception {
	//		Assert.assertTrue(verifyElementIsPresent(articlelisteing.relatedArticlesLogo()));
	//
	//	}
	//
	//	@Override
	//	public void relatedArticlesTitle() throws Exception {
	//		Assert.assertTrue(verifyElementIsPresent(articlelisteing.relatedArticlesTitle()));
	//
	//	}
	//
	//	@Override

	//
	//	@Override
	//	public void relatedArticlesDesc() throws Exception {
	//		List<WebElement> ra = articlelisteing.relatedArticlesDesc();
	//		System.out.println(ra.size());
	//		boolean flag=true;
	//		for (int i = 0; i <ra.size();i++) {
	//			String s = ra.get(i).getText();
	//			if(s==null) {
	//				flag=false;
	//				break;
	//			}
	//		}
	//		Assert.assertTrue(flag, "Related article does not have description");
	//	}
	//
	//	@Override
	//	public void relatedArticlesReadMore() throws Exception {
	//		List<WebElement> ra = articlelisteing.relatedArticlesReadMore();
	//		System.out.println(ra.size());
	//		for (int i = 1; i <= ra.size(); i++) 
	//		{
	//			ra.get(1).click();
	//			break;
	//		}
	//
	//	}
	//
	//	// sig-in
	//	@Override
	//	public void siginModelWindowClosed() throws Exception {
	//		ClickOnElement(megamenu.siginModelWindowClosed());
	//
	//	}
	//
	//	// Forgot Paaword
	//	@Override
	//	public void enterRongMail(String email) throws Exception {
	//		forgotpassword.enterRongMail().sendKeys(email);
	//
	//	}
	//
	//	@Override
	//	public void verifyErrorMessage() throws Exception {
	//		String s = forgotpassword.verifyErrorMessage().getText();
	//		System.out.println(s);
	//
	//	}
	//
	//	@Override
	//	public void forgotCancel() throws Exception {
	//		ClickOnElement(forgotpassword.forgotCancel());
	//
	//	}
	//
	//	@Override
	//	public void clickOnForgotPassword() throws Exception {
	//		ClickOnElement(forgotpassword.enterRongMail());
	//
	//	}
	//
	//	// contact-us
	//	@Override
	//	public void contactUsMessageTextBoxErrorMessage() throws Exception {
	//		String sc = contactus.contactUsMessageTextBoxErrorMessage().getText();
	//		System.out.println(sc);
	//
	//	}
	//
	//	// home page
	//	@Override
	//	public void hct() throws Exception {
	//		Assert.assertTrue(verifyElementIsPresent(homePages.hct()));
	//
	//	}
	//
	//	@Override
	//	public void hcd() throws Exception {
	//		String s = homePages.hcd().getText();
	//		System.out.println(s);
	//
	//	}
	//
	//	// megamenu
	//	@Override
	//	public void clickOnWelcomeUser() throws Exception {
	//		ClickOnElement(megamenu.clickOnWelcomeUser());
	//
	//	}
	//
	//	// forgot password
	//	@Override
	//	public void forgotPassTitle() throws Exception {
	//		Assert.assertTrue(verifyElementIsPresent(forgotpassword.forgotPassTitle()));
	//
	//	}
	//
	//	@Override
	//	public void forgotPassDes() throws Exception {
	//		Assert.assertTrue(verifyElementIsPresent(forgotpassword.forgotPassDes()));
	//
	//	}
	//
	//	// Globel header
	//	@Override
	//	public void clickOnIp() throws Exception {
	//		ClickOnElement(globelheader.clickOnIp());
	//
	//	}
	//
	//	// home page fp
	//	@Override
	//	public void fphDesc() throws Exception {
	//		Assert.assertTrue(verifyElementIsPresent(homePages.fphDesc()));
	//
	//	}
	//
	//	// Article details
	//	@Override
	//	public void articlebc() throws Exception {
	//		ClickOnElement(articlelisteing.articlebc());
	//
	//	}
	//
	//	// cadence2AboutUs
	//	@Override
	//	public void clickOnAboutUs() throws Exception {
	//		try {
	//			//Assert.
	//			Assert.assertTrue(verifyElementIsPresent(aboutus.clickOnAboutUs()),"About Us Menu is not displayed in header");
	//			ClickOnElement(aboutus.clickOnAboutUs());
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnWhoWeAre() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			Actions builder = new Actions(driver);
	//			builder.clickAndHold(aboutus.clickOnAboutUs());
	//			Assert.assertTrue(verifyElementIsPresent(aboutus.clickOnwhoweare()),"Who We Are sub-menu is not displayed in header");
	//			builder.moveToElement(aboutus.clickOnwhoweare()).click().perform();
	//			
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void Auhbtitle() throws Exception {
	//		try {
	//			Assert.assertTrue(verifyElementIsPresent(aboutus.Auhbtitle()),"About Us : Title not displayed in Hero banner");
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void Auhbdescription() throws Exception {
	//		try {
	//			Assert.assertTrue(verifyElementIsPresent(aboutus.Auhbdescription()),"About Us : Description not displayed in Hero banner");
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	
	//	@Override
	//	public void VerifyTitle_LeftImages() throws Exception {
	//		try {
	//			
	//			List<WebElement> title = aboutus.Title_leftimages();
	//			for (WebElement title_leftimage :title)
	//			{
	//				if(title_leftimage.getText()!=null)
	//					break;
	//			}
	//		}
	//		catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//	
	//	@Override
	//	public void VerifyDescription_LeftImages() throws Exception {
	//		try {
	//			
	//			List<WebElement> desc = aboutus.Description_leftimages();
	//			for (WebElement description_leftimage :desc)
	//			{
	//				if(description_leftimage.getText()!=null)
	//					break;
	//			}
	//		}
	//		catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void ClickReadMoreBtn_LeftImages() throws Exception {
	//		try {
	//			
	//			List<WebElement> readmore = aboutus.ReadMoreBtn_leftimages();
	//			for (WebElement readmoreBtn_leftimg :readmore)
	//			{
	//				//scrollToElement(readmoreBtn_leftimg,driver);
	//				readmoreBtn_leftimg.click();
	//				driver.navigate().back();
	//				
	//			}
	//		}
	//		catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	
	//	
	//	@Override
	//	public void VerifyTitle_RightImages() throws Exception {
	//		try {
	//			
	//			List<WebElement> title = aboutus.Title_leftimages();
	//			for (WebElement title_leftimage :title)
	//			{
	//				if(title_leftimage.getText()!=null)
	//					break;
	//			}
	//		}
	//		catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//	
	//	@Override
	//	public void VerifyDescription_RightImages() throws Exception {
	//		try {
	//			
	//			List<WebElement> desc = aboutus.Description_leftimages();
	//			for (WebElement description_leftimage :desc)
	//			{
	//				if(description_leftimage.getText()!=null)
	//					break;
	//			}
	//		}
	//		catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void ClickReadMoreBtn_RightImages() throws Exception {
	//		try {
	//			
	//			List<WebElement> readmore = aboutus.ReadMoreBtn_leftimages();
	//			for (WebElement readmoreBtn_leftimg :readmore)
	//			{
	//				//scrollToElement(readmoreBtn_leftimg,driver);
	//				readmoreBtn_leftimg.click();
	//				driver.navigate().back();
	//				
	//			}
	//		}
	//		catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	
	//	
	//	
	//	
	//	@Override
	//	public void AuOHTitle() throws Exception {
	//		try {
	//			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	//			Assert.assertTrue(verifyElementIsPresent(aboutus.AuOHTitle()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AuOHImages() throws Exception {
	//		try {
	//			Assert.assertTrue(verifyElementIsPresent(aboutus.AuOHImages()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AuOHDesc() throws Exception {
	//		try {
	//			Assert.assertTrue(verifyElementIsPresent(aboutus.AuOHDesc()));
	//			Assert.assertTrue(verifyElementIsPresent(aboutus.AuOHDesc2()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AuOPTitle() throws Exception {
	//		try {
	//			Assert.assertTrue(verifyElementIsPresent(aboutus.AuOPTitle()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AuOPDesc() throws Exception {
	//		try {
	//			Assert.assertTrue(verifyElementIsPresent(aboutus.AuOPDesc()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnOPRMButton() throws Exception {
	//		try {
	//			ClickOnElement(aboutus.clickOnOPRMButton());
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AuFBTitle() throws Exception {
	//		try {
	//			scrollToElement(aboutus.AuFBTitle(),driver);
	//			Assert.assertTrue(verifyElementIsPresent(aboutus.AuFBTitle()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AuFBDesc() throws Exception {
	//		try {
	//			Assert.assertTrue(verifyElementIsPresent(aboutus.AuFBDesc()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AuHeader() throws Exception {
	//			Thread.sleep(6000);
	//			waitTillElementExist(aboutus.AuHeader());
	//			Assert.assertTrue(verifyElementIsPresent(aboutus.AuHeader()),"About Us header is not displayed");
	//		
	//	}
	//
	//	// About-Us Quality
	//	@Override
	//	public void AbusClickOnQuality() throws Exception {
	//		try {
	//			List<WebElement> auq = aboutusquality.AbusClickOnQuality();
	//			System.out.println(auq.size());
	//			for (int ABus = 2; ABus < auq.size() - 0; ABus++) {
	//				String t = auq.get(ABus).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("quality")) {
	//					auq.get(ABus).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AbUsQualityTitle() throws Exception {
	//		try {
	//			JavascriptExecutor js = (JavascriptExecutor) driver;
	//			js.executeScript("window.scrollBy(0,1000)");
	//			String s = aboutusquality.AbUsQualityTitle().getText();
	//			System.out.println(s + " " + "Vedio Title");
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AbUsQualityDesc() throws Exception {
	//		try {
	//			String s = aboutusquality.AbUsQualityDesc().getText();
	//			System.out.println(s + " " + "Vedio Desc");
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AbUsClickOnVedio() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			for (int vedio = 0; vedio < 2; vedio++) {
	//				//scrollToElement(aboutusquality.AbUsClickOnVedio(),driver);
	//				Actions builder = new Actions(driver);
	//				builder.moveToElement(aboutusquality.AbUsClickOnVedio()).click().perform();
	//				//ClickOnElement(aboutusquality.AbUsClickOnVedio());
	//				Thread.sleep(10000);
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void QualityTitle_RightSection() throws Exception {
	//		try {
	//			List<WebElement> title_rightsection = aboutusquality.QualityPage_Title_rightsection();
	//            for (WebElement Title :title_rightsection)
	//            {
	//            	String Title_text=Title.getText();
	//            	if(Title_text!=null)
	//            		break;
	//            	else
	//            		throw new Exception("Title is not available for Right section");		}
	//			} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void QualityDescription_RightSection() throws Exception {
	//		try {
	//			List<WebElement> description_rightsection = aboutusquality.QualityPage_Description_rightsection();
	//            for (WebElement Title :description_rightsection)
	//            {
	//            	String Title_text=Title.getText();
	//            	if(Title_text!=null)
	//            		break;
	//            	else
	//            		throw new Exception("Title is not available for Right section");		}
	//			} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AbUsISDesc() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			Assert.assertTrue(verifyElementIsPresent(aboutusquality.AbUsISDesc()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AbUsISImage() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			Assert.assertTrue(verifyElementIsPresent(aboutusquality.AbUsISImage()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AbUsFDTitle() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			Assert.assertTrue(verifyElementIsPresent(aboutusquality.AbUsFDTitle()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AbUsFDDesc() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			Assert.assertTrue(verifyElementIsPresent(aboutusquality.AbUsFDDesc()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AbUsFDImage() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			Assert.assertTrue(verifyElementIsPresent(aboutusquality.AbUsFDImage()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AbUsMTitle() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			Assert.assertTrue(verifyElementIsPresent(aboutusquality.AbUsMTitle()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AbUsMDesc() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			Assert.assertTrue(verifyElementIsPresent(aboutusquality.AbUsMDesc()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AbUsMImage() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			Assert.assertTrue(verifyElementIsPresent(aboutusquality.AbUsMImage()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AbUsLTitle() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			scrollToElement(aboutusquality.AbUsLTitle(),driver);
	//			Assert.assertTrue(verifyElementIsPresent(aboutusquality.AbUsLTitle()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AbUsLDesc() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			Assert.assertTrue(verifyElementIsPresent(aboutusquality.AbUsLDesc()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AbUsLImage() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			Assert.assertTrue(verifyElementIsPresent(aboutusquality.AbUsLImage()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AbUsQFBTitle() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			Assert.assertTrue(verifyElementIsPresent(aboutusquality.AbUsQFBTitle()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AbUsQFBDesc() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			Assert.assertTrue(verifyElementIsPresent(aboutusquality.AbUsQFBDesc()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AbUsQBFImage() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> bfi = aboutusquality.AbUsQBFImage();
	//			System.out.println(bfi.size());
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AbUsQFACTitle() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			WaitTillElementExist(aboutusquality.AbUsQFACTitle(),10);
	//			Assert.assertTrue(verifyElementIsPresent(aboutusquality.AbUsQFACTitle()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	public void AbUsQFACDesc() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			Assert.assertTrue(verifyElementIsPresent(aboutusquality.AbUsQFACDesc()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void AbUsQFACImage() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> aci = aboutusquality.AbUsQFACImage();
	//			System.out.println(aci.size());
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//
	//	}
	//
	//	@Override
	//	public void cDPageLinks() throws Exception {
	//		try {
	//			List<WebElement> linkList = driver.findElements(By.tagName("a"));
	//			System.out.println("size of full links ---->" + linkList);// TODO
	//																		// Auto-generated
	//																		// method
	//																		// stub
	//
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the links", e);
	//		}
	//	}
	//
	//	// patient resource page
	//	@Override
	//	public void patientRTHead() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			Assert.assertTrue(verifyElementIsPresent(patientresourcepage.patientRTHead()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//

	//
	//	@Override
	public void patientRTResourcesAlpha() throws Exception {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			List<WebElement> pres = patientresourcepage.patientRTResourcesAlpha();
			System.out.println(pres.size());
			int c = 0;
			for (int pr = 0; pr < pres.size(); pr++) {
				String alpha = pres.get(pr).getText();
				System.out.println(alpha + " " + "Alphabatical order");
				if (pr < pres.size() - 1) {
					String alp = pres.get(pr + 1).getText();
					if (alpha.compareTo(alp) > 0) {
						c = 1;
					}
				}
			}
			if (c == 1) {
				System.out.println(c + " " + "Not in order");
			} else {
				System.out.println(c + " " + "it is in order");
			}
		} catch (Exception e) {
			throw new Exception("Unknown error while validating the css path", e);
		}

	}
	//
	//	@Override
	//	public void patientRClickOnPI() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> rcon = patientresourcepage.patientRClickOnPI();
	//			System.out.println(rcon.size());
	//			for (int con = 0; con < rcon.size() - 1; con++) {
	//				String t = rcon.get(con).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("PRODUCT INFORMATION")) {
	//					rcon.get(con).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//
	//	}
	//

	//
	//	@Override
	//	public void patientRPIR() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			String s = patientresourcepage.patientRPIR().getText();
	//			System.out.println(s + " " + " total no of resources in the patient information");
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void patientRTClickOnHc() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> rcon = patientresourcepage.patientRClickOnPI();
	//			System.out.println(rcon.size());
	//			for (int con = 1; con < rcon.size() - 0; con++) {
	//				String t = rcon.get(con).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("HEALTH CONDITIONS")) {
	//					rcon.get(con).click();
	//					Thread.sleep(2000);
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//
	//	}
	//
	//	@Override
	//	public void patientRHCR() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			String s = patientresourcepage.patientRHCR().getText();
	//			System.out.println(s + " " + "total no of resources in the health condition");
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void patientRVerifyHCPdfs() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pres = patientresourcepage.patientRVerifyHCPdfs();
	//			System.out.println(pres.size());
	//			for (int pr = 0; pr < pres.size(); pr++) {
	//				if (pr == 0 || pr == 1 || pr == 2 || pr == 3) {
	//					pres.get(pr).isEnabled();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	// search pane
	//	@Override
	//	public void clickOnSearchPane() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			ClickOnElement(globelheadersearch.clickOnSearchPane());
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void verifyPP() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pres = globelheadersearch.verifyPP();
	//			System.out.println(pres.size());
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void verifyPA() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pres = globelheadersearch.verifyPA();
	//			System.out.println(pres.size());
	//
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void enterThreecCharSP() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			ClickOnElement(globelheadersearch.clickOnSearchPane());
	//			globelheadersearch.enterThreecCharSP().sendKeys("key");
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnSProd() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pres = globelheadersearch.clickOnSProd();
	//			System.out.println(pres.size());
	//			for (int pr = 1; pr < pres.size() - 3; pr++) {
	//				if (pr == 0 || pr == 1 || pr == 2 || pr == 3 || pr == 4) {
	//					pres.get(pr).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnSAT() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pres = globelheadersearch.clickOnSAT();
	//			System.out.println(pres.size());
	//			for (int pr = 1; pr < pres.size() - 3; pr++) {
	//				if (pr == 0 || pr == 1 || pr == 2 || pr == 3 || pr == 4) {
	//					pres.get(pr).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnSPVAP() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			ClickOnElement(globelheadersearch.clickOnSPVAP());
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	// product listeing
	//	@Override
	//	public void clickOnMMenuProducts() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			ClickOnElement(productlisting.clickOnMMenuProducts());
	//
	//			/*	 Actions builder = new Actions(driver);
	//		     builder.moveToElement(plp.clickOnMMenuProducts()).click(plp.clickOnMMenuProducts());
	//		     builder.perform();
	//			 */
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnMMVAP() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pl = productlisting.clickOnMMVAP();
	//			System.out.println(pl.size());
	//			for (int plpp = 3; plpp < pl.size() - 0; plpp++) {
	//				String t = pl.get(plpp).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("view all products")) {
	//					pl.get(plpp).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//
	//	}
	//
	//	@Override
	//	public void clickOnPI() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pl = productlisting.clickOnPI();
	//			System.out.println(pl.size());
	//			for (int plpp = 0; plpp < pl.size() - 23; plpp++) {
	//				if (plpp == 0) {
	//					pl.get(plpp).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//
	//	}
	//
	//	@Override
	//	public void navigateBack() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			productlisting.navigateBack();
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnProdTitle() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pl = productlisting.clickOnProdTitle();
	//			System.out.println(pl.size());
	//			for (int plpp = 0; plpp < pl.size() - 23; plpp++) {
	//				String t = pl.get(plpp).getText();
	//				System.out.println(t);
	//				if (plpp == 0) {
	//					pl.get(plpp).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//
	//	}
	//
	//	int i = 0;
	//
	//	@Override
	//
	//	@Override
	//	public void clickOnCAF() throws Exception {
	//		try {
	//			scrollToElement(productlisting.NowFilterBySection(),driver);
	//			Thread.sleep(3000);
	//		    WaitTillElementExist(productlisting.clickOnCAF(),20);
	//			ClickOnElement(productlisting.clickOnCAF());
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override

	//	@Override
	//	public void clickOnSMR() throws Exception {
	//		try {
	//			JavascriptExecutor js = (JavascriptExecutor) driver;
	//			js.executeScript("window.scrollBy(0,1000)");
	//			for (int plpp = 0; plpp < 4; plpp++) {
	//				ClickOnElement(productlisting.clickOnSMR());
	//				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnBreadCrumb() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			ClickOnElement(productlisting.clickOnBreadCrumb());
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnViewDetails() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pl = productlisting.clickOnViewDetails();
	//			System.out.println(pl.size());
	//			for (int plpp = 0; plpp < pl.size() - 23; plpp++) {
	//				String t = pl.get(plpp).getText();
	//				System.out.println(t);
	//				if (plpp == 0) {
	//					pl.get(plpp).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//
	//	}
	//
	//	@Override
	//	public void clickOnForm() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pl = productlisting.clickOnForm();
	//			System.out.println(pl.size());
	//			for (int plpp = 0; plpp < pl.size() - 4; plpp++) {
	//				String t = pl.get(plpp).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("Form")) {
	//					pl.get(plpp).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//
	//	}
	//
	//	@Override
	//	public void clickOnKeyIngredients() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pl = productlisting.clickOnKeyIngredients();
	//			System.out.println(pl.size());
	//			for (int plpp = 1; plpp < pl.size() - 3; plpp++) {
	//				String t = pl.get(plpp).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("DIETARY NEEDS")) {
	//					pl.get(plpp).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//
	//	}
	//
	//	@Override
	//	public void clickOnProductType() throws Exception {
	//		try {
	//			JavascriptExecutor js = (JavascriptExecutor) driver;
	//			js.executeScript("window.scrollBy(0,300)");
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pl = productlisting.clickOnProductType();
	//			System.out.println(pl.size());
	//			for (int plpp = 2; plpp < pl.size() - 2; plpp++) {
	//				String t = pl.get(plpp).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("FORMULATION")) {
	//					pl.get(plpp).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//
	//	}
	//
	//	@Override
	//	public void clickOnHealthFilter() throws Exception {
	//		try {
	//			JavascriptExecutor js = (JavascriptExecutor) driver;
	//			js.executeScript("window.scrollBy(0,700)");
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pl = productlisting.clickOnHealthFilter();
	//			System.out.println(pl.size());
	//			for (int plpp = 3; plpp < pl.size() - 1; plpp++) {
	//				String t = pl.get(plpp).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("PRODUCT TYPE")) {
	//					pl.get(plpp).click();
	//
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//
	//	}
	//
	//	@Override
	//	public void clickOnWomensFreeForm() throws Exception {
	//		try {
	//			JavascriptExecutor js = (JavascriptExecutor) driver;
	//			js.executeScript("window.scrollBy(0,600)");
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pl = productlisting.clickOnWomensFreeForm();
	//			System.out.println(pl.size());
	//			for (int plpp = 4; plpp < pl.size(); plpp++) {
	//				String t = pl.get(plpp).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("HEALTH")) {
	//					pl.get(plpp).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//
	//	}
	//
	//	@Override
	//	public void clickOnSortProducts() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> sortproducts = productlisting.clickOnSortProducts();
	//			System.out.println(sortproducts.size());
	//		
	//			for (int plpp = 0; plpp < sortproducts.size()-1; plpp++) {
	//				String t = sortproducts.get(plpp).getText();
	//				String A_Z_FirstProduct=productlisting.Firstproduct_PDP().getText();
	//				if (t.equalsIgnoreCase("A-Z")) {
	//					sortproducts.get(plpp).click();
	//				}
	//				else if(t.equalsIgnoreCase("Z-A")) {
	//					sortproducts.get(plpp).click();
	//					String Z_A_FirstProduct=productlisting.Firstproduct_PDP().getText();
	//                    if(Z_A_FirstProduct==A_Z_FirstProduct)
	//                    	throw new Exception ("Error");
	//					
	//				}				
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//
	//	}
	//

	//	// Globel header integrative pro products
	//	@Override
	//	public void clickOnRFPI() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			ClickOnElement(globalheaderintegrativeproduct.clickOnRFPI());
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnRFPVD() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			ClickOnElement(globalheaderintegrativeproduct.clickOnRFPVD());
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnByType() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> ghip = globalheaderintegrativeproduct.clickOnByType();
	//			System.out.println(ghip.size());
	//			for (int globalheaderintegrativeproduct = 0; globalheaderintegrativeproduct < ghip.size() - 3; globalheaderintegrativeproduct++) {
	//				String t = ghip.get(globalheaderintegrativeproduct).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("By Type")) {
	//					ghip.get(globalheaderintegrativeproduct).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//
	//	}
	//
	//	@Override
	//	public void clickOnAntioxidants() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> ghip = globalheaderintegrativeproduct.clickOnAntioxidants();
	//			System.out.println(ghip.size());
	//			for (int globalheaderintegrativeproduct = 0; globalheaderintegrativeproduct < ghip.size() - 18; globalheaderintegrativeproduct++) {
	//				String t = ghip.get(globalheaderintegrativeproduct).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("Antioxidants")) {
	//					ghip.get(globalheaderintegrativeproduct).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//
	//	}
	//
	//	@Override
	//	public void clickOnHI() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> ghip = globalheaderintegrativeproduct.clickOnHI();
	//			System.out.println(ghip.size());
	//			for (int globalheaderintegrativeproduct = 1; globalheaderintegrativeproduct < ghip.size() - 2; globalheaderintegrativeproduct++) {
	//				String t = ghip.get(globalheaderintegrativeproduct).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("by health interest")) {
	//					ghip.get(globalheaderintegrativeproduct).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//
	//	}
	//
	//	@Override
	//	public void clickOnAS() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> ghip = globalheaderintegrativeproduct.clickOnAS();
	//			System.out.println(ghip.size());
	//			for (int globalheaderintegrativeproduct = 8; globalheaderintegrativeproduct < ghip.size() - 10; globalheaderintegrativeproduct++) {
	//				String t = ghip.get(globalheaderintegrativeproduct).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("ADRENAL / STRESS / SLEEP")) {
	//					ghip.get(globalheaderintegrativeproduct).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//
	//	}
	//
	//	@Override
	//	public void clickOnPK() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> ghip = globalheaderintegrativeproduct.clickOnPK();
	//			System.out.println(ghip.size());
	//			for (int globalheaderintegrativeproduct = 2; globalheaderintegrativeproduct < ghip.size() - 1; globalheaderintegrativeproduct++) {
	//				String t = ghip.get(globalheaderintegrativeproduct).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("protocol kits")) {
	//					ghip.get(globalheaderintegrativeproduct).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//
	//	}

	//	@Override
	//	public void clickOnSubitems() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> ghip = globalheaderintegrativeproduct.clickOnSubitems();
	//			System.out.println(ghip.size());
	//			for (int globalheaderintegrativeproduct = 17; globalheaderintegrativeproduct < ghip.size() - 1; globalheaderintegrativeproduct++) {
	//				String t = ghip.get(globalheaderintegrativeproduct).getText();
	//				System.out.println(t);
	//				if (globalheaderintegrativeproduct < 17) {
	//					ghip.get(globalheaderintegrativeproduct).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//
	//	}
	//
	//	// product details page
	//	@Override
	//	public void clickOnSPI() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pd = productdetailpage.clickOnSPI();
	//			System.out.println(pd.size());
	//			for (int pdv = 0; pdv < pd.size(); pdv++) {
	//				if (pdv == 0 || pdv == 1) {
	//					pd.get(pdv).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnSV() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pd = productdetailpage.clickOnSV();
	//			System.out.println(pd.size());
	//			for (int pdv = 0; pdv < pd.size(); pdv++) {
	//				if (pdv == 0 || pdv == 1 || pdv == 2 || pdv == 3 || pdv == 4 || pdv == 5 || pdv == 6) {
	//					pd.get(pdv).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnPQuality() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pd = productdetailpage.clickOnPQuality();
	//			System.out.println(pd.size());
	//			for (int pdv = 0; pdv < pd.size() - 99; pdv++) {
	//				if (pdv == 0) {
	//					pd.get(pdv).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnPUnit() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pd = productdetailpage.clickOnPUnit();
	//			System.out.println(pd.size());
	//			for (int pdv = 0; pdv < pd.size() - 1; pdv++) {
	//				String t = pd.get(pdv).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("units")) {
	//					pd.get(pdv).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void qDP() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			ClickOnElement(productdetailpage.qDP());
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void cDP() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			ClickOnElement(productdetailpage.cDP());
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnFavorites() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			ClickOnElement(productdetailpage.clickOnFavorites());
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void pDPBC() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pd = productdetailpage.pDPBC();
	//			System.out.println(pd.size());
	//			for (int pdv = 0; pdv < -1; pdv--) {
	//				if (pdv == 0 || pdv == 1 || pdv == 2) {
	//					pd.get(pdv).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override

	//
	//	@Override
	//	public void clickOnQues() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			ClickOnElement(productdetailpage.clickOnQues());
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnContactUs() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			ClickOnElement(productdetailpage.clickOnContactUs());
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void ClickOnpImageRA() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pl = productdetailpage.ClickOnpImageRA();
	//			System.out.println(pl.size());
	//			for (int plpp = 28; plpp < pl.size() - 91; plpp++) {
	//				if (plpp == 28) {
	//					pl.get(plpp).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//
	//	}
	//
	//	@Override
	//	public void Display_RelatedProductTitle_PDP() throws Exception {
	//		 try {
	//			explicitWait(productdetailpage.Title_RelatedProduct_PDP());
	//			highLighterMethod(driver,productdetailpage.Title_RelatedProduct_PDP());
	//			Assert.assertTrue(productdetailpage.Title_RelatedProduct_PDP().isDisplayed(),"Related Products section is not displayed in PDP");
	//			String s = productdetailpage.Title_RelatedProduct_PDP().getText();
	//			System.out.println(s + "Title");
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//	
	//	@Override
	//	public void ClickFeaturedArticle_Img_PDP() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pd = productdetailpage.clickOnFPI();
	//			System.out.println(pd.size());
	//			for (int pdv = 0; pdv < pd.size() - 2; pdv++) {
	//				if (pdv == 0) {
	//					pd.get(pdv).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void VerifyFeaturedArticle_PDP() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			scrollToElement(productdetailpage.verifyFPH(),driver);
	//			String s = productdetailpage.verifyFPH().getText();
	//			System.out.println(s + "Title");
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//	
	//	@Override
	//	public void Click_ViewDetails_Btn_RelatedProd() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pd = productdetailpage.clickOnVDRA();
	//			System.out.println(pd.size());
	//			for (int pdv = 0; pdv < pd.size() - 2; pdv++) {
	//				String t = pd.get(pdv).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("view details")) {
	//					pd.get(pdv).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//	
	//	@Override
	//	public void Click_RelatedProduct_PDP() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	//			List<WebElement> pd = productdetailpage.relatedproducts_PDP();
	//			System.out.println(pd.size());
	//			for (int pdv = 0; pdv < pd.size() - 2; pdv++) {
	//				if (pdv == 0) {
	//					pd.get(pdv).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	/*@Override
	//	public void vAH() throws Exception {
	//	
	//		 try {
	//			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	//			Thread.sleep(10000);
	//			String s = productdetailpage.vAH().getText();
	//			System.out.println(s + "Title");
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnRAI() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	//			List<WebElement> pd = productdetailpage.clickOnRAI();
	//			System.out.println(pd.size());
	//			for (int pdv = 0; pdv < pd.size() - 2; pdv++) {
	//				if (pdv == 0) {
	//					pd.get(pdv).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//	*/
	//
	//	@Override
	//	public void clickOnRAMT() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pd = productdetailpage.clickOnRAMT();
	//			System.out.println(pd.size());
	//			for (int pdv = 0; pdv < pd.size() - 2; pdv++) {
	//				String t = pd.get(pdv).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("Blue Heron")) {
	//					pd.get(pdv).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnVDRA() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pd = productdetailpage.clickOnVDRA();
	//			System.out.println(pd.size());
	//			for (int pdv = 0; pdv < pd.size() - 2; pdv++) {
	//				String t = pd.get(pdv).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("view details")) {
	//					pd.get(pdv).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnFPA() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pl = productdetailpage.clickOnFPA();
	//			System.out.println(pl.size());
	//			for (int plpp = 28; plpp < pl.size() - 91; plpp++) {
	//				if (plpp == 28) {
	//					pl.get(plpp).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//
	//	}
	//
	//	@Override
	//	public void clickOnFPI() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pd = productdetailpage.clickOnFPI();
	//			System.out.println(pd.size());
	//			for (int pdv = 0; pdv < pd.size() - 2; pdv++) {
	//				if (pdv == 0) {
	//					pd.get(pdv).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void verifyFPT() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pd = productdetailpage.verifyFPT();
	//			System.out.println(pd.size());
	//			for (int pdv = 0; pdv < pd.size() - 2; pdv++) {
	//				String t = pd.get(pdv).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("Alpha Lipoic Acid (Ala): Benefits & Dosing")) {
	//					pd.get(pdv).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void verifyFPH() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			String s = productdetailpage.verifyFPH().getText();
	//			System.out.println(s + "Title");
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnRMFP() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> pd = productdetailpage.clickOnVDRA();
	//			System.out.println(pd.size());
	//			for (int pdv = 0; pdv < pd.size() - 2; pdv++) {
	//				String t = pd.get(pdv).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("read more")) {
	//					pd.get(pdv).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	// cadence3 product detail page
	//	@Override
	//	public void favSucessMessage() throws Exception {
	//		// TODO Auto-generated method stub
	//
	//	}
	//
	//	@Override
	//	public void clickOnHowToOrder() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			ClickOnElement(pdp3.clickOnHowToOrder());
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void sigInPDP() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			ClickOnElement(pdp3.sigInPDP());
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void ATCB() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			ClickOnElement(pdp3.ATCB());
	//			Thread.sleep(10000);
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//	
	//	@Override
	//	public void Display_Subtotal() throws Exception {
	//		try {
	//			verifyElementIsPresent(pdp3.Subtotal_text());
	//			verifyElementIsPresent(pdp3.No_Of_Items());
	//			
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		
	//		}
	//	
	//	}
	//	
	//	@Override
	//	public void VerifyCartDrawer_Displayed() throws Exception {
	//		try {
	//			waitTillElementExist(pdp3.CartDrawer());
	//			Assert.assertTrue(pdp3.CartDrawer().isDisplayed(), "Cart Drawer is not displayed");
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//	
	//	// usersAccount
	//
	//	/*	@Override
	//	public void clickOnAD() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> ua = UAP.clickOnAD();
	//			System.out.println(ua.size());
	//			for (int uap = 0; uap < ua.size() - 7; uap++) {
	//				String t = ua.get(uap).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("Account Details")) {
	//					ua.get(uap).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnMO() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> ua = UAP.clickOnMO();
	//			System.out.println(ua.size());
	//			for (int uap = 1; uap < ua.size() - 6; uap++) {
	//				String t = ua.get(uap).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("My orders")) {
	//					ua.get(uap).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnPO() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> ua = UAP.clickOnPO();
	//			System.out.println(ua.size());
	//			for (int uap = 4; uap < ua.size() - 3; uap++) {
	//				String t = ua.get(uap).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("Payment methods")) {
	//					ua.get(uap).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnCP() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> ua = UAP.clickOnCP();
	//			System.out.println(ua.size());
	//			for (int uap = 3; uap < ua.size() - 4; uap++) {
	//				String t = ua.get(uap).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("Communication Preferences")) {
	//					ua.get(uap).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void clickOnFP() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> ua = UAP.clickOnFP();
	//			System.out.println(ua.size());
	//			for (int uap = 2; uap < ua.size() - 5; uap++) {
	//				String t = ua.get(uap).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("Favorites")) {
	//					ua.get(uap).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	 */
	//
	//	// cartdawer
	//	@Override
	//	public void PTCO() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> cartd = cartdrawer.PTCO();
	//			System.out.println(cartd.size());
	//			for (int cd = 0; cd < cartd.size() - 1; cd++) {
	//				String t = cartd.get(cd).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("proceed to checkout")) {
	//					cartd.get(cd).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void CS() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> cartd = cartdrawer.CS();
	//			System.out.println(cartd.size());
	//			for (int cd = 1; cd < cartd.size(); cd++) {
	//				String t = cartd.get(cd).getText();
	//				System.out.println(t);
	//				if (t.equalsIgnoreCase("continue shopping")) {
	//					cartd.get(cd).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void del() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> cartd = cartdrawer.del();
	//			System.out.println(cartd.size());
	//			for (int cd = 0; cd < cartd.size(); cd++) {
	//				if (cd == 0 || cd == 1 || cd == 2 || cd == 3 || cd == 4) {
	//					Thread.sleep(10000);
	//					cartd.get(cd).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void SFL() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> cartd = cartdrawer.SFL();
	//			System.out.println(cartd.size());
	//			for (int cd = 0; cd < cartd.size(); cd++) {
	//				if (cd == 0 || cd == 1 || cd == 2 || cd == 3 || cd == 4) {
	//					Thread.sleep(10000);
	//					cartd.get(cd).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//	@Override
	//	public void verify_ProceedToCheckout_Btn() throws Exception {
	//	Assert.assertTrue(cartdrawer.Proceed_To_Checkout_Btn().isDisplayed(), "Proceed To Checkout Button is not displayed");
	//	}
	//
	//	@Override
	//	public void verify_ContinueShopping_Btn() throws Exception {
	//		Assert.assertTrue(cartdrawer.ContinueShopping_Btn().isDisplayed(), "Continue Shopping Button is not displayed");
	//
	//	}
	//	
	//	// MiniCart
	//	@Override
	//	public void ClickOnHc() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			ClickOnElement(minicartpage.ClickOnHc());
	//			Thread.sleep(10000);
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	//Interaction & Depletions 
	//
	//	@Override
	//	public void searchADrug(String drugname) throws Exception {
	//		ClickOnElement(globelheadersearch.clickOnSearchPane());
	//		globelheadersearch.enterThreecCharSP().sendKeys(drugname);
	//		Thread.sleep(20000);
	//		List<WebElement> suggested=globelheadersearch.clickOnSProd();
	//		for(WebElement drug: suggested) {
	//			if(drug.getText().equalsIgnoreCase(drugname)) {
	//			Actions builder = new Actions(driver);
	//			builder.moveToElement(drug).click().perform();
	//			//Assert.assertTrue(drug.isDisplayed(), drug +"is not displayed in search pane");
	//			Thread.sleep(20000);}
	//			
	//		   }		
	//	 }
	//	

	//

	//
	//	//User Account Page		
	//	@Override
	//	public void loginSuccess() throws Exception {			
	//		verifyElementIsPresent(megamenu.clickOnWelcomeUser());
	//	}
	//
	//	@Override
	//	public void chevron_Username()throws Exception {
	//		verifyElementIsPresent(myaccount.chevron_username());
	//	}
	//

	//	@Override
	//	public void clickAccountInformation()throws Exception {
	//		ClickOnElement(myaccount.chevron_username());
	//		verifyElementIsPresent(myaccount.accountDetails_menu());
	//		ClickOnElement(myaccount.accountDetails_menu());
	//	}
	//
	//	@Override
	//	public void verifyAccountInfo_Page()throws Exception {
	//		String Account_Page=driver.getCurrentUrl();
	//		Thread.sleep(50000);
	//		if(!Account_Page.contains("accountmanagement"))
	//			Assert.fail("App is not redirected to Account Information Page");
	//		verifyElementIsPresent(myaccount.accountinfo_section());						
	//	}
	//
	//	@Override
	//	public void verifyusericon_Accountinfo()throws Exception {
	//		ClickOnElement(myaccount.chevron_username());
	//		verifyElementIsPresent(myaccount.usericon_Accountinfo());						
	//
	//	}
	//
	//	@Override
	//	public void verifyusericon_MyOrder() throws Exception {
	//		ClickOnElement(myaccount.chevron_username());
	//		verifyElementIsPresent(myaccount.usericon_MyOrders());						
	//
	//	}
	//
	//	@Override
	//	public void clickMyOrder_Dropdown() throws Exception {
	//		ClickOnElement(myaccount.chevron_username());
	//		verifyElementIsPresent(myaccount.myorders_menu());	
	//		ClickOnElement(myaccount.myorders_menu());	
	//
	//	}
	//
	//	@Override
	//	public void verifyMyOrder_Page() throws Exception {
	//		String MyOrder_Page=driver.getCurrentUrl();
	//		if(!MyOrder_Page.contains("myorders"))
	//			Assert.fail("App is not redirected to My Order Page");
	//	}
	//
	//	@Override
	//	public void verifyusericon_PaymentMethod() throws Exception{
	//		ClickOnElement(myaccount.chevron_username());
	//		verifyElementIsPresent(myaccount.usericon_PaymentMethods());	
	//
	//	}
	//
	//	@Override
	//	public void verifyusericon_CommunicationPreference() throws Exception{
	//		ClickOnElement(myaccount.chevron_username());
	//		verifyElementIsPresent(myaccount.usericon_CommunicationPreference());	
	//
	//	}
	//
	//	@Override
	//	public void verifyusericon_Favorite() throws Exception{
	//		ClickOnElement(myaccount.chevron_username());
	//		verifyElementIsPresent(myaccount.usericon_Favorite());	
	//
	//	}	
	//
	//	@Override
	//	public void verifyusericon_SignOut() throws Exception{
	//		ClickOnElement(myaccount.chevron_username());
	//		verifyElementIsPresent(myaccount.usericon_SignOut());	
	//
	//	}
	//
	//	@Override
	//	public void click_Signout() throws Exception {
	//		ClickOnElement(myaccount.chevron_username());
	//		verifyElementIsPresent(myaccount.usericon_SignOut());	
	//		ClickOnElement(myaccount.signout_menu());
	//
	//	}
	//
	//	@Override
	//	public void signout_success() throws Exception {
	//		Thread.sleep(10000);
	//		if(!verifyElementIsPresent(megamenu.signinbtn_header()) && !verifyElementIsPresent(megamenu.createAnAccount()))
	//			throw new Exception("App is not logged out successfully");	
	//
	//
	//	}
	//
	//	@Override
	//	public void clickOnAD() throws Exception {
	//		// TODO Auto-generated method stub
	//
	//	}
	//
	//	@Override
	//	public void clickOnMO() throws Exception {
	//		// TODO Auto-generated method stub
	//
	//	}
	//
	//	@Override
	//	public void clickOnPO() throws Exception {
	//		// TODO Auto-generated method stub
	//
	//	}
	//
	//	@Override
	//	public void clickOnCP() throws Exception {
	//		// TODO Auto-generated method stub
	//
	//	}
	//
	//	@Override
	//	public void clickOnFP() throws Exception {
	//		// TODO Auto-generated method stub
	//
	//	}
	//		

	//	
	//	@Override
	//	public void selectvalue_PrefixField() throws Exception {
	//		
	//		waitTillElementExist(createaccount.Prefix_Field());
	//		Select dropdown=new Select(createaccount.Prefix_Field());
	//		dropdown.selectByIndex(1);
	//		Thread.sleep(3000);
	//		Assert.assertTrue(createaccount.Prefix_Field().getAttribute("data-valid").contains("true"), "Prefix value is not selected");	
	//	}
	//	
	//	@Override
	//	public void selectvalue_PractitonerType() throws Exception {
	//		Select dropdown=new Select(createaccount.PractitionerType_Field());
	//		dropdown.selectByIndex(1);
	//		Thread.sleep(2000);
	//		Assert.assertTrue(createaccount.PractitionerType_Field().getAttribute("data-valid").contains("true"), "Prefix value is not selected");	
	//	} 
	//	
	//	@Override
	//	public void selectvalue_UsuageTypes() throws Exception {
	//		Select dropdown=new Select(createaccount.UsageTypes());
	//		dropdown.selectByIndex(1);
	//		Thread.sleep(2000);
	//		Assert.assertTrue(createaccount.UsageTypes().getAttribute("data-valid").contains("true"), "Prefix value is not selected");	
	//	} 
	//	
	//	@Override
	//	public void selectvalue_State() throws Exception {
	//		Select dropdown=new Select(createaccount.businessstate());
	//		dropdown.selectByIndex(1);
	//		Thread.sleep(2000);
	//		Assert.assertTrue(createaccount.businessstate().getAttribute("data-valid").contains("true"), "Prefix value is not selected");	
	//	} 
	//	
	//	@Override
	//	public void PrepopulateValue_CityState() throws Exception {
	//		createaccount.businessaddress2().click();
	//        Thread.sleep(10000);
	//		Assert.assertTrue(createaccount.businesscity().getAttribute("data-valid").contains("true"), "City field is auto populated");	
	//		Assert.assertTrue(createaccount.businessstate().getAttribute("data-valid").contains("true"), "State field is auto populated");	
	//
	//	} 
	//	
	//	@Override
	//	public void selectvalue_PractitonerCount() throws Exception {
	//		Select dropdown=new Select(createaccount.PractitionersCount());
	//		dropdown.selectByIndex(1);
	//		Thread.sleep(2000);
	//		Assert.assertTrue(createaccount.PractitionersCount().getAttribute("data-valid").contains("true"), "Prefix value is not selected");	
	//	} 
	//	
	//	
	//	@Override
	//	public void verifyError_Prefix(String text) throws Exception {
	//		
	//		String Curent_Text=createaccount.Err_Msg_Prefix().getText();	
	//		System.out.println(Curent_Text);
	//		Thread.sleep(5000);
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error");		
	//
	//	}
	//
	//	@Override
	//	public void VerifyHeader_CreateAcct() throws Exception {
	//		createaccount.CreateAccount_Header().isDisplayed();
	//	}
	//
	//
	//	@Override 
	//	public void Click_MedicalStudent_Radio() throws Exception {
	//		createaccount.MedicalStudent_RadioBtns().click();
	//		Thread.sleep(5000);
	//	}
	//
	//	@Override
	//	public void PrefixField_Empty() throws Exception {	
	//		Select dropdown=new Select(createaccount.Prefix_Field());
	//		dropdown.selectByIndex(2);
	//		dropdown.selectByIndex(0);
	//		createaccount.FirstName_Field().click();
	//		Thread.sleep(2000);
	//
	//	}
	//
	//	@Override
	//	public void Click_CreateAccout_Btn() throws Exception {
	//		explicitWait(createaccount.CreateAccount_Btn());
	//		createaccount.CreateAccount_Btn().click();
	//		verifyElementIsPresent(createaccount.CreateAccount_Header());
	//	}
	//
	//	@Override
	//	public void verifyError_FirstName(String text) throws Exception {	
	//		String Curent_Text=createaccount.Err_Msg_FirstName().getText();	
	//		System.out.println(Curent_Text);
	//		Thread.sleep(3000);
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//
	//	}
	//
	//	@Override
	//	public void LastName_Empty() throws Exception {
	//		createaccount.LastName_Field().sendKeys("");
	//		createaccount.FirstName_Field().click();
	//	}
	//
	//	@Override
	//	public void verifyError_LastName(String text) throws Exception {	
	//		String Curent_Text=createaccount.Err_Msg_LastName().getText();	
	//		System.out.println(Curent_Text);
	//		Thread.sleep(3000);
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//
	//	}
	//
	//	@Override
	//	public void FirstName_Empty() throws Exception {
	//		createaccount.FirstName_Field().sendKeys("");
	//		createaccount.LastName_Field().click();
	//	}
	//
	//	@Override
	//	public void verifyError_PostNominals(String text) throws Exception {	
	//		String Curent_Text=createaccount.Err_Msg_PostNominal().getText();	
	//		System.out.println(Curent_Text);
	//		Thread.sleep(3000);
	//
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//
	//	}
	//
	//	@Override
	//	public void PostNominals_Empty() throws Exception {
	//		createaccount.PostNominals_Field().sendKeys("");
	//		createaccount.LastName_Field().click();
	//	}
	//
	//
	//	@Override
	//	public void verifyError_PractitonerType(String text) throws Exception {	
	//		String Curent_Text=createaccount.Err_Msg_PractitionerType().getText();	
	//		System.out.println(Curent_Text);
	//		Thread.sleep(3000);
	//
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//
	//	}
	//
	//	@Override
	//	public void PractitonerType_Empty() throws Exception {
	//		createaccount.PractitionerType_Field().sendKeys("");
	//		createaccount.LastName_Field().click();
	//	}
	//
	//	@Override
	//	public void verifyError_PhoneNo(String text) throws Exception {	
	//		String Curent_Text=createaccount.Err_Msg_PhoneNumber().getText();	
	//		System.out.println(Curent_Text);
	//		Thread.sleep(3000);
	//
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//
	//	}
	//
	//	@Override
	//	public void PhoneNo_Empty() throws Exception {
	//		createaccount.PhoneNo_Field().sendKeys("");
	//		createaccount.LastName_Field().click();
	//	}
	//
	//
	//	@Override
	//	public void verifyError_EmailAdd(String text) throws Exception {	
	//		String Curent_Text=createaccount.Err_Msg_BusinessEmail().getText();	
	//		System.out.println(Curent_Text);
	//		Thread.sleep(3000);
	//
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//
	//	}
	//
	//	@Override
	//	public void EmailAddress_Empty() throws Exception {
	//		createaccount.EmailAddress().sendKeys("");
	//		createaccount.LastName_Field().click();
	//	}
	//
	//	@Override
	//	public void verifyError_LicenseNo(String text) throws Exception {	
	//		String Curent_Text=createaccount.Err_Msg_LicenseNumber().getText();	
	//		System.out.println(Curent_Text);
	//		Thread.sleep(4000);
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//
	//	}
	//
	//	@Override
	//	public void LicenseNo_Empty() throws Exception {
	//		createaccount.LicenseNo().sendKeys("");
	//		createaccount.EmailAddress().click();
	//	}
	//
	//	@Override
	//	public void verifyError_UsageTypes(String text) throws Exception {	
	//		String Curent_Text=createaccount.Err_Msg_UsageTypes().getText();	
	//		System.out.println(Curent_Text);
	//		Thread.sleep(3000);
	//
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//
	//	}
	//
	//	@Override
	//	public void UsageTypes_Empty() throws Exception {
	//		createaccount.UsageTypes().sendKeys("");
	//		createaccount.LastName_Field().click();
	//	}
	//
	//	@Override
	//	public void Enter_InvalidEmail(String text) throws Exception {
	//		createaccount.EmailAddress().getAttribute("placeholder");
	//		createaccount.EmailAddress().sendKeys(text);
	//
	//	}
	//
	//	@Override
	//	public void verifyError_InvalidEmail(String text) throws Exception {
	//		String Curent_Text=createaccount.Err_Msg_BusinessEmail().getText();	
	//		System.out.println(Curent_Text);
	//		Thread.sleep(3000);
	//
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//
	//	}
	//
	//	@Override
	//	public void Enter_PhoneNo(String text) throws Exception {
	//		createaccount.PhoneNo_Field().getAttribute("placeholder");
	//		createaccount.PhoneNo_Field().sendKeys(text);
	//
	//	}
	//
	//	@Override
	//	public void Enter_InvalidPhoneNo(String text) throws Exception {
	//		createaccount.PhoneNo_Field().getAttribute("placeholder");
	//		createaccount.PhoneNo_Field().sendKeys(text);
	//		driver.findElement(By.cssSelector(".default-device")).click();
	//
	//	}	
	//
	//
	//	@Override
	//	public void verifyError_InvalidPhoneNo(String text) throws Exception {
	//		String Curent_Text=createaccount.Err_Msg_PhoneNumber().getText();	
	//		System.out.println(Curent_Text);
	//		Thread.sleep(3000);
	//
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//
	//	}
	//
	//	@Override
	//	public void verifyError_Password(String text) throws Exception {
	//		String Curent_Text=createaccount.Err_Msg_Password().getText();	
	//		System.out.println(Curent_Text);
	//		Thread.sleep(2000);
	//
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//
	//	}
	//
	//	@Override
	//	public void verifyError_Re_EnterPassword(String text) throws Exception {
	//		String Curent_Text=createaccount.Err_Msg_ConfirmPassword().getText();	
	//		System.out.println(Curent_Text);
	//		Thread.sleep(2000);
	//
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//
	//	}
	//
	//	@Override
	//	public void verifyError_BussinessPhone(String text) throws Exception {
	//		String Curent_Text=createaccount.Err_Msg_BusinessPhone().getText();	
	//		System.out.println(Curent_Text);
	//		Thread.sleep(2000);
	//
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//
	//	}
	//
	//	@Override
	//	public void verifyError_College(String text) throws Exception {
	//		String Curent_Text=createaccount.Err_Msg_CollegeName().getText();	
	//		System.out.println(Curent_Text);
	//		Thread.sleep(2000);
	//
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//
	//	}
	//
	//	@Override
	//	public void verifyError_GraduationMonth(String text) throws Exception {
	//		String Curent_Text=createaccount.Err_Msg_GraduationMonths().getText();	
	//		System.out.println(Curent_Text);
	//		Thread.sleep(2000);
	//
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//
	//	}
	//
	//	@Override
	//	public void verifyError_GraduationYear(String text) throws Exception {
	//		String Curent_Text=createaccount.Err_Msg_GraduationYears().getText();	
	//		System.out.println(Curent_Text);
	//		Thread.sleep(4000);
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//
	//	}
	//
	//	@Override
	//	public void verifyError_Degree(String text) throws Exception {
	//		String Curent_Text=createaccount.Err_Msg_DegreeObtained().getText();	
	//		System.out.println(Curent_Text);
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//
	//	}
	//
	//	@Override
	//	public void verifyError_Program(String text) throws Exception {
	//		String Curent_Text=createaccount.Err_Msg_Program().getText();	
	//		System.out.println(Curent_Text);
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//
	//	}
	//
	//
	//	@Override
	//	public void verifyError_NoOfPractitioner(String text) throws Exception {
	//		String Curent_Text=createaccount.Err_Msg_TotalNumberofPractitioners().getText();	
	//		System.out.println(Curent_Text);
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//
	//	}
	//
	//	@Override
	//	public void verifyError_State(String text) throws Exception {
	//		String Curent_Text=createaccount.Err_Msg_State().getText();	
	//		System.out.println(Curent_Text);
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//
	//	}
	//
	//	@Override
	//	public void verifyError_City(String text) throws Exception {
	//		String Curent_Text=createaccount.Err_Msg_City().getText();	
	//		System.out.println(Curent_Text);
	//		Thread.sleep(3000);
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//	}
	//
	//	@Override
	//	public void verifyError_ZipCode(String text) throws Exception {
	//		String Curent_Text=createaccount.Err_Msg_Zipcode().getText();	
	//		System.out.println(Curent_Text);
	//		Thread.sleep(3000);
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//	}
	//
	//	@Override
	//	public void verifyError_BussinessName(String text) throws Exception {
	//		WaitTillElementExist(createaccount.Err_Msg_BussinessName(), 10);
	//		String Curent_Text=createaccount.Err_Msg_BussinessName().getText();	
	//		System.out.println(Curent_Text);
	//		Thread.sleep(3000);
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//	}
	//
	//	@Override
	//	public void verifyError_Address1(String text) throws Exception {
	//		String Curent_Text=createaccount.Err_Msg_Address1_HealthCare().getText();	
	//		System.out.println(Curent_Text);
	//		Thread.sleep(3000);
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//	}
	//
	//	@Override
	//	public void verifyError_Address2(String text) throws Exception {
	//		String Curent_Text=createaccount.Err_Msg_Address2_HealthCare().getText();	
	//		System.out.println(Curent_Text);
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//	}
	//
	//	@Override
	//	public void Password_Empty() throws Exception {
	//		createaccount.Password().sendKeys("");
	//		createaccount.ReEnter_Password().click();
	//	}
	//
	//	@Override
	//	public void Re_EnterPassword_Empty() throws Exception {
	//		createaccount.ReEnter_Password().sendKeys("");
	//		createaccount.Password().click();
	//	}
	//
	//	@Override
	//	public void BussinessPhone_Empty() throws Exception {
	//		createaccount.BusinessPhone().click();
	//		createaccount.PractitionersCount().click();
	//	}
	//
	//	@Override
	//	public void NoOfPractitioner_Empty() throws Exception {
	//		Select dropdown=new Select(createaccount.PractitionersCount());
	//		dropdown.selectByIndex(1);
	//		dropdown.selectByIndex(0);
	//		//createaccount.PractitionersCount().sendKeys("");
	//		//createaccount.businessaddress1().click();
	//	}
	//
	//	@Override
	//	public void State_Empty() throws Exception {
	//		createaccount.businessstate().click();
	//		Select dropdown=new Select(createaccount.businessstate());
	//		dropdown.selectByIndex(2);
	//		dropdown.selectByIndex(0);
	//
	//		//createaccount.businessstate().sendKeys("");
	//		//createaccount.businessaddress1().click();
	//	}
	//
	//	@Override
	//	public void City_Empty() throws Exception {
	//		createaccount.businesscity().sendKeys("");
	//		createaccount.businessstate().click();
	//	}
	//
	//	@Override
	//	public void ZipCode_Empty() throws Exception {
	//		createaccount.businesszipcode().sendKeys("");
	//		createaccount.businessaddress1().click();
	//	}
	//
	//	@Override
	//	public void Address1_Empty() throws Exception {
	//		createaccount.businessaddress1().sendKeys("");
	//		createaccount.businessaddress2().click();
	//	}
	//
	//	@Override
	//	public void Address2_Empty() throws Exception {
	//		createaccount.businessaddress2().sendKeys("");
	//		createaccount.businessaddress1().click();
	//	}
	//
	//	@Override
	//	public void BussinessName_Empty() throws Exception {
	//		createaccount.BusinessName().sendKeys("");
	//		createaccount.businessaddress1().click();
	//	}
	//
	//	@Override
	//	public void CollegeField_Empty() throws Exception {		
	//		createaccount.CollegeName_Field().sendKeys("");
	//		createaccount.GraduationMonth_Field().click();
	//	}
	//
	//	@Override
	//	public void GraduationMonthField_Empty() throws Exception {		
	//		Select dropdown=new Select(createaccount.GraduationMonth_Field());
	//		dropdown.selectByIndex(2);
	//		dropdown.selectByIndex(0);
	//	}
	//
	//	@Override
	//	public void GraduationYearField_Empty() throws Exception {		
	//		Select dropdown=new Select(createaccount.GraduationYear_Field());
	//		dropdown.selectByIndex(2);
	//		dropdown.selectByIndex(0);
	//	}
	//
	//	@Override
	//	public void DegreeField_Empty() throws Exception {	
	//		createaccount.DegreeObtained().sendKeys("");
	//		createaccount.CollegeName_Field().click();
	//
	//	}
	//
	//	@Override
	//	public void ProgramField_Empty() throws Exception {		
	//		createaccount.Program().sendKeys("");
	//		createaccount.CollegeName_Field().click();
	//
	//	}
	//
	//
	//	@Override
	//	public void EnterPassword(String Password , String ConfirmPassword) throws Exception {
	//		createaccount.Password().sendKeys(Password);
	//		createaccount.ReEnter_Password().sendKeys(ConfirmPassword);
	//
	//	}
	//
	//
	//
	//	@Override
	//	public void verifyError_pwd_DoNotMatch(String text) throws Exception {
	//		String Curent_Text=createaccount.Err_Msg_ConfirmPassword().getText();	
	//		System.out.println(Curent_Text);
	//		if(!Curent_Text.equalsIgnoreCase(text))
	//			throw new Exception("Error message does not match");		
	//
	//	}
	//
	//	@Override
	//	public void VerifySubmitBtn_Disabled() throws Exception {
	//		String CheckStatus=createaccount.SubmitBtn().getAttribute("disabled");
	//		if(CheckStatus.length() > 0)
	//			System.out.print("SUBMIT Button is disabled");
	//
	//	}
	//	
	//	@Override
	//	public void EnableCheckBox_ResalePolicy() throws Exception {
	//		Actions enablechkbox= new Actions(driver);
	//		enablechkbox.moveToElement(createaccount.ResalePolicy()).click().perform();
	//		String CheckStatus=createaccount.ResalePolicy().getAttribute("data-valid");
	//		if(CheckStatus.equalsIgnoreCase("true"))
	//			System.out.print("Resale Policy Button is enabled");
	//		
	//	}
	//	@Override
	//	public void EnableCheckBox_Subscribe() throws Exception {
	//		Actions enablechkbox= new Actions(driver);
	//		enablechkbox.moveToElement(createaccount.CheckSubscribe()).click().perform();
	//		String CheckStatus=createaccount.CheckSubscribe().getAttribute("data-valid");
	//		if(CheckStatus.equalsIgnoreCase("true"))
	//			System.out.print("Subscribe Button is enabled");
	//		
	//	}
	//	
	//	@Override
	//	public void Click_SubmitAcctRequest() throws Exception {
	//		createaccount.SubmitBtn().click();
	//		Thread.sleep(20000);
	//	
	//	}
	//	
	//	@Override
	//	public void Practitioner_Account_Created() throws Exception {
	//		explicitWait(createaccount.WelcomeUsername_header());
	//		Assert.assertTrue(createaccount.WelcomeUsername_header().isDisplayed(), "Account creation is not successful");
	//	}
	//	
	//	
	//
	//	@Override
	//	public void clickExpandIcon() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			List<WebElement> ho = howtoorder.clickExpandIcon();
	//			System.out.println(ho.size());
	//			for (int hto = 0; hto < ho.size() ; hto++) {
	//				if (hto==0) {
	//					ho.get(hto).click();
	//				}else if(hto==1){
	//					ho.get(hto).click();
	//				}else if(hto==2){
	//					ho.get(hto).click();
	//				}else if(hto==3){
	//					ho.get(hto).click();
	//				}
	//			}
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void Click_ShopAmazonNow_Btn() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			//highLightElement(howtoorder.ShopAmazonNow_Btn());
	//			ClickOnElement(howtoorder.ShopAmazonNow_Btn());
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void click_AmazonLink() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			Assert.assertTrue(verifyElementIsPresent(howtoorder.ShopAmazonNow_Btn()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void Verify_ShopAmazonNow_Btn() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			//highLightElement(howtoorder.ShopAmazonNow_Btn());
	//			ClickOnElement(howtoorder.ShopAmazonNow_Btn());
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void verify_AmazonTitle() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			//highLightElement(howtoorder.verifyAmazonTitle());
	//			Assert.assertTrue(verifyElementIsPresent(howtoorder.verifyAmazonTitle()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void howtoorderDesc() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			//highLightElement(howtoorder.howtoorderDesc());
	//			Assert.assertTrue(verifyElementIsPresent(howtoorder.howtoorderDesc()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void howtoorderTitle() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			Assert.assertTrue(verifyElementIsPresent(howtoorder.BannerImg_HowToOrderPg()));
	//			//highLightElement(howtoorder.howtoorderTitle());
	//			Assert.assertTrue(verifyElementIsPresent(howtoorder.howtoorderTitle()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//
	//	@Override
	//	public void verifyAmazonLogo() throws Exception {
	//		try {
	//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//			//highLightElement(howtoorder.verifyAmazonLogo());
	//			Assert.assertTrue(verifyElementIsPresent(howtoorder.verifyAmazonLogo()));
	//		} catch (Exception e) {
	//			throw new Exception("Unknown error while validating the css path", e);
	//		}
	//	}
	//	
	//	*/
	//
	//	/* Sprint 4ConShopping
	//		@Override
	//		public void cartpageClickOnConShopping() throws Exception {
	//			try {
	//				Thread.sleep(10000);
	//				ClickOnElement(csp.cartpageClickOnConShopping());
	//			} catch (Exception e) {
	//				throw new Exception("Unknown error while validating the css path", e);
	//			}
	//		}
	//
	//		// cartpageCheckOut
	//		@Override
	//		public void cartpageCheckOut() throws Exception {
	//			try {
	//				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//				ClickOnElement(cop.cartpageCheckOut());
	//			} catch (Exception e) {
	//				throw new Exception("Unknown error while validating the css path", e);
	//			}
	//		}
	//
	//	 */
	//	
	//	// Promo pop up
	public void removeFromFavoriteFunctionality() throws Exception {
		int navigationbar = pagination.NavigationBar().size();
		if (navigationbar>0)
		{
			Assert.assertTrue(verifyElementIsPresent(pagination.nextpagePLP()));
			javascriptExecutorClick(pagination.nextpagePLP());
			
		}
		int productcount=myaccount_favorite.productcounts_FavoritePage().size();
		javascriptExecutorClick(myaccount_favorite.removefavorite_links_FavoritePage().get(0));
		int updatedproductcount=myaccount_favorite.productcounts_FavoritePage().size();
		Assert.assertTrue(productdetailpage.FavoriteAlert().getText().equalsIgnoreCase("This product has been successfully removed from your favorites"),"Alert Message For Removing Products From Favorite Page Is Not Displayed");	

	}
	
	public void verifyFavoritePage_HasAtleastOneProduct() throws Exception {
		int productcount=myaccount_favorite.productcounts_FavoritePage().size();
		if(productcount<0)
		{
			javascriptExecutorClick(productlisting.productsmenu_header());
			holdAndClick(productlisting.productsmenu_header(),productlisting.viewallproducts_header());		
            javascriptExecutorClick(productlisting.selectaproductbyTitle());
			verifyElementIsPresent(productdetailpage.clickOnFavorites());
			javascriptExecutorClick(productdetailpage.clickOnFavorites());
			Assert.assertTrue(productdetailpage.FavoriteAlert().getText().equalsIgnoreCase("Added to favorites"),"Added to Favorites alert is not displayed ");	
			waitVisibilityOfElement(myaccount.chevron_username());
			ClickOnElement(myaccount.chevron_username());
			Assert.assertTrue(verifyElementIsPresent(myaccount.favorites_menu()));	
			ClickOnElement(myaccount.favorites_menu());
			waitForPageLoaded();
		    productcount=myaccount_favorite.productcounts_FavoritePage().size();
        	Assert.assertTrue(productcount>0,"Product Added To Favorite Is Not Displayed In Favorite Page");
			
		}	
	}
	
	public void addProductExceedLimit_FavoritePage() throws Exception {
		ClickOnElement(myaccount.chevron_username());
		Assert.assertTrue(verifyElementIsPresent(myaccount.favorites_menu()));
	    ClickOnElement(myaccount.favorites_menu());	
		verifyExtendedUrl("favorites");

		int productcount=myaccount_favorite.productcounts_FavoritePage().size();
		if(productcount==24)
		{
			String pagetitle=productlisting.selectaproductbyTitle().getText();
			javascriptExecutorClick(productlisting.productsmenu_header());
			holdAndClick(productlisting.productsmenu_header(),productlisting.viewallproducts_header());		
			Assert.assertTrue(verifyElementIsPresent(pagination.nextpagePLP()));
			javascriptExecutorClick(pagination.nextpagePLP());
			String updatedpagetitle=productlisting.selectaproductbyTitle().getText();
            Assert.assertNotEquals(productlisting, pagetitle);
			
            javascriptExecutorClick(productlisting.selectaproductbyTitle());
            
			int valuechecks=productdetailpage.Fav_Marked().size();
            if(valuechecks==0)
			 {
				String product_title_PDP=productdetailpage.productTitle_PDP().getText();
				verifyElementIsPresent(productdetailpage.clickOnFavorites());
				javascriptExecutorClick(productdetailpage.clickOnFavorites());
				Assert.assertTrue(productdetailpage.FavoriteAlert().getText().equalsIgnoreCase("Added to favorites"),"Added to Favorites alert is not displayed ");	
				
				waitVisibilityOfElement(myaccount.chevron_username());
				ClickOnElement(myaccount.chevron_username());
				Assert.assertTrue(verifyElementIsPresent(myaccount.favorites_menu()));	
				ClickOnElement(myaccount.favorites_menu());
				waitForPageLoaded();
				
				List<WebElement> productlist_favoritepage=myaccount_favorite.productTitleList();
				for (int j=0;j<productlist_favoritepage.size();j++)
				{
					scrollToElement(productlist_favoritepage.get(j));
					if(productlist_favoritepage.get(j).getText().contains("product_title_PDP"))
					{
						break;
					}		
				}
				Assert.assertTrue(verifyElementIsPresent(pagination.nextpagePLP()));
				javascriptExecutorClick(pagination.nextpagePLP());
				for (int j=0;j<productlist_favoritepage.size();j++)
				{
					scrollToElement(productlist_favoritepage.get(j));
					if(productlist_favoritepage.get(j).getText().contains("product_title_PDP"))
					{
						break;
					}		
				}
				
			 }
		}
		
	}
	
	public void verifyProductList_FavoritePage() throws Exception {
		int productcount=myaccount_favorite.productcounts_FavoritePage().size();
		if(productcount<25)
	{
			javascriptExecutorClick(productlisting.productsmenu_header());
			holdAndClick(productlisting.productsmenu_header(),productlisting.viewallproducts_header());
			List<WebElement> productlist_plp=productlisting.productTitleList();
			for (int i=0;i<productlist_plp.size();i++)
		{
				productlist_plp=productlisting.productTitleList();
				waitForPageLoaded();
				verifyElementIsPresent(productlist_plp.get(i));
				clickOnList_Index(productlist_plp,i);
				int valuechecks=productdetailpage.Fav_Marked().size();
				
				if(valuechecks==0)
				 {
					String product_title_PDP=productdetailpage.productTitle_PDP().getText();
					verifyElementIsPresent(productdetailpage.clickOnFavorites());
					javascriptExecutorClick(productdetailpage.clickOnFavorites());
					Assert.assertTrue(productdetailpage.FavoriteAlert().getText().equalsIgnoreCase("Added to favorites"),"Added to Favorites alert is not displayed ");	
					
					waitVisibilityOfElement(myaccount.chevron_username());
					ClickOnElement(myaccount.chevron_username());
					Assert.assertTrue(verifyElementIsPresent(myaccount.favorites_menu()));	
					ClickOnElement(myaccount.favorites_menu());
					waitForPageLoaded();
					
					List<WebElement> productlist_favoritepage=myaccount_favorite.productTitleList();
					for (int j=0;j<productlist_favoritepage.size();j++)
					{
						scrollToElement(productlist_favoritepage.get(j));
						if(productlist_favoritepage.get(j).getText().contains("product_title_PDP"))
						{
							break;
						}		
					}
						if(productcount==24)
							break;
					javascriptExecutorClick(productlisting.productsmenu_header());
					holdAndClick(productlisting.productsmenu_header(),productlisting.viewallproducts_header());				
			    	}	
				
				else
				{
				driver.navigate().back();
				waitForPageLoaded();
				}
		  }							
	 }		
}			
			
			
			

	public void verifyWelcomeScreen_FavoritePage() throws Exception {
		
		verifyElementIsPresent(myaccount_favorite.WelcomeMessage_Header_FavoritePage());
		verifyElementIsPresent(myaccount_favorite.display_username_FavoritePage());
		String username_favoritepage=myaccount_favorite.WelcomeMessage_Header_FavoritePage().getText();
		Assert.assertTrue(username_favoritepage.contains("Welcome")," Welcome Message is not displayed in Header - Favorite Page");
		Assert.assertTrue(username_favoritepage.contains(myaccount_favorite.display_username_FavoritePage().getText())," User-Name is not displayed in Header - Favorite Page");

	}
	
	
	public void favoritefunctionality_PDP() throws Exception
	{
		int valuechecks=productdetailpage.Fav_Marked().size();
		if(valuechecks>0)
		{
			clickButtonWithOutScroll(productdetailpage.Fav_Marked().get(0));
			Assert.assertTrue(productdetailpage.FavoriteAlert().getText().equalsIgnoreCase("Removed from favorites"),"Removed from Favorites alert is not displayed ");

	     }
		else
		{
			clickButtonWithOutScroll(productdetailpage.clickOnFavorites());
			Assert.assertTrue(productdetailpage.FavoriteAlert().getText().equalsIgnoreCase("Added to favorites"),"Added to Favorites alert is not displayed ");
			
		}
	
	}
	public void question_answerAccordian_HowToOrder() throws Exception {
		List<WebElement> expand=howtoorder.clickExpandIcon_HowToOrder();
		List<WebElement> collapse=howtoorder.verifyCollapseIcon_HowToOrder();
		for (WebElement click_Expand:expand)
		{
			scrollToElement(click_Expand);
			Assert.assertTrue(verifyElementIsPresent(click_Expand));
			javascriptExecutorClick(click_Expand);
			for(WebElement click_Collapse : collapse)
			{
			    waitFor(1000);
				verifyElementIsPresent(click_Collapse);
				break;		
			}
			javascriptExecutorClick(click_Expand);
		}

	}

	public void multiplequestion_answerAccordian_HowToOrder() throws Exception {
		List<WebElement> expand=howtoorder.clickExpandIcon_HowToOrder();
		List<WebElement> collapse=howtoorder.verifyCollapseIcon_HowToOrder();
		for (WebElement click_Expand:expand)
		{
			scrollToElement(click_Expand);
			Assert.assertTrue(verifyElementIsPresent(click_Expand));
			javascriptExecutorClick(click_Expand);
			for(WebElement click_Collapse : collapse)
			{
			    waitFor(1000);
				verifyElementIsPresent(click_Collapse);
				break;		
			}
			
		}

	}


	public void verifyAutopopulation_ShippingAddress() throws Exception {
		selectTabKey();
		waitForPageLoaded();
		Assert.assertTrue(verifyElementIsPresent(shippinginformation.cityAutoPopulated_shippingAddress()), "City field is not auto populated");
		Assert.assertTrue(verifyElementIsPresent(shippinginformation.stateAutoPopulated_shippingAddress()), "State field is not auto populated");

	}

	
	public String verifyDefaultValueDropdown(WebElement ele)  throws Exception {
		
		Select select = new Select(ele);
		WebElement option = select.getFirstSelectedOption();
		String defaultItem = option.getText();
		return defaultItem;
	}
	
	public void verifyAutopopulation_Practitioner() throws Exception {
		Select select = new Select(createaccount.PractitionerType_Field());
		WebElement option = select.getFirstSelectedOption();
		String defaultItem = option.getText();
		System.out.println(defaultItem );
	
	}
	
	public void verifyAutopopulation_City_State() throws Exception {
		Select select = new Select(createaccount.businessstate());
		WebElement option = select.getFirstSelectedOption();
		String defaultItem = option.getText();
		System.out.println(defaultItem );
		
//		waitForPageLoaded();
//		selectTabKey();
//		waitFor(2000);	
//		getAndCheckAttributeValue(createaccount.businessstate(),"data-valid","true");
//		getAndCheckAttributeValue(createaccount.businesscity(),"data-valid","true");

	}


	//Create Account  
	public void verifyRadioBtn() throws Exception {
		if(!createaccount.HealthCare_RadioBtns().isEnabled())
		{
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",createaccount.HealthCare_RadioBtns());
		}
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",createaccount.MedicalStudent_RadioBtns());
		if((!createaccount.HealthCare_RadioBtns().isEnabled()) && (!createaccount.MedicalStudent_RadioBtns().isEnabled()))
			throw new Exception ("Error");
	}


	public void checkMaxLength_TextBox_HearAboutUsTxtBox() throws Exception {
		int text;
		for(int i=0;i<51;i++)
		{
			enterText(createaccount.HearAboutUsTxtBox(),"Naturesway");
			text=createaccount.HearAboutUsTxtBox().getText().length();
			if(text>500)
				throw new Exception("Error");
		}

	}

	public void checkMaxLength_TextBox_AnyQnsTxtBox() throws Exception {
		int text;
		for(int i=0;i<51;i++)
		{
			enterText(createaccount.AnyQnsTxtBox(),"Naturesway");
			text=createaccount.AnyQnsTxtBox().getText().length();
			if(text>500)
				throw new Exception("Error");
		}

	}

	//User Account

	public void verifyUserName_Header() throws Exception {
		Assert.assertTrue(verifyElementIsPresent(myaccount.chevron_username()));
		ClickOnElement(myaccount.chevron_username());
		Assert.assertTrue(verifyElementIsPresent(myaccount.accountDetails_menu()));
        ClickOnElement(myaccount.accountDetails_menu());	
		javascriptExecutorClick(myaccount.EditLink_PersonalInfoaccountDetail());
		String LastName=getAttributeValue(myaccount.LastName_EditInfo(),"value");

		String username=myaccount.username_header().getText();	
		if((username.contains("Mr")) || (username.contains("Mrs")) || (username.contains("Dr")) || (username.contains("Miss")) || (username.contains("Ms")))
		{
			Assert.assertTrue(username.startsWith("Welcome"), "Welcome Message is not displayed in User Name");
			if(!username.contains(LastName))
				Assert.fail("Last Name is Not Displayed in Global Header");
		}
		else 
			Assert.fail("Prefix is not displayed in User Name - Header");
	}


	public void EditInformation_AccountDetails() throws Exception {
		waitForPageLoaded();
		 DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		 Date date = new Date();
		String date1= dateFormat.format(date);
		String result = date1.replaceAll("[:]","");
		Actions move=new Actions(driver);
		String fnameValue= "first"+result;
		String lnameValue= "last"+result;
		String postnominalnameValue= "pn";
		String phnoValue= result +"0000";

		verifyElementIsPresent(myaccount.EditLink_PersonalInfoaccountDetail());
		clickButtonWithOutScroll(myaccount.EditLink_PersonalInfoaccountDetail());
		String Prefix=getAttributeValue(myaccount.Prefix_EditInfo(),"value");
		String FirstName=getAttributeValue(myaccount.FirstName_EditInfo(),"value");
		String LastName=getAttributeValue(myaccount.LastName_EditInfo(),"value");
		String PostNominals=getAttributeValue(myaccount.PostNominals_EditInfo(),"value");
		String Phone=getAttributeValue(myaccount.PersonalPhNo_EditInfo(),"value");
		
		myaccount.FirstName_EditInfo().clear();
		enterText(myaccount.FirstName_EditInfo(),fnameValue);
		myaccount.LastName_EditInfo().clear();
		enterText(myaccount.LastName_EditInfo(),lnameValue);
		myaccount.PostNominals_EditInfo().clear();
		enterText(myaccount.PostNominals_EditInfo(),postnominalnameValue);
		myaccount.PersonalPhNo_EditInfo().clear();
		enterText(myaccount.PersonalPhNo_EditInfo(),phnoValue);
		String attributevalue=getAttributeValue(myaccount.Email_EditInfo(),"disabled");
		clickButtonWithOutScroll(myaccount.saveButton_EditInfo());
		verifyElementIsNotDisplayed(myaccount.EditPersonalInfoSection());
		List<WebElement> Title_EditInfo=myaccount.Title_EditInformation();
		for(WebElement checktitle :Title_EditInfo)
	{
		if(checktitle.getText().contains("Name"))
		 {
			String checkvalue=myaccount.UpdatedName_EditInfo().getText();
			if((checkvalue.contains(fnameValue)) && (checkvalue.contains(lnameValue)) && (checkvalue.contains(PostNominals)))
			{
				System.out.print("Updated Names Are Available In Edit Information Section");
			}
			else 
				Assert.fail("Updated Name is not available");
			    break;    
		   }
		if(checktitle.getText().contains("Phone Number:"))
		{
			String checkvalue=myaccount.UpdatedPhNo_EditInfo().getText();
			Assert.assertTrue(checkvalue==phnoValue);
			break;
			
		}
	 }
 }


	public void dropdownmenus_Useracct(DataTable submenu) throws Exception, IOException, ParseException {	
		for (Map<String, String> row : submenu.asMaps(String.class, String.class)) {
			String tableElement = row.get("Drop Down Menu");
			switch (tableElement) {
			case "Account Details":
				ClickOnElement(myaccount.chevron_username());
				verifyElementIsPresent(myaccount.accountDetails_menu());
				Thread.sleep(10000);
				continue;
			case "My Orders":
				Thread.sleep(10000);
				verifyElementIsPresent(myaccount.myorders_menu());
				continue;
			case "Payment Information":
				Thread.sleep(10000);
				verifyElementIsPresent(myaccount.paymentmethods_menu());
				continue;
			case "Favorites":
				Thread.sleep(10000);
				verifyElementIsPresent(myaccount.favorites_menu());
				continue;
			case "Sign Out":
				Thread.sleep(10000);
				verifyElementIsPresent(myaccount.signout_menu());
				continue;
			case "Shipping Information":
				Thread.sleep(10000);
				verifyElementIsPresent(myaccount.shippingaddresses_menu());			
				continue;				
			case "Practitioner Directory":
				Thread.sleep(10000);
				verifyElementIsPresent(myaccount.findAPractitionerProfile_menu());
				break;				
			default:
				Assert.fail("There are no sub menus available under account details section");
				break;
			}
		}
	}	


	//Interactions and Depletions
	public void ExpandInteraction_listed() throws Exception {
		scrollToElement(interactiondepletion.ClickInteraction_Avl());
		javascriptExecutorClick(interactiondepletion.ClickInteraction_Avl());		
		driver.switchTo().frame("hni_ContentFrame0");
		List<WebElement> expand=interactiondepletion.ExpandInteraction();
		for(WebElement expanding :expand)    
		{
			scrollToElement(expanding);
			javascriptExecutorClick(expanding);	
			//scrollToElement(interactiondepletion.ExpandedContent());
			verifyElementIsPresent(interactiondepletion.ExpandedContent());
			javascriptExecutorClick(expanding);
		}
		driver.switchTo().parentFrame();
	}

	public void NagiateToInteraction() throws Exception{
		scrollToElement(interactiondepletion.Interaction_Depletion_Menu(), driver);
		Assert.assertTrue(verifyElementIsPresent(interactiondepletion.Interaction_Depletion_Menu()));
		ClickOnElement(interactiondepletion.Interaction_Depletion_Menu());	
		Thread.sleep(20000);
		List<WebElement> Title_SelectedMenu=interactiondepletion.MenuTitle_Submenus();
		for(WebElement title_menu :Title_SelectedMenu)
			try {
				if(title_menu.getText().contains("Interaction"))
					System.out.println(title_menu.getText());
			}
		catch(Exception e)
		{
			throw new Exception("Title of the selected menu does not contains Interaction & Depletions");
		}		
	}

	public void verify_LeftpaneMenus(DataTable leftpanemenus) throws Exception {	
		try {
			for (Map<String, String> row : leftpanemenus.asMaps(String.class, String.class)) {
				String tableElement = row.get("Menus In PDP page");
				switch (tableElement) {
				case "Details":
					Assert.assertTrue(verifyElementIsPresent(productdetailpage.DetailsMenu_LeftPane()));
					javascriptExecutorClick(productdetailpage.DetailsMenu_LeftPane());
					Assert.assertTrue(verifyElementIsPresent(productdetailpage.Detailsofproduct_LeftPane()));
					Thread.sleep(1000);
					continue;					
				case "Ingredients":
					Assert.assertTrue(verifyElementIsPresent(productdetailpage.IngredientsMenu_LeftPane()));
					javascriptExecutorClick(productdetailpage.IngredientsMenu_LeftPane());
					Assert.assertTrue(verifyTextIsDisplayed(productdetailpage.Ingredientstitle_LeftPane()));
					Assert.assertTrue(verifyElementIsPresent(productdetailpage.Ingredientsofproduct_LeftPane()));
                	Thread.sleep(1000);
					continue;						
				case "Directions":
					Assert.assertTrue(verifyElementIsPresent(productdetailpage.DirectionsMenu_LeftPane()));
					javascriptExecutorClick(productdetailpage.DirectionsMenu_LeftPane());
					Assert.assertTrue(verifyTextIsDisplayed(productdetailpage.DirectionsorDosageTitle_LeftPane()));
					Assert.assertTrue(verifyTextIsDisplayed(productdetailpage.DirectionsorDosageMessage_LeftPane()));
					Assert.assertTrue(verifyTextIsDisplayed(productdetailpage.WarningTitle_LeftPane()));
					Assert.assertTrue(verifyTextIsDisplayed(productdetailpage.WarningMessage_LeftPane()));
					Thread.sleep(1000);
					continue;						
				case "Interactions":
					Assert.assertTrue(verifyElementIsPresent(productdetailpage.Interactions_DepletionsMenu_LeftPane()));
					clickButtonWithOutScroll(productdetailpage.Interactions_DepletionsMenu_LeftPane());
					Assert.assertTrue(verifyTextIsDisplayed(productdetailpage.Interactionstitle_LeftPane()));
				    int interactions_section=productdetailpage.InteractionsdetailsEmpty_LeftPane().size();
				    if(interactions_section>0)
				    {
						Assert.assertTrue(verifyElementIsPresent(productdetailpage.InteractionsdetailsEmpty_LeftPane().get(0)));
				    }
					Thread.sleep(1000);
					continue;						
				case "Patient Support":
					Assert.assertTrue(verifyElementIsPresent(productdetailpage.PatientSupportMenu_LeftPane()));
					clickButtonWithOutScroll(productdetailpage.PatientSupportMenu_LeftPane());
					Assert.assertTrue(verifyTextIsDisplayed(productdetailpage.PatientSupportMenu_Title()));
				    int patientsupport_section=productdetailpage.PatientSupportEmpty_LeftPane().size();
				    if(patientsupport_section>0)
				    {
						Assert.assertTrue(verifyElementIsPresent(productdetailpage.PatientSupportEmpty_LeftPane().get(0)));
				    }	
					Thread.sleep(1000);
					continue;						
				case "Faqs":
					Assert.assertTrue(verifyElementIsPresent(productdetailpage.FAQsMenu_LeftPane()));
					clickButtonWithOutScroll(productdetailpage.FAQsMenu_LeftPane());
					Assert.assertTrue(verifyTextIsDisplayed(productdetailpage.Title_FAQsMenu_LeftPane()));
					Assert.assertTrue(verifyElementIsDisplayed(productdetailpage.QuestionAnswerSection_FAQSMenu()),"FAQs Question & Answer Section are not displayed");
					Thread.sleep(1000);
					continue;
				default:
					Assert.fail("There is no menus available in leftpane PDP page");
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();}	
	}


	public void navigatetourl()
	{
		driver.navigate().to("https://qa.sitecore.naturesway.com/products/adrenal-complex");
	}
	public void verifyproductorder_shoppingcart() throws Exception {
		String firstproductTitle_shoppingcart=cartdrawer.firstproductTitle_shoppingcart().get(0).getText();
		Assert.assertTrue(firstproductTitle_shoppingcart==DRUG,"Last Added product is not displayed in Shopping Cart");

	}

	public void verify_CartIcon_header_Empty() throws Exception {
		if(cartdrawer.emptycart_header().size()>0)
		{
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",cartdrawer.cartIcon_header());
			 waitForPageLoaded();
			List<WebElement> productlist_shoppingcart = cartdrawer.shoppingcartlist_cartpage();
			List<WebElement> delete_link_shoppingcart = cartdrawer.DeleteLink_ShoppingCart();
			int count=productlist_shoppingcart.size();
			if(count>0)   
			{
			 for(WebElement delete :delete_link_shoppingcart)
				{   count=cartdrawer.shoppingcartlist_cartpage().size();
		
					List<WebElement> producttitle_Shoppingcart = cartdrawer.ProductTitle_ShoppingCart();
			for (int i=0;i<producttitle_Shoppingcart.size();i++)
					{
						String title_ToBeDeleted = producttitle_Shoppingcart.get(i).getText();
						((JavascriptExecutor) driver).executeScript("arguments[0].click();",cartdrawer.DeleteLinkAProduct_ShoppingCart());
						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.shopping-cart-lines .cart-item-info .cart-item-title")));

					//	verifyElementIsNotDisplayed(producttitle_Shoppingcart.get(i));	
						break;
					}
					int updatedcount=cartdrawer.shoppingcartlist_cartpage().size();
					Assert.assertTrue(updatedcount==count-1, "Product is not deleted from Shopping cart section"); 
				}
			} 
		}
	}		

		public void verifysaveforlatersection_Empty() throws Exception {
			List<WebElement> productslist_saveforlatersection =cartdrawer.saveforlater_productlist_cartpage();
			List<WebElement> deletelink_saveforlatersection =cartdrawer.DeleteLink_saveforlater();

			if(productslist_saveforlatersection.size()>0)
			{
				for (WebElement deleteALink: deletelink_saveforlatersection)
				{
					int count=cartdrawer.saveforlater_productlist_cartpage().size();
					List<WebElement> producttitle_Saveforlater = cartdrawer.ProductTitle_SaveforLater();
					waitFor(2000);
					for(WebElement title:producttitle_Saveforlater)
					{
						String title_ToBeDeleted=title.getText();
						((JavascriptExecutor) driver).executeScript("arguments[0].click();",cartdrawer.DeleteLinkAProduct_SaveforLater());
						//verifyElementIsNotDisplayed(title);
						 wait.until(ExpectedConditions.invisibilityOfElementLocated((By.cssSelector("div.saved-shopping-cart-wrapper .cart-item-info .cart-item-title"))));  		
						break;
					}
					int updatedcount=cartdrawer.saveforlater_productlist_cartpage().size();
					Assert.assertTrue(updatedcount==count-1, "Product is not deleted from Save for Later Section"); 
			   }
			}
			 
	  }
		
	public void verifyProductComponents_Shoppingcart() throws Exception {

		List<WebElement> product_list_shoppingcart = cartdrawer.shoppingcart_productlist_cartpage();
		List<WebElement> productimage_shoppingcart = cartdrawer.firstproductImage_shoppingcart();
		List<WebElement> producttitle_shoppingcart = cartdrawer.firstproductTitle_shoppingcart();
		List<WebElement> productSKU_shoppingcart = cartdrawer.firstproductSKU_shoppingcart();
		List<WebElement> productSize_shoppingcart = cartdrawer.firstproductSize_shoppingcart();
		List<WebElement> productQTY_shoppingcart = cartdrawer.firstproductQty_shoppingcart();
		List<WebElement> productunits_shoppingcart = cartdrawer.firstproductUnits_shoppingcart();
		List<WebElement> productflavor_shoppingcart = cartdrawer.firstproductFlavor_shoppingcart();
		List<WebElement> productactualprice_shoppingcart = cartdrawer.firstproduct_actualpriceOfProduct();
		List<WebElement> producttotalsum_shoppingcart = cartdrawer.firstproductTotalSum_shoppingcart();
		List<WebElement> productdeletelink_shoppingcart = cartdrawer.DeleteLink_ShoppingCart();
		List<WebElement> productsaveforlaterlink_shoppingcart = cartdrawer.SaveForLaterLink_ShoppingCart();
		System.out.println(product_list_shoppingcart.size());
        waitForPageLoaded();
		for(int i=0;i<product_list_shoppingcart.size();i++)
		{
			Assert.assertTrue(verifyElementPresent_Index(productimage_shoppingcart,i));
			Assert.assertTrue(verifyTextIsPresent_Index(producttitle_shoppingcart,i));
			Assert.assertTrue(verifyTextIsPresent_Index(productSKU_shoppingcart,i));
			Assert.assertTrue(verifyTextIsPresent_Index(productSize_shoppingcart,i));
			Assert.assertTrue(verifyElementPresent_Index(productQTY_shoppingcart,i));
			Assert.assertTrue(verifyElementPresent_Index(productunits_shoppingcart,i));
			Assert.assertTrue(verifyTextIsPresent_Index(productactualprice_shoppingcart,i));
			Assert.assertTrue(verifyTextIsPresent_Index(producttotalsum_shoppingcart,i));
			Assert.assertTrue(verifyElementPresent_Index(productdeletelink_shoppingcart,i));
			Assert.assertTrue(verifyElementPresent_Index(productsaveforlaterlink_shoppingcart,i));
		}

	}

	public void verifyProductComponents_saveforlater() throws Exception {
		List<WebElement> product_list_saveforlater = cartdrawer.saveforlater_List_cartpage();
		List<WebElement> productimage_saveforlater = cartdrawer.saveforlaterfirstproductImage();
		List<WebElement> producttitle_saveforlater = cartdrawer.saveforlaterfirstproductTitle();
		List<WebElement> productSKU_saveforlater = cartdrawer.saveforlaterfirstproductSKU();
		List<WebElement> productSize_saveforlater = cartdrawer.saveforlaterfirstproductSize();
		List<WebElement> productQTY_saveforlater = cartdrawer.saveforlaterfirstproductQty();
		List<WebElement> productunits_saveforlater = cartdrawer.saveforlaterfirstproductUnits();
		List<WebElement> productactualprice_saveforlater = cartdrawer.saveforlaterfirstproduct_actualpriceOfProduct();
		List<WebElement> producttotalsum_saveforlater = cartdrawer.saveforlaterfirstproductTotalSum_shoppingcart();
		List<WebElement> productdeletelink_saveforlater = cartdrawer.DeleteLink_saveforlater();
		List<WebElement> productaddtocartlink_saveforlater = cartdrawer.addtocartlink_saveforlatersection();
		System.out.println(product_list_saveforlater.size());

		for(int i=0;i<product_list_saveforlater.size();i++)
		{
			Assert.assertTrue(verifyElementPresent_Index(productimage_saveforlater,i));
			Assert.assertTrue(verifyTextIsPresent_Index(producttitle_saveforlater,i));
			Assert.assertTrue(verifyTextIsPresent_Index(productSKU_saveforlater,i));
			//  verifyTextIsPresent_Index(productflavor_saveforlater,i);
			Assert.assertTrue(verifyTextIsPresent_Index(productSize_saveforlater,i));
			Assert.assertTrue(verifyElementPresent_Index(productQTY_saveforlater,i));
			Assert.assertTrue(verifyElementPresent_Index(productunits_saveforlater,i));
			Assert.assertTrue(verifyTextIsPresent_Index(productactualprice_saveforlater,i));
			Assert.assertTrue(verifyTextIsPresent_Index(producttotalsum_saveforlater,i));
			Assert.assertTrue(verifyElementPresent_Index(productdeletelink_saveforlater,i));
			Assert.assertTrue(verifyElementPresent_Index(productaddtocartlink_saveforlater,i));
		}

	}

	public void VerifyProducts_moved_FromSaveforLater() throws Exception {
		 waitForPageLoaded();
		 waitTillTextExist(DRUG,cartdrawer.firstproductTitle_shoppingcart());
		
	}

	public void verifyAddedProduct_DisplayedFirst() throws Exception {
		waitForPageLoaded();
		waitTillElementExist(cartdrawer.firstproductTitle_shoppingcart().get(0));
		String firstproductTitle_shoppingcart=cartdrawer.firstproductTitle_shoppingcart().get(0).getText();
		Assert.assertEquals(firstproductTitle_shoppingcart, DRUG);

	}

	public void verifyAddedProduct_DisplayedFirst_SaveforLatersection() throws Exception {
		List<WebElement> productlist_Saveforlatersection = cartdrawer.saveforlater_productlist_cartpage();	
		for (int j=0;j<productlist_Saveforlatersection.size();j++)
		{
          String title=cartdrawer.ProductTitle_SaveforLater().get(j).getText();
			if(title==DRUG)
				break;
		}
	}

	public void verifyCartCount_and_AddtoCartButton() throws Exception {	
		    List<WebElement> CartCount_Header = cartdrawer.CartCount_Header();
		    int cartcount=cartdrawer.CartCount_Header().size();
		    Assert.assertTrue(productdetailpage_3.AddToCartButton().isEnabled());
		    clickButtonWithOutScroll(productdetailpage_3.AddToCartButton());
			waitTillElementExist(cartdrawer.CartDrawerScreen());
			String cartcount_updated=cartdrawer.cartcount_header().getText();
			int count_updated = Integer.parseInt(cartcount_updated);
			Assert.assertTrue(count_updated==cartcount+1, "Cart Count in header is updated on adding products");
	
	}

	public void saveforlaterFunctionality_cartPage() throws Exception {
		waitTillElementExist(cartdrawer.cartpage());
		waitForPageLoaded();
		int productlist_count=cartdrawer.shoppingcart_productlist_cartpage().size();
		String sub_total=cartdrawer.Subtotal_itemsCount_CartPage().getText();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",cartdrawer.SaveForLaterLinkAProduct_ShoppingCart());
		Thread.sleep(1000);
		 wait.until(ExpectedConditions.invisibilityOfElementLocated((By.cssSelector("div.shopping-cart-lines .cart-item-info .cart-item-title"))));  		

		//verifyElementIsNotDisplayed(cartdrawer.ProductTitle_ShoppingCart().get(0));	
		String updated_sub_total=cartdrawer.Subtotal_itemsCount_CartPage().getText();
		int productlist_updated=cartdrawer.shoppingcart_productlist_cartpage().size();
		Assert.assertTrue(productlist_updated==productlist_count-1, "Save for Later functionality is not working");
		boolean ProductTitle_Saveforlater=cartdrawer.saveforlaterfirstproductTitle().get(0).getText().contains(DRUG);
		Assert.assertTrue(ProductTitle_Saveforlater=true, "Recently added product is not displayed in save for later section");
		Assert.assertNotEquals(sub_total, updated_sub_total, "Subtotal is not changed in Shopping cart when product is moved to save for later section");

	}


	public void saveforlater_AfterReLogin() throws Exception {
		boolean ProductTitle_Saveforlater=cartdrawer.saveforlaterfirstproductTitle().get(0).getText().contains(DRUG);
		Assert.assertTrue(ProductTitle_Saveforlater=true, "Recently added product is not displayed in save for later section after re-login");


	}
	public void deleteproduct_cartdrawer() throws Exception {
		List<WebElement> productlist = cartdrawer.productslist_cartdrawer();
		int count=cartdrawer.productslist_cartdrawer().size();
		verifyElementIsDisplayed(cartdrawer.delete_cartdrawer());
		clickOnLinkWithOutScroll(cartdrawer.delete_cartdrawer());	
		int count_updated=cartdrawer.productslist_cartdrawer().size();	
		Assert.assertTrue(count_updated==count-1,"Delete is not working in cart drawer screen");
	}	

	public void clickAddToCartLink_Saveforlatersection() throws Exception {
		WebElement clickElement=cartdrawer.addtocartlink_saveforlatersection().get(0);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",clickElement);
		 wait.until(ExpectedConditions.invisibilityOfElementLocated((By.cssSelector("div.saved-shopping-cart-wrapper div.saved-btn a:nth-child(2) span"))));  		

		//verifyElementIsNotDisplayed(clickElement);
	}

	public void DeleteFunctionality_ShoppingCart_cartPage() throws Exception {	
		String count_minicart=cartdrawer.emptycart_header().get(0).getText();
		String Product_Title=cartdrawer.firstproductTitle_shoppingcart().get(0).getText();
		int count_minicartIcon=Integer.parseInt(count_minicart);
		System.out.print(count_minicartIcon);
		int product_count=cartdrawer.shoppingcartlist_cartpage().size();
		String subtotal_Count=cartdrawer.Subtotal_itemsCount_CartPage().getText();	
		String Subtotal_Price=cartdrawer.Subtotal_PriceDetails_CartPage().getText();
		List<WebElement> producttitle_Shoppingcart = cartdrawer.ProductTitle_ShoppingCart();
        for (WebElement title : producttitle_Shoppingcart)
        {
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",cartdrawer.DeleteLinkAProduct_ShoppingCart());
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.shopping-cart-lines .cart-item-info .cart-item-title")));
		//verifyElementIsNotDisplayed(title);			
		break;
        }
		int updated_count =cartdrawer.shoppingcartlist_cartpage().size();
		String updatedsubtotal_Count=cartdrawer.Subtotal_itemsCount_CartPage().getText();	
		int updated_count_MinicartIcon=cartdrawer.emptycart_header().size();
		String updatedSubtotal_Price=cartdrawer.Subtotal_PriceDetails_CartPage().getText();
		Assert.assertNotEquals(subtotal_Count, updatedsubtotal_Count, "Subtotal : Items Count is not changed in Shopping cart when product is deleted");
		Assert.assertNotEquals(updatedSubtotal_Price, updatedsubtotal_Count, "Subtotal : Price is not changed in Shopping cart when product is deleted");
		Assert.assertEquals(updated_count,product_count-1, "Delete functionality is not working as expected in shopping cart section");
		Assert.assertTrue(updated_count_MinicartIcon==0, "Count in Mini Cart is not updated When product is deleted in shopping cart section of cart page");

	}

	public void checkSaveForLaterSectionEmpty() throws Exception {
		waitForPageLoaded();
		if(cartdrawer.DeleteLink_saveforlater().size()>0)
		{
			int product_count=cartdrawer.DeleteLink_saveforlater().size();
			List<WebElement> producttitle_Saveforlater = cartdrawer.ProductTitle_SaveforLater();
	        for (WebElement title : producttitle_Saveforlater)
	        {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",cartdrawer.DeleteLinkAProduct_SaveforLater());
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.saved-shopping-cart-wrapper .cart-item-info .cart-item-title")));
        //	verifyElementIsNotDisplayed(title);			
			break;
	        }
			int updated_count =cartdrawer.DeleteLink_saveforlater().size();
			Assert.assertEquals(updated_count,product_count-1, "Delete functionality is not working as expected");
		}
		
	}
	
	public void DeleteFunctionality_saveforLater() throws Exception {
		waitForPageLoaded();
		int product_count=cartdrawer.DeleteLink_saveforlater().size();
		List<WebElement> producttitle_Saveforlater = cartdrawer.ProductTitle_SaveforLater();
        for (WebElement title : producttitle_Saveforlater)
        {
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",cartdrawer.DeleteLinkAProduct_SaveforLater());
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.saved-shopping-cart-wrapper .cart-item-info .cart-item-title")));
		//verifyElementIsNotDisplayed(title);			
		break;
        }
		int updated_count =cartdrawer.DeleteLink_saveforlater().size();
		Assert.assertEquals(updated_count,product_count-1, "Delete functionality is not working as expected");
	}

	public void DeleteFunctionality_cartDrawer() throws Exception {
		int product_count=cartdrawer.deletelink_Cartdrawer().size();
		WebElement product_ToBeDeleted=cartdrawer.deletelink_Cartdrawer().get(0);
		clickOnList_Index(cartdrawer.deletelink_Cartdrawer(),0);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("span.prod-del")));

		//verifyElementIsNotDisplayed(product_ToBeDeleted);
		int productcount_updated=cartdrawer.deletelink_Cartdrawer().size();
		Assert.assertEquals(productcount_updated,product_count-1, "Delete functionality - Cart Drawer Screen is not working as expected");
	}

	public void displayRecentlyAddedProduct_CartDrawer() throws Exception {
		List<WebElement> ProductList_CartDrawer = cartdrawer.productslistTitle_cartdrawer();
		Assert.assertTrue(ProductList_CartDrawer.size()>0,"Recently Added Product is not displayed in Cart Drawer Screen");
		ProductList_CartDrawer.get(0).getText().equals(DRUG);
	}

	
	public void redirectionOfPDF_PatientSupport() throws Exception 
	{
		Set<String> allwindow = driver.getWindowHandles();
		String parentWindowHandle = driver.getWindowHandle();
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		clickButtonWithOutScroll(productdetailpage.PatientSupportMenu_PDFLink());
		waitForPageLoaded();
		//driver.switchTo().window(tabs.get(1));
		//driver.switchTo().window(tabs.get(0));
		//driver.switchTo().window(parentWindowHandle);
		boolean currentpage=driver.getCurrentUrl().contains(".pdf");
		System.out.print(currentpage);
		Assert.assertTrue(currentpage==true,"There is no PDF navigation available under Patient Support Menu");
		//driver.switchTo().window(parentWindowHandle);
		
	}
	public void noOfrelatedArticles() throws Exception {
		List<WebElement> ra = articlelisteing.noOfrelatedArticles();
		System.out.println(ra.size());
		boolean flag=false;
		if(ra.size()>=1)
		{
			flag=true;
		}
		Assert.assertTrue(flag, "Related Articles is >=1: ");	
	}

	public void productinformation_PDFs() throws Exception {
		try {
			List<WebElement> pdf_list = patientresourcepage.patientresourcePDFsList();
			System.out.println(pdf_list.size());
			String parentWindowHandle = driver.getWindowHandle();
			for (WebElement selectPDF :pdf_list)
			{
				selectPDF.click();
				driver.switchTo().window(parentWindowHandle);

			}
		} catch (Exception e) {
			throw new Exception("Unknown error while validating the css path", e);
		}
	}

	public void healthconditions_PDFs() throws Exception {
		try {
			List<WebElement> pdf_list = patientresourcepage.patientresourcePDFsList();
			System.out.println(pdf_list.size());
			String parentWindowHandle = driver.getWindowHandle();
			for (WebElement selectPDF :pdf_list)
			{
				selectPDF.click();
				driver.switchTo().window(parentWindowHandle);

			}
		} catch (Exception e) {
			throw new Exception("Unknown error while validating the css path", e);
		}
	}


	public void ClickAllButton_NavigateBack(List<WebElement> ListElements) {
		waitFor(1000);
		for (WebElement clickElement : ListElements)
		{
			scrollToElement(clickElement);
			waitVisibilityOfElement(clickElement);
			waitElementToBeClickable(clickElement);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",clickElement);
			driver.navigate().back();
		}
	}


	public void waitVisibilityOfALLElement(List<WebElement> list) {
		wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfAllElements(list));
	}

	public boolean listcontainsRequiredItems(List<WebElement> li, int count)
	{
		boolean flag =false;
		if(li.size()>=count)
		{
			flag = true;
		}

		return flag;

	}
	
	public String getTextOnList_BasedOnGivenIndex(List<WebElement> li, int index)
	{
		return li.get(index).getText();
	}

	public String clickOnListReturnText_Index(List<WebElement> li, int index)
	{
		String text = li.get(index).getText();
		li.get(index).click();

		return text;
	}
	

	public static String getAttributeValue(WebElement ele,String attributeName)
	{
		String attributeValue = ele.getAttribute(attributeName);
		return attributeValue;
	}
	
	
	
	public static String getAndCheckAttributeValue(WebElement ele,String attributeName,String value)
	{
		String attributeValue = ele.getAttribute(attributeName);
		Assert.assertEquals(attributeValue, value);
	//	Assert.assertTrue(attributeValue.equalsIgnoreCase(value));
		return attributeValue;
	}

	public void implicitWait(int time)
	{
		driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);

	}

	public static void  verifyExtendedUrl(String extendedUrl) 
	{
		waitForPageLoaded();
		String currentUrl = driver.getCurrentUrl();
		boolean verify= currentUrl.contains(extendedUrl);
		Assert.assertTrue(verify, "User lands on" +extendedUrl+ "page");

	}

	
	public static void  verifyExtendedUrl_NewTab(String extendedUrl) 
	{
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		String currentUrl = driver.getCurrentUrl();
		System.out.print(currentUrl);
		boolean verify= currentUrl.contains(extendedUrl);
		Assert.assertTrue(verify, "User lands on" +extendedUrl+ "page");
		driver.close();


	}

	public void selectTabKey() throws AWTException
	{
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);

		Actions builder = new Actions(driver);         
		builder.sendKeys(Keys.TAB).build().perform();
		builder.release().perform();
	}

	public static void keyboradActions(String value) throws AWTException
	{
		Robot robot = new Robot();
		
		if(value.equalsIgnoreCase("enter"))
		{
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		}

		else if (value.equalsIgnoreCase("Copy"))
		{
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_C);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_C);

		}
		else if (value.equalsIgnoreCase("paste")) 
		{
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V); 
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_V);

		}
		else if (value.equalsIgnoreCase("new tab")) 
		{
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_T);
			robot.keyRelease(KeyEvent.VK_T);
			robot.keyRelease(KeyEvent.VK_CONTROL);

		}
		
		else if (value.equalsIgnoreCase("Tab")) 
		{
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
		}
	}

	public void clickOnList_Index(List<WebElement> li, int index) throws InterruptedException
	{
		WebElement clickonelement=li.get(index);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",clickonelement);
		Thread.sleep(1000);

	}
	
	public void javascriptExecutorClick(WebElement clickonelement) {
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",clickonelement);

	}

	public void patientResourceTwoFilters() throws Exception {
		boolean flag =false;
		List<WebElement> patientresource_filtercount = patientresourcepage.patientresource_filters();
		int count =patientresource_filtercount.size();
		if(count==2)
		{ flag = true;
		}	 
	}

	public void sortTheProd() throws Exception {
		try {
			waitForPageLoaded();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			List<WebElement> dropdown_value_productselection = productlisting.DropDown_Value_SortingProducts();
			String CurrentProductName=productlisting.ProductSelected_Title().getText();

			for (int i=0;i<dropdown_value_productselection.size();i++)
			{
				ClickOnElement(productlisting.sortTheProd());
				dropdown_value_productselection.get(i).click();
			}
		} catch (Exception e) {
			throw new Exception("Unknown error while validating the css path", e);
		}
	}



	public void clicksOnFormsFilter() throws Exception {
		waitForPageLoaded();
		Thread.sleep(5000);
		List<WebElement> Form_listitems = productlisting.clicksOnFormsFilter();
		System.out.println(Form_listitems.size());
		for (WebElement formlistitem_selection : Form_listitems)
		{

			((JavascriptExecutor) driver).executeScript("arguments[0].click();",formlistitem_selection);
			Thread.sleep(2000);	

		}
	}

	@Override
	public void clicksonDietaryNeedsFilter() throws Exception {
		waitForPageLoaded();
		Thread.sleep(2000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		List<WebElement> Form_listitems = productlisting.clicksonDietaryNeedsFilter();
		System.out.println(Form_listitems.size());
		for (WebElement formlistitem_selection : Form_listitems)
		{

			((JavascriptExecutor) driver).executeScript("arguments[0].click();",formlistitem_selection);
			Thread.sleep(2000);	

		}

	}

	@Override
	public void clicksOnProductTypeFilter() throws Exception {
		waitForPageLoaded();
		Thread.sleep(2000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		List<WebElement> Form_listitems = productlisting.clicksOnProductTypeFilter();
		System.out.println(Form_listitems.size());
		for (WebElement formlistitem_selection : Form_listitems)
		{

			((JavascriptExecutor) driver).executeScript("arguments[0].click();",formlistitem_selection);
			Thread.sleep(2000);	

		}
	}


	@Override
	public void clicksOnHealthFilter() throws Exception {
		waitForPageLoaded();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		List<WebElement> Form_listitems = productlisting.clicksOnHealthFilter();
		System.out.println(Form_listitems.size());
		for (WebElement formlistitem_selection : Form_listitems)
		{

			((JavascriptExecutor) driver).executeScript("arguments[0].click();",formlistitem_selection);
			Thread.sleep(2000);			

		}


	}

	@Override
	public void clicksOnformulationFilter() throws Exception {
		waitForPageLoaded();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		List<WebElement> Form_listitems = productlisting.clicksOnFormulationFilter();
		System.out.println(Form_listitems.size());
		for (WebElement formlistitem_selection : Form_listitems)
		{

			((JavascriptExecutor) driver).executeScript("arguments[0].click();",formlistitem_selection);
			Thread.sleep(2000);		

		}
	}

	//	
	public void clickOnFilterCheckBox() throws Exception {
		waitForPageLoaded();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		List<WebElement> checkbox = productlisting.clickOnFilterCheckBox();
		System.out.println(checkbox.size());
		for(WebElement enablecheckbox :checkbox)
		{
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",enablecheckbox);
			Thread.sleep(2000);		
		}

	}


	public void navigateBack() throws Exception {
		driver.navigate().back();
	}

	public void navigateFAQ_PDP() throws Exception {
		try {
			waitFor(2000);
			List<WebElement> submenus_PDP = productdetailpage.clickOnSV();
			for (WebElement FAQ_menu :submenus_PDP)
			{
				if(FAQ_menu.getAttribute("title").equalsIgnoreCase("Faqs"))
				{
					FAQ_menu.click();
					break;
				}
			}
		} catch (Exception e) {
			throw new Exception("Unknown error while validating the css path", e);
		}
	}

	public void clickOnShareIcon() throws Exception {
		waitFor(5000);
		clickOnLink(articlelisteing.articlepage_shareicon(),articlelisteing.articlepage_shareicon());
	}

	public void variantChange_PDP() throws Exception {
		List<WebElement> variants_list=productdetailpage.variants_list();
		for (WebElement selectavariant :variants_list)
		{
			String currentvariant=driver.getCurrentUrl();
			if((!selectavariant.getAttribute("class").contains("active")) || (!selectavariant.getAttribute("class").contains("in-active")))
			{
				selectavariant.click();
				String selected_varianturl=driver.getCurrentUrl();
				Assert.assertNotSame(selected_varianturl, currentvariant,"Product Variant Does Not Change On Clicking");
			}
		}		
	}

	public void sizeChange_PDP() throws Exception {
		List<WebElement> size_list=productdetailpage.size_list();
		for (WebElement selectASize : size_list)
		{ 
			String currentPage = driver.getCurrentUrl();
			String currentPrice=productdetailpage.PriceDetails_PDP().getText();
			if(size_list.size()>0)
			{ 
				if(getAttributeValue(selectASize,"class").endsWith("productsize"))
				{
				    clickButtonWithOutScroll(selectASize);
					String navigatedPage = driver.getCurrentUrl();
					String updatedPrice=productdetailpage.PriceDetails_PDP().getText();
					Assert.assertNotSame(updatedPrice, currentPrice,"Product Price Does Not Change On Clicking the different sizes");
					Assert.assertNotSame(navigatedPage, currentPage,"Product Size Does Not Change On Clicking");

				}
			} 			
		}
	}
	
	public void pricechange_OnChanging_Quantity_Units() throws Exception {
		String currentprice=productdetailpage.PriceDetails_PDP().getText();
		clickButtonWithOutScroll(productdetailpage.qualitydropdown());
		clickOnList_Index(productdetailpage.quantity_value(),10);
		String pricechange_onQtychange=productdetailpage.PriceDetails_PDP().getText();
		Assert.assertNotSame(pricechange_onQtychange, currentprice,"Price Does Not Change on changing Qty");
		clickOnList_Index(productdetailpage.units_value(),1);
		String pricechange_onUnitschange=productdetailpage.PriceDetails_PDP().getText();
		Assert.assertNotSame(pricechange_onUnitschange, currentprice,"Price Does Not Change on changing Units");
		
	}
	
	public void searchADrug(String drugname) throws Exception {
		DRUG=drugname;
		ClickOnElement(globelheadersearch.clickOnSearchPane());
		globelheadersearch.enterThreecCharSP().sendKeys(drugname);	
		globelheadersearch.enterThreecCharSP().sendKeys(Keys.ENTER);
		waitForPageLoaded();
		verifyElementIsPresent(globelheadersearch.viewdetailsAProduct_PLP());
		WaitTillElementExist(globelheadersearch.viewdetailsAProduct_PLP(),60);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",globelheadersearch.viewdetailsAProduct_PLP());	
		WaitTillElementExist(globelheadersearch.clickedProduct_Name(),30);
		String Product_Title=globelheadersearch.clickedProduct_Name().getText();
		Assert.assertTrue(Product_Title.equalsIgnoreCase(DRUG),"Searched Product and Product Title does not match");
	}

	public void Verify_Promo_PopUp_Displayed() throws Exception {
		Actions builder= new Actions(driver);
		waitForPageLoaded();
		waitFor(1000);
		WaitTillElementExist(promopopup.Close_Icon_Promo_Popup(),60);
		Boolean verifyElementIsPresent = verifyElementIsDisplayed_WithOutMoveToEle(promopopup.Close_Icon_Promo_Popup());
		if (verifyElementIsPresent=true) 
		{
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", promopopup.Close_Icon_Promo_Popup());
		}

	}

	public void close_browser() throws Exception {
		try {
			driver.quit();
		} catch (Exception e) {
			throw new Exception("Unknown error while validating logout", e);
		}
	}

	public static void quitBrowser() {
		driver.quit();
	}

	public boolean clickOnList_Value(List<WebElement> li, String value)
	{
		boolean flag=false;
		for(int i=0; i<li.size();i++)
		{
			String actual = li.get(i).getText();
			if(value.equalsIgnoreCase(actual))
			{
				li.get(i).click();
				flag = true;
				break;
			}
		}        

		return flag;
	}

	public void moveToElement(WebElement ele)
	{
		Actions actions=new Actions(driver);
		actions.moveToElement(ele).perform();
	}

	public boolean verifyTextInList(List<WebElement> li, String expected)
	{
		boolean flag =false;
		for (int i=0;i<li.size();i++)
		{
			String actual= li.get(i).getText();
			if (expected.equalsIgnoreCase(actual)) 
			{
				flag = true;
				break;
			}
		}

		return flag;

	}
	//Verify contains text in the list of given index
	public boolean verifyContainsTextInGivenIndex_List(List<WebElement> li, int index, String expected)
	{
		boolean flag =false;
			String actual= li.get(index).getText();
			System.out.println("Actual Amt"+ actual);
			if (actual.contains(expected)) 
			{
				flag = true;
			}

		return flag;

	}

	public boolean moveToElement_List(List<WebElement> li, String expected)
	{
		boolean flag =false;
		for (int i=0;i<li.size();i++)
		{
			String actual= li.get(i).getText();
			if (expected.equalsIgnoreCase(actual))
			{
				moveToElement(li.get(i));
				flag = true;
				break;
			}
		}

		return flag;

	}
	public boolean verifyElementPresent_Index(List<WebElement> li, int index)
	{ 
		WebElement tocheck=li.get(index);	
	Boolean isElementPresent = false;  
	waitVisibilityOfElement(tocheck);
	if (tocheck.isDisplayed()) 
	{  
		isElementPresent = true;
	} 

	return isElementPresent;

	}


	public void waitFor(int sleepTime) {

		try {
			Thread.sleep(sleepTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void scrollToElement(WebElement element) {

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	}

	public static void waitVisibilityOfElement(WebElement element) {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(element));

	}

	public void verifyContainsText(WebElement ele, String containsText)
	{
		String actual = ele.getText();
		boolean verify= actual.contains(containsText);
		waitFor(5000);
		System.out.println(containsText);
		System.out.println(actual);
		Assert.assertTrue(verify, "User lands on " +containsText+ " page");
	}


	public boolean waitInVisibilityOfElement(WebElement element) {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		return wait.until(ExpectedConditions.invisibilityOf(element));

	}

	public void verifyElementIsNotDisplayed(List<WebElement> list) 
	{
		boolean flag= true;
		int size = list.size();
		if(size!=0) 
		{
			flag=false;
		}
		Assert.assertTrue(flag, "Element is visible");
	}

	
//	public static boolean verifyElementIsNotDisplayed(WebElement element) 
//	{
//		boolean present = false;
//		 present = wait
//				    .ignoring(StaleElementReferenceException.class)
//	                .ignoring(NoSuchElementException.class)
//				    .until(ExpectedConditions.invisibilityOf(element));
//		 waitInVisibilityOfElement(element);
//		 if(element.isDisplayed())
//		 {
//			 Assert.fail("Element is Visibile");
//		 }
//		 else 
//		Assert.assertTrue(!element.isDisplayed(),"Element is not visible");
//		
//		 return present;	
//	}

	public boolean verifyElementIsNotDisplayed(WebElement element) 
	{ 
		WebDriverWait wait = new WebDriverWait(driver, 50);
		 boolean present = wait
				   .ignoring(StaleElementReferenceException.class)
	                .ignoring(NoSuchElementException.class)
	                .until(ExpectedConditions.invisibilityOf(element));
		Assert.assertTrue(waitInVisibilityOfElement(element));
		if(element.isDisplayed())
		Assert.fail("Error");

		return present;
	}

	public void waitElementToBeClickable(WebElement element) {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public boolean verifyTextIsDisplayed(WebElement ele) throws Exception
	{
		waitFor(1000);
		waitForPageLoaded();
		boolean flag = true;
		moveToElement(ele);
		waitVisibilityOfElement(ele);
		String text = ele.getText();
		System.out.println(text);
		if(text ==null)
		{
			flag = false;
		}

		return flag;

	}
	

	public boolean verifyTextIsPresent_Index(List<WebElement> li, int index)
	{  
		WebElement tocheck=li.get(index);
		String text=tocheck.getText();
		Boolean isElementPresent = true;  
		waitVisibilityOfElement(tocheck);
		if(text ==null)
		{  
			isElementPresent = false;

		} 

		return isElementPresent;

	}


	public void verifyExpectedText(String fullText, WebElement ele)
	{
		waitFor(2000);
		String actual = ele.getText();
		System.out.println("Expected : "+ fullText);
		System.out.println("Actual : "+ actual);
		//Assert.assertEquals(actual, fullText);
		Assert.assertTrue(fullText.equalsIgnoreCase(actual));

	}



	public Boolean verifyElementIsDisplayed(WebElement element) throws Exception {
		Boolean isElementPresent = false;  
		waitVisibilityOfElement(element);
		if (element.isDisplayed()) 
		{

			isElementPresent = true;
		} 

		return isElementPresent;
	}

	public Boolean verifyElementIsDisplayedBasedOnIndex(List<WebElement> ele,int index) throws Exception {
		Boolean isElementPresent = false;  
		WebElement element= ele.get(index);
		moveToElement(element);
		waitVisibilityOfElement(element);
		if (element.isDisplayed()) 
		{

			isElementPresent = true;
		} 

		return isElementPresent;
	}


	public Boolean verifyElementsAreDisplayed(List<WebElement> elements) throws Exception {
		Boolean isElementPresent = false;  
		for (WebElement elementcheck :elements )
		{ 
			moveToElement(elementcheck);
			waitVisibilityOfElement(elementcheck);
			if (elementcheck.isDisplayed()) 
			{
				isElementPresent = true;
			} 		
		}		
		return isElementPresent;
	}

	public Boolean verifyElementsAreDisplayed_WithOutMoveToEle(List<WebElement> elements) throws Exception {
		Boolean isElementPresent = false;  
		for (WebElement elementcheck :elements )
		{ 
			waitVisibilityOfElement(elementcheck);
			if (elementcheck.isDisplayed()) 
			{
				isElementPresent = true;
			} 		
		}		
		return isElementPresent;
	}
	
	public Boolean verifyElementIsDisplayed_WithOutMoveToEle(WebElement element) throws Exception {
        Boolean isElementPresent = false;  
        waitVisibilityOfElement(element);
        if (element.isDisplayed()) 
        {

                isElementPresent = true;
        } 

        return isElementPresent;
}
	
	
	
	public boolean verifyTextAreDisplayed(List<WebElement> text_in_list) throws Exception
	{
		waitFor(1000);
		waitForPageLoaded();
		boolean flag = false ;
		for (WebElement text:text_in_list) {
			moveToElement(text);
			flag = true;
			waitVisibilityOfElement(text);
			String text_value = text.getText();
			System.out.println(text);
			if(text ==null)
			{
				flag = false;
			}
		}
		return flag;
	}

	public void clickButton(WebElement scrollToElement, WebElement clickElement) {

		waitFor(10000);
		scrollToElement(scrollToElement);
		waitVisibilityOfElement(clickElement);
		waitElementToBeClickable(clickElement);
		clickElement.click();
	}



	public void clickButtonWithOutScroll(WebElement element) {

		waitFor(2000);
		waitVisibilityOfElement(element);
		waitElementToBeClickable(element);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",element);

	}

	public void clickOnLinkWithOutScroll(WebElement Linkelement) {

		waitFor(2000);
		waitVisibilityOfElement(Linkelement);
		waitElementToBeClickable(Linkelement);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",Linkelement);

	}

	public void clickOnLink(WebElement scrollToElement, WebElement Linkelement) {

		waitFor(1000);
		scrollToElement(scrollToElement);
		waitFor(1000);
		waitVisibilityOfElement(Linkelement);
		waitElementToBeClickable(Linkelement);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",Linkelement);
	}

	public void enterText(WebElement element,String text) {
		waitFor(2000);
		waitVisibilityOfElement(element);
		waitElementToBeClickable(element);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",element);
		element.sendKeys(text);

	}

	public void selectATab(WebElement element) {
		waitVisibilityOfElement(element);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",element);
		element.sendKeys(Keys.TAB);	
	}
	
	public void verifyButtonDisabled(WebElement element) throws Exception {
		waitFor(2000);
		waitVisibilityOfElement(element);
		String button_value=element.getAttribute("disabled");
		if(button_value.equalsIgnoreCase("disabled"))
			throw new Exception("Button is  disabled");

	}

	public void holdAndClick(WebElement holdelement , WebElement clickelement) throws Exception {
		waitVisibilityOfElement(holdelement);
		waitElementToBeClickable(holdelement);
		Actions builder=new Actions(driver);
		verifyElementIsDisplayed(holdelement);
		builder.clickAndHold(holdelement);
		waitFor(5000);
		waitElementToBeClickable(clickelement);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",clickelement);


	}

	//ForgotPassword
	public void error_Message_ForgotPassword() throws Exception {
		waitFor(2000);
		forgotpassword.ForgotPasswordScreen_Title().click();
	}

	//Home Page - Our Story section
	public void verifyImage_Or_Video() throws Exception {
		if(verifyElementIsDisplayed(homePages.Image_OurStorysection()))
			highLightMethod(homePages.Image_OurStorysection());
		else
			verifyElementIsDisplayed(homePages.video());

	}

	// FAQ Page : Q&A session
	public void QAsession_FAQPage() throws Exception {
		List<WebElement> Question_Session = FAQS.Questionsesstion_FaqPage();
		List<WebElement> Answer_session = FAQS.Answersesesstion_FaqPage();

		for (WebElement Question:Question_Session ) {
			Question.isDisplayed();
			Question.click();
			for (WebElement Answer:Answer_session )
			{
				Answer.isDisplayed();
			}	
		}

	}

	public void selectElement_DropDown(WebElement element, int index)
	{
		Select dropdown=new Select(element);
		dropdown.selectByIndex(index);
	}

	public void selectElement_DropDownBasedOnValue(WebElement element, String value)
	{
		Select dropdown=new Select(element);
		dropdown.selectByValue(value);
	}
	
	//Get selected webelement from  the dropdown
	public WebElement getDropdownSelectedValue(WebElement element)
	{
		Select dropdown=new Select(element);
		return dropdown.getFirstSelectedOption();
		
	}
	
	// Dropdown method - without "Select" tag in DOM.
		public static void dropdownWithoutSelect(List<WebElement> list, String text)
		{
			for (int i = 0; i < list.size(); i++) 
			{
				String textOfList = list.get(i).getText();
				if(textOfList.equalsIgnoreCase(text))
				{
					list.get(i).click();
				}
			}
		}
		
	//Convert double to string
	public static String doubleToString(double value) 
		{
			return String.valueOf(value);

		}	
		
	//Convert String to double
	public static double stringToDouble(String string) {
			double convertedDouble = Double.parseDouble(string);
			return convertedDouble;
		}
		
	//Convert and multiply the double values
	public double multiplyDoubles(String value1, String value2)
	{
		double Value1 = stringToDouble(value1);
		double Value2 = stringToDouble(value2);
		
		return Value1*Value2;
		
	}
	
	//Convert and subtract the double values
	public double subtractDoubles(String value1, String value2)
	{
		double Value1 = stringToDouble(value1);
		double Value2 = stringToDouble(value2);
		
		return Value1-Value2;
		
	}
	
	//Convert to 2 decimal points using string
	public String convertDecimals(String Value, int Decimalcount)
	{
		return new BigDecimal(Value).setScale(Decimalcount, BigDecimal.ROUND_HALF_UP).toString();
	}
	
	//Replace the empty value in the string
	public static String replaceStringValue(String string, String replace)
	{
		String replacedValue = string.replaceAll(replace, "");
		return replacedValue;
	}
		

	//Take screen shot and store in the given location
	public static void takeScreenshot(Scenario scenario) throws IOException 
	{
		String scrname = scenario.getId().replace(";","").replace("-","").replace(":","");
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		org.apache.commons.io.FileUtils.copyFile(scrFile,
				new File("./target/report/Screenshot/" + scrname + ".png"));
		System.out.println("inside screenshot");
		Reporter.addScreenCaptureFromPath(
				"./Screenshot/"+scrname+".png");
	}
	
	public String Date()throws Exception {
		 String path = "src/test/resources/testdata/Email.xlsx";
		 DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		 Date date = new Date();
		 String date1= dateFormat.format(date);
		 String currentDate = date1.replaceAll("[:]","");
		 currentDate = "QA"+currentDate+"@yopmail.com";
         Files.write(Paths.get(path), currentDate.getBytes());
		 return currentDate;

	}
	
	public void pagination() throws Exception {	
		moveToElement(pagination.Page1PLP());
		String checkactivepage= getAttributeValue(pagination.Page1PLP(),"disabled");
		if(checkactivepage.contains("true"))
		{
			String previouspage= getAttributeValue(pagination.previouspagePLP(),"disabled");
			String firstpage= getAttributeValue(pagination.fistpagePLP(),"disabled");
			if((!previouspage.contains("true")) && (!firstpage.contains("true")))
			Assert.fail("Previous and First Page is not disabled when user is on Page1");
		}
		
		String firstproduct=productlisting.productTitleList().get(0).getText();
		List<WebElement> navigationbar = pagination.NavigationBar();
		for (int i=0;i<navigationbar.size();i++)
		{
			Thread.sleep(1000);
			moveToElement(navigationbar.get(i));
			while(!(navigationbar.get(i).getAttribute("class").contains("inactive")))
			{
				if(!navigationbar.get(i).getAttribute("title").contains("1")) 
				{
				navigationbar.get(i).click();
				waitForPageLoaded();
				String firstproduct_afternavigation=productlisting.productTitleList().get(0).getText();
				Assert.assertNotSame(firstproduct_afternavigation,firstproduct);
				Assert.assertTrue(!navigationbar.get(i).getAttribute("class").contains("inactive"));
				}
				break;
			}
		}		
	}
	
	public void highlightActivePage() throws Exception {		
		List<WebElement> navigationbar = pagination.NavigationBar();
		String activecolor = null, colorchange;
        for (WebElement activepage:navigationbar)
        {
        	if(getAttributeValue(activepage,"class").contains("btn navigation-link active"))
        	{
        		 activecolor = activepage.getCssValue("background-color");
        		 break;
        	}    
        }
        pagination.lastpagePLP().click();
        colorchange= pagination.lastpagePLP().getCssValue("background-color");
        Assert.assertNotSame(colorchange, activecolor);		
	}
	
	public void Reversepagination() throws Exception {			
		pagination.lastpagePLP().click();
		String lastproduct=productlisting.productTitleList().get(0).getText();
		List<WebElement> navigationbar = pagination.NavigationBar();
		for (int i=navigationbar.size()-1;i<navigationbar.size();i--)
		{
			String lastpagevalue=pagination.lastPageNumber_ListingPage().getText();
			while(!(getAttributeValue(navigationbar.get(i),"class").contains("inactive")))
			{
				if(!navigationbar.get(i).getText().contains(lastpagevalue)) 
				{
					navigationbar.get(i).click();
					waitForPageLoaded();
					String producttitle_afternavigation=productlisting.productTitleList().get(0).getText();
					Assert.assertNotSame(lastproduct,producttitle_afternavigation);
					Assert.assertTrue(!navigationbar.get(i).getAttribute("class").contains("inactive"));
				}
				break;		
			}		
		}
	}	

	public void nextpageUsingIconPageNavigation() throws Exception {	
		String firstproduct=productlisting.productTitleList().get(0).getText();
		List<WebElement> navigationbar = pagination.NavigationBar();
		for (int i=0;i<navigationbar.size();i++)
		{
			Thread.sleep(1000);
			moveToElement(navigationbar.get(i));
			while(!(getAttributeValue(navigationbar.get(i),"class").contains("inactive")))
			{
				if(!getAttributeValue(navigationbar.get(i),"title").contains("1")) 
				{
				pagination.nextpagePLP().click();
				waitForPageLoaded();
				String firstproduct_afternavigation=productlisting.productTitleList().get(0).getText();
				Assert.assertNotSame(firstproduct_afternavigation,firstproduct);
				Assert.assertTrue(!navigationbar.get(i).getAttribute("class").contains("inactive"));
				}
				break;
			}
		}		
	}
	
	public void previouspageUsingIconPageNavigation() throws Exception {	
		String firstproduct=productlisting.productTitleList().get(0).getText();
		List<WebElement> navigationbar = pagination.NavigationBar();
		for (int i=0;i<navigationbar.size();i++)
		{
			Thread.sleep(1000);
			moveToElement(navigationbar.get(i));
			while(!(getAttributeValue(navigationbar.get(i),"class").contains("inactive")))
			{
				if(!getAttributeValue(navigationbar.get(i),"title").contains("1")) 
				{
				pagination.previouspagePLP().click();
				waitForPageLoaded();
				String firstproduct_afternavigation=productlisting.productTitleList().get(0).getText();
				Assert.assertNotSame(firstproduct_afternavigation,firstproduct);
				Assert.assertTrue(!navigationbar.get(i).getAttribute("class").contains("inactive"));
				}
				break;
			}
		}		
	}
	
	public void firstpageFunctionality() throws Exception {
		waitFor(1000);
		clickButtonWithOutScroll(pagination.Page3_PLP());
		String product_title=productlisting.productTitleList().get(0).getText();
		waitForPageLoaded();
		clickButtonWithOutScroll(pagination.fistpagePLP());
		String updatedproduct_title=productlisting.productTitleList().get(0).getText();
		Assert.assertTrue(pagination.Page1PLP().getText().contains("1"),"Navigate to first page functionality is not working");
		Assert.assertNotSame(updatedproduct_title, product_title,"Product Title does not change during pagination");			
	}
	
	
	public void lastpageFunctionality() throws Exception {
		waitFor(2000);	
		String pagenumber;
		clickButtonWithOutScroll(pagination.Page3_PLP());
		String product_title=productlisting.productTitleList().get(0).getText();
		clickButtonWithOutScroll(pagination.lastpagePLP());
		String updatedproduct_title=productlisting.productTitleList().get(0).getText();
		List<WebElement> navigationbar = pagination.NavigationBar();
		for (WebElement navigatetolastpage:navigationbar)
		{
			if(getAttributeValue(navigatetolastpage,"class").contains("btn navigation-link active"))
			{
				lastpagenumber_pagination=navigatetolastpage.getText();
			}
		}
		Assert.assertNotSame(updatedproduct_title, product_title,"Prodcut Title does not change during pagination");			
		Assert.assertTrue(lastpagenumber_pagination==pagination.lastPageNumber_ListingPage().getText(),"Last page functionality is not working");

	}
	
	
	@Override
	public void contactUsMessageTextBoxErrorMessage() throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public void lTabs() throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public void lTitle() throws Exception {
		// TODO Auto-generated method stub

	}



	@Override
	public void verifyTheDisplayOfGlobelHeader() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnFaqsLink() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void checkFaqsPageContent() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void subtextBelowToFAQsPageHeader() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void contactUsCTAbuttonFAQs() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void FaqsPageComponentSequence() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void headerIntegraiveprologo() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void headerCart() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void headerSearch() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnIp() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyB2B_Image_caroulsel_links() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void click_OurStory_ReadMore_button() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyB2B_Image_slider_navigation() throws Exception {
		// TODO Auto-generated method stub

	}



	@Override
	public void enabledEmailTextbox() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnEmailButton() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void emailSucessMessageAd() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void iterationOfSocialMediaIcons() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnCopyRightLink() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void enabledWhiteColourTickMark() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void iterationOfFeatureProducts() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void fpButton() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void fpButtonClick() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void articleCount() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void scrollAllTheImages() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void featureProductSymbol() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void featureProductBackGroundImage() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void featureProductDesc() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void featureProductButton2() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void createAnAccount() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnCreateNewAccount() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnPasswordEyeIcon() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOurStoryReadMore() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void contactUsSucessMessage() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void legalTermsOfUse() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void legalResalePolicy() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void legalPrivacyPolicy() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void legalHippa() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void pBVerified() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnPB() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void featureProductIcon() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void featureProductTitle() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void hct() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void hcd() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void fphDesc() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void backToArticles() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void articlebc() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void drugNutrientInteraction() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void patientResource() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void conditions() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void findAPractitioner() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnWelcomeUser() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void phoneNumber() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void relatedArticlesLogo() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void relatedArticlesTitle() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void relatedArticlesDesc() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void relatedArticlesReadMore() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void siginModelWindowClosed() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void enterRongMail(String email) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyErrorMessage() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void forgotCancel() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnForgotPassword() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void forgotPassTitle() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void forgotPassDes() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnAboutUs() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnWwa() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void Auhbtitle() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void Auhbdescription() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AuIMTitle() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AuIMDesc() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnIMRMButton() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AuOHTitle() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AuOHImages() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AuOHDesc() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AuOPTitle() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AuOPDesc() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnOPRMButton() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AuFBTitle() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AuFBDesc() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AuHeader() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbusClickOnQuality() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbUsQualityTitle() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbUsQualityDesc() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbUsClickOnVedio() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbUsISTitle() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbUsISDesc() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbUsISImage() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbUsFDTitle() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbUsFDDesc() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbUsFDImage() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbUsMTitle() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbUsMDesc() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbUsMImage() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbUsLTitle() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbUsLDesc() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbUsLImage() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbUsQFBTitle() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbUsQFBDesc() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbUsQBFImage() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbUsQFACTitle() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbUsQFACDesc() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void AbUsQFACImage() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void patientRTHead() throws Exception {
		// TODO Auto-generated method stub

	}




	@Override
	public void patientRClickOnPI() throws Exception {
		// TODO Auto-generated method stub

	}




	@Override
	public void patientRPIR() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void patientRTClickOnHc() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void patientRHCR() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void patientRVerifyHCPdfs() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnSearchPane() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPP() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPA() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void enterThreecCharSP() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnSProd() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnSAT() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnSPVAP() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnMMenuProducts() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnMMVAP() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnPI() throws Exception {
		// TODO Auto-generated method stub

	}


	@Override
	public void clickOnProdTitle() throws Exception {
		// TODO Auto-generated method stub

	}


	@Override
	public void clickOnCAF() throws Exception {
		// TODO Auto-generated method stub

	}


	@Override
	public void clickOnSMR() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnBreadCrumb() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnViewDetails() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnForm() throws Exception {
		// TODO Auto-generated method stub

	}


	@Override
	public void clickOnProductType() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnHealthFilter() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnWomensFreeForm() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnSortProducts() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnRFPI() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnRFPVD() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnByType() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnAntioxidants() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnHI() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnAS() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnPK() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnSubitems() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnSPI() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnSV() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnPQuality() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnPUnit() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void qDP() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void cDP() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnFavorites() throws Exception {
		// TODO Auto-generated method stub

	}




	@Override
	public void clickOnQues() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnContactUs() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void ClickOnpImageRA() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void vAH() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnRAI() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnRAMT() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnVDRA() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnFPA() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnFPI() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyFPT() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyFPH() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnRMFP() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void favSucessMessage() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnHowToOrder() throws Exception {
		// TODO Auto-generated method stub

	}


	@Override
	public void ATCB() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnAD() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnMO() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnPO() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnCP() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickOnFP() throws Exception {
		// TODO Auto-generated method stub

	}



	@Override
	public void CS() throws Exception {
		// TODO Auto-generated method stub

	}



	@Override
	public void SFL() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void ClickOnHc() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void loginSuccess() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void chevron_Username() throws Exception {
		// TODO Auto-generated method stub

	}



	@Override
	public void clickAccountInformation() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyAccountInfo_Page() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyusericon_Accountinfo() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyusericon_MyOrder() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickMyOrder_Dropdown() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyMyOrder_Page() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyusericon_PaymentMethod() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyusericon_CommunicationPreference() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyusericon_Favorite() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyusericon_SignOut() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void click_Signout() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void signout_success() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_Prefix(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void PrefixField_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void Click_CreateAccout_Btn() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_FirstName(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void FirstName_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_PostNominals(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void PostNominals_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_PractitonerType(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void PractitonerType_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_PhoneNo(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void PhoneNo_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_EmailAdd(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void EmailAddress_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_LicenseNo(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void LicenseNo_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void UsageTypes_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_UsageTypes(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void LastName_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_LastName(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void Enter_InvalidEmail(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void Enter_PhoneNo(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void Password_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void Re_EnterPassword_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void VerifyHeader_CreateAcct() throws Exception {
		// TODO Auto-generated method stub

	}



	@Override
	public void EnterPassword(String Password, String ConfirmPassword) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_InvalidPhoneNo(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_Password(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_Re_EnterPassword(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_BussinessPhone(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_College(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_GraduationMonth(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_GraduationYear(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_Degree(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_Program(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_NoOfPractitioner(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_State(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_City(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_ZipCode(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_BussinessName(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_Address1(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_Address2(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_pwd_DoNotMatch(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyError_InvalidEmail(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void BussinessPhone_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void BussinessName_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void Address2_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void Address1_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void City_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void ZipCode_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void State_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void NoOfPractitioner_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void Enter_InvalidPhoneNo(String text) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void Click_MedicalStudent_Radio() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void CollegeField_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void GraduationMonthField_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void GraduationYearField_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void DegreeField_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void ProgramField_Empty() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void VerifySubmitBtn_Disabled() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void clickExpandIcon() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void Click_ShopAmazonNow_Btn() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void click_AmazonLink() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void Verify_ShopAmazonNow_Btn() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verify_AmazonTitle() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void howtoorderDesc() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void howtoorderTitle() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyAmazonLogo() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void JOTSbUTTON() throws Exception {
		// TODO Auto-generated method stub

	}
	
//************************************
	//Signin Methods
//************************************
	
	public void check_for_create_an_account_link_in_signin_model_window() throws Exception {
		Assert.assertTrue(signInPage.signInPageCreateAccount().isDisplayed(), "Create an account section is not displayed in signin model window");
	}
	
	public void check_close_icon_in_signin_model_window() throws Exception {
		Assert.assertTrue(signInPage.signInWindowCloseIcon().isDisplayed(), "Close icon in SignIn model window is not displayed");
	}
	
	
	public void verify_signin_components(DataTable components) throws Exception {
		try {
			for (Map < String, String > row: components.asMaps(String.class, String.class)) {
				String SignInElement = row.get("SignIn Components");
				switch (SignInElement) {
				case "Brand Box":
					Assert.assertTrue(signInPage.signInLogo().isDisplayed(), "Brand Box not displayed");
					break;
					
				case "Integrative logo":
					Assert.assertTrue(signInPage.signInLogo().isDisplayed(), "Interactive logo not displayed");
					break;
					
				case "Brand Image":
					Assert.assertTrue(signInPage.signInBrandImage().isDisplayed(), "Brand Image not displayed");
					break;
					
				case "Model window header":
					Assert.assertTrue(signInPage.signInTitle().isDisplayed(), "Sign In header not displayed");
					break;
					
				case "Text Fields":
					Assert.assertTrue(signInPage.usernameField().isDisplayed(), "UserName text box not displayed");
					Assert.assertTrue(signInPage.passwordField().isDisplayed(), "Password text box not displayed");
					break;
					
				case "Forgot password":
					check_forget_password_link_in_signin_window();
					break;
					
				case "Submit":
					Assert.assertTrue(signInPage.submitSignIn().isDisplayed(), "Sign In Submit button not displayed");
					break;
					
				case "Create an acoount":
					check_for_create_an_account_link_in_signin_model_window();
					break;
					
				case "Close icon":
					check_close_icon_in_signin_model_window();
					break;

				default:
					Assert.fail("The is no SignInElement to verify in Sign In Modal");
					break;
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void check_signin_header() throws Exception {
		Assert.assertTrue(signInPage.signInTitle().isDisplayed(), "Sign In header not displayed");
	}
	
	public void click_on_signin_field() throws Exception {
		signInPage.usernameField().click();
	}

	
	public void click_on_password_field() throws Exception {
		signInPage.passwordField().click();
	}

	public void check_watermark_on_signin_field() throws Exception {
		Assert.assertEquals(signInPage.usernameField().getAttribute("placeholder"), "Enter your email address", "Enter your email address watermark text is not displayed");
	}
	
	public void check_watermark_on_password_field() throws Exception {
		Assert.assertEquals(signInPage.passwordField().getAttribute("placeholder"), "Password", "Password watermark text is not displayed");
	}
	
	public void check_label_on_signin_field() throws Exception {
		Assert.assertEquals(signInPage.usernameLabel().getText(), "EMAIL ADDRESS", "Email address label not available");		
	}

	public void check_label_on_password_field() throws Exception {
		Assert.assertEquals(signInPage.passwordLabel().getText(), "PASSWORD", "Password label not available");
	}
		
	public void validate_SignIn_Modal_Window() throws Exception {
		Assert.assertTrue(signInPage.signInModalWindow().isDisplayed(), "Sign In Model Window not displayed");
	}

	public void validate_SignIn_Modal_Window_Closed() throws Exception {
		Assert.assertFalse(signInPage.signInModalWindow().isDisplayed(), "Sign In Model Window displayed");
		Thread.sleep(2000);
		Assert.assertTrue(signInPage.signInInHomePage().isDisplayed(), "Sign In icon not displayed");
	}


	public void validate_close_icon_signin_model_window() throws Exception {
		signInPage.signInWindowCloseIcon().click();
		Assert.assertFalse(signInPage.signInWindowCloseIcon().isDisplayed(), "Close icon in SignIn model window is still displayed");
	}


	public void validate_signin_model_window_outside_click_functionality() throws Exception {
		Actions act = new Actions(driver);
		act.moveByOffset(2, 2).click().build().perform();
	}

	
	public void validate_create_an_account_link_in_signin_model_window() throws Exception {
		Assert.assertEquals(signInPage.signInPageCreateAccount().getText(), "Create an account", "Create an account hyperlink is not available");
		signInPage.signInPageCreateAccount().click();
		waitForPageLoaded();
	}
	
	
	public void validate_create_an_account_page() throws Exception {
		Assert.assertEquals(signInPage.createAccountPageEle().getText(), "Create an Account", "Not navigated to Create an Account Page");
	}
	
	
	public void close_create_an_account_page() throws Exception {
		signInPage.continueAsGuest().click();
		waitForPageLoaded();
		Assert.assertTrue(signInPage.signInInHomePage().isDisplayed(), "Sign In icon not displayed");
	}

	
	public void validate_user_signed_in() throws Exception {
//		signInPage.welcomeUserText().click();
		Actions actObj = new Actions(driver);
		actObj.moveToElement(signInPage.welcomeUserText()).build().perform();
		Thread.sleep(2000);
		Assert.assertTrue(signInPage.signOut().isDisplayed(), "Sign Out Link is not displayed");
	}

	
	public void validate_password_field_validation_error() throws Exception {
		waitVisibilityOfElement(signInPage.signInErrorMessage());
		Assert.assertTrue(signInPage.signInErrorMessage().isDisplayed(), "Password field validation error is not displayed");
	}

	
	public void validate_error_message_for_invalid_signin() throws Exception {
		Assert.assertEquals(signInPage.signInErrorMessage().getText(), "Information provided doesn't match", "Validation message not displayed for Invalid login");
	}

	
	public void check_forget_password_link_in_signin_window() throws Exception {
		Assert.assertEquals(signInPage.signInforgotPassword().getText(), "Forgot password?", "Forgot password link text is not displayed in sign in window");
	}

	
	public void validate_forget_password() throws Exception {
		signInPage.signInforgotPassword().click();
		waitForPageLoaded();
	}

	
	public void validate_forgot_password_content_window() throws Exception {
		Assert.assertEquals(forgotpassword.forgotPasswordTitle().getText(), "Forgot Password", "Forgot password content page is displayed");
	}

	
	public void validate_password_eye_icon_with_slash() throws Exception {
		Assert.assertTrue(signInPage.showIconInPasswordField().isDisplayed(), "Password Field eye icon not displayed with slash");
	}

	
	public void validate_password_eye_icon_without_slash() throws Exception {
		Assert.assertTrue(signInPage.hideIconInPasswordField().isDisplayed(), "Password Field eye icon not displayed without slash");
	}

	
	public void validate_masked_characters_in_password_field() throws Exception {
		String PasswordType = signInPage.passwordField().getAttribute("type");
		Assert.assertEquals(PasswordType, "password", "Masked Characters were not displayed in Password field");
	}

	
	public void validate_unmasked_characters_in_password_field() throws Exception {
		String PasswordType = signInPage.passwordField().getAttribute("type");
		System.out.println(PasswordType);
		Assert.assertEquals(PasswordType, "text", "Unmasked Characters were not displayed in Password field");
	}


	public void validate_click_eye_icon_in_password_field() throws Exception {
		if(signInPage.hideIconInPasswordFieldList().size() != 0) {
			signInPage.hideIconInPasswordField().click();
		}else if(signInPage.showIconInPasswordFieldList().size() != 0){
			signInPage.showIconInPasswordField().click();
		}else {
			Assert.fail();
		}
		Thread.sleep(3000);
	}

	
	public void enter_password(String passwordText) throws Exception {
		Assert.assertTrue(signInPage.passwordField().isDisplayed());
		signInPage.passwordField().sendKeys(passwordText);
	}

	
	public void validate_double_click_show_icon() throws Exception {
		if((signInPage.showIconInPasswordFieldList().size() == 0)) {
			validate_click_eye_icon_in_password_field();
			Thread.sleep(2000);
		}
		validate_click_eye_icon_in_password_field();
		validate_click_eye_icon_in_password_field();
		Thread.sleep(2000);
		
		validate_password_eye_icon_with_slash();
	}


	public void validate_double_click_hide_icon() throws Exception {
		if((signInPage.hideIconInPasswordFieldList().size() == 0)) {
			validate_click_eye_icon_in_password_field();
			Thread.sleep(2000);
		}
		validate_click_eye_icon_in_password_field();
		validate_click_eye_icon_in_password_field();
		Thread.sleep(2000);
		
		validate_password_eye_icon_without_slash();		
	}
	
	public void tap_signin() throws Exception {
		signInPage.signInInHomePage().click();
		waitForPageLoaded();
	}
	
	public void signIn_with_default_credentials() throws Exception {
		waitForPageLoaded();
		waitVisibilityOfElement(signInPage.usernameField());
		signInPage.usernameField().sendKeys(DefaultUserEmail);
		Thread.sleep(2000);
		signInPage.passwordField().sendKeys(DefaultPassword);
		Thread.sleep(2000);
		signInPage.submitSignIn().click();
		Thread.sleep(6000);
		waitForPageLoaded();
	}
	
	public void logOutApp() throws Exception {
		waitForPageLoaded();
		waitVisibilityOfElement(signInPage.welcomeUserText());
		Actions act = new Actions(driver);
		act.moveToElement(signInPage.welcomeUserText()).build().perform();
		Thread.sleep(2000);
		signInPage.welcomeUserText().click();
		Thread.sleep(2000);
		clickButton(signInPage.signOut(), signInPage.signOut());
		waitForPageLoaded();
	}
	
	


}