package helpers;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;

import platforms.AndroidPlatform;
//import platforms.IOSPlatform;
import platforms.TestPlatform;
import platforms.WebPortal;

public class PlatformHelper {

	public static TestPlatform getPlatformInstance(String type) {

		TestPlatform platform;
		switch (type) {
		case "mac_web":       // To launch browser in MAC
			platform = new WebPortal();
			break;
		case "windows_web":   //To launch browser in windows
			platform = new WebPortal();
			break;
		case "windows_mobile": // To launch browser in Android device
			platform = new AndroidPlatform();
			break;
		/*case "ios_mobile": // "android":
			platform = new IOSPlatform();
			break;*/

		default:
			throw new RuntimeException("Invalid platform");
		}
		return platform;
	}

	public static TestPlatform getCurrentPlatform() throws FileNotFoundException, IOException, ParseException {
		return PlatformHelper.getPlatformInstance(ConfigurationHelper.getPlatform());
	}

}
