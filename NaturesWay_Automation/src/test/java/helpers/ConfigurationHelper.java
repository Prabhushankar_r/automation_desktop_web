package helpers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

//import configuration.RunConfig;

public class ConfigurationHelper {

	private static JSONObject platformProperties = null;
	private static String platformStr = null;
	private static String username = null;
	private static String password = null;



	public static void init() throws FileNotFoundException, IOException, ParseException {
		platformStr=getPlatform();
		platformProperties=getPlatformProperties();
		username=getUserName();
		password=getPassword();
		
	}

	public static String getPlatform() throws FileNotFoundException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader("src/test/configuration/RunConfig.json"));
		JSONObject jsonObject = (JSONObject) obj;
		platformStr = (String) jsonObject.get("platform");
		return platformStr;
	}
	
	public static String getBrowserDetails() throws FileNotFoundException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader("src/test/configuration/RunConfig.json"));
		JSONObject jsonObject = (JSONObject) obj;
		platformStr = (String) jsonObject.get("browser");
		return platformStr;
	}
	
	private static JSONObject getPlatformProperties() throws FileNotFoundException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader("src/test/configuration/Config.json"));
		JSONObject jsonObject = (JSONObject) obj;
		JSONObject platformProperties = (JSONObject) jsonObject.get(platformStr);
		return platformProperties;
	}

	public static String getUserName() throws FileNotFoundException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader("src/test/configuration/RunConfig.json"));
		JSONObject jsonObject = (JSONObject) obj;
		username = (String) jsonObject.get("username");
		return username;
	}
	
	public static String getPassword() throws FileNotFoundException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader("src/test/configuration/RunConfig.json"));
		JSONObject jsonObject = (JSONObject) obj;
		password = (String) jsonObject.get("password");
		return password;
	}
	
	public static String getFeatureFile() throws FileNotFoundException, IOException, ParseException {
		init();
		return (String) platformProperties.get("featureFile");
	}
	
	public static String getBaseUri() throws FileNotFoundException, IOException, ParseException {
		return (String) platformProperties.get("baseUri");
	}
	
	public static String getPlatformName() throws FileNotFoundException, IOException, ParseException {
		return (String) platformProperties.get("platformName");
	}
	
	public static String getDeviceName() throws FileNotFoundException, IOException, ParseException {
		return (String) platformProperties.get("deviceName");
	}
	
	public static String getPlatformVersion() throws FileNotFoundException, IOException, ParseException {
		return (String) platformProperties.get("platformVersion");
	}
	
	public static String getDriverUrl() throws FileNotFoundException, IOException, ParseException {
		return (String) platformProperties.get("driverUrl");
	}
	
	public static String getAppPackage() throws FileNotFoundException, IOException, ParseException {
		return (String) platformProperties.get("appPackage");
	}
	
	public static String getAppActivity() throws FileNotFoundException, IOException, ParseException {
		return (String) platformProperties.get("appActivity");
	}
	
	public static String getBrowserName() throws FileNotFoundException, IOException, ParseException {
		return (String) platformProperties.get("browserName");
	}
	
	public static String getUdid() throws FileNotFoundException, IOException, ParseException {
		return (String) platformProperties.get("udid");
	}
	public static String getAndroidBrowser() throws FileNotFoundException, IOException, ParseException {
		return (String) platformProperties.get("getAndroidBrowser");
	}

}

// private static RunConfig runConfig;
//
// private static RunConfig getRunConfig() {
// if (runConfig == null) {
// try {
// BufferedReader reader = new BufferedReader(new
// FileReader("src/test/resources/testData/Run.config"));
// Gson gson = new Gson();
// runConfig = gson.fromJson(reader, RunConfig.class);
// } catch (FileNotFoundException e) {
// e.printStackTrace();
// throw new RuntimeException("Unable to read Run.config");
// }
// }
//
// return runConfig;
// }